package com.yumzy.orderfood.data.cart

import com.google.gson.annotations.SerializedName
import com.yumzy.orderfood.data.models.CartItemDTO
import com.yumzy.orderfood.data.models.Coupon


data class CartResponseDTO(
    @field:SerializedName("cartId") val cartID: String,
    @field:SerializedName("outlets") val outlets: List<Outlet>?,
    @field:SerializedName("coupons") val coupons: List<Coupon>?,
    @field:SerializedName("billing") val billing: DataBilling
)

data class DataBilling(
    @field:SerializedName("subTotal") val subTotal: Double,
    @field:SerializedName("discount") val discount: Double,
    @field:SerializedName("roundOff") val roundOff: Double,
    @field:SerializedName("totalAmount") val totalAmount: Double,
    @field:SerializedName("loyaltyUsed") val loyaltyUsed: Double,
    @field:SerializedName("charges") val charges: List<Charge>?,
    @field:SerializedName("taxes") val taxes: List<Charge>?
)

data class Charge(
    @field:SerializedName("name") val name: String,
    @field:SerializedName("value") val value: Double,
    @field:SerializedName("displayName") val displayName: String
)


data class Outlet(
    @field:SerializedName("location") val location: Location,
    @field:SerializedName("outletId") val outletID: String?,

    @field:SerializedName("outletName") val outletName: String,
    @field:SerializedName("outletLogo") val outletLogo: String,
    @field:SerializedName("scheme") val scheme: String?,
    @field:SerializedName("gst") val gst: String?,
    @field:SerializedName("items") val itemDTOS: List<CartItemDTO>,
    @field:SerializedName("offer") val offer: Offer?,
    @field:SerializedName("billing") val billing: OutletBilling?
)

data class OutletBilling(
    @field:SerializedName("totalAmount") val totalAmount: Long,
    @field:SerializedName("discount") val discount: Long,
    @field:SerializedName("subTotal") val subTotal: Long,
    @field:SerializedName("charges") val charges: List<Charge>,
    @field:SerializedName("taxes") val taxes: List<Charge>
)

data class Addon(
    @field:SerializedName("addonId") val addonID: String,

    @field:SerializedName("addonName") val addonName: String,
    @field:SerializedName("options") val options: List<Option>,
    @field:SerializedName("isVariant") val isVariant: Boolean,
    @field:SerializedName("maxSelection") val maxSelection: Long,
    @field:SerializedName("minSelection") val minSelection: Long,
    @field:SerializedName("_id") val id: String? = null
)

data class Option(
    @field:SerializedName("optionName") val optionName: String,
    @field:SerializedName("cost") val cost: Long,
    @field:SerializedName("isVeg") val isVeg: Boolean
)


data class Location(
    @field:SerializedName("coordinates") val coordinates: List<Double>,
    @field:SerializedName("type") val type: String
)

data class Offer(
    @field:SerializedName("discountType") val discountType: String,
    @field:SerializedName("usedBudget") val usedBudget: Long,

    @field:SerializedName("outletId") val outletID: String,
    @field:SerializedName("promoId") val promoID: String,

    @field:SerializedName("value") val value: Long,
    @field:SerializedName("offerFor") val offerFor: String,
    @field:SerializedName("minOrderAmount") val minOrderAmount: Long,
    @field:SerializedName("maxDiscount") val maxDiscount: Long,
    @field:SerializedName("promoBudget") val promoBudget: Long
)
