/*
 * Created By Shriom Tripathi 12 - 5 - 2020
 */

/*
 * Created By Shriom Tripathi 12 - 5 - 2020
 */

package com.yumzy.orderfood.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class HelpSubHeadingDTO {

    @SerializedName("subItemId")
    @Expose
    var subItemId: String = ""

    @SerializedName("question")
    @Expose
    var question: String = ""

    @SerializedName("answer")
    @Expose
    var answer: String = ""

    @SerializedName("chatLanding")
    @Expose
    var chatLanding: String = ""

}

class SubHeadingDTO {

    @SerializedName("headingId")
    @Expose
    var headingId: Int = 0

    @SerializedName("title")
    @Expose
    var title: String = ""

    @SerializedName("subItems")
    @Expose
    var subItems: List<HelpSubHeadingDTO> = emptyList()

}