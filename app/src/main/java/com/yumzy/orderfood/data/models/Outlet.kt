package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName

data class Outlet(
    @field:SerializedName("title") val title: String,
    @field:SerializedName("rating") val rating: Double,
    @field:SerializedName("duration") val duration: Int,
    @field:SerializedName("offer") val offer: Int)