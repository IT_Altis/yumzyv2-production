package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName
import com.yumzy.orderfood.data.cart.Charge

data class PreCheckoutResDTO(
    @field:SerializedName("error") val error: String,
    @field:SerializedName("code") var code: Int,
    @field:SerializedName("ids") val ids: List<String>?,
    @field:SerializedName("cartDoc") val cartDoc: CartDoc?,
    @field:SerializedName("deliveryInfo") val deliveryInfo: DeliveryInfo
)

data class CartDoc(
    @field:SerializedName("cartId") val cartId: String,
    @field:SerializedName("outlets") val outlets: List<PreCheckOutOutlet?>,
    @field:SerializedName("coupons") val coupons: List<PreCheckCoupon?>,
    @field:SerializedName("billing") val billing: CartDocBilling,
    @field:SerializedName("offer") val offer: MissingOfferDTO,
    @field:SerializedName("payments") val payments: List<PaymentDTO>?
)

data class PreCheckCoupon(
    @field:SerializedName("code") val code: String?,
    @field:SerializedName("title") val title: String?,
    @field:SerializedName("couponId") val couponId: String?,
    @field:SerializedName("discountType") val discountType: String?,
    @field:SerializedName("minOrderAmount") val minOrderAmount: Long?,
    @field:SerializedName("maxDiscount") val maxDiscount: Long?
)

data class CartDocBilling(
    @field:SerializedName("subTotal") val subTotal: Double,
    @field:SerializedName("discount") val discount: Double,
    @field:SerializedName("roundOff") val roundOff: Double,
    @field:SerializedName("totalAmount") val totalAmount: Double,
    @field:SerializedName("loyaltyUsed") val loyaltyUsed: Double,
    @field:SerializedName("charges") val charges: List<Charge>,
    @field:SerializedName("taxes") val taxes: List<Charge>,
    @field:SerializedName("originalDelivery") val originalDelivery: Double
)

data class Charge(
    @field:SerializedName("name") val name: String,
    @field:SerializedName("value") val value: Double
)

data class PreCheckOutOutlet(
    @field:SerializedName("location") val location: Location,
    @field:SerializedName("outletId") val outletId: String,
    @field:SerializedName("outletName") val outletName: String,
    @field:SerializedName("outletLogo") val outletLogo: String,
    @field:SerializedName("scheme") val scheme: String,
    @field:SerializedName("gst") val gst: String,
    @field:SerializedName("locality") val locality: String,
    @field:SerializedName("items") val itemDTOS: List<CartItemDTO>,
    @field:SerializedName("offer") val offer: OutletOffer,
    @field:SerializedName("billing") val billing: OutletBilling
)

data class OutletBilling(
    @field:SerializedName("totalAmount") val totalAmount: Double,
    @field:SerializedName("discount") val discount: Double,
    @field:SerializedName("subTotal") val subTotal: Double,
    @field:SerializedName("charges") val charges: List<Charge>,
    @field:SerializedName("taxes") val taxes: List<Charge>
)


data class Addon(
    @field:SerializedName("addonId") val addonId: String,
    @field:SerializedName("addonName") val addonName: String,
    @field:SerializedName("options") val options: List<PreCheckoutOption>,
    @field:SerializedName("isVariant") val isVariant: Boolean,
    @field:SerializedName("maxSelection") val maxSelection: Double,
    @field:SerializedName("minSelection") val minSelection: Double
)

data class PreCheckoutOption(
    @field:SerializedName("optionName") val optionName: String,
    @field:SerializedName("cost") val cost: Double,
    @field:SerializedName("isVeg") val isVeg: Boolean
)

data class ItemOffer(
    @field:SerializedName("discountType") val discountType: String,
    @field:SerializedName("usedBudget") val usedBudget: Double,
    @field:SerializedName("outletId") val outletId: String,
    @field:SerializedName("promoId") val promoId: String,
    @field:SerializedName("value") val value: Double,
    @field:SerializedName("offerFor") val offerFor: String
)

data class PreCheckOutVariant(
    @field:SerializedName("id") val id: String,
    @field:SerializedName("addonId") val addonId: String,
    @field:SerializedName("addonName") val addonName: String,
    @field:SerializedName("options") val options: Option,
    @field:SerializedName("isVariant") val isVariant: Boolean,
    @field:SerializedName("maxSelection") val maxSelection: Double,
    @field:SerializedName("minSelection") val minSelection: Double
)

data class Location(
    @field:SerializedName("coordinates") val coordinates: List<Double>,
    @field:SerializedName("type") val type: String
)

data class OutletOffer(
    @field:SerializedName("discountType") val discountType: String,
    @field:SerializedName("usedBudget") val usedBudget: Double,
    @field:SerializedName("outletId") val outletId: String,
    @field:SerializedName("promoId") val promoId: String,
    @field:SerializedName("value") val value: Double,
    @field:SerializedName("offerFor") val offerFor: String,
    @field:SerializedName("minOrderAmount") val minOrderAmount: Double,
    @field:SerializedName("maxDiscount") val maxDiscount: Double,
    @field:SerializedName("promoBudget") val promoBudget: Double
)

data class DeliveryInfo(
    @field:SerializedName("transactionId") val transactionId: String,
    @field:SerializedName("categoryId") val categoryId: String,
    @field:SerializedName("distance") val distance: Double,
    @field:SerializedName("estimatedPrice") val estimatedPrice: Double,
    @field:SerializedName("eta") val eta: Double,
    @field:SerializedName("canSelfPickup") val canSelfPickup: Boolean,
    @field:SerializedName("deliveryCharge") val deliveryCharge: Double,
    @field:SerializedName("zone") val zone: String
)