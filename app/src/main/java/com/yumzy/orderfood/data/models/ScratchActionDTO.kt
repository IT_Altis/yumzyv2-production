package com.yumzy.orderfood.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScratchActionDTO(
    @field:SerializedName("text") val text: String,
    @field:SerializedName("url") val url: String,
    @field:SerializedName("clickEvent") val clickEvent: String,
    @field:SerializedName("couponCode") val couponCode: String? = ""
) : Parcelable