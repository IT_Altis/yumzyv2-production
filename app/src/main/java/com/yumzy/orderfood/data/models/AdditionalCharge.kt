package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName

data class AdditionalChargeDTO(
    @field:SerializedName("chargeType") val chargeType: String,
    @field:SerializedName("name") val name: String,
    @field:SerializedName("value") val value: Int
)