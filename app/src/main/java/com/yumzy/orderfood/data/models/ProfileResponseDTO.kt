package com.yumzy.orderfood.data.models

import com.google.gson.JsonArray
import com.google.gson.annotations.SerializedName


data class ProfileResponseDTO(

    @field:SerializedName("id")  val id: String,
    @field:SerializedName("countryCode")  val countryCode: String,
    @field:SerializedName("name")  val name: String?,
    @field:SerializedName("lastName")  val lastName: String?,
    @field:SerializedName("email")  val email: String,
    @field:SerializedName("currentLocality")  val currentLocality: String,
    @field:SerializedName("city")  val city: String,
    @field:SerializedName("profession")  val profession: String,
    @field:SerializedName("profileCompleted")  val profileCompleted: Double,
    @field:SerializedName("creationStatus")  val creationStatus: String,
    @field:SerializedName("creationDate")  val creationDate: String,
    @field:SerializedName("referralDeepLink")  val referralDeepLink: String,
    @field:SerializedName("userId")  val userId: String,
    @field:SerializedName("mobile")  val mobile: String,
    @field:SerializedName("address")  val address: List<UserAddress>,
    @field:SerializedName("kshetras")  val kshetras: List<String>,
    @field:SerializedName("kshetras1")  val kshetras1: List<String>,
    @field:SerializedName("currentLocation")  val currentLocation: CurrentLocation,
    @field:SerializedName("likeTagsSelected")  val likeTagsSelected: Boolean,
    @field:SerializedName("referralCode")  val referralCode: String,
    @field:SerializedName("referredBy")  val referredBy: String,
    @field:SerializedName("bmi")  val bmi: Bmi,
    @field:SerializedName("gender")  val gender: String,
    @field:SerializedName("age")  val age: Double,
    @field:SerializedName("dob")  val dob: String,
    @field:SerializedName("height")  val height: Any? = null,
    @field:SerializedName("weight")  val weight: Any? = null,
    @field:SerializedName("profilePicLink")  val profilePicLink: String?,
    @field:SerializedName("preferenceType")  val preferenceType: String,
    @field:SerializedName("dietType")  val dietType: String,
    @field:SerializedName("foodConsistency")  val foodConsistency: JsonArray,
    @field:SerializedName("preferredTaste")  val preferredTaste: JsonArray,
    @field:SerializedName("eatingStyle")  val eatingStyle: JsonArray,
    @field:SerializedName("foodStates")  val foodStates: JsonArray,
    @field:SerializedName("cuisinePref")  val cuisinePref: JsonArray,
    @field:SerializedName("allergens")  val allergens: JsonArray,
    @field:SerializedName("healthProb")  val healthProb: JsonArray,
    @field:SerializedName("isExplorer")  val isExplorer: Boolean,
    @field:SerializedName("foodBudget")  val foodBudget: JsonArray,
    @field:SerializedName("allowRecurringFood")  val allowRecurringFood: Boolean,
    @field:SerializedName("likedFoodCategories")  val likedFoodCategories: List<String>,
    @field:SerializedName("likedFoodTags")  val likedFoodTags: List<String>,
    @field:SerializedName("preferredFoodStyle")  val preferredFoodStyle: String,
    @field:SerializedName("rewardPoints")  val rewardPoints: Double
)

data class UserAddress(
    @field:SerializedName("name") val name: String,
    @field:SerializedName("landmark") val landmark: String,
    @field:SerializedName("googleLocation") val googleLocation: String,
    @field:SerializedName("fullAddress") val fullAddress: String,
    @field:SerializedName("addressTag") val addressTag: String,
    @field:SerializedName("houseNum") val houseNum: String,
    @field:SerializedName("longLat") val longLat: UserLongLat? = null,
    @field:SerializedName("addressId") val addressId: String
)

data class UserLongLat(
    @field:SerializedName("coordinates") val coordinates: List<Double>
)

data class Bmi(
    @field:SerializedName("value")  val value: Double,
    @field:SerializedName("category") val category: String
)

data class CurrentLocation(
    @field:SerializedName("coordinates") val coordinates: List<Double>,
    @field:SerializedName("type") val type: String
)
