package com.yumzy.orderfood.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class MetaDTO(
   @field:SerializedName("additionalCharges") val additionalCharges: List<AdditionalChargeDTO>,
   @field:SerializedName("containsEgg") val containsEgg: Boolean,
   @field:SerializedName("duration") val duration: Double,
   @field:SerializedName("isVeg") val isVeg: Boolean,
   @field:SerializedName("itemId") val itemId: String,
   @field:SerializedName("itemName") val itemName: String,
   @field:SerializedName("costForTwo") val costForTwo: String?,
   @field:SerializedName("offers") val offers: List<OfferDTO>,
   @field:SerializedName("price") val price: Double,
   @field:SerializedName("clickUrl") var clickUrl: String,
   @field:SerializedName("itemUrl") var itemUrl: String
) {
    @field:SerializedName("prepTime")
    @Expose
    var prepTime: String = "0min"
}