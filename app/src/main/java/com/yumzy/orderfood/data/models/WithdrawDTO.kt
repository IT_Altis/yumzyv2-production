package com.yumzy.orderfood.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WithdrawDTO
    (
    @field:SerializedName("status") val status: String,
    @field:SerializedName("statusDescription") val statusDescription: String
) : Parcelable
