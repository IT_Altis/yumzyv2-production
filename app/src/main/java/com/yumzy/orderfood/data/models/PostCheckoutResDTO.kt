package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName

data class PostCheckoutResDTO
    (
    @field:SerializedName("status")  val status: String,
    @field:SerializedName("earnedScratch") val earnedScratch: Boolean

)