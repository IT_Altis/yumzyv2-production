package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class WalletTransactionDTO(
    @field:SerializedName("_id")val id: String,
    @field:SerializedName("createdOn")val createdOn: String,
    @field:SerializedName("entryType")val entryType: String,
    @field:SerializedName("paymentTransactionId")val paymentTransactionId: String,
    @field:SerializedName("points")val points: Double,
    @field:SerializedName("reason")val reason: String
): Serializable {

    override fun toString(): String {
        return """
            _id = $id
            createdOn = $createdOn
            entryType = $entryType
            paymentTransactionId = $paymentTransactionId
            points = $points
            reason = $reason
        """.trimIndent()
    }
}