/*
 *   Created by Sourav Kumar Pandit  27 - 4 - 2020
 */

package com.yumzy.orderfood.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.yumzy.orderfood.data.models.AddressDTO

@Dao
interface SelectedAddressDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateOrInsert(user: AddressDTO)

    @Update
    fun updatePickedAddress(vararg address: AddressDTO)

    @Query("SELECT * FROM AddressDTO")
    fun getPickedAddress(): LiveData<AddressDTO>

    @Query("DELETE FROM AddressDTO")
    fun removePickedAddress()


}
