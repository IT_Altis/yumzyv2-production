/*
 * Created By Shriom Tripathi 2 - 5 - 2020
 */

package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName


data class PersonalisationDTO(
    @field:SerializedName("selected") val selected: List<String>,
    @field:SerializedName("filter") val filter: String


)