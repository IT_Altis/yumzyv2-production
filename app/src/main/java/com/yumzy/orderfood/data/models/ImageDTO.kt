package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName

data class ImageDTO(
    @field:SerializedName("caption") val caption: String = "",
    @field:SerializedName("url") val url: String = ""
)