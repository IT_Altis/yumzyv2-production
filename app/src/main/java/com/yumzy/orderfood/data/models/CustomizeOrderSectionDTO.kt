package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Shriom Tripathi.
 */
data class CustomizeOrderItemDTO(
    @field:SerializedName("itemName")  val itemName:String,
    @field:SerializedName("isVeg")  val isVeg:Boolean,
    @field:SerializedName("itemPrice")  val itemPrice: Double,
    @field:SerializedName("itemOfferPrice")  val itemOfferPrice: Double = 0.0,
    @field:SerializedName("addOnId")  val addOnId: String,
    @field:SerializedName("isSelected")  val isSelected: Boolean = false
) {
    @field:SerializedName("iconText")
    var iconText: String = ""
}

data class CustomizeOrderSectionDTO(
   @field:SerializedName("menuSectionTitile")  val menuSectionTitile: String,
   @field:SerializedName("isMultiSelectEnable")  val isMultiSelectEnable: Boolean,
   @field:SerializedName("items")  val items: List<CustomizeOrderItemDTO>,
   @field:SerializedName("addOnId")  val addOnId: String,
   @field:SerializedName("addOnName")  val addOnName: String,
   @field:SerializedName("min")  val min: Int,
   @field:SerializedName("max")  val max: Int,
   @field:SerializedName("doInitialSelection")  val doInitialSelection: Boolean = true)

