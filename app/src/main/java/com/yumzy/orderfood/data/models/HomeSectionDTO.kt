package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName

data class HomeSectionDTO(
    @field:SerializedName("displayOrderSeq") val displayOrderSeq: Int,
    @field:SerializedName("heading") val heading: String,
    @field:SerializedName("layoutType") val layoutType: Int,
    @field:SerializedName("list") val list: ArrayList<HomeListDTO>,
    @field:SerializedName("orientation") val orientation: Int,
    @field:SerializedName("subHeading") val subHeading: String
)