package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName


data class RatingFeedbackDTO(
    @field:SerializedName("foodRating") val foodRating: List<RatingDTO>,
    @field:SerializedName("orderId") val orderId: String,
    @field:SerializedName("orderNum") val orderNum: String,
    @field:SerializedName("deliveryrating") val deliveryrating: List<RatingDTO>,
    @field:SerializedName("comment") val comment: String
)

data class RatingDTO(
      @field:SerializedName("ratedFor")  val ratedFor: List<String>,
      @field:SerializedName("rating")  val rating: Double
)
