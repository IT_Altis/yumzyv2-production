package com.yumzy.orderfood.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScratchCardDetailDTO(
    @field:SerializedName("scratchedAmount") val scratchedAmount: Double,
    @field:SerializedName("banner") val banner: ScratchBannerDTO
) : Parcelable