package com.yumzy.orderfood.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScratchSummaryDTO(
    @field:SerializedName("scratchAmount") val scratchAmount: Double,
    @field:SerializedName("scratchCount") val scratchCount: Int,
    @field:SerializedName("unscratched") val unscratched: Int
) : Parcelable