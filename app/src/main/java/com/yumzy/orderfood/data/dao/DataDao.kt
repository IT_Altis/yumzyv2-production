package com.yumzy.orderfood.data.dao

import androidx.room.Dao
import androidx.room.Query

@Dao
abstract class DataDao : BaseDao<ValuePairSS> {

    /**
     * Get all data from the Data table.
     */
    @Query("SELECT * FROM valuePairSS")
    abstract fun getData(): List<ValuePairSS>
}