/*
 * Created By Shriom Tripathi 2 - 5 - 2020
 */

package com.yumzy.orderfood.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity
class AddressDTO : Serializable{
    /*This is to mentain that only on address will be saved in in data base */
    /*@PrimaryKey(autoGenerate = false)
    var uniqueAddressId: Int = 15682
*/
    @PrimaryKey
    @SerializedName("addressId")
    @Expose
    var addressId = ""

    @SerializedName("googleLocation")
    @Expose
    var googleLocation: String = ""

    @SerializedName("name")
    @Expose
    var name: String = ""

    @SerializedName("fullAddress")
    @Expose
    var fullAddress: String = ""

    @SerializedName("addressTag")
    @Expose
    var addressTag: String = ""

    @SerializedName("landmark")
    @Expose
    var landmark: String = ""

    @SerializedName("houseNum")
    @Expose
    var houseNum: String = ""

    @SerializedName("longLat")
    @Expose
    var longLat: LongLat? = null

    @SerializedName("shortAddress")
    @Expose
    var shortAddress: String = ""

    var isExpanded = false

    var city = ""

    override fun toString(): String {
        return """
            name = $name \n
            fullAddress = $fullAddress \n
            addresTag = $addressTag \n
            landmark = $landmark \n
            hourNum = $houseNum \n
            latLan = ${longLat?.coordinates?.joinToString(", ")} \n
            googleLocation = $googleLocation
        """.trimIndent()
    }
}

/*

class LongLatNew : Serializable{
    @SerializedName("coordinates")
    @Expose
    var coordinates: List<Double>? = null
}*/
