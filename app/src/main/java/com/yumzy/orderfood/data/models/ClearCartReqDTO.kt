package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName


data class ClearCartReqDTO(
    @field:SerializedName("cartId")  val cartId: String
)