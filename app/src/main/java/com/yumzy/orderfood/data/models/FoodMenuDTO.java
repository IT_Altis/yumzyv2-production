package com.yumzy.orderfood.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Bhupendra Kumar Sahu.
 */
public class FoodMenuDTO {

    @SerializedName("menuItems")
    @Expose
    private List<MenuItem> menuItems = null;
    @SerializedName("addons")
    @Expose
    private Addons addons;
    @SerializedName("outlet")
    @Expose
    private Outlet outlet;

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public Addons getAddons() {
        return addons;
    }

    public void setAddons(Addons addons) {
        this.addons = addons;
    }

    public Outlet getOutlet() {
        return outlet;
    }

    public void setOutlet(Outlet outlet) {
        this.outlet = outlet;
    }

    public class Addons {


    }


    public class MenuItem {

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("offers")
        @Expose
        private List<Object> offers = null;
        @SerializedName("containsEgg")
        @Expose
        private Boolean containsEgg;
        @SerializedName("addons")
        @Expose
        private List<Object> addons = null;
        @SerializedName("outletId")
        @Expose
        private String outletId;
        @SerializedName("ribbon")
        @Expose
        private String ribbon;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("itemId")
        @Expose
        private String itemId;
        @SerializedName("outletName")
        @Expose
        private String outletName;
        @SerializedName("unit")
        @Expose
        private String unit;
        @SerializedName("displayType")
        @Expose
        private String displayType;
        @SerializedName("isVeg")
        @Expose
        private Boolean isVeg;
        @SerializedName("additionalCharges")
        @Expose
        private List<AdditionalChargeDTO> additionalCharges = null;
        @SerializedName("displayTags")
        @Expose
        private List<Object> displayTags = null;
        @SerializedName("price")
        @Expose
        private Integer price;
        @SerializedName("imageUrl")
        @Expose
        private String imageUrl;
        @SerializedName("portionSize")
        @Expose
        private String portionSize;
        @SerializedName("nextAvailable")
        @Expose
        private String nextAvailable;
        @SerializedName("displayImage")
        @Expose
        private Boolean displayImage;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Object> getOffers() {
            return offers;
        }

        public void setOffers(List<Object> offers) {
            this.offers = offers;
        }

        public Boolean getContainsEgg() {
            return containsEgg;
        }

        public void setContainsEgg(Boolean containsEgg) {
            this.containsEgg = containsEgg;
        }

        public List<Object> getAddons() {
            return addons;
        }

        public void setAddons(List<Object> addons) {
            this.addons = addons;
        }

        public String getOutletId() {
            return outletId;
        }

        public void setOutletId(String outletId) {
            this.outletId = outletId;
        }

        public String getRibbon() {
            return ribbon;
        }

        public void setRibbon(String ribbon) {
            this.ribbon = ribbon;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getItemId() {
            return itemId;
        }

        public void setItemId(String itemId) {
            this.itemId = itemId;
        }

        public String getOutletName() {
            return outletName;
        }

        public void setOutletName(String outletName) {
            this.outletName = outletName;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getDisplayType() {
            return displayType;
        }

        public void setDisplayType(String displayType) {
            this.displayType = displayType;
        }

        public Boolean getIsVeg() {
            return isVeg;
        }

        public void setIsVeg(Boolean isVeg) {
            this.isVeg = isVeg;
        }

        public List<AdditionalChargeDTO> getAdditionalCharges() {
            return additionalCharges;
        }

        public void setAdditionalCharges(List<AdditionalChargeDTO> additionalCharges) {
            this.additionalCharges = additionalCharges;
        }

        public List<Object> getDisplayTags() {
            return displayTags;
        }

        public void setDisplayTags(List<Object> displayTags) {
            this.displayTags = displayTags;
        }

        public Integer getPrice() {
            return price;
        }

        public void setPrice(Integer price) {
            this.price = price;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getPortionSize() {
            return portionSize;
        }

        public void setPortionSize(String portionSize) {
            this.portionSize = portionSize;
        }

        public String getNextAvailable() {
            return nextAvailable;
        }

        public void setNextAvailable(String nextAvailable) {
            this.nextAvailable = nextAvailable;
        }

        public Boolean getDisplayImage() {
            return displayImage;
        }

        public void setDisplayImage(Boolean displayImage) {
            this.displayImage = displayImage;
        }

    }

    public class Outlet {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("outletId")
        @Expose
        private String outletId;
        @SerializedName("available")
        @Expose
        private String available;
        @SerializedName("costForTwo")
        @Expose
        private String costForTwo;
        @SerializedName("displayName")
        @Expose
        private String displayName;
        @SerializedName("facilities")
        @Expose
        private List<Object> facilities = null;
        @SerializedName("gallery")
        @Expose
        private List<Object> gallery = null;
        @SerializedName("locality")
        @Expose
        private String locality;
        @SerializedName("outletName")
        @Expose
        private String outletName;
        @SerializedName("reviews")
        @Expose
        private List<Object> reviews = null;
        @SerializedName("cuisines")
        @Expose
        private String cuisines;
        @SerializedName("gstinType")
        @Expose
        private String gstinType;
        @SerializedName("gstPercent")
        @Expose
        private String gstPercent;
        @SerializedName("lic")
        @Expose
        private String lic;
        @SerializedName("imageUrl")
        @Expose
        private String imageUrl;
        @SerializedName("deliveryTime")
        @Expose
        private Integer deliveryTime;
        @SerializedName("hygineRating")
        @Expose
        private String hygineRating;
        @SerializedName("items")
        @Expose
        private String items;
        @SerializedName("prepTime")
        @Expose
        private Integer prepTime;
        @SerializedName("timeToOpen")
        @Expose
        private Integer timeToOpen;
        @SerializedName("timeToClose")
        @Expose
        private Integer timeToClose;
        @SerializedName("offers")
        @Expose
        private String offers;
        @SerializedName("favorite")
        @Expose
        private Boolean favorite;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOutletId() {
            return outletId;
        }

        public void setOutletId(String outletId) {
            this.outletId = outletId;
        }

        public String getAvailable() {
            return available;
        }

        public void setAvailable(String available) {
            this.available = available;
        }

        public String getCostForTwo() {
            return costForTwo;
        }

        public void setCostForTwo(String costForTwo) {
            this.costForTwo = costForTwo;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public List<Object> getFacilities() {
            return facilities;
        }

        public void setFacilities(List<Object> facilities) {
            this.facilities = facilities;
        }

        public List<Object> getGallery() {
            return gallery;
        }

        public void setGallery(List<Object> gallery) {
            this.gallery = gallery;
        }

        public String getLocality() {
            return locality;
        }

        public void setLocality(String locality) {
            this.locality = locality;
        }

        public String getOutletName() {
            return outletName;
        }

        public void setOutletName(String outletName) {
            this.outletName = outletName;
        }

        public List<Object> getReviews() {
            return reviews;
        }

        public void setReviews(List<Object> reviews) {
            this.reviews = reviews;
        }

        public String getCuisines() {
            return cuisines;
        }

        public void setCuisines(String cuisines) {
            this.cuisines = cuisines;
        }

        public String getGstinType() {
            return gstinType;
        }

        public void setGstinType(String gstinType) {
            this.gstinType = gstinType;
        }

        public String getGstPercent() {
            return gstPercent;
        }

        public void setGstPercent(String gstPercent) {
            this.gstPercent = gstPercent;
        }

        public String getLic() {
            return lic;
        }

        public void setLic(String lic) {
            this.lic = lic;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public Integer getDeliveryTime() {
            return deliveryTime;
        }

        public void setDeliveryTime(Integer deliveryTime) {
            this.deliveryTime = deliveryTime;
        }

        public String getHygineRating() {
            return hygineRating;
        }

        public void setHygineRating(String hygineRating) {
            this.hygineRating = hygineRating;
        }

        public String getItems() {
            return items;
        }

        public void setItems(String items) {
            this.items = items;
        }

        public Integer getPrepTime() {
            return prepTime;
        }

        public void setPrepTime(Integer prepTime) {
            this.prepTime = prepTime;
        }

        public Integer getTimeToOpen() {
            return timeToOpen;
        }

        public void setTimeToOpen(Integer timeToOpen) {
            this.timeToOpen = timeToOpen;
        }

        public Integer getTimeToClose() {
            return timeToClose;
        }

        public void setTimeToClose(Integer timeToClose) {
            this.timeToClose = timeToClose;
        }

        public String getOffers() {
            return offers;
        }

        public void setOffers(String offers) {
            this.offers = offers;
        }

        public Boolean getFavorite() {
            return favorite;
        }

        public void setFavorite(Boolean favorite) {
            this.favorite = favorite;
        }


    }
}
