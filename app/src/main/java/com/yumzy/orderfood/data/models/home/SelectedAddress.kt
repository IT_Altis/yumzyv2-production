package com.yumzy.orderfood.data.models.home

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


data class SelectedAddress(
    @PrimaryKey(autoGenerate = false)
    @field:SerializedName("addressId") val addressId: Int = 15682,
    @field:SerializedName("locality") val locality: String,
    @field:SerializedName("city") val city: String,
    @field:SerializedName("country") val country: String,
    @field:SerializedName("state") val state: String,
    @field:SerializedName("fullAddress") val fullAddress: String,
    @field:SerializedName("knownName") val knownName: String,
    @field:SerializedName("latitude") val latitude: Double?,
    @field:SerializedName("longitude") val longitude: Double?
)