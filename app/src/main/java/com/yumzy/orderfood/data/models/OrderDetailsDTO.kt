package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName
import com.yumzy.orderfood.data.cart.Charge


data class Billing(
    @field:SerializedName("totalAmount") val totalAmount: Double,
    @field:SerializedName("discount") val discount: Double,
    @field:SerializedName("subTotal") val subTotal: Double,
    @field:SerializedName("charges") val charges: List<Charge>?,
    @field:SerializedName("taxes") val taxes: List<Charge>?,
    @field:SerializedName("originalDelivery") val originalDelivery: Double
)
data class Item(
   @field:SerializedName("itemId") val itemId: String,
   @field:SerializedName("name") val name: String,
   @field:SerializedName("quantity") val quantity: Double,
   @field:SerializedName("price") val price: Double,
   @field:SerializedName("addons") val addons: String
)
