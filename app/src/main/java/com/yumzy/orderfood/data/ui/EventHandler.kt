package com.yumzy.orderfood.data.ui

import android.graphics.Color
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.laalsa.laalsalib.utilcode.util.ActivityUtils
import com.laalsa.laalsalib.utilcode.util.ToastUtils

typealias ClickHandler = (View, (Int) -> Unit) -> Unit
class EventHandler {
    private val topActivity: AppCompatActivity?
        get() = ActivityUtils.getTopActivity() as AppCompatActivity

    fun onHandleClick(view: View?) {
        if (topActivity == null) return
        ToastUtils.setBgColor(Color.BLUE)
        Toast.makeText(topActivity, "Invite ", Toast.LENGTH_LONG).show()


    }
}