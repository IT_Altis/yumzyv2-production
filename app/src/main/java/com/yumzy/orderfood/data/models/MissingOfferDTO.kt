package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName

data class MissingOfferDTO(
    @field:SerializedName("description") val description: String,
    @field:SerializedName("discountType") val discountType: String,
    @field:SerializedName("maxDiscount") val maxDiscount: Int,
    @field:SerializedName("minOrderAmount") val minOrderAmount: Int,
    @field:SerializedName("promoId") val promoId: String,
    @field:SerializedName("promoName") val promoName: String,
    @field:SerializedName("value") val value: Int
)