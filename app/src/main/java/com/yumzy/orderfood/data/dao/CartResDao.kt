package com.yumzy.orderfood.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.yumzy.orderfood.data.models.CartResDTO
import javax.inject.Singleton

/**
 * Created by Bhupendra Kumar Sahu.
 */
@Dao
interface CartResDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateOrInsert(cartResDTO: CartResDTO)

    @Update
    fun updateCartRes(vararg cartResDTO: CartResDTO)

    @Query("SELECT * FROM CartResDTO")
    fun getCartRes(): LiveData<CartResDTO>

    @Query("DELETE FROM CartResDTO")
    fun removeCartRes()

}