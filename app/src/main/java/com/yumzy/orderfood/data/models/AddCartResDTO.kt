package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName
import com.yumzy.orderfood.data.cart.Charge


open class AddCartResDTO(

    @field:SerializedName("deviceId")  val deviceId: String,
    @field:SerializedName("outlets")  val outlets: List<CartOutlet>,
    @field:SerializedName("cartId")  val cartId: String,
    @field:SerializedName("coupons")  val coupons: List<Any?>,
    @field:SerializedName("billing")  val billing: DataBilling
)

data class DataBilling(
   @field:SerializedName("subTotal")  val subTotal: Double,
   @field:SerializedName("discount")  val discount: Double,
   @field:SerializedName("roundOff")  val roundOff: Double,
   @field:SerializedName("totalAmount")  val totalAmount: Double,
   @field:SerializedName("loyaltyUsed")  val loyaltyUsed: Double,
   @field:SerializedName("charges")  val charges: List<Charge>,
   @field:SerializedName("taxes")  val taxes: List<Charge>
)


data class CartOutlet(
   @field:SerializedName("location")  val location: LocationCart,
   @field:SerializedName("outletId")  val outletId: String,
   @field:SerializedName("outletName")  val outletName: String,
   @field:SerializedName("outletLogo")  val outletLogo: String,
   @field:SerializedName("scheme")  val scheme: String,
   @field:SerializedName("gst")  val gst: String,
   @field:SerializedName("items")  val items: List<CartItems>,
   @field:SerializedName("offer")  val offer: Offer,
   @field:SerializedName("billing")  val billing: OutletBillings
)

data class OutletBillings(
   @field:SerializedName("totalAmount")  val totalAmount: Double,
   @field:SerializedName("discount")  val discount: Double,
   @field:SerializedName("subTotal")  val subTotal: Double,
   @field:SerializedName("charges")  val charges: List<Charge>,
   @field:SerializedName("taxes")  val taxes: List<Charge>
)

data class CartItems(
   @field:SerializedName("offerPrice")  val offerPrice: Double,
   @field:SerializedName("itemId")  val itemId: String,
   @field:SerializedName("outletId")  val outletId: String,
   @field:SerializedName("name")  val name: String,
   @field:SerializedName("price")  val price: Double,
   @field:SerializedName("isVeg")  val isVeg: Boolean,
   @field:SerializedName("containsEgg")  val containsEgg: Boolean,
   @field:SerializedName("gst")  val gst: Double,
   @field:SerializedName("addons")  val addons: List<CartAddOn>,
   @field:SerializedName("variant")  val variant: CartVariant,
   @field:SerializedName("quantity")  val quantity: Int,
   @field:SerializedName("additionalCharges")  val additionalCharges: List<Charge>
)

data class CartAddOn (
   @field:SerializedName("addonId")  val addonId: String,
   @field:SerializedName("addonName")  val addonName: String,
   @field:SerializedName("options")  val options: List<Option>,
   @field:SerializedName("isVariant")  val isVariant: Boolean,
   @field:SerializedName("maxSelection")  val maxSelection: Int,
   @field:SerializedName("minSelection")  val minSelection: Int
)

data class CartVariant(
   @field:SerializedName("addonId")  val addonId: String,
   @field:SerializedName("addonName")  val addonName: String,
   @field:SerializedName("options")  val options: Option,
   @field:SerializedName("isVariant")  val isVariant: Boolean,
   @field:SerializedName("maxSelection")  val maxSelection: Int,
   @field:SerializedName("minSelection")  val minSelection: Int
)

data class Option(
   @field:SerializedName("optionName")  val optionName: String,
   @field:SerializedName("cost")  val cost: Long,
   @field:SerializedName("isVeg")  val isVeg: Boolean
)

data class LocationCart(
   @field:SerializedName("coordinates")  val coordinates: List<Double>,
   @field:SerializedName("type")  val type: String
)

data class Offer(
   @field:SerializedName("discountType")  val discountType: String,
   @field:SerializedName("usedBudget")  val usedBudget: Long,
   @field:SerializedName("outletId")  val outletId: String,
   @field:SerializedName("promoId")  val promoId: String,
   @field:SerializedName("value")  val value: Long,
   @field:SerializedName("offerFor")  val offerFor: String,
   @field:SerializedName("minOrderAmount")  val minOrderAmount: Long,
   @field:SerializedName("maxDiscount")  val maxDiscount: Long,
   @field:SerializedName("promoBudget")  val promoBudget: Long
)

