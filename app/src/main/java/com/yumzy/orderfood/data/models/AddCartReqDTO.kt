package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName


data class NewAddItemDTO(
    @field:SerializedName("item") val item: NewItemDTO,
    @field:SerializedName("cartId") val cartID: String
)

data class NewItemDTO(
    @field:SerializedName("outletId") val outletID: String,
    @field:SerializedName("itemId") val itemID: String,
    @field:SerializedName("price") val price: Double,
    @field:SerializedName("quantity") val quantity: Int,
    @field:SerializedName("variant") val variant: Variant?,
    @field:SerializedName("addons") val addons: List<Variant>?
)

data class Variant(
    @field:SerializedName("addonId")val addonID: String,
    @field:SerializedName("addonName") val addonName: String,
    @field:SerializedName("options") val options: List<String>,
    @field:SerializedName("price") val price: Double,
    @field:SerializedName("selectedCount") val selectedCount: Int
) {
    var selectedItemsPrice: Float = 0f
}
