package com.yumzy.orderfood.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.yumzy.orderfood.data.cart.Addon
import com.yumzy.orderfood.data.cart.Charge
import org.jetbrains.annotations.NotNull

/**
 * Created by Bhupendra Kumar Sahu.
 */
@Entity
data class CartResDTO(
    /*
     warning:@field userCart=2760
     added this line for maintain single cart object for application .2760
     just random number if any issue in cart or want to clear cart on update change this line

     */
    @NotNull
    @PrimaryKey(autoGenerate = false)
    @field:SerializedName("userCart") val userCart: Int = 2760,
    @field:SerializedName("deviceId") val deviceId: String?,
    @field:SerializedName("outlets") val outletDTOS: List<CartOutletDetailsDTO?>?,
    @field:SerializedName("cartId") val cartId: String? = null,
    @field:SerializedName("coupons") val coupons: List<Coupon?>?,
    @field:SerializedName("billing") val billing: CartBilling?,
    @field:SerializedName("payments") val payments: List<PaymentDTO>?
)

data class Coupon(
    @field:SerializedName("code") val code: String?,
    @field:SerializedName("title") val title: String?,
    @field:SerializedName("couponId") val couponId: String?,
    @field:SerializedName("discountType") val discountType: String?,
    @field:SerializedName("minOrderAmount") val minOrderAmount: Double?,
    @field:SerializedName("maxDiscount") val maxDiscount: Double?
)


data class CartBilling(
    @field:SerializedName("subTotal") val subTotal: Double,
    @field:SerializedName("discount") val discount: Double,
    @field:SerializedName("couponDiscount") val couponDiscount: Double,
    @field:SerializedName("roundOff") val roundOff: Double,
    @field:SerializedName("totalAmount") val totalAmount: Double,
    @field:SerializedName("loyaltyUsed") val loyaltyUsed: Double,
    @field:SerializedName("charges") val charges: List<Charge>?,
    @field:SerializedName("taxes") val taxes: List<Charge>?,
    @field:SerializedName("originalDelivery") val originalDelivery: Double
)


data class CartOutletDetailsDTO(
    @field:SerializedName("location") val locationDTO: mLocationDTO,
    @field:SerializedName("outletId") val outletId: String,
    @field:SerializedName("outletName") val outletName: String,
    @field:SerializedName("outletLogo") val outletLogo: String,
    @field:SerializedName("scheme") val scheme: String,
    @field:SerializedName("gst") val gst: String,
    @field:SerializedName("items") val itemDTOS: List<CartItemDTO>,
    @field:SerializedName("offer") val offerDTO: CartOfferDTO,
    @field:SerializedName("billing") val billing: Billing
)


data class CartItemDTO(
    @field:SerializedName("offerPrice") val offerPrice: Double,
    @field:SerializedName("itemId") val itemID: String,
    @field:SerializedName("outletId") val outletID: String,
    @field:SerializedName("name") val name: String,
    @field:SerializedName("price") val price: Double,
    @field:SerializedName("itemPrice") val itemPrice: Double,
    @field:SerializedName("isVeg") val isVeg: Boolean,
    @field:SerializedName("isUnavailable") var isUnavailable: Boolean,
    @field:SerializedName("containsEgg") val containsEgg: Boolean,
    @field:SerializedName("gst") val gst: Double,
    @field:SerializedName("addons") val addons: List<Addon>,
    @field:SerializedName("variant") val variant: VariantsDTO?,
    @field:SerializedName("quantity") var quantity: Int,
    @field:SerializedName("itemPosition") var itemPosition: Int,
    @field:SerializedName("additionalCharges") val additionalCharges: List<Charge>,
    @field:SerializedName("billing") val billing: Billing

) {
    var itemIndex = -1
}

data class VariantsDTO(
    @field:SerializedName("addonId") val addonId: String,
    @field:SerializedName("addonName") val addonName: String,
    @field:SerializedName("options") val optionsDTO: OptionsDTO,
    @field:SerializedName("isVariant") val isVariant: Boolean?,
    @field:SerializedName("maxSelection") val maxSelection: Int,
    @field:SerializedName("minSelection") val minSelection: Int
)

data class OptionsDTO(
    @field:SerializedName("optionName") val optionName: String,
    @field:SerializedName("cost") val cost: Double,
    @field:SerializedName("isVeg") val isVeg: Boolean
)

data class mLocationDTO(
    @field:SerializedName("coordinates") val coordinates: List<Double>,
    @field:SerializedName("type") val type: String
)

data class CartOfferDTO(
    @field:SerializedName("discountType") val discountType: String,
    @field:SerializedName("usedBudget") val usedBudget: Double,
    @field:SerializedName("outletId") val outletId: String,
    @field:SerializedName("promoId") val promoId: String,
    @field:SerializedName("value") val value: Double,
    @field:SerializedName("offerFor") val offerFor: String,
    @field:SerializedName("minOrderAmount") val minOrderAmount: Double,
    @field:SerializedName("maxDiscount") val maxDiscount: Double,
    @field:SerializedName("promoBudget") val promoBudget: Double
)

