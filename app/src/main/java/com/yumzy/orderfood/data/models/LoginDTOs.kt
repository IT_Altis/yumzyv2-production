/*
 *   Created by Sourav Kumar Pandit  22 - 4 - 2020
 */

package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName

data class RegisterDTO(
    @field:SerializedName("firstName") val firstName: String,
    @field:SerializedName("lastName") val lastName: String,
    @field:SerializedName("email") val email: String,
    @field:SerializedName("otp") val otp: String,
    @field:SerializedName("mobile") val mobile: String,
    @field:SerializedName("countryCode") val countryCode: String,
    @field:SerializedName("deviceId") val deviceID: String,
    @field:SerializedName("gcmToken") val gcmToken: String

)
data class SendOtpDTO(
    @field:SerializedName("mobile") val mobile: String,
    @field:SerializedName("countryCode") val countryCode: String,
    @field:SerializedName("action") val action: String
)

data class VerifyOtpDTO(
    @field:SerializedName("mobile") val mobile: String,
    @field:SerializedName("otp") val otp: String,
    @field:SerializedName("countryCode") val countryCode: String,
    @field:SerializedName("deviceId") val deviceId: String
)

data class ExploreUser(
    @field:SerializedName("deviceId") val deviceId: String
)
