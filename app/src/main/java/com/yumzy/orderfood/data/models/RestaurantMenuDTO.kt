package com.yumzy.orderfood.data.models

import androidx.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.yumzy.orderfood.appGson
import com.yumzy.orderfood.data.dao.ValuePairSI
import com.yumzy.orderfood.util.viewutils.YumUtil
import java.io.Serializable


/**
 * Created by Shriom Tripathi.
 */


@Entity
data class RestaurantDTO(
    @PrimaryKey
    @field:SerializedName("relationConst")
    var relationConst: Int = 54465,

    @field:SerializedName("menuItems")
    @Expose
    var menuSectionDTOS: List<RestaurantMenuSectionDTO>? = null,

    @field:SerializedName("categoryList")
    @Expose
    var menuCategoryDTOS: List<RestaurantMenuCategoryDTO>? = null,

    @field:SerializedName("addons")
    @Expose
    var menuAddons: Map<String, RestaurantAddOnDTO>? = null,

    @field:SerializedName("outlet")
    @Expose
    var restaurantDetailDTO: RestaurantDetailDTO? = null,

    )

@Entity
data class RestaurantMenuSectionDTO(
    @PrimaryKey
    @field:SerializedName("relationConst")
    var relationConst: Int = 54465,

    @field:SerializedName("sectionIndex")
    var sectionIndex: Int = 0,

    @field:SerializedName("type")
    @Expose
    var type: String = "",

    @field:SerializedName("name")
    @Expose
    var name: String = "",

    @field:SerializedName("layoutType")
    @Expose
    var layoutType: Int = 0,

    @field:SerializedName("list")
    @Expose
    var list: List<RestaurantItem>? = null,

    @field:SerializedName("outletOperational")
    @Expose
    var outletOperational: Boolean = false,

    @field:SerializedName("outletAvailable")
    @Expose
    var outletAvailable: Boolean = false,

    )

@Entity
data class RestaurantItem(

    @field:SerializedName("relationConst")
    var relationConst: Int = 54465,

    @PrimaryKey
    @field:SerializedName("itemId")
    @Expose
    var itemId: String = "",

    @field:SerializedName("type")
    @Expose
    var type: String = "",

    @field:SerializedName("offers")
    @Expose
    var offers: List<RestaurantItemOffer>? = null,

    @field:SerializedName("containsEgg")
    @Expose
    var containsEgg: Boolean = false,

    @field:SerializedName("addons")
    @Expose
    var addons: List<String> = emptyList(),

    @field:SerializedName("outletId")
    @Expose
    var outletId: String = "",

    @field:SerializedName("description")
    @Expose
    var description: String = "",

    @field:SerializedName("outletName")
    @Expose
    var outletName: String = "",

    @field:SerializedName("unit")
    @Expose
    var unit: String = "",

    @Ignore
    @field:SerializedName("displayType")
    @Expose
    var displayType: String = "",

    @field:SerializedName("isVeg")
    @Expose
    var isVeg: Boolean = false,

    @field:SerializedName("additionalCharges")
    @Expose
    var additionalCharges: List<RestaurantItemAddititonalCharges>? = null,

    @Ignore
    @field:SerializedName("displayTags")
    @Expose
    var displayTags: List<Any>? = null,

    @field:SerializedName("price")
    @Expose
    var price: Double = 0.0,

    @field:SerializedName("imageUrl")
    @Expose
    var imageUrl: String = "",

    @field:SerializedName("portionSize")
    @Expose
    var portionSize: String = "",

    @field:SerializedName("nextAvailable")
    @Expose
    var nextAvailable: String = "",

    @field:SerializedName("displayImage")
    @Expose
    var displayImage: Boolean = false,

    @field:SerializedName("prevQuantity")
    @Expose
    var prevQuantity: Int = 0,

    @field:SerializedName("isItemAdding")
    @Expose
    var isItemAdding: Boolean = false,

    ) : Serializable {

    @field:SerializedName("ribbon")
    @Expose
    var ribbon: String = ""
        set(value) {
            field = value.capitalize()
        }

    @field:SerializedName("name")
    @Expose
    var name: String = ""
        set(value) {
            val list = value.split(" ")
            if (list.isNotEmpty()) {
                list[0].capitalize()
                field = list.joinToString(" ")
            } else {
                field = value.capitalize()
            }
        }

    @field:SerializedName("currentOfferPrice")
    @Expose
    var currentOfferPrice: Double = 0.0
        get() {
            val zoneOffer = YumUtil.setRecZoneOfferString(offers, price)
            if (zoneOffer != null) {
                field = zoneOffer.second
            } else {
                field = 0.0
            }
            return field
        }

    @field:SerializedName("index")
    @Expose
    var indexPosition: Int = 0

    @field:SerializedName("isLastItem")
    @Expose
    var isLastItem: Boolean = false

    @field:SerializedName("itemOperational")
    var itemOperational: Boolean? = null
    @field:SerializedName("outletOperational")
    var outletOperational: Boolean? = null
    @field:SerializedName("itemAvailable")
    var itemAvailable: Boolean? = null
    @field:SerializedName("outletAvailable")
    var outletAvailable: Boolean? = null
}

data class ItemQuantityDTO(
    @Embedded
    val restaurantItem: RestaurantItem,
    @Relation(
        parentColumn = "itemId",
        entityColumn = "id"
    )
    val itemQuantity: ValuePairSI
) : Serializable

data class RestaurantItemOffer(

    @PrimaryKey
    @field:SerializedName("itemId")
    @Expose
    var itemId: String = "",

    @field:SerializedName("endDate")
    @Expose
    var endDate: String = "",

    @field:SerializedName("usedBudget")
    @Expose
    var usedBudget: Double = 0.0,

    @field:SerializedName("outletId")
    @Expose
    var outletId: String = "",

    @field:SerializedName("description")
    @Expose
    var description: String = "",

    @field:SerializedName("discountType")
    @Expose
    var discountType: String = "",

    @field:SerializedName("startTime")
    @Expose
    var startTime: String = "",

    @field:SerializedName("endTime")
    @Expose
    var endTime: String = "",

    @field:SerializedName("value")
    @Expose
    var value: Int = 0,

    @field:SerializedName("startDate")
    @Expose
    var startDate: String = ""

) : Serializable

data class RestaurantItemAddititonalCharges(

    @field:SerializedName("name")
    @Expose
    var name: String = "",

    @field:SerializedName("value")
    @Expose
    var value: Int = 0

) : Serializable

data class RestaurantMenuCategoryDTO(

    @field:SerializedName("key")
    @Expose
    var key: String = "",

    @field:SerializedName("value")
    @Expose
    var value: Int = 0,

    @field:SerializedName("hasVeg")
    @Expose
    var hasVeg: Boolean = false,

    @field:SerializedName("hasEgg")
    @Expose
    var hasEgg: Boolean = false,

    @field:SerializedName("hasBoth")
    @Expose
    var hasBoth: Boolean = false

) : Serializable


data class RestaurantAddOnDTO(

    @field:SerializedName("addonId")
    @Expose
    var addonId: String = "",

    @field:SerializedName("addonName")
    @Expose
    var addonName: String = "",

    @field:SerializedName("displayName")
    @Expose
    var displayName: String = "",

    @field:SerializedName("description")
    @Expose
    var description: String = "",

    @field:SerializedName("selectType")
    @Expose
    var selectType: String = "",

    @field:SerializedName("maxSelection")
    @Expose
    var maxSelection: Int = 0,

    @field:SerializedName("minSelection")
    @Expose
    var minSelection: Int = 0,

    @field:SerializedName("isVariant")
    @Expose
    var isVariant: Boolean = false,

    @field:SerializedName("status")
    @Expose
    var status: String = "",

    @field:SerializedName("options")
    @Expose
    var options: List<RestaurantAddOnOption>? = null,


    @field:SerializedName("doInitialSelection")
    @Expose
    var doInitialSelection: Boolean = true,

    ) : Serializable {

    fun clone(): RestaurantAddOnDTO {
        val stringProject = appGson.toJson(this, RestaurantAddOnDTO::class.java)
        return appGson.fromJson(stringProject, RestaurantAddOnDTO::class.java)
    }

}

data class RestaurantAddOnOption(

    @field:SerializedName("optionName")
    @Expose
    var optionName: String = "",

    @field:SerializedName("cost")
    @Expose
    var cost: Double = 0.0,

    @field:SerializedName("isVeg")
    @Expose
    var isVeg: Boolean = false,

    @field:SerializedName("offerPrice")
    @Expose
    var offerPrice: Double = 0.0,

    @field:SerializedName("isSelected")
    @Expose
    var isSelected: Boolean = false

) : Serializable {}

data class RestaurantDetailDTO(

    @field:SerializedName("_id")
    @Expose
    var id: String = "",

    @field:SerializedName("outletId")
    @Expose
    var outletId: String = "",

    @field:SerializedName("available")
    @Expose
    var available: String = "",

    @field:SerializedName("costForTwo")
    @Expose
    var costForTwo: String = "",

    @field:SerializedName("displayName")
    @Expose
    var displayName: String = "",

    @field:SerializedName("facilities")
    @Expose
    var facilities: List<Any>? = null,

    @field:SerializedName("gallery")
    @Expose
    var gallery: List<Any>? = null,

    @field:SerializedName("locality")
    @Expose
    var locality: String = "",

    @field:SerializedName("outletName")
    @Expose
    var outletName: String = "",

    @field:SerializedName("reviews")
    @Expose
    var reviews: List<Any>? = null,

    @field:SerializedName("cuisines")
    @Expose
    var cuisines: String = "",

    @field:SerializedName("gstinType")
    @Expose
    var gstinType: String = "",

    @field:SerializedName("gstPercent")
    @Expose
    var gstPercent: String = "",

    @field:SerializedName("lic")
    @Expose
    var lic: String = "",

    @field:SerializedName("imageUrl")
    @Expose
    var imageUrl: String = "",

    @field:SerializedName("deliveryTime")
    @Expose
    var deliveryTime: Double = 0.0,

    @field:SerializedName("hygineRating")
    @Expose
    var hygineRating: String = "",

    @field:SerializedName("rating")
    @Expose
    var rating: String = "",

    @field:SerializedName("items")
    @Expose
    var items: String = "",

    @field:SerializedName("prepTime")
    @Expose
    var prepTime: Int = 0,

    @field:SerializedName("timeToOpen")
    @Expose
    var timeToOpen: Int = 0,

    @field:SerializedName("timeToClose")
    @Expose
    var timeToClose: Int = 0,

    @field:SerializedName("offers")
    @Expose
    var offers: String = "",

    @field:SerializedName("favorite")
    @Expose
    var favorite: Boolean = false,

    @field:SerializedName("deepLinkUrl")
    @Expose
    var deepLinkUrl: String = ""

) : Serializable {}