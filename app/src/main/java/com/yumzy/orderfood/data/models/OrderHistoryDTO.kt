package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName

data class OrderHistoryDTO(

    @field:SerializedName("orders")  val orders: List<Order>,
    @field:SerializedName("skip") val skip: Int
)

data class Order(
   @field:SerializedName("orderNum") val orderNum: String,
   @field:SerializedName("locality") val locality: String,
   @field:SerializedName("creationDate") val creationDate: String,
   @field:SerializedName("rated") var rated: Boolean,
   @field:SerializedName("items") val items: String,
   @field:SerializedName("outletId") val outletId: String,
   @field:SerializedName("outletName") val outletName: String,
   @field:SerializedName("orderId") val orderId: String,
   @field:SerializedName("status") val status: String,
   @field:SerializedName("totalAmount") val totalAmount: Double,
   @field:SerializedName("couponDiscount") val couponDiscount: Double,
   @field:SerializedName("rating") var rating: Double
)