package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName

data class PaymentDTO(
    @field:SerializedName("paidVia") var paidVia: String?="",
    @field:SerializedName("method") var method: String?="",
    @field:SerializedName("provider") var provider: String?="",
    @field:SerializedName("status") var status: String?="",
    @field:SerializedName("amount") val amount: Double?,
    @field:SerializedName("paid") val paid: Double?=0.0
) {
}