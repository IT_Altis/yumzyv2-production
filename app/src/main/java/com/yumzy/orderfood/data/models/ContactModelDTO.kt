package com.yumzy.orderfood.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ContactModelDTO(
    @field:SerializedName("name") var name: String = "",
    @Transient @field:SerializedName("photoUri")var photoUri: String? = "",
    @field:SerializedName("number")var number: String = "",
    @Transient @field:SerializedName("type") var type: String = "contact",
) : Serializable { }