/*
 *   Created by Sourav Kumar Pandit  27 - 4 - 2020
 */

package com.yumzy.orderfood.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.yumzy.orderfood.data.models.AddressDTO

@Dao
interface AddressDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateOrInsert(address: AddressDTO)

    @Query("SELECT * FROM AddressDTO ")
    fun getAll(): LiveData<List<AddressDTO>>

    @Query("SELECT * FROM AddressDTO ")
    fun getAllSync(): List<AddressDTO>

    @Query("SELECT * FROM AddressDTO where addressId == :id")
    fun getById(id: String): LiveData<AddressDTO>

    @Query("SELECT * FROM AddressDTO where addressId == :id")
    fun getByIdSync(id: String): AddressDTO

    @Query("DELETE FROM AddressDTO where addressId NOT in (:ids)")
    fun deleteAllIfExistsSync(ids: List<String>)

    @Query("DELETE FROM AddressDTO where addressId == :id")
    fun deleteById(id: String)


}
