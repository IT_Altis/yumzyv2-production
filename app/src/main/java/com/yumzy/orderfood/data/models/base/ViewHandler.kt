/*
 *   Created by Sourav Kumar Pandit  24 - 4 - 2020
 */

package com.yumzy.orderfood.data.models.base

import com.yumzy.orderfood.ui.base.IView

@Suppress("UNCHECKED_CAST")
class ViewHandler<T>(view: IView, result: ResponseDTO<T>, callerFun: ((T) -> Unit)? = null) {
    init {
        when (result.type) {
            APIConstant.Status.LOADING -> {
                view.showProgressBar()
            }
            APIConstant.Status.ERROR -> {
                view.hideProgressBar()
                when {
                    result.code != null -> view.showCodeError(
                        code = result.code,
                        title = "${result.title}",
                        message = "${result.message}"
                    )
                    else -> view.showCodeError(
                        code = null, title = result.title,
                        message = "${result.message}"
                    )
                }
            }
            APIConstant.Status.SUCCESS -> {
                view.hideProgressBar()
                when {
                    result.data != null -> {
                        try {
                            callerFun?.invoke(result.data)
                        } catch (exception: Exception) {
                            view.showCodeError(APIConstant.Status.ERROR, exception.message)
                        }
                    }
                    result.code != null -> view.onResponse(result.code, result.data)
                    else -> view.showCodeError(null, "${result.data}")
                }
            }

        }
    }
}
