package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName


data class FavRestaurantResDTO(
    @field:SerializedName("items")  val items: List<Any?>,
    @field:SerializedName("outlets")  val outlets: List<FavOutlet>
)

data class FavOutlet(
   @field:SerializedName("itemName") val itemName: String,
   @field:SerializedName("itemId") val itemId: String,
   @field:SerializedName("imageUrl") val imageUrl: String,
   @field:SerializedName("offers") val offers: List<OfferDTO>,
   @field:SerializedName("cuisines") val cuisines: List<String>,
   @field:SerializedName("rating") val rating: String?,
   @field:SerializedName("subtitle") val subtitle: String? = "",
   @field:SerializedName("costForTwo") val costForTwo: String? = "",
   @field:SerializedName("locality") val locality: String? = ""
)