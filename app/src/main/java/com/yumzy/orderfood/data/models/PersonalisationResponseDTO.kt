package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName


data class PersonalisationResponseDTO(
    @field:SerializedName("title")  val title: String,
    @field:SerializedName("values")  val values: List<String>,
    @field:SerializedName("prefId")  val prefId: Long
)

