package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName


data class NewOrderResDTO
    (
    @field:SerializedName("transactionId") val transactionId: String,
    @field:SerializedName("orderId") val orderId: String,
    @field:SerializedName("orderNum") val orderNum: String,
    @field:SerializedName("sdkOrderId") val sdkOrderId: String,
    @field:SerializedName("totalAmount") val totalAmount: String

)