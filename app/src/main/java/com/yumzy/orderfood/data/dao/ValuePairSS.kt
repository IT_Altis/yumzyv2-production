package com.yumzy.orderfood.data.dao

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "valuePairSS")
data class ValuePairSS(@PrimaryKey val id: String, val value: String)

@Entity(tableName = "valuePairSI")
data class ValuePairSI(@PrimaryKey val id: String, val value: Int)

