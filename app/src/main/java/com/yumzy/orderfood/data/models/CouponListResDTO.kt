package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName


data class CouponListResDTO
    (
    @field:SerializedName("code") val code: String,
    @field:SerializedName("title") val title: String,
    @field:SerializedName("desc") val desc: String,
    @field:SerializedName("couponId") val couponId: String,
    @field:SerializedName("expiry") val expiry: Any? = null,
    @field:SerializedName("discountType") val discountType: String,
    @field:SerializedName("value") val value: Long,
    @field:SerializedName("minOrderAmount") val minOrderAmount: Long,
    @field:SerializedName("maxDiscount") val maxDiscount: Long,
    @field:SerializedName("terms") val terms: List<String?>,
    @field:SerializedName("count") val count: Long,
    @field:SerializedName("referralDeepLink") val referralDeepLink: String
)