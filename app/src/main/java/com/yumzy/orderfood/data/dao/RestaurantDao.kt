package com.yumzy.orderfood.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.yumzy.orderfood.data.models.RestaurantDTO
import com.yumzy.orderfood.data.models.RestaurantItem
import com.yumzy.orderfood.data.models.RestaurantMenuSectionDTO


class MenuSectionRelation {
    @Embedded
    lateinit var restaurantDTO: RestaurantDTO

    @Relation(
        parentColumn = "relationConst",
        entityColumn = "relationConst",
        entity = RestaurantMenuSectionDTO::class
    )
    lateinit var menuSectionDTO: List<RestaurantMenuSectionDTO>
}

//@Entity(primaryKeys = [])

/*
class RestaurantItemList {
    @Embedded
    lateinit var restaurantMenuSection: ArrayList<RestaurantMenuSection>

    @Relation(parentColumn = "menuSections", entityColumn = "list", entity = RestaurantItem::class)
    lateinit var imageContents: List<RestaurantItem>
}
*/

@Dao
interface RestaurantDao {

    @Query("SELECT * FROM RestaurantDTO")
    fun getLegoThemes(): LiveData<RestaurantDTO>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(plants: RestaurantDTO)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateOrInsert(user: RestaurantDTO)

    @Update
    fun updateUsers(vararg users: RestaurantDTO)

    @Query("SELECT * FROM RestaurantDTO")
    fun getRestaurant(): LiveData<RestaurantDTO>


    @Transaction
    @Query("SELECT * FROM RestaurantDTO ")
    fun getUsersAndLibraries(): LiveData<List<MenuSectionRelation>>

//    @Transaction
//    @Query("SELECT menuSections FROM RestaurantDTO WHERE ")
//    fun getFilterItem(): LiveData<List<MenuSectionRelation>>


//    @Query("SELECT menuSections FROM RestaurantDTO ")
//    fun getRestaurantSection(): LiveData<List<RestaurantMenuSection>?>

//    @Transaction
//    @Query("SELECT menuSections FROM RestaurantDTO where menuSections  ")
//    fun getSelectedItemList(): LiveData<ArrayList<RestaurantItem>?>

    @Query("DELETE FROM RestaurantDTO")
    fun removeRestaurantDTO()

}
@Dao
interface RestaurantItemDao {

    @Query("SELECT * FROM RestaurantItem ORDER BY name ASC")
    fun getSearchItemList(): LiveData<List<RestaurantItem>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(plants: List<RestaurantItem>)

    @Query("Select * from RestaurantItem where  name like  :query or description like :query")
    fun getSearchDishItems(query: String): LiveData<List<RestaurantItem>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(legoTheme: RestaurantItem)


    @Query("SELECT * FROM RestaurantItem WHERE itemId = :id")
    fun getRestaurantItem(id: String): LiveData<RestaurantItem>

    @Query("DELETE FROM RestaurantItem")
    suspend fun deleteAll()

}


@Dao
interface SelectedItemDao {

    @Query("SELECT * FROM ValuePairSI ORDER BY id DESC")
    fun getSelectedItem(): LiveData<List<ValuePairSI>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(plants: List<ValuePairSI>)


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertItem(legoTheme: ValuePairSI)


    @Query("SELECT * FROM ValuePairSI WHERE id = :itemId")
    fun getItemList(itemId: String): LiveData<ValuePairSI>

    @Query("DELETE FROM valuePairSI WHERE id=:itemId")
    fun removeItemById(itemId: String): Int


    @Delete
    suspend fun delete(legoTheme: ValuePairSI)

    @Query("DELETE FROM ValuePairSI")
    suspend fun deleteAll()


    @Query("SELECT * FROM ValuePairSI WHERE id = :itemId")
    fun getItemQuantityById(itemId: String): LiveData<ValuePairSI?>

//    @Query("DELETE From ")
//    fun deleteAll();
}

/*

@Dao
interface SelectedItemDao {

    @Query("SELECT * FROM RestaurantItem ORDER BY itemId DESC")
    fun getLegoThemes(): LiveData<List<RestaurantItem>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(plants: List<RestaurantItem>)

    @Query("Select * from RestaurantItem where name like  :query")
    fun getSearchResults(query : String) : LiveData<List<RestaurantItem>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(legoTheme: RestaurantItem)


    @Query("SELECT * FROM RestaurantItem WHERE itemId = :id")
    fun getLegoTheme(id: String): LiveData<RestaurantItem>

    @Delete
    suspend  fun  delete(legoTheme: RestaurantItem)

    @Query("DELETE FROM RestaurantItem")
    suspend fun deleteAll()
//    @Query("DELETE From ")
//    fun deleteAll();
}

*/
