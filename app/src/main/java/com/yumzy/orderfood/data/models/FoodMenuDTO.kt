package com.yumzy.orderfood.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Bhupendra Kumar Sahu.
 */
//class FoodMenuDTO {
//    @SerializedName("menuItems")
//    @Expose
//    var menuItems: List<MenuItem>? =
//        null
//
//    @SerializedName("addons")
//    @Expose
//    var addons: Addons? = null
//
//    @SerializedName("outlet")
//    @Expose
//    var outlet: Outlet? = null
//
//    inner class Addons { }
//
//    inner class MenuItem {
//        @SerializedName("type")
//        @Expose
//        var type: String = ""
//
//        @SerializedName("name")
//        @Expose
//        var name: String = ""
//
//        @SerializedName("offers")
//        @Expose
//        var offers: List<Any> = emptyList()
//
//        @SerializedName("containsEgg")
//        @Expose
//        var containsEgg: Boolean = false
//
//        @SerializedName("addons")
//        @Expose
//        var addons: List<Any> = emptyList()
//
//        @SerializedName("outletId")
//        @Expose
//        var outletId: String = ""
//
//        @SerializedName("ribbon")
//        @Expose
//        var ribbon: String = ""
//
//        @SerializedName("description")
//        @Expose
//        var description: String = ""
//
//        @SerializedName("itemId")
//        @Expose
//        var itemId: String = ""
//
//        @SerializedName("outletName")
//        @Expose
//        var outletName: String = ""
//
//        @SerializedName("unit")
//        @Expose
//        var unit: String = ""
//
//        @SerializedName("displayType")
//        @Expose
//        var displayType: String = ""
//
//        @SerializedName("isVeg")
//        @Expose
//        var isVeg: Boolean = false
//
//        @SerializedName("additionalCharges")
//        @Expose
//        var additionalCharges: List<AdditionalChargeDTO>? = null
//
//        @SerializedName("displayTags")
//        @Expose
//        var displayTags: List<Any> = emptyList()
//
//        @SerializedName("price")
//        @Expose
//        var price: Int = 0
//
//        @SerializedName("imageUrl")
//        @Expose
//        var imageUrl: String = ""
//
//        @SerializedName("portionSize")
//        @Expose
//        var portionSize: String = ""
//
//        @SerializedName("nextAvailable")
//        @Expose
//        var nextAvailable: String = ""
//
//        @SerializedName("displayImage")
//        @Expose
//        var displayImage: Boolean = false
//
//        @SerializedName("quantity")
//        @Expose
//        var quantity: Int = 0
//    }
//
//    inner class Outlet {
//        @SerializedName("_id")
//        @Expose
//        var id: String = ""
//
//        @SerializedName("outletId")
//        @Expose
//        var outletId: String = ""
//
//        @SerializedName("available")
//        @Expose
//        var available: String = ""
//
//        @SerializedName("costForTwo")
//        @Expose
//        var costForTwo: String = ""
//
//        @SerializedName("displayName")
//        @Expose
//        var displayName: String = ""
//
//        @SerializedName("facilities")
//        @Expose
//        var facilities: List<Any> = emptyList()
//
//        @SerializedName("gallery")
//        @Expose
//        var gallery: List<Any> = emptyList()
//
//        @SerializedName("locality")
//        @Expose
//        var locality: String = ""
//
//        @SerializedName("outletName")
//        @Expose
//        var outletName: String = ""
//
//        @SerializedName("reviews")
//        @Expose
//        var reviews: List<Any> = emptyList()
//
//        @SerializedName("cuisines")
//        @Expose
//        var cuisines: String = ""
//
//        @SerializedName("gstinType")
//        @Expose
//        var gstinType: String = ""
//
//        @SerializedName("gstPercent")
//        @Expose
//        var gstPercent: String = ""
//
//        @SerializedName("lic")
//        @Expose
//        var lic: String = ""
//
//        @SerializedName("imageUrl")
//        @Expose
//        var imageUrl: String = ""
//
//        @SerializedName("deliveryTime")
//        @Expose
//        var deliveryTime: Int = 0
//
//        @SerializedName("hygineRating")
//        @Expose
//        var hygineRating: String = ""
//
//        @SerializedName("items")
//        @Expose
//        var items: String = ""
//
//        @SerializedName("prepTime")
//        @Expose
//        var prepTime: Int = 0
//
//        @SerializedName("timeToOpen")
//        @Expose
//        var timeToOpen: Int = 0
//
//        @SerializedName("timeToClose")
//        @Expose
//        var timeToClose: Int = 0
//
//        @SerializedName("offers")
//        @Expose
//        var offers: String = ""
//
//        @SerializedName("favorite")
//        @Expose
//        var favorite: Boolean = false
//
//    }
//}