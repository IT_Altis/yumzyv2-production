package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName

data class HeroImageDTO(
    @field:SerializedName("url")   val url: String
)