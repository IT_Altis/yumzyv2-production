/*
 *   Created by Sourav Kumar Pandit  24 - 4 - 2020
 */

package com.yumzy.orderfood.data.models.base

interface APIConstant {
    interface Status {
        companion object {
            const val SUCCESS = 200
            const val IGNORE = -2000
            const val ERROR = 500
            const val SESSION_EXPIRED = 1009
            const val UNKNOWN = 503
            const val LOADING = -1
            const val NO_NETWORK = -404
            const val OFFLINE = -405
            const val FROM_ROOM = -406
        }
    }

    interface Code {
        companion object {
            const val DATA_UPDATED = 1000
            const val CART_VALIDATION_ERROR = 1001
            const val ADDRESS_NAME_SAME = 1002
            const val USER_NOT_FOUND = 1003
            const val USER_EXISTS = 1004
            const val LOGGED_OUT = 1006
            const val INVALID_USER = 1022
            const val NO_DELIVERY_PARTNER = 1023
            const val COUPON_FAILD = 1007
            const val COUPON_NOT_APPLICABLE = 1017
            const val UNKNOWN = 503
            const val SESSION_EXPIRE = 1009
            const val INVALID_CART_PAYLOAD = 1008
            const val ITEM_NOT_SERVING = 1010
            const val OUTLET_NOT_SERVING = 1011
            const val OFFER_INVALID = 1012
            const val OFFER_MIN_ORDER = 1013
            const val DELIVERY_PERSON_UNAVAILABLE = 1014
            const val LOCATION_UNSERVICEABLE = 1015
            const val NOT_SERVING = 1021
            const val NO_RESTAURANT_AT_LOCATION = 1020
            const val EMPTY_CART = 1016

            const val QUERY_DATABASE = -5008
            const val QUERY_FAILED = -5009
            const val CODE_NOT_FOUND = -5010
            const val INVALID_REFERRAL = 6000

        }
    }

    interface Actions {
        companion object {
            const val TASK_EXECUTED = 5001
            const val LOGIN = "login"
            const val SIGN_UP = "signup"
            const val VOICE = "voice"
            const val TEXT = "text"
        }

    }


    interface URL {
        companion object {
            const val LOGIN = "api/login"
            const val SEND_OTP = "account/sendOtp"
            const val VERIFY_OTP = "account/verifyOtp"
            const val EXPLORE_USER = "account/explore"
            const val APPLY_REFERRAL = "coupon/addReferral"
            const val LOGOUT = "account/logout"
            const val SIGN_OUT = "account/logout"
            const val RESEND_OTP = "account/sendOtp?method=resendOtp"
            const val SIGN_UP = "account/signup"
            const val UPDATE_LOCATION = "account/location"
            const val UPDATE_FCM = "account/fcm"
            const val WALLET_TRANSACTIONS = "account/wallet/account/ledger"
            const val WALLET_DETAIL = "account/wallet/account/wallet"
            const val USE_WALLET = "cart/use-wallet"
            const val UNUSE_WALLET = "cart/unuse-wallet"
            const val VALIDATE_VPA = "account/validate-vpa"
            const val WITHDRAW_INITIATE = "account/wallet/account/withdraw/initiate"
            const val SCRATCH_CARD_LIST = "account/scratchCard/viewCards"
            const val SCRATCH_CARD_INFO = "account/scratchCard/scratchInfo"
            const val REDEEM_SCRATCH = "account/scratchCard/redeem"
            const val SCRATCH_CARD_SUMMARY = "account/scratchCard/scratchSummary"


            //HomeFragment
            const val HOME_FOOD = "home/food"

            const val PROFILE = "account/profile"
            const val ADDRESS = "account/address"
            const val CONTACTS = "account/contacts/save"

            const val DISHES = "promotion/dishes"
            const val RESTAURANTS = "promotion/restaurants"

            const val FOOD_PREFERENCES = "food/foodPreferencesList"
            const val FOOD_PREFERENCES_TAG = "food/foodPreferencesTags"
            const val FOOD_MENU = "food/menu"


            //ExploreFragment
            const val SEARCH = "food/search"
            const val EXPAND_SEARCH = "food/expandSearch"

            //Orders
            const val ORDER_HISTORY = "order/orderHistory"
            const val ORDER_DETAILS = "order/orderDetails"
            const val RATE_ORDER = "order/rateOrder"

            const val HELP = "app/help"
            const val ORDER_STATUS = "order/orderStatus"
            const val CART = "cart"
            const val PRE_CHECKOUT = "order/preCheckout"
            const val APPLY_COUPON = "cart/coupon"
            const val COUPON_LIST = "coupon/couponList"
            const val NEW_ORDER = "order/newOrder"
            const val POST_CHECKOUT = "order/postCheckout"
            const val ADD_FAV = "account/favourites"
            const val GET_FAV = "account/favourites"
            const val ITEM_COUNT = "cart/item"
            const val CLEAR_CART = "cart/clear"
            const val REMOVE_COUPON = "cart/coupon"
            const val APP_START = "app/appStart"
        }

    }

}

/*
export const ErrorCodesMap: { [key: string]: number } = {
  SUCCESS: 200, // Standard success
  ERROR: 500, // standard error
  UNKNOWN: 503, // standard unknown
  // know error codes
  DATA_UPDATED: 1000,
  CART_VALIDATION_ERROR: 1001,
  ADDRESS_NAME_SAME: 1002,
  USER_NOT_FOUND: 1003,
  USER_EXISTS: 1004,
  LOGGED_OUT: 1006,
};

*/