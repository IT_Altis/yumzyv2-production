package com.yumzy.orderfood.data.models.orderdetails

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.yumzy.orderfood.data.models.PaymentDTO
import java.io.Serializable

/**
 * Created by Bhupendra Kumar Sahu on 03-Jun-20.
 */
@Entity(tableName = "orders")
data class NewOrderDetailsDTO
    (

    @Expose(serialize = false, deserialize = false)
    @NonNull
    @PrimaryKey(autoGenerate = false)
    @field:SerializedName("orderId") var orderId: String = "",
    @field:SerializedName("deliveryEmployee") var deliveryEmployee: String? = "",
    @field:SerializedName("orderNum") var orderNum: String? = "",
    @field:SerializedName("creationDate") var creationDate: String? = "",
    @field:SerializedName("outlets") var outlets: List<Outlet>,
    @field:SerializedName("rating") var rating: Float? = 0F,
    @field:SerializedName("rated") var rated: Boolean? = false,
    @field:SerializedName("specialInstructions") var specialInstructions: String? = "",
    @field:SerializedName("couponDiscount") var couponDiscount: Long? = 0L,
    @field:SerializedName("totalSavings") var totalSavings: Double? = 0.0,
    @field:SerializedName("orderStatus") var orderStatus: String? = "",
    @field:SerializedName("statusText") var statusText: String? = "",
    @field:SerializedName("statusDesc") var statusDesc: String? = "",
    @field:SerializedName("deliveryPartner") var deliveryPartner: String? = "",
    @field:SerializedName("loyaltyDiscount") var loyaltyDiscount: Double? = 0.0,
    @field:SerializedName("gstPercent") var gstPercent: Double? = 0.0,
    @field:SerializedName("paymentType") var paymentType: String? = "",
    @field:SerializedName("earnedScratch") var earnedScratch: Boolean=false,
    @field:SerializedName("payments") val payments: List<PaymentDTO>,

    ) {

    @Ignore
    @field:SerializedName("deliveryAddress")
    var deliveryAddress: DeliveryAddress? = null

    @Ignore
    @field:SerializedName("billing")
    var billing: XBilling? = null


}

data class XBilling(
    @field:SerializedName("laalsaCharge") var laalsaCharge: Double,
    @field:SerializedName("subTotal") var subTotal: Double,
    @field:SerializedName("charges") var charges: List<Charge>,
    @field:SerializedName("discount") var discount: Double,
    @field:SerializedName("roundOff") var roundOff: Double,
    @field:SerializedName("taxes") var taxes: List<Charge>,
    @field:SerializedName("loyaltyDiscount") var loyaltyDiscount: Double,
    @field:SerializedName("totalSavings") var totalSavings: Double,
    @field:SerializedName("totalAmount") var totalAmount: Double
)

data class Charge(
    @field:SerializedName("name") var name: String,
    @field:SerializedName("displayName") var displayName: String,
    @field:SerializedName("value") var value: Double
)

data class DeliveryAddress(
    @field:SerializedName("customerId") var customerId: String,
    @field:SerializedName("mobile") var mobile: String,
    @field:SerializedName("customerName") var customerName: String,
    @field:SerializedName("address") var address: Address,
    @field:SerializedName("loyaltyUsed") var loyaltyUsed: Long
)

data class Address(
    @field:SerializedName("name") var name: String,
    @field:SerializedName("landmark") var landmark: String,
    @field:SerializedName("addressId") var addressId: String,
    @field:SerializedName("addressTag") var addressTag: String,
    @field:SerializedName("googleLocation") var googleLocation: String,
    @field:SerializedName("houseNum") var houseNum: String,
    @field:SerializedName("fullAddress") var fullAddress: String,
    @field:SerializedName("city") var city: String,
    @field:SerializedName("state") var state: String,
    @field:SerializedName("pincode") var pincode: String,
    @field:SerializedName("country") var country: String,
    @field:SerializedName("longLat") var longLat: LongLat
) : Serializable {
    var isEdit = false
}

data class LongLat(
    @field:SerializedName("coordinates") var coordinates: List<Double>,
    @field:SerializedName("type") var type: String
)

data class Outlet(
    @field:SerializedName("outletLocation") var outletLocation: String,
    @field:SerializedName("specialInstructions") var specialInstructions: String,
    @field:SerializedName("outletName") var outletName: String,
    @field:SerializedName("outletId") var outletId: String,
    @field:SerializedName("orderNum") var orderNum: String,
    @field:SerializedName("items") var items: List<NewBillingItem>
)

data class NewBillingItem(
    @field:SerializedName("itemId") var itemId: String,
    @field:SerializedName("name") var name: String,
    @field:SerializedName("quantity") var quantity: Int,
    @field:SerializedName("price") var price: Double,
    @field:SerializedName("addons") var addons: String,
    @field:SerializedName("isVeg") var isVeg: Boolean
)