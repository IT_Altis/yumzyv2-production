package com.yumzy.orderfood.data.dao;

import com.yumzy.orderfood.data.models.CartItemDTO;

//@Dao
public interface CartDao {

    //    @Insert
    void insertMenuToCart(CartItemDTO cartItemDTO);
/*

    @Query("DELETE from cart_table WHERE itemCartId = :cartId")
    void deleteMenuFromCart(int cartId);

    @Query("UPDATE cart_table SET itemQuantity = :quantity,created_date = :date WHERE itemCartId = :cartId")
    void updateMenuQuantity(int cartId, int quantity, Date date);

    @Query("SELECT * FROM cart_table")
    LiveData<List<CartItem>> getAllCartItems();


    @Query("SELECT DISTINCT restaurantId FROM cart_table")
    String getRestaurantId();

    @Query("SELECT COUNT(*) FROM cart_table WHERE itemMenuId = :menuId")
    int getCartCount(String menuId);

    @Query("SELECT * FROM cart_table WHERE itemMenuId = :menuId")
    List<CartItem> getCartItembyMenuId(String menuId);

    @Query("SELECT * FROM cart_table WHERE itemMenuId= :menuId ORDER BY created_date DESC;")
    CartItem getLastCartItem(String menuId);

    @Query("UPDATE cart_table SET itemAvailable = :status WHERE itemCartId= :itemCartId ")
    void updateItemUnAvailable(int itemCartId,int status);

    @Query("DELETE FROM cart_table;")
    void clearCartTable();

*/

}
