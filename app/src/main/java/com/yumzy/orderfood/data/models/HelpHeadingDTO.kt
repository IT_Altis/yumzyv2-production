/*
 * Created By Shriom Tripathi 12 - 5 - 2020
 */

package com.yumzy.orderfood.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class HelpHeadingDTO {

    @SerializedName("headingId")
    @Expose
    var headingId: Int = 0

    @SerializedName("title")
    @Expose
    var title: String = ""

}