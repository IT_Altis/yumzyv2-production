package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName


data class DishesResDTO
    (
    @field:SerializedName("items")  val items: List<DishesItem>,
    @field:SerializedName("addons")  val addons: Map<String, RestaurantAddOnDTO>
)

data class DishesItem(
   @field:SerializedName("offers")   val offers: List<RestaurantItemOffer>?,
   @field:SerializedName("containsEgg")   val containsEgg: Boolean,
   @field:SerializedName("outletId")   val outletId: String,
   @field:SerializedName("ribbon")   var ribbon: String?,
   @field:SerializedName("description")   val description: String,
   @field:SerializedName("itemId")   val itemId: String,
   @field:SerializedName("outletName")   var outletName: String,
   @field:SerializedName("unit")   val unit: String,
   @field:SerializedName("displayType")   val displayType: String,
   @field:SerializedName("isVeg")   val isVeg: Boolean,
   @field:SerializedName("additionalCharges")   val additionalCharges: List<AdditionalCharge>,
   @field:SerializedName("displayTags")   val displayTags: List<Any?>,
   @field:SerializedName("price")   val price: Double,
   @field:SerializedName("imageUrl")   val imageUrl: String,
   @field:SerializedName("name")   val name: String,
   @field:SerializedName("portionSize")   val portionSize: Long,
   @field:SerializedName("prevQuantity")   var prevQuantity: Int = 0,
   @field:SerializedName("subLayoutType")   var subLayoutType: Int = 0
) {

}

data class AdditionalCharge(
   @field:SerializedName("name")  val name: String,
   @field:SerializedName("chargeType")  val chargeType: String,
   @field:SerializedName("value")  val value: Long
)




