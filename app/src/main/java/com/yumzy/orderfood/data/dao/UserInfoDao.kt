/*
 *   Created by Sourav Kumar Pandit  27 - 4 - 2020
 */

package com.yumzy.orderfood.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.yumzy.orderfood.data.models.UserDTO

@Dao
interface UserInfoDao {
    /* @Transaction
     open suspend fun updateData(users: List<UserDTO>) {
         removeUser()
         updateOrInsert(users)
     }*/

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateOrInsert(user: UserDTO)

    @Update
    fun updateUsers(vararg users: UserDTO)

    @Query("SELECT * FROM UserDTO")
    fun getUser(): LiveData<UserDTO>

    @Query("DELETE FROM UserDTO")
    fun removeUser()


}
