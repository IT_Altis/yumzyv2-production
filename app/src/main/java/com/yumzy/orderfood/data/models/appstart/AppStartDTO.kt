package com.yumzy.orderfood.data.models.appstart

import com.google.gson.annotations.SerializedName


data class AppStartDTO(
    @field:SerializedName("location") val location: Location,
    @field:SerializedName("infoBar") val infoBar: List<InfoBarDTO>
)

data class InfoBarDTO(
   @field:SerializedName("position") val position: Long,
   @field:SerializedName("icon") val icon: String,
   @field:SerializedName("landingPage") val landingPage: String,
   @field:SerializedName("isMandatory") val isMandatory: Boolean,
   @field:SerializedName("callbackUrl") val callbackUrl: String,
   @field:SerializedName("orderId") val orderId: String? = null,
   @field:SerializedName("status") val status: String?,
   @field:SerializedName("outletName") val outletName: String,
   @field:SerializedName("orderNum") val orderNum: String,
   @field:SerializedName("orderStatus") val orderStatus: String? = null,
   @field:SerializedName("text") val text: String? = null,
   @field:SerializedName("description") val description: String?,
   @field:SerializedName("override") val override: Boolean? = null,
   @field:SerializedName("subject") val subject: String? = null,
   @field:SerializedName("webviewUrl") val webviewUrl: String? = null
)

data class Location(
 @field:SerializedName("coordinates")   val coordinates: List<Double>,
 @field:SerializedName("city")   val city: String,
 @field:SerializedName("locality")   val locality: String
)

