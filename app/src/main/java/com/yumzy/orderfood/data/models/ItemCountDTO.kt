package com.yumzy.orderfood.data.models

import com.google.gson.annotations.SerializedName


data class ItemCountDTO(
    @field:SerializedName("outletId") val outletId: String?,
    @field:SerializedName("cartId")  val cartId: String
)