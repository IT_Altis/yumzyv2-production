package com.yumzy.orderfood.util

import com.google.firebase.crashlytics.FirebaseCrashlytics


fun getQueryParam(query: String, url: String): String {
    try {
        url.split("?")[1].split("&").forEach { params ->
            if (params.contains(query)) {
                return params.split("=")[1]
            }
        }
    } catch (e: java.lang.Exception) {
        FirebaseCrashlytics.getInstance()
            .setCustomKey("functionName", "getQueryParam($query, $url)")
        FirebaseCrashlytics.getInstance().setCustomKey(
            "notification_without_url",
            "Received $url as Destination in Notification"
        )
    }

    return ""
}

