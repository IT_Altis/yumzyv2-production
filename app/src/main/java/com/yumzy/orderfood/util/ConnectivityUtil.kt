package com.yumzy.orderfood.util

import com.laalsa.laalsalib.utilcode.util.NetworkUtils

object ConnectivityUtil {

    fun isConnected(): Boolean {
        return  NetworkUtils.isConnected()

    }
}