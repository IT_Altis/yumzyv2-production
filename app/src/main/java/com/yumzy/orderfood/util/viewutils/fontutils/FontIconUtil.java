package com.yumzy.orderfood.util.viewutils.fontutils;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yumzy.orderfood.ui.helper.ThemeConstant;

/*TODO fixme */
// FIXME: 22-05-2020 
public class FontIconUtil {
    public static final String ROOT = "fonts/";
    public static final String DEFAULT_FONT = "fonts/yumzy_Icon.ttf";
    public static final String NEW_FONT = "fonts/yumzy_Icon.ttf";

    private FontIconUtil() {
    }

    public static void markAsIconContainer(View v, Typeface typeface) {
        if (v instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) v;

            for (int i = 0; i < vg.getChildCount(); ++i) {
                View child = vg.getChildAt(i);
                markAsIconContainer(child, typeface);
            }
        } else if (v instanceof TextView) {
            ((TextView) v).setTypeface(typeface);
        }

    }

    public static Typeface getTypeface(Context context, String font) {
        return Typeface.createFromAsset(context.getAssets(), font);
    }

    public static Drawable getFontDrawable(Context context, int size, String sResIconId, int iColor) {
        IconFontDrawable faIcon = new IconFontDrawable(context);
        faIcon.setTextSize(1, (float) size);
        faIcon.setTextAlign(Layout.Alignment.ALIGN_OPPOSITE);
        faIcon.setTextColor(iColor);
        faIcon.setTypeface(getTypeface(context, "fonts/yumzy_Icon.ttf"));
        faIcon.setText(sResIconId);
        return faIcon;
    }

    public static Drawable getFontDrawable2(Context context, int size, String sResIconId, int iColor) {
        IconFontDrawable faIcon = new IconFontDrawable(context);
        faIcon.setTextSize(1, (float) size);
        faIcon.setTextAlign(Layout.Alignment.ALIGN_OPPOSITE);
        faIcon.setTextColor(iColor);
        faIcon.setTypeface(getTypeface(context, "fonts/yumzy_Icon.ttf"));
        faIcon.setText(sResIconId);
        return faIcon;
    }

    public static Drawable getFontDrawable(Context context, int size, String sResIconId) {
        return getFontDrawable(context, size, sResIconId, ThemeConstant.darkBlue);
    }
}
