package com.yumzy.orderfood.util.maputils

import android.os.Parcel
import android.os.Parcelable
import com.google.android.gms.maps.model.*
import com.google.maps.android.PolyUtil
import com.google.maps.android.SphericalUtil
import java.util.*


/**
 * Created by tamhuynh on 9/5/17.
 *
 */
/*
class Route : Parcelable {
    var latLngBounds: LatLngBounds?
        private set
    var durationText: String?
        private set
    var durationValue: Int
        private set
    var distanceText: String?
        private set
    var distanceValue: Int
        private set
    var points: List<LatLng>?
        private set
    var patterns: List<PatternItem>?
        private set

    private constructor(builder: Builder) {
        latLngBounds = builder.mLatLngBounds
        durationText = builder.mDurationText
        durationValue = builder.mDurationValue
        distanceText = builder.mDistanceText
        distanceValue = builder.mDistanceValue
        points = builder.mPoints
        patterns = builder.mPatterns
    }

    private constructor(`in`: Parcel) {
        latLngBounds = `in`.readParcelable(LatLngBounds::class.java.classLoader)
        durationText = `in`.readString()
        durationValue = `in`.readInt()
        distanceText = `in`.readString()
        distanceValue = `in`.readInt()
        points = `in`.createTypedArrayList(LatLng.CREATOR)
        patterns = `in`.createTypedArrayList(PatternItem.CREATOR)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeParcelable(latLngBounds, flags)
        dest.writeString(durationText)
        dest.writeInt(durationValue)
        dest.writeString(distanceText)
        dest.writeInt(distanceValue)
        dest.writeTypedList(points)
        dest.writeTypedList(patterns)
    }

    class Builder {
        var mLatLngBounds: LatLngBounds? = null
        var mDurationText = LONG_ROUTE_DEFAULT_TEXT
        var mDurationValue = LONG_ROUTE_DEFAULT_VALUE
        var mDistanceText = LONG_ROUTE_DEFAULT_TEXT
        var mDistanceValue = LONG_ROUTE_DEFAULT_VALUE
        var mPoints: List<LatLng>? = null
        var mPatterns: List<PatternItem>? = null
        fun setLatLngBounds(vararg points: LatLng?): Builder {
            val builder = LatLngBounds.Builder()
            for (point in points) builder.include(point)
            mLatLngBounds = builder.build()
            return this
        }

        fun setDurationText(durationText: String): Builder {
            mDurationText = durationText
            return this
        }

        fun setDurationValue(durationValue: Int): Builder {
            mDurationValue = durationValue
            return this
        }

        fun setDistanceText(distanceText: String): Builder {
            mDistanceText = distanceText
            return this
        }

        fun setDistanceValue(distanceValue: Int): Builder {
            mDistanceValue = distanceValue
            return this
        }

        fun setPoints(encodedPolyline: String?): Builder {
            mPoints = PolyUtil.decode(encodedPolyline)
            return this
        }

        fun setPoints(points: List<LatLng>?): Builder {
            mPoints = points
            return this
        }

        fun setPoints(vararg points: LatLng?): Builder {
            mPoints = points.filter { latLng -> latLng != null }.toList()
            return this
        }

        fun setPatterns(patterns: List<PatternItem>?): Builder {
            mPatterns = patterns
            return this
        }

        fun build(): Route {
            return Route(this)
        }
    }

    companion object {
        const val LONG_ROUTE_DEFAULT_TEXT = "long_route"
        const val LONG_ROUTE_DEFAULT_VALUE = -1
        private const val LONG_ROUTING_DISTANCE_LIMIT = 1000000
        private const val DEFAULT_CURVE_ROUTE_CURVATURE = 0.3
        private const val DEFAULT_CURVE_POINTS = 60
        private val CURVE_ROUTE_PATTERNS =
            listOf(Dash(30F), Gap(20F))

      */
/*  fun createFromDirectionResponse(response: DirectionApiResponse): Route? {
            val route: DirectionApiResponse.RouteResponse = response.getFirstRoute() ?: return null
            val leg: DirectionApiResponse.Leg = route.getFirstLeg() ?: return null
            return Builder()
                .setDistanceText(leg.mDistance.getText())
                .setDistanceValue(leg.mDistance.getValue())
                .setDurationText(leg.mDuration.getText())
                .setDurationValue(leg.mDuration.getValue())
                .setLatLngBounds(route.getBounds())
                .setPoints(route.getPolyline())
                .build()
        }*//*


        fun createLongDistanceRoute(
            origin: LatLng,
            dest: LatLng
        ): Route {
            val distance =
                SphericalUtil.computeDistanceBetween(origin, dest)
            return if (distance <= LONG_ROUTING_DISTANCE_LIMIT) createCurveRoute(
                origin,
                dest,
                distance
            ) else createCrowFlightRoute(origin, dest, distance)
        }

        */
/**
         * Create a curve route between origin and dest
         *//*

        private fun createCurveRoute(
            origin: LatLng,
            dest: LatLng,
            distance: Double
        ): Route {
            var distance = distance
            if (distance == 0.0) distance =
                SphericalUtil.computeDistanceBetween(origin, dest)
            val heading =
                SphericalUtil.computeHeading(origin, dest)
            val halfDistance = distance / 2

            // Calculate midpoint position
            val midPoint =
                SphericalUtil.computeOffset(origin, halfDistance, heading)

            // Calculate position of the curve center point
            val sqrCurvature =
                DEFAULT_CURVE_ROUTE_CURVATURE * DEFAULT_CURVE_ROUTE_CURVATURE
            val extraParam =
                distance / (4 * DEFAULT_CURVE_ROUTE_CURVATURE)
            val midPerpendicularLength = (1 - sqrCurvature) * extraParam
            val r = (1 + sqrCurvature) * extraParam
            val circleCenterPoint =
                SphericalUtil.computeOffset(
                    midPoint,
                    midPerpendicularLength,
                    heading + 90.0
                )

            // Calculate heading between circle center and two points
            val headingToOrigin =
                SphericalUtil.computeHeading(circleCenterPoint, origin)

            // Calculate positions of points on the curve
            val step =
                Math.toDegrees(Math.atan(halfDistance / midPerpendicularLength)) * 2 / DEFAULT_CURVE_POINTS
            val points: MutableList<LatLng> =
                ArrayList()
            for (i in 0 until DEFAULT_CURVE_POINTS) {
                points.add(
                    SphericalUtil.computeOffset(
                        circleCenterPoint,
                        r,
                        headingToOrigin + i * step
                    )
                )
            }
            return Builder()
                .setLatLngBounds(origin, dest)
                .setPoints(points)
                .setDistanceText(
                    String.format(
                        Locale.getDefault(),
                        "> %.2f km",
                        distance / 1000
                    )
                )
                .setDistanceValue(distance.toInt())
                .setPatterns(CURVE_ROUTE_PATTERNS)
                .build()
        }

        */
/**
         * Create crowflight route
         *//*

        private fun createCrowFlightRoute(
            origin: LatLng,
            dest: LatLng,
            distance: Double
        ): Route {
            var distance = distance
            if (distance == 0.0) distance =
                SphericalUtil.computeDistanceBetween(origin, dest)
            return Builder()
                .setLatLngBounds(origin, dest)
                .setPoints(origin, dest)
                .setDistanceText(
                    String.format(
                        Locale.getDefault(),
                        "> %.2f km",
                        distance / 1000
                    )
                )
                .setDistanceValue(distance.toInt())
                .setPatterns(CURVE_ROUTE_PATTERNS)
                .build()
        }

        val CREATOR: Parcelable.Creator<Route> = object : Parcelable.Creator<Route?> {
            override fun createFromParcel(source: Parcel): Route? {
                return Route(source)
            }

            override fun newArray(size: Int): Array<Route?> {
                return arrayOfNulls(size)
            }
        }
    }
}
*/
