package com.yumzy.orderfood.util.viewutils.fontutils;

import android.widget.TextView;

/**
 * Created by Bhupendra Kumar Sahu.
 */
public class SymbolExtentionUtils {

    public static void setExtensionRupee(TextView tv, String data) {
        tv.setText("₹" + data);
    }

    public static void setExtensionHash(TextView tv, String data) {
        tv.setText("Order #" + data);
    }

}
