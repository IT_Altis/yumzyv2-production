package com.yumzy.orderfood.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.AdapterSearchViewBinding
import com.yumzy.orderfood.ui.common.search.SimpleSearchView
import com.yumzy.orderfood.ui.common.search.utils.OutletSearchView
import com.yumzy.orderfood.ui.helper.hideKeyboard

/**
 * Created by Shriom Tripathi on 11-Sept-20.
 */
class SearchBarItem : AbstractBindingItem<AdapterSearchViewBinding>() {

    private var listener: OutletSearchView.OnQueryTextListener? = null

    private var hint: String = ""

    override val type: Int
        get() = R.id.item_search_bar

    override fun bindView(binding: AdapterSearchViewBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)

        binding.searchView.hideKeyboard()
        binding.searchView.setHint(hint)
        binding.searchView.showSearch(false)
        binding.searchView.setOnQueryTextListener(listener)
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterSearchViewBinding {
        return AdapterSearchViewBinding.inflate(inflater, parent, false)
    }

    fun withHint(hint: String): SearchBarItem {
        this.hint = hint
        return this
    }

    fun withListener(listener: OutletSearchView.OnQueryTextListener): SearchBarItem {
        this.listener = listener
        return this
    }


}