package com.yumzy.orderfood.util.dateformater

import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat

class UTCtoNormalDate {
    companion object {
        fun getDate(date: String): String? {
            val DATE_FORMAT = DateTimeFormat.forPattern("dd MMMM, yyyy")
            return DATE_FORMAT.print(DateTime.parse(date.substring(0, 10)))
        }

        fun isYesterday(date: String): Boolean? {
            return DateTime.parse(date.substring(0, 10))
                .isEqual(DateTime.parse(LocalDate.now().minusDays(1).toString()))
        }

        fun isToday(date: String): Boolean? {
            return DateTime.parse(date.substring(0, 10))
                .isEqual(DateTime.parse(LocalDate.now().toString()))
        }

        fun getDateTime(date: String): String? {
            val DATE_FORMAT = DateTimeFormat.forPattern("dd MMMM, yyyy HH:mm a").withZone(
                DateTimeZone.getDefault()
            )
            return DATE_FORMAT.print(DateTime.parse(date))

        }

        fun getDateMillis(date: String): Long {
            return DateTime.parse(date.substring(0, 10)).millis
        }

    }
}

