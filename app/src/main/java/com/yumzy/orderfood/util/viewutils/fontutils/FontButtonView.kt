package com.yumzy.orderfood.util.viewutils.fontutils

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import androidx.appcompat.widget.AppCompatButton
import androidx.core.widget.TextViewCompat
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsaui.drawable.DrawableUtils

class FontButtonView : AppCompatButton {
    private var isBrandingIcon = false
    private var isSolidIcon = false

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet) : this(context, attrs, 0)
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyle: Int = 0
    ) : super(context, attrs, defStyle) {
        initAttr(context, attrs)
        init()
    }


    private fun initAttr(context: Context, attrs: AttributeSet?) {
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(
            this,
            12,
            1000,
            1,
            TypedValue.COMPLEX_UNIT_SP
        )
        val a = context.theme.obtainStyledAttributes(
            attrs, R.styleable.FontButtonView,
            0, 0
        )
        this.gravity = Gravity.CENTER
        isSolidIcon = a.getBoolean(R.styleable.FontButtonView_buttonSolidIcon, false)
        isBrandingIcon = a.getBoolean(R.styleable.FontButtonView_buttonBrandIcon, false)
        val buttonText = a.getString(R.styleable.FontButtonView_buttonIcon) ?: ""
        val buttonRounded = a.getDimension(R.styleable.FontButtonView_buttonRounded,VUtil.dpToPx(12).toFloat())
        a.recycle()
        this.setTextColor(Color.WHITE)
        this.minimumWidth = 0
        this.minimumHeight = 0
        this.minWidth = 0
        this.minHeight = 0
        this.text = buttonText

        this.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
        this.background = DrawableUtils.getRoundDrawableListState(
            ThemeConstant.buttonColor,
            buttonRounded,
            ThemeConstant.buttonColorDark,
            buttonRounded
        )

    }


    private fun init() {
        typeface =
            when {
                isBrandingIcon -> FontCache[context, "fonts/yumzy_Icon.ttf"]
                isSolidIcon -> FontCache[context, "fonts/yumzy_Icon.ttf"]
                else -> FontCache[context, "fonts/yumzy_Icon.ttf"]
            }
    }

    override fun setTextSize(unit: Int, size: Float) {
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(
            this,
            size.toInt(),
            1000,
            1,
            unit
        )
        super.setTextSize(unit, size)

    }
}