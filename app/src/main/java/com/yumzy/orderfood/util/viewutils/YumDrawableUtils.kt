package com.yumzy.orderfood.util.viewutils

import android.R
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.*
import android.graphics.drawable.shapes.RoundRectShape
import android.os.Build
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import com.yumzy.orderfood.util.IConstants
import com.laalsa.laalsalib.ui.VUtil
import java.util.*


object YumDrawableUtils {
    const val BORDER_TOP: Byte = 0
    const val BORDER_BOTTOM: Byte = 1
    const val BORDER_LEFT: Byte = 2
    const val BORDER_RIGHT: Byte = 3

    const val TOP_TO_BOTTOM: Byte = 0
    const val BOTTOM_TO_TOP: Byte = 1
    const val LEFT_TO_RIGHT: Byte = 2
    const val RIGHT_TO_LEFT: Byte = 3

    fun getVectorDrawable(
        clContext: Context,
        drawableResId: Int,
        colorFilter: Int
    ): Drawable? {
        val drawable: Drawable? = VectorDrawableCompat.create(
            clContext.resources,
            drawableResId,
            clContext.theme
        )
        drawable?.setTint(colorFilter)
        return drawable
    }

    fun getNormalDrawable(
        clContext: Context,
        drawableResId: Int,
        colorFilter: Int
    ): Drawable? {
        var drawable: Drawable? = clContext.resources.getDrawable(drawableResId, clContext.theme)
        drawable?.setTint(colorFilter)
        return drawable
    }

    fun getSelectableDrawableFor(color: Int): Drawable {
        return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            val stateListDrawable =
                StateListDrawable()
            stateListDrawable.addState(
                intArrayOf(R.attr.state_pressed),
                ColorDrawable(lightenOrDarken(color, 0.20))
            )
            stateListDrawable.addState(
                intArrayOf(R.attr.state_focused),
                ColorDrawable(lightenOrDarken(color, 0.40))
            )
            stateListDrawable.addState(
                intArrayOf(),
                ColorDrawable(color)
            )
            stateListDrawable
        } else {
            val pressedColor = ColorStateList.valueOf(lightenOrDarken(color, 0.2))
            val defaultColor = ColorDrawable(color)
            val rippleColor = getRippleColor(color)
            RippleDrawable(
                pressedColor,
                defaultColor,
                rippleColor
            )
        }
    }

    private fun getRippleColor(color: Int): Drawable {
        val outerRadii = FloatArray(8)
        Arrays.fill(outerRadii, 3f)
        val r = RoundRectShape(outerRadii, null, null)
        val shapeDrawable = ShapeDrawable(r)
        shapeDrawable.paint.color = color
        return shapeDrawable
    }

    private fun lightenOrDarken(color: Int, fraction: Double): Int {
        return if (canLighten(color, fraction)) {
            lighten(color, fraction)
        } else {
            darken(color, fraction)
        }
    }

    private fun lighten(color: Int, fraction: Double): Int {
        var red: Int = Color.red(color)
        var green: Int = Color.green(color)
        var blue: Int = Color.blue(color)
        red = lightenColor(red, fraction)
        green = lightenColor(green, fraction)
        blue = lightenColor(blue, fraction)
        val alpha: Int = Color.alpha(color)
        return Color.argb(alpha, red, green, blue)
    }

    private fun darken(color: Int, fraction: Double): Int {
        var red: Int = Color.red(color)
        var green: Int = Color.green(color)
        var blue: Int = Color.blue(color)
        red = darkenColor(red, fraction)
        green = darkenColor(green, fraction)
        blue = darkenColor(blue, fraction)
        val alpha: Int = Color.alpha(color)
        return Color.argb(alpha, red, green, blue)
    }

    private fun canLighten(color: Int, fraction: Double): Boolean {
        val red: Int = Color.red(color)
        val green: Int = Color.green(color)
        val blue: Int = Color.blue(color)
        return (canLightenComponent(red, fraction)
                && canLightenComponent(green, fraction)
                && canLightenComponent(blue, fraction))
    }

    private fun canLightenComponent(
        colorComponent: Int,
        fraction: Double
    ): Boolean {
        val red: Int = Color.red(colorComponent)
        val green: Int = Color.green(colorComponent)
        val blue: Int = Color.blue(colorComponent)
        return red + red * fraction < 255 && green + green * fraction < 255 && blue + blue * fraction < 255
    }

    private fun darkenColor(color: Int, fraction: Double): Int {
        return Math.max(color - color * fraction, 0.0).toInt()
    }

    private fun lightenColor(color: Int, fraction: Double): Int {
        return Math.min(color + color * fraction, 255.0).toInt()
    }


    fun getRPLEffectPreLOP(
        iSelectedColor: Int,
        iNormalColor: Int
    ): StateListDrawable? {
        val res =
            StateListDrawable()
        res.addState(intArrayOf(R.attr.state_pressed), ColorDrawable(iSelectedColor))
        res.addState(intArrayOf(R.attr.state_selected), ColorDrawable(iSelectedColor))
        res.addState(intArrayOf(R.attr.state_activated), ColorDrawable(iSelectedColor))
        res.addState(intArrayOf(), ColorDrawable(iNormalColor))
        return res
    }


    fun getRPLEffectPreLOP1(
        iSelectedColor: Int,
        iNormalColor: Int
    ): StateListDrawable? {
        val res =
            StateListDrawable()
        res.addState(intArrayOf(R.attr.state_pressed), ColorDrawable(iSelectedColor))
        res.addState(intArrayOf(R.attr.state_checked), ColorDrawable(iSelectedColor))
        res.addState(intArrayOf(), ColorDrawable(iNormalColor))
        return res
    }

    fun getSelectorDrawable(color: Int): StateListDrawable? {
        val res =
            StateListDrawable()
        res.addState(intArrayOf(R.attr.state_pressed), ColorDrawable(color))
        res.addState(intArrayOf(R.attr.state_selected), ColorDrawable(color))
        res.addState(intArrayOf(R.attr.state_activated), ColorDrawable(color))
        res.addState(intArrayOf(), ColorDrawable(Color.TRANSPARENT))
        return res
    }

    fun getLinearDrawable(
        iBackgroundColor: Int,
        iLayerColor: Int,
        iLayerType: Byte
    ): Drawable? {
        var iOrientation: GradientDrawable.Orientation? = null
        iOrientation =
            if (iLayerType == TOP_TO_BOTTOM) GradientDrawable.Orientation.TOP_BOTTOM else if (iLayerType == BOTTOM_TO_TOP) GradientDrawable.Orientation.BOTTOM_TOP else if (iLayerType == LEFT_TO_RIGHT) GradientDrawable.Orientation.LEFT_RIGHT else if (iLayerType == RIGHT_TO_LEFT) GradientDrawable.Orientation.RIGHT_LEFT else null
        var clBackground: GradientDrawable? = null
        if (iOrientation != null) {
            clBackground = GradientDrawable(
                iOrientation,
                intArrayOf(iBackgroundColor, iBackgroundColor, iLayerColor)
            ) //new int[]{startColor, centerColor, endColor}
            clBackground.setGradientCenter(0.5f, 0.5f)
            clBackground.shape = GradientDrawable.RECTANGLE
        }
        return clBackground
    }

    fun getLinearDrawableBackground(
        iBackgroundColor: Int,
        iLayerColor: Int,
        iLayerType: Byte
    ): Drawable? {
        var iOrientation: GradientDrawable.Orientation? = null
        iOrientation =
            if (iLayerType == TOP_TO_BOTTOM) GradientDrawable.Orientation.TOP_BOTTOM else if (iLayerType == BOTTOM_TO_TOP) GradientDrawable.Orientation.BOTTOM_TOP else if (iLayerType == LEFT_TO_RIGHT) GradientDrawable.Orientation.LEFT_RIGHT else if (iLayerType == RIGHT_TO_LEFT) GradientDrawable.Orientation.RIGHT_LEFT else null
        var clBackground: GradientDrawable? = null
        if (iOrientation != null) {
            clBackground = GradientDrawable(
                iOrientation,
                intArrayOf(iBackgroundColor, iBackgroundColor, iLayerColor)
            ) //new int[]{startColor, centerColor, endColor}
            clBackground.setGradientCenter(0.5f, 0.5f)
            clBackground.shape = GradientDrawable.RECTANGLE
        }
        return clBackground
    }
    fun getLayerListDrawable(
        clContext: Context?,
        iBackgroundColor: Int,
        iLayerColor: Int,
        iLayerType: Int,
        iLayerHeight: Int
    ): Drawable? {
        var iLayerHeight = iLayerHeight
        if (iLayerHeight <= 0) iLayerHeight = VUtil.dpToPx(10)
        val clBackground = GradientDrawable(
            GradientDrawable.Orientation.TOP_BOTTOM,
            intArrayOf(iBackgroundColor, iLayerColor)
        )
        clBackground.shape = GradientDrawable.RECTANGLE
        clBackground.setColor(iBackgroundColor)
        val clBackgroundLayer = GradientDrawable()
        clBackgroundLayer.shape = GradientDrawable.RECTANGLE
        clBackgroundLayer.setColor(iLayerColor)
        val clLayerList = arrayOf<Drawable>(clBackgroundLayer, clBackground)
        val clLinearDrawable = LayerDrawable(clLayerList)

        //setLayerInset(layer, leftOffset, topOffset, rightOffset, bottomOffset)
        clLinearDrawable.setLayerInset(0, 0, 0, 0, 0)
        clLinearDrawable.setLayerInset(
            1,
            if (iLayerType == BORDER_LEFT.toInt()) iLayerHeight else 0,
            if (iLayerType == BORDER_TOP.toInt()) iLayerHeight else 0,
            if (iLayerType == BORDER_RIGHT.toInt()) iLayerHeight else 0,
            if (iLayerType == BORDER_BOTTOM.toInt()) iLayerHeight else 0
        )
        return clLinearDrawable
    }

    fun getReportBorderDrawable(
        clContext: Context?,
        iBackgroundColor: Int,
        iLayerColor: Int,
        iLayerType: Int,
        iLayerHeight: Int
    ): Drawable? {
        var iLayerHeight = iLayerHeight
        if (iLayerHeight <= 0) iLayerHeight = VUtil.dpToPx(10)
        val clBackground = GradientDrawable()
        clBackground.shape = GradientDrawable.RECTANGLE
        clBackground.setColor(iBackgroundColor)
        val clBackgroundLayer = GradientDrawable()
        clBackgroundLayer.shape = GradientDrawable.RECTANGLE
        clBackgroundLayer.setColor(iLayerColor)
        val clLayerList = arrayOf<Drawable>(clBackgroundLayer, clBackground)
        val clLinearDrawable = LayerDrawable(clLayerList)
        when (iLayerType) {
            IConstants.IBorderType.LEFT, IConstants.IBorderType.TOP, IConstants.IBorderType.RIGHT, IConstants.IBorderType.BOTTOM -> clLinearDrawable.setLayerInset(
                1,
                if (iLayerType == IConstants.IBorderType.LEFT) iLayerHeight else 0,
                if (iLayerType == IConstants.IBorderType.TOP) iLayerHeight else 0,
                if (iLayerType == IConstants.IBorderType.RIGHT) iLayerHeight else 0,
                if (iLayerType == IConstants.IBorderType.BOTTOM) iLayerHeight else 0
            )
            IConstants.IBorderType.LEFT_RIGHT -> clLinearDrawable.setLayerInset(
                1,
                iLayerHeight,
                0,
                iLayerHeight,
                0
            )
            IConstants.IBorderType.TOP_BOTTOM -> clLinearDrawable.setLayerInset(
                1,
                0,
                iLayerHeight,
                0,
                iLayerHeight
            )
        }
        return clLinearDrawable
    }


    fun getLayerListDrawable(
        clContext: Context?,
        iBackgroundColor: Int,
        clColorStateList: ColorStateList,
        iLayerType: Int,
        iBorderWidth: Int
    ): Drawable? {
        var iBorderWidth = iBorderWidth
        if (iBorderWidth < 0) iBorderWidth = VUtil.dpToPx(5)
        val clBackground = GradientDrawable(
            GradientDrawable.Orientation.TOP_BOTTOM,
            intArrayOf(iBackgroundColor, clColorStateList.defaultColor)
        )
        clBackground.shape = GradientDrawable.RECTANGLE
        clBackground.setColor(iBackgroundColor)
        val clBackgroundLayer = GradientDrawable()
        clBackgroundLayer.shape = GradientDrawable.RECTANGLE
        clBackgroundLayer.color = clColorStateList
        val clLayerList = arrayOf<Drawable>(clBackgroundLayer, clBackground)
        val clLinearDrawable = LayerDrawable(clLayerList)

        //setLayerInset(layer, leftOffset, topOffset, rightOffset, bottomOffset)
        clLinearDrawable.setLayerInset(0, 0, 0, 0, 0)
        clLinearDrawable.setLayerInset(
            1,
            if (iLayerType == BORDER_LEFT.toInt()) iBorderWidth else 0,
            if (iLayerType == BORDER_TOP.toInt()) iBorderWidth else 0,
            if (iLayerType == BORDER_RIGHT.toInt()) iBorderWidth else 0,
            if (iLayerType == BORDER_BOTTOM.toInt()) iBorderWidth else 0
        )
        return clLinearDrawable
    }

}