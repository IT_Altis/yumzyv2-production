package com.yumzy.orderfood.util.viewutils.fontutils

import android.content.Context
import android.graphics.Typeface
import java.util.*

object FontCache {
    const val FA_FONT_REGULAR = "fonts/yumzy_Icon.ttf"
    const val FA_FONT_SOLID = "fonts/yumzy_Icon.ttf"
    const val FA_FONT_BRANDS = "fonts/yumzy_Icon.ttf"
    private val fontCache =
        Hashtable<String, Typeface?>()

    @JvmStatic
    operator fun get(context: Context, name: String): Typeface? {
        var typeface = fontCache[name]
        if (typeface == null) {
            typeface = try {
                Typeface.createFromAsset(context.assets, name)
            } catch (e: Exception) {
                return null
            }
            fontCache[name] = typeface
        }
        return typeface
    }
}