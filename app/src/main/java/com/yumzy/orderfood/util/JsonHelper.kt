package com.yumzy.orderfood.util

import java.lang.reflect.Method

object JsonHelper {
    fun arrayToString(cuisines: List<String?>?): String {
        if (cuisines == null) {
            return ""
        }
        val value: String = cuisines.toString()

        if (value.length >= 2) {
            return value.substring(1, value.length - 1)
        }

        return value
    }

    fun dataToMap(obj: Any): Map<String, Any> {
        val hashMap: MutableMap<String, Any> =
            HashMap()
        try {
            val c: Class<out Any> = obj.javaClass
            val m: Array<Method> = c.methods
            for (i in m.indices) {
                if (m[i].name.indexOf("get") === 0) {
                    val name: String =
                        m[i].name.toLowerCase().substring(3, 4) + m[i].name
                            .substring(4)
                    hashMap[name] = m[i].invoke(obj, arrayOfNulls<Any>(0))
                }
            }
        } catch (e: Throwable) {
            //log error
        }
        return hashMap
    }
}