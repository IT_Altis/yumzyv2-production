package com.yumzy.orderfood.worker

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.Status
import com.yumzy.orderfood.ui.screens.login.data.SmsListener
import java.util.regex.Pattern

class SMSBroadCastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent) {
        if (SmsRetriever.SMS_RETRIEVED_ACTION == intent.action) {
            val extras = intent.extras
            if (extras != null) {
                val status =
                    (extras[SmsRetriever.EXTRA_STATUS] as Status?)!!
                when (status.statusCode) {
                    CommonStatusCodes.SUCCESS -> {
                        // Get SMS message contents
                        val message =
                            extras[SmsRetriever.EXTRA_SMS_MESSAGE] as String?
                        val pattern = Pattern.compile("\\d{4}")
                        val matcher = pattern.matcher(message)
                        if (matcher.find()) {
                            val messageMatch = matcher.group(0)
                            //phone.setText(messageMatch);
                            //Pass on the text to our listener.
                            if (mListener != null) mListener!!.messageReceived(
                                messageMatch
                            )
                        }
                    }
                    CommonStatusCodes.TIMEOUT -> {
                    }
                }
            }
        }
    }

    fun bindListener(listener: SmsListener?) {
        mListener = listener
    }

    fun unbindListener() {
        if (mListener != null) mListener = null
    }

    private var mListener: SmsListener? = null


}
