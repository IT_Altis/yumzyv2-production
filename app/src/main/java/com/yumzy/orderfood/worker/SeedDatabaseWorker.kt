package com.yumzy.orderfood.worker

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.google.gson.stream.JsonReader
import com.yumzy.orderfood.data.AppDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext
import timber.log.Timber

class SeedDatabaseWorker(
    context: Context,
    workerParams: WorkerParameters
) : CoroutineWorker(context, workerParams) {

    override suspend fun doWork(): Result = coroutineScope {
        withContext(Dispatchers.IO) {

            try {
                applicationContext.assets.open(AppDatabase.DATABASE_NAME).use { inputStream ->
                    JsonReader(inputStream.reader()).use { _ ->
                        /*todo :seeding data from json file*/
//                        val type = object : TypeToken<List<LegoSet>>() {}.type
//                        val list: List<LegoSet> = appGson.fromJson(jsonReader, type)
//                        AppDatabase.getInstance(applicationContext).legoSetDao().insertAll(list)

                        Result.success()
                    }
                }
            } catch (e: Exception) {
                Timber.e(e, "Error seeding database")
                Result.failure()
            }
        }
    }
}