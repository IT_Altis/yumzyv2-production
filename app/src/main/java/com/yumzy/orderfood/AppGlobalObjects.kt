package com.yumzy.orderfood
import android.annotation.SuppressLint
import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.clevertap.android.sdk.CleverTapAPI
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import com.google.gson.Gson
import com.laalsa.laalsalib.utilcode.util.Utils
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.tools.notification.YumzyFirebaseMessaging


lateinit var appAnalytics: FirebaseAnalytics
lateinit var appRemoteConfig: FirebaseRemoteConfig
lateinit var appCrashlytics: FirebaseCrashlytics
lateinit var appCleverTap: CleverTapAPI
lateinit var appGson: Gson

@SuppressLint("MissingPermission")
class AppGlobalObjects(private val context: Context) {
    init {
        val application = context as Application
        appAnalytics = FirebaseAnalytics.getInstance(context)
        appRemoteConfig = Firebase.remoteConfig
        val configSettings = remoteConfigSettings {
            minimumFetchIntervalInSeconds = 3600
        }
        appRemoteConfig.setConfigSettingsAsync(configSettings)
        appCrashlytics = FirebaseCrashlytics.getInstance()
        appCrashlytics.setCrashlyticsCollectionEnabled(true)

        if (BuildConfig.DEBUG)
            CleverTapAPI.setDebugLevel(CleverTapAPI.LogLevel.DEBUG)
        else
            CleverTapAPI.setDebugLevel(CleverTapAPI.LogLevel.OFF)
        Utils.init(application)
        createNotificationChannel()

        /*gson object use for explicit Convert type into DTO*/
        appGson = Gson()

    }

    companion object {

        private val _isSelectedAddress: MutableLiveData<AddressDTO> = MutableLiveData()

        val getAddressLiveData: LiveData<AddressDTO>
            get() = _isSelectedAddress

        var selectedAddress: AddressDTO? = null
            set(value) {
                _isSelectedAddress.value = value
                field = value
            }
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(
                YumzyFirebaseMessaging.channelId,
                YumzyFirebaseMessaging.channelName,
                importance
            )
            val notificationManager = context.getSystemService(
                NotificationManager::class.java
            )
            notificationManager.createNotificationChannel(channel)
        }
    }
}