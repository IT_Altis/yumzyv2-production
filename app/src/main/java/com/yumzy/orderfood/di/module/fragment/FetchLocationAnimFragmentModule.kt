package com.yumzy.orderfood.di.module.fragment

import androidx.lifecycle.ViewModel
import com.yumzy.orderfood.api.HomeServiceAPI
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.dao.SelectedAddressDao
import com.yumzy.orderfood.data.dao.SelectedItemDao
import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.home.data.HomeRemoteDataSource
import com.yumzy.orderfood.ui.screens.login.fragment.FetchLocationAnimViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class FetchLocationAnimFragmentModule {
    companion object {
        @JvmStatic
        @FetchLocationAnimFragmentScope
        @Provides
        fun provideUserDao(db: AppDatabase): UserInfoDao = db.userInfoDao()

        @JvmStatic
        @FetchLocationAnimFragmentScope
        @Provides
        fun provideSelectedAddress(db: AppDatabase): SelectedAddressDao = db.selectedAddressDao()

        @JvmStatic
        @FetchLocationAnimFragmentScope
        @Provides
        fun provideSelectedItem(db: AppDatabase): SelectedItemDao = db.selectedItemDao()

        @JvmStatic
        @FetchLocationAnimFragmentScope
        @Provides
        fun provideHomeService(retrofit: Retrofit): HomeServiceAPI {
            return retrofit.create(HomeServiceAPI::class.java)
        }

        @JvmStatic
        @FetchLocationAnimFragmentScope
        @Provides
        fun provideHomeDataSource(apiService: HomeServiceAPI) =
            HomeRemoteDataSource(apiService)


    }

    @Binds
    @IntoMap
    @ViewModelKey(FetchLocationAnimViewModel::class)
    abstract fun bindFetchLocationAnimViewModel(fetchLocationAnimViewModel: FetchLocationAnimViewModel): ViewModel?


}