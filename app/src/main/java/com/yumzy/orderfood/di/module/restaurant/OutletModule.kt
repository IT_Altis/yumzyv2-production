package com.yumzy.orderfood.di.module.restaurant

import com.yumzy.orderfood.api.CartServiceAPI
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.dao.RestaurantDao
import com.yumzy.orderfood.data.dao.RestaurantItemDao
import com.yumzy.orderfood.data.dao.SelectedItemDao
import com.yumzy.orderfood.ui.screens.restaurant.data.RestaurantRemoteDataSource
import com.yumzy.orderfood.ui.screens.restaurant.data.RestaurantRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
object OutletModule {

    @OutletActivityScope
    @Provides
    fun provideAppApi(retrofit: Retrofit): CartServiceAPI {
        return retrofit.create(CartServiceAPI::class.java)
    }

    @OutletActivityScope
    @Provides
    fun provideRestaurantDao(db: AppDatabase): RestaurantDao = db.restaurantDao()

    @OutletActivityScope
    @Provides
    fun provideItemListDao(db: AppDatabase): RestaurantItemDao = db.restaurantItemDao()

    @OutletActivityScope
    @Provides
    fun provideSelectedDao(db: AppDatabase): SelectedItemDao = db.selectedItemDao()


    @OutletActivityScope
    @Provides
    fun provideRestaurantRemoteDataSource(cartServiceApi: CartServiceAPI) =
        RestaurantRemoteDataSource(cartServiceApi)


    @OutletActivityScope
    @Provides
    fun provideRestaurantRepository(
        dataSource: RestaurantRemoteDataSource,
        restaurantDao: RestaurantDao,
        restaurantItemDao: RestaurantItemDao,
        selectedItemDao: SelectedItemDao
    ) =
        RestaurantRepository(dataSource, restaurantDao, restaurantItemDao, selectedItemDao)
}