package com.yumzy.orderfood.di.module.myrewards

import javax.inject.Scope

@Scope
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class RewardsActivityScope