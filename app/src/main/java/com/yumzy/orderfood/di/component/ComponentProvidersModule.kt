package com.yumzy.orderfood.di.component

import android.app.Activity
import dagger.Module
import dagger.multibindings.Multibinds

@Module
interface ComponentProvidersModule {

    // Define that a Dagger Component can provide Multibind Map of Activities by class
    @Multibinds
    fun provideActivities(): Map<Class<out Activity>, @JvmSuppressWildcards Activity>
}