package com.yumzy.orderfood.di.module.dialogs

import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.module.restaurant.OrderCustomizationViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class OrderCustomizationModule {

    companion object {


    }

    @Binds
    @IntoMap
    @OrderCustomizationScope
    @ViewModelKey(OrderCustomizationViewModel::class)
    abstract fun bindCustomizeOrderViewModelViewModel(customizationViewModel: OrderCustomizationViewModel?): OrderCustomizationViewModel?
}