package com.yumzy.orderfood.di.module.location

import androidx.lifecycle.ViewModel
import com.yumzy.orderfood.api.AddressServiceAPI
import com.yumzy.orderfood.api.LoginServiceAPI
import com.yumzy.orderfood.api.ProfileServiceAPI
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.dao.AddressDao
import com.yumzy.orderfood.data.dao.SelectedAddressDao
import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.di.module.login.LoginActivityScope
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.location.LocationViewModel
import com.yumzy.orderfood.ui.screens.location.data.LocationRemoteDataSource
import com.yumzy.orderfood.ui.screens.location.data.LocationRepository
import com.yumzy.orderfood.ui.screens.login.LoginViewModel
import com.yumzy.orderfood.ui.screens.login.data.LoginRemoteDataSource
import com.yumzy.orderfood.ui.screens.login.data.LoginRepository
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRemoteDataSource
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class LocationViewModule {

    companion object {

        @JvmStatic
        @LocationActivityScope
        @Provides
        fun providesAddressDao(db: AppDatabase): AddressDao = db.addressDao()

        @JvmStatic
        @LocationActivityScope
        @Provides
        fun provideSelectedAddressDao(db: AppDatabase): SelectedAddressDao = db.selectedAddressDao()

        @JvmStatic
        @LocationActivityScope
        @Provides
        fun provideAddressServiceApi(retrofit: Retrofit): AddressServiceAPI =
            retrofit.create(AddressServiceAPI::class.java)


        @JvmStatic
        @LocationActivityScope
        @Provides
        fun provideLocationRemoteDataSource(addressServiceAPI: AddressServiceAPI) =
            LocationRemoteDataSource(addressServiceAPI)

        @JvmStatic
        @LocationActivityScope
        @Provides
        fun provideLocationRepository(
            selectedAddressDao: SelectedAddressDao?,
            addressDao: AddressDao,
            remoteSource: LocationRemoteDataSource,
        ) =
            LocationRepository(selectedAddressDao, addressDao, remoteSource)

    }

    @Binds
    @IntoMap
    @ViewModelKey(LocationViewModel::class)
    abstract fun bindLocationViewModel(viewModel: LocationViewModel?): ViewModel?

}