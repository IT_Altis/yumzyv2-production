package com.yumzy.orderfood.di.module.home

import androidx.lifecycle.ViewModel
import com.yumzy.orderfood.api.HomeServiceAPI
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.dao.SelectedAddressDao
import com.yumzy.orderfood.data.dao.SelectedItemDao
import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.home.HomePageViewModel
import com.yumzy.orderfood.ui.screens.home.data.HomeRemoteDataSource
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class HomeViewModelModule {
    companion object {

        @JvmStatic
        @HomePageActivityScope
        @Provides
        fun provideUserDao(db: AppDatabase): UserInfoDao = db.userInfoDao()

        @JvmStatic
        @HomePageActivityScope
        @Provides
        fun provideSelectedAddress(db: AppDatabase): SelectedAddressDao = db.selectedAddressDao()

        @JvmStatic
        @HomePageActivityScope
        @Provides
        fun provideSelectedItem(db: AppDatabase): SelectedItemDao = db.selectedItemDao()

        @JvmStatic
        @HomePageActivityScope
        @Provides
        fun provideHomeService(retrofit: Retrofit): HomeServiceAPI {
            return retrofit.create(HomeServiceAPI::class.java)
        }

        @JvmStatic
        @HomePageActivityScope
        @Provides
        fun provideHomeDataSource(apiService: HomeServiceAPI) =
            HomeRemoteDataSource(apiService)


    }

    @Binds
    @IntoMap
    @ViewModelKey(HomePageViewModel::class)
    abstract fun bindHomeActivityViewModel(homeActivityViewModel: HomePageViewModel): ViewModel?


}