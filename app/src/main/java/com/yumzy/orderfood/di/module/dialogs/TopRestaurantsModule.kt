package com.yumzy.orderfood.di.module.dialogs

import com.yumzy.orderfood.api.HomeServiceAPI
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.module.toprestaurant.RestaurantViewModel
import com.yumzy.orderfood.ui.screens.module.toprestaurant.data.TopRestaurantsRemoteDataSource
import com.yumzy.orderfood.ui.screens.module.toprestaurant.data.TopRestaurantsRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class TopRestaurantsModule {
    companion object {
        @JvmStatic
        @TopRestaurantsScope
        @Provides
        fun provideAppApi(retrofit: Retrofit): HomeServiceAPI {
            return retrofit.create(HomeServiceAPI::class.java)
        }

        @JvmStatic
        @TopRestaurantsScope
        @Provides
        fun provideTopRestaurantsRemoteDataSource(apiServiceAPI: HomeServiceAPI) =
            TopRestaurantsRemoteDataSource(apiServiceAPI)

        @JvmStatic
        @TopRestaurantsScope
        @Provides
        fun provideTopRestaurantsRepository(dataSource: TopRestaurantsRemoteDataSource) =
            TopRestaurantsRepository(dataSource)

    }

    @Binds
    @IntoMap
    @TopRestaurantsScope
    @ViewModelKey(RestaurantViewModel::class)
    abstract fun bindTopRestaurantsViewModel(viewModel: RestaurantViewModel?): RestaurantViewModel?
}
