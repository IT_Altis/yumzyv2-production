package com.yumzy.orderfood.di.module.dialogs

import com.yumzy.orderfood.api.LoginServiceAPI
import com.yumzy.orderfood.api.ProfileServiceAPI
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.login.data.LoginRemoteDataSource
import com.yumzy.orderfood.ui.screens.login.data.LoginRepository
import com.yumzy.orderfood.ui.screens.module.OTPVerifyViewModel
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRemoteDataSource
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class OtpVerifyModule {


    companion object {

        @JvmStatic
        @OtpVerifyDialogScope
        @Provides
        fun provideAppApi(retrofit: Retrofit): LoginServiceAPI =
            retrofit.create(LoginServiceAPI::class.java)

        @JvmStatic
        @OtpVerifyDialogScope
        @Provides
        fun provideProfileServiceApi(retrofit: Retrofit): ProfileServiceAPI =
            retrofit.create(ProfileServiceAPI::class.java)

        @JvmStatic
        @OtpVerifyDialogScope
        @Provides
        fun provideProfileRemoteDataSource(profileServiceAPI: ProfileServiceAPI) =
            ProfileRemoteDataSource(profileServiceAPI)

        @JvmStatic
        @OtpVerifyDialogScope
        @Provides
        fun provideLoginRemoteDataSource(loginServiceAPI: LoginServiceAPI) =
            LoginRemoteDataSource(loginServiceAPI)

        @JvmStatic
        @OtpVerifyDialogScope
        @Provides
        fun provideLoginRepository(
            dataSource: LoginRemoteDataSource,
            profileRemoteDataSource: ProfileRemoteDataSource
        ) =
            LoginRepository(null, null, dataSource,
                profileRemoteDataSource)


    }

    @Binds
    @IntoMap
    @OtpVerifyDialogScope
    @ViewModelKey(OTPVerifyViewModel::class)
    abstract fun bindOTPVerifyViewModel(viewModel: OTPVerifyViewModel?): OTPVerifyViewModel?
}
