package com.yumzy.orderfood.di.module.restaurant

import javax.inject.Scope

@Scope
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class RestaurantActivityScope