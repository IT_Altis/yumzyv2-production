package com.yumzy.orderfood.di.module

//import com.yumzy.orderfood.di.module.cart.CartModule
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.yumzy.orderfood.BuildConfig
import com.yumzy.orderfood.api.AuthInterceptor
import com.yumzy.orderfood.api.CacheInterceptor
import com.yumzy.orderfood.api.CartServiceAPI
import com.yumzy.orderfood.api.session.SessionManager
import com.yumzy.orderfood.appGson
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.SharedPrefsHelper
import com.yumzy.orderfood.di.CoroutineScopeDefault
import com.yumzy.orderfood.di.CoroutineScopeIO
import com.yumzy.orderfood.ui.common.cart.CartHelper
import com.yumzy.orderfood.ui.screens.cart.CartRemoteDataSource
import com.yumzy.orderfood.ui.screens.cart.CartRepository
import com.yumzy.orderfood.util.IConstants
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module(includes = [ActivityBuilderModule::class, DialogBuilderModule::class, FragmentBuilderModule::class])
class AppModule {
    @Singleton
    @Provides
    fun provideSharedPrefs(application: Application): SharedPreferences {
        return application.getSharedPreferences(IConstants.SHARE_PREF_NAME, Context.MODE_PRIVATE)
    }

    @Singleton
    @Provides
    fun provideRetrofitInstance(
        okHttpClient: OkHttpClient,
        converterFactory: GsonConverterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(IConstants.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(converterFactory)
            .build()
    }

    @Singleton
    @Provides
    fun provideOkHttpClient(
        application: Application,
        authInterceptor: AuthInterceptor,
        interceptor: HttpLoggingInterceptor,
        cacheInterceptor: CacheInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .cache(
                Cache(
                    application.cacheDir, // app private cache directory
                    20 * 1024 * 1024
                )  // 20MB
            )
            .addInterceptor(authInterceptor)
            .addInterceptor(interceptor)
            .addNetworkInterceptor(cacheInterceptor).build()
    }

    @Singleton
    @Provides
    fun provideLoggingInterceptor() =
        HttpLoggingInterceptor().apply {
            level =
                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        }

    @Provides
    @Singleton
    fun provideGson(): Gson = appGson

    @Provides
    @Singleton
    fun provideGsonConverterFactory(gson: Gson): GsonConverterFactory =
        GsonConverterFactory.create(gson)


    @Singleton
    @Provides
    fun provideToken(sessionManager: SessionManager) = AuthInterceptor(sessionManager)

    @Singleton
    @Provides
    fun provideCache() = CacheInterceptor()

    @Singleton
    @Provides
    fun providePrefHelper(sharedPreferences: SharedPreferences) =
        SharedPrefsHelper(sharedPreferences)


    @Singleton
    @Provides
    fun provideDb(app: Application) = AppDatabase.getInstance(app)

    @Provides
    @Singleton
    fun provideSessionManager(prefsHelper: SharedPrefsHelper): SessionManager =
        SessionManager(prefsHelper)

    @CoroutineScopeIO
    @Provides
    fun provideCoroutineScopeIO() = CoroutineScope(Dispatchers.IO)

    @CoroutineScopeDefault
    @Provides
    fun provideCoroutineScopeDI() = CoroutineScope(Dispatchers.Default)
//
//    @Singleton
//    @ContributesAndroidInjector(modules = [CartModule::class])
//    fun cartHelper() = CartHelper()

    @Singleton
    @Provides
    fun provideCartHelper(
        sharedPrefsHelper: SharedPrefsHelper,
        appDatabase: AppDatabase,
        retrofit: Retrofit
    ): CartHelper {
        val cartRepository = retrofit.create(CartServiceAPI::class.java)
        val selectedItemDao = appDatabase.selectedItemDao()

        return CartHelper(
            sharedPrefsHelper,
            CartRepository(
                appDatabase.cartResDao(),
                CartRemoteDataSource(cartRepository),
                selectedItemDao
            )
        )

    }

}
