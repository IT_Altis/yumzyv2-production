package com.yumzy.orderfood.di.module.dialogs

import com.yumzy.orderfood.api.RewardServiceAPI
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.module.CouponViewModel
import com.yumzy.orderfood.ui.screens.rewards.data.RewardsRemoteDataSource
import com.yumzy.orderfood.ui.screens.rewards.data.RewardsRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class CouponModule {


    companion object {
        @JvmStatic
        @ReferralDialogScope
        @Provides
        fun provideUserDao(db: AppDatabase): UserInfoDao = db.userInfoDao()

        @JvmStatic
        @ReferralDialogScope
        @Provides
        fun provideApi(retrofit: Retrofit): RewardServiceAPI =
            retrofit.create(RewardServiceAPI::class.java)


        @JvmStatic
        @ReferralDialogScope
        @Provides
        fun provideRemoteDataSource(rewardServiceAPI: RewardServiceAPI) =
            RewardsRemoteDataSource(rewardServiceAPI)

        @JvmStatic
        @ReferralDialogScope
        @Provides
        fun provideRewardRepository(dao: UserInfoDao, dataSource: RewardsRemoteDataSource) =
            RewardsRepository(dao, dataSource)

    }

    @Binds
    @IntoMap
    @ReferralDialogScope
    @ViewModelKey(CouponViewModel::class)
    abstract fun bindViewModel(viewModel: CouponViewModel?): CouponViewModel?
}
