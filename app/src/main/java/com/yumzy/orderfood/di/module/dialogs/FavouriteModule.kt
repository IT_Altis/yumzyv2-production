package com.yumzy.orderfood.di.module.dialogs

import com.yumzy.orderfood.api.ProfileServiceAPI
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.module.favourites.FavouritesViewModel
import com.yumzy.orderfood.ui.screens.module.favourites.data.FavouriteRestaurantRepository
import com.yumzy.orderfood.ui.screens.module.favourites.data.FavourriteRestaurantRemoteDataSource
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class FavouriteModule {
    companion object {
        @JvmStatic
        @FavouritesScope
        @Provides
        fun provideAppApi(retrofit: Retrofit): ProfileServiceAPI {
            return retrofit.create(ProfileServiceAPI::class.java)
        }

        @JvmStatic
        @FavouritesScope
        @Provides
        fun provideFavourriteRestaurantRemoteDataSource(apiServiceAPI: ProfileServiceAPI) =
            FavourriteRestaurantRemoteDataSource(apiServiceAPI)

        @JvmStatic
        @FavouritesScope
        @Provides
        fun provideFavouriteRestaurantRepository(dataSource: FavourriteRestaurantRemoteDataSource) =
            FavouriteRestaurantRepository(dataSource)

    }

    @Binds
    @IntoMap
    @FavouritesScope
    @ViewModelKey(FavouritesViewModel::class)
    abstract fun bindFavouriteViewModel(viewModel: FavouritesViewModel?): FavouritesViewModel?
}
