package com.yumzy.orderfood.di.module.dialogs

import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.module.address.AddAddressViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
@Module
abstract class AddAddressModule {
    @Binds
    @IntoMap
    @AddAddressDialogScope
    @ViewModelKey(AddAddressViewModel::class)
    abstract fun bindOTPAddressViewModel(viewModelAdd: AddAddressViewModel?): AddAddressViewModel?
}
