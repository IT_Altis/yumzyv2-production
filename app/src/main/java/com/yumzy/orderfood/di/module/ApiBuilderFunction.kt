package com.yumzy.orderfood.di.module

import com.yumzy.orderfood.util.IConstants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


public fun <T> provideService(
    okhttpClient: OkHttpClient,
    converterFactory: GsonConverterFactory, clazz: Class<T>
): T {
    return createRetrofit(okhttpClient, converterFactory).create(clazz)
}

fun createRetrofit(
    okhttpClient: OkHttpClient,
    converterFactory: GsonConverterFactory
): Retrofit {

    return Retrofit.Builder()
        .baseUrl(IConstants.BASE_URL)
        .client(okhttpClient)
        .addConverterFactory(converterFactory)
        .build()
}
