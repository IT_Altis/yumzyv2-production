package com.yumzy.orderfood.di.module.mywallet

import androidx.lifecycle.ViewModel
import com.yumzy.orderfood.api.ProfileServiceAPI
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRemoteDataSource
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRepository
import com.yumzy.orderfood.ui.screens.wallet.WalletViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

/**
 * Created by Bhupendra Kumar Sahu.
 */
@Module
abstract class MyWalletViewModule {
    companion object
    {
        @JvmStatic
        @MyWalletActivityScope
        @Provides
        fun provideUserDao(db: AppDatabase): UserInfoDao = db.userInfoDao()

        @JvmStatic
        @MyWalletActivityScope
        @Provides
        fun provideAppApi(retrofit: Retrofit): ProfileServiceAPI {
            return retrofit.create(ProfileServiceAPI::class.java)
        }

        @JvmStatic
        @MyWalletActivityScope
        @Provides
        fun provideProfileRemoteDataSource(apiServiceAPI: ProfileServiceAPI) =
            ProfileRemoteDataSource(apiServiceAPI)

        @JvmStatic
        @MyWalletActivityScope
        @Provides
        fun provideProfileRepository(dao: UserInfoDao, dataSource: ProfileRemoteDataSource) =
            ProfileRepository(dao, dataSource)
    }
    @Binds
    @IntoMap
    @ViewModelKey(WalletViewModel::class)
    abstract fun bindMyWalletViewModel(viewModel: WalletViewModel): ViewModel
}