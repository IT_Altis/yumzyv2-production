package com.yumzy.orderfood.di.module.scratchcard

import androidx.lifecycle.ViewModel
import com.yumzy.orderfood.api.ScratchCardAPI
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.scratchcard.ScratchCardViewModel
import com.yumzy.orderfood.ui.screens.scratchcard.data.ScratchDataSource
import com.yumzy.orderfood.ui.screens.scratchcard.data.ScratchRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class ScratchModule {
    companion object {


        @ScratchActivityScope
        @Provides
        fun provideAppApi(retrofit: Retrofit): ScratchCardAPI {
            return retrofit.create(ScratchCardAPI::class.java)
        }
        @ScratchActivityScope
        @Provides
        fun provideProfileRemoteDataSource(apiCardAPI: ScratchCardAPI) =
            ScratchDataSource(apiCardAPI)

        @JvmStatic
        @ScratchActivityScope
        @Provides
        fun provideProfileRepository(dataSource: ScratchDataSource) =
            ScratchRepository(dataSource)

    }

    @Binds
    @IntoMap
    @ScratchActivityScope
    @ViewModelKey(ScratchCardViewModel::class)
    abstract fun bindViewModel(cardViewModel: ScratchCardViewModel): ViewModel
}