package com.yumzy.orderfood.di.module.home

import javax.inject.Scope

@Scope
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class PlateFragmentScope