package com.yumzy.orderfood.di.module.offer

import androidx.lifecycle.ViewModel
import com.yumzy.orderfood.api.CartServiceAPI
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.offer.OfferViewModel
import com.yumzy.orderfood.ui.screens.offer.data.OfferRemoteDataSource
import com.yumzy.orderfood.ui.screens.offer.data.OfferRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

/**
 * Created by Bhupendra Kumar Sahu.
 */
@Module
abstract class OfferViewModule {
    companion object
    {
        @JvmStatic
        @OfferActivityScope
        @Provides
        fun provideAppApi(retrofit: Retrofit): CartServiceAPI {
            return retrofit.create(CartServiceAPI::class.java)
        }


        @JvmStatic
        @OfferActivityScope
        @Provides
        fun provideOfferRemoteDataSource(cartServiceApi: CartServiceAPI) =
            OfferRemoteDataSource(cartServiceApi)

        @JvmStatic
        @OfferActivityScope
        @Provides
        fun provideOfferRepository(dataSource: OfferRemoteDataSource) =
            OfferRepository(dataSource)
    }
    @Binds
    @IntoMap
    @ViewModelKey(OfferViewModel::class)
    abstract fun bindOfferViewModel(viewModel: OfferViewModel): ViewModel
}