package com.yumzy.orderfood.di.module.invite

import androidx.lifecycle.ViewModel
import com.yumzy.orderfood.api.HomeServiceAPI
import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.invite.InviteViewModel
import com.yumzy.orderfood.ui.screens.invite.data.InviteRemoteDataSource
import com.yumzy.orderfood.ui.screens.invite.data.InviteRepository

import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class InviteViewModule {
    companion object
    {


        @InviteActivityScope
        @Provides
        fun provideAppApi(retrofit: Retrofit): HomeServiceAPI {
            return retrofit.create(HomeServiceAPI::class.java)
        }

        @InviteActivityScope
        @Provides
        fun provideInviteRemoteDataSource(apiServiceAPI: HomeServiceAPI) =
            InviteRemoteDataSource(apiServiceAPI)

        @InviteActivityScope
        @Provides
        fun provideInviteRepository(dataSource: InviteRemoteDataSource) =
            InviteRepository(dataSource)

    }
    @Binds
    @IntoMap
    @ViewModelKey(InviteViewModel::class)
    abstract fun bindInviteViewModel(viewModel: InviteViewModel): ViewModel
}