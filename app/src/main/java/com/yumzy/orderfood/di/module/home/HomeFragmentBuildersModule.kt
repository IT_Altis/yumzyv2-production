package com.yumzy.orderfood.di.module.home

import com.yumzy.orderfood.ui.screens.home.fragment.*
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class HomeFragmentBuildersModule {

    @HomeFragmentScope
    @ContributesAndroidInjector(modules = [HomeModule::class])
    abstract fun contributeHomeFragment(): HomeFragment

    @ExploreFragmentScope
    @ContributesAndroidInjector(modules = [ExploreModule::class])
    abstract fun contributeExploreFragment(): ExploreFragment


    @PlateFragmentScope
    @ContributesAndroidInjector(modules = [PlateModule::class])
    abstract fun contributePlateFragment(): PlateFragment

    @AccountFragmentScope
    @ContributesAndroidInjector(modules = [AccountModule::class])
    abstract fun contributeAccountFragment(): AccountFragment

    @AccountOptionsFragmentScope
    @ContributesAndroidInjector(modules = [AccountOptionsModule::class])
    abstract fun contributeAccountOptionsFragment(): AccountOptionsFragment


}