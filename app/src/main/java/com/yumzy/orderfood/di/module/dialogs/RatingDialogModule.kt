package com.yumzy.orderfood.di.module.dialogs

import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.module.appRating.RatingViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
@Module
abstract class RatingDialogModule {
    @Binds
    @IntoMap
    @RatingDialogScope
    @ViewModelKey(RatingViewModel::class)
    abstract fun bindRatingViewModel(viewModelAdd: RatingViewModel?): RatingViewModel?
}
