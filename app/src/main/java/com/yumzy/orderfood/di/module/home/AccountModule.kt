package com.yumzy.orderfood.di.module.home

import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.home.data.AccountRepository
import com.yumzy.orderfood.ui.screens.home.data.HomeRemoteDataSource
import com.yumzy.orderfood.ui.screens.home.fragment.AccountViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap


@Module
abstract class AccountModule {


    companion object {

        @JvmStatic
        @AccountFragmentScope
        @Provides
        fun provideAccountRepository(
            userInfoDao: UserInfoDao,
            homeRemoteDataSource: HomeRemoteDataSource
        ) =
            AccountRepository(userInfoDao, homeRemoteDataSource)

    }

    @Binds
    @IntoMap
    @AccountFragmentScope
    @ViewModelKey(AccountViewModel::class)
    abstract fun bindAccountViewModel(viewModel: AccountViewModel?): AccountViewModel?

}
