package com.yumzy.orderfood.di.module.fragment

import com.yumzy.orderfood.api.LoginServiceAPI
import com.yumzy.orderfood.api.ProfileServiceAPI
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.login.LoginViewModel
import com.yumzy.orderfood.ui.screens.login.data.LoginRemoteDataSource
import com.yumzy.orderfood.ui.screens.login.data.LoginRepository
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRemoteDataSource
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class OnBoardFragmentModule {


    companion object {

        @JvmStatic
        @OnBoardFragmentScope
        @Provides
        fun provideAppApi(retrofit: Retrofit): LoginServiceAPI =
            retrofit.create(LoginServiceAPI::class.java)

        @JvmStatic
        @OnBoardFragmentScope
        @Provides
        fun provideProfileServiceApi(retrofit: Retrofit): ProfileServiceAPI =
            retrofit.create(ProfileServiceAPI::class.java)

        @JvmStatic
        @OnBoardFragmentScope
        @Provides
        fun provideProfileRemoteDataSource(profileServiceAPI: ProfileServiceAPI) =
            ProfileRemoteDataSource(profileServiceAPI)

        @JvmStatic
        @OnBoardFragmentScope
        @Provides
        fun provideLoginRemoteDataSource(loginServiceAPI: LoginServiceAPI) =
            LoginRemoteDataSource(loginServiceAPI)

        @JvmStatic
        @OnBoardFragmentScope
        @Provides
        fun provideLoginRepository(
            dataSource: LoginRemoteDataSource,
            profileRemoteDataSource: ProfileRemoteDataSource
        ) =
            LoginRepository(
                null, null, dataSource,
                profileRemoteDataSource
            )


    }

    @Binds
    @IntoMap
    @OnBoardFragmentScope
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindViewModel(viewModel: LoginViewModel?): LoginViewModel?
}
