package com.yumzy.orderfood.di.module.dialogs

import com.yumzy.orderfood.api.ProfileServiceAPI
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.module.foodpreference.FoodPreferenceViewModel
import com.yumzy.orderfood.ui.screens.module.foodpreference.data.FoodPreferenceRemoteDataSource
import com.yumzy.orderfood.ui.screens.module.foodpreference.data.FoodPreferenceRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class FoodPreferenceModule {

    companion object {
        @JvmStatic
        @FoodPreferenceDialogScope
        @Provides
        fun provideAppApi(retrofit: Retrofit): ProfileServiceAPI {
            return retrofit.create(ProfileServiceAPI::class.java)
        }

        @JvmStatic
        @FoodPreferenceDialogScope
        @Provides
        fun provideFoodPreferenceRemoteDataSource(apiServiceAPI: ProfileServiceAPI) =
            FoodPreferenceRemoteDataSource(apiServiceAPI)

        @JvmStatic
        @FoodPreferenceDialogScope
        @Provides
        fun provideFoodPreferenceRepository(dataSource: FoodPreferenceRemoteDataSource) =
            FoodPreferenceRepository(dataSource)

    }

    @Binds
    @IntoMap
    @FoodPreferenceDialogScope
    @ViewModelKey(FoodPreferenceViewModel::class)
    abstract fun bindFoodPreferenceViewModel(viewModel: FoodPreferenceViewModel): FoodPreferenceViewModel
}