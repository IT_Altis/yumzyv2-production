package com.yumzy.orderfood.di.module.home

import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.dao.OrdersDao
import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.home.data.HomeRemoteDataSource
import com.yumzy.orderfood.ui.screens.home.data.HomeRepository
import com.yumzy.orderfood.ui.screens.home.fragment.HomeViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
abstract class HomeModule {

    companion object {

        @JvmStatic
        @HomeFragmentScope
        @Provides
        fun provideLocalDao(appDatabase: AppDatabase) : OrdersDao {
            return appDatabase.ordersDao()
        }


        @JvmStatic
        @HomeFragmentScope
        @Provides
        fun provideLoginRepository(
            userInfoDao: UserInfoDao,
            homeRemoteDataSource: HomeRemoteDataSource,
            orderLocalDataSource: OrdersDao
        ) =
            HomeRepository(userInfoDao, homeRemoteDataSource, orderLocalDataSource)

    }

    @Binds
    @IntoMap
    @HomeFragmentScope
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(viewModel: HomeViewModel?): HomeViewModel?


}
