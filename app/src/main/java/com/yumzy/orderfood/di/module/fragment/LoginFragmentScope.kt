package com.yumzy.orderfood.di.module.fragment

import javax.inject.Scope

@Scope
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class LoginFragmentScope