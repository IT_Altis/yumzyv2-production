package com.yumzy.orderfood.di.module

import com.yumzy.orderfood.di.module.earncoupon.EarnCouponActivityScope
import com.yumzy.orderfood.di.module.earncoupon.EarnCouponViewModule
import com.yumzy.orderfood.di.module.helpheading.HelpHeadingActivityScope
import com.yumzy.orderfood.di.module.helpheading.HelpHeadingModule
import com.yumzy.orderfood.di.module.helpsubheading.HelpSubHeadingActivityScope
import com.yumzy.orderfood.di.module.helpsubheading.HelpSubHeadingModule
import com.yumzy.orderfood.di.module.home.HomeFragmentBuildersModule
import com.yumzy.orderfood.di.module.home.HomePageActivityScope
import com.yumzy.orderfood.di.module.home.HomeViewModelModule
import com.yumzy.orderfood.di.module.invite.InviteActivityScope
import com.yumzy.orderfood.di.module.invite.InviteViewModule
import com.yumzy.orderfood.di.module.location.LocationActivityScope
import com.yumzy.orderfood.di.module.location.LocationViewModule
import com.yumzy.orderfood.di.module.login.LoginActivityScope
import com.yumzy.orderfood.di.module.login.LoginViewModule
import com.yumzy.orderfood.di.module.manageaddress.ManageAddressActivityScope
import com.yumzy.orderfood.di.module.manageaddress.ManageAddressViewModule
import com.yumzy.orderfood.di.module.myrewards.MyRewardsViewModule
import com.yumzy.orderfood.di.module.myrewards.RewardsActivityScope
import com.yumzy.orderfood.di.module.mywallet.MyWalletActivityScope
import com.yumzy.orderfood.di.module.mywallet.MyWalletViewModule
import com.yumzy.orderfood.di.module.offer.OfferActivityScope
import com.yumzy.orderfood.di.module.offer.OfferViewModule
import com.yumzy.orderfood.di.module.orderhistory.OrderHistoryActivityScope
import com.yumzy.orderfood.di.module.orderhistory.OrderHistoryViewModule
import com.yumzy.orderfood.di.module.payment.PaymentActivityScope
import com.yumzy.orderfood.di.module.payment.PaymentModule

import com.yumzy.orderfood.di.module.reorder.ReorderActivityScope
import com.yumzy.orderfood.di.module.reorder.ReorderViewModule
import com.yumzy.orderfood.di.module.restaurant.OutletActivityScope
import com.yumzy.orderfood.di.module.restaurant.OutletModule
import com.yumzy.orderfood.di.module.restaurant.RestaurantViewModule
import com.yumzy.orderfood.di.module.scratchcard.ScratchActivityScope
import com.yumzy.orderfood.di.module.scratchcard.ScratchModule
import com.yumzy.orderfood.tools.address.CurrentLocationPickerActivity
import com.yumzy.orderfood.ui.screens.earncoupon.EarnCouponActivity
import com.yumzy.orderfood.ui.screens.helpfaq.faq.HelpHeadingActivity
import com.yumzy.orderfood.ui.screens.helpfaq.faq.HelpSubheadingActivity
import com.yumzy.orderfood.ui.screens.home.HomePageActivity
import com.yumzy.orderfood.ui.screens.invite.InviteActivity
import com.yumzy.orderfood.ui.screens.location.LocationInfoActivity
import com.yumzy.orderfood.ui.screens.location.LocationPickrActivity
import com.yumzy.orderfood.ui.screens.location.LocationSearchActivity
import com.yumzy.orderfood.ui.screens.login.LoginActivity
import com.yumzy.orderfood.ui.screens.login.RegisterActivity
import com.yumzy.orderfood.ui.screens.login.StartedActivity
import com.yumzy.orderfood.ui.screens.login.TempLoginActivity
import com.yumzy.orderfood.ui.screens.manageaddress.ManageAddressActivity
import com.yumzy.orderfood.ui.screens.offer.OfferActivity
import com.yumzy.orderfood.ui.screens.orderhistory.OrderHistoryActivity
import com.yumzy.orderfood.ui.screens.payment.PaymentActivity
import com.yumzy.orderfood.ui.screens.payment.PaymentConfirmActivity
import com.yumzy.orderfood.ui.screens.reorder.ReorderActivity
import com.yumzy.orderfood.ui.screens.restaurant.OutletActivity
import com.yumzy.orderfood.ui.screens.restaurant.OutletSearchActivity
import com.yumzy.orderfood.ui.screens.rewards.RewardsActivity
import com.yumzy.orderfood.ui.screens.scratchcard.ScratchCardActivity
import com.yumzy.orderfood.ui.screens.wallet.WalletActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module()
abstract class ActivityBuilderModule {

    /**
     * For SplashScreen,Login and Registration
     * */

    @LoginActivityScope
    @ContributesAndroidInjector(modules = [LoginViewModule::class])
    abstract fun contributeStartedActivity(): StartedActivity


    @LoginActivityScope
    @ContributesAndroidInjector(modules = [LoginViewModule::class])
    abstract fun contributeLoginActivity(): LoginActivity


    @LoginActivityScope
    @ContributesAndroidInjector(modules = [LoginViewModule::class])
    abstract fun contributeCurrentlocationPickerActviity(): CurrentLocationPickerActivity
/*

    @LoginActivityScope
    @ContributesAndroidInjector(modules = [LoginViewModule::class])
    abstract fun contributeLocationPickerActivity(): LocationPickerActivity
*/

    @LoginActivityScope
    @ContributesAndroidInjector(modules = [LoginViewModule::class])
    abstract fun contributeRegisterActivity(): RegisterActivity

    @HomePageActivityScope
    @ContributesAndroidInjector(modules = [HomeFragmentBuildersModule::class, HomeViewModelModule::class])
    abstract fun contributeHomePageActivity(): HomePageActivity

    @ManageAddressActivityScope
    @ContributesAndroidInjector(modules = [ManageAddressViewModule::class])
    abstract fun contributeManageAddressActivity(): ManageAddressActivity


    @OutletActivityScope
    @ContributesAndroidInjector(modules = [OutletModule::class, RestaurantViewModule::class])
    abstract fun contributeOutletActivity(): OutletActivity

    @OutletActivityScope
    @ContributesAndroidInjector(modules = [OutletModule::class, RestaurantViewModule::class])
    abstract fun contributeOutletSearchActivity(): OutletSearchActivity

    @RewardsActivityScope
    @ContributesAndroidInjector(modules = [MyRewardsViewModule::class])
    abstract fun contributeMyRewardsActivity(): RewardsActivity

    @MyWalletActivityScope
    @ContributesAndroidInjector(modules = [MyWalletViewModule::class])
    abstract fun contributeMyWalletActivity(): WalletActivity

    @OrderHistoryActivityScope
    @ContributesAndroidInjector(modules = [OrderHistoryViewModule::class])
    abstract fun contributeOrderHistoryActivity(): OrderHistoryActivity

    @OfferActivityScope
    @ContributesAndroidInjector(modules = [OfferViewModule::class])
    abstract fun contributeOfferActivity(): OfferActivity

    @EarnCouponActivityScope
    @ContributesAndroidInjector(modules = [EarnCouponViewModule::class])
    abstract fun contributeEarnAndCouponsActivity(): EarnCouponActivity

    @HelpSubHeadingActivityScope
    @ContributesAndroidInjector(modules = [HelpSubHeadingModule::class])
    abstract fun contributeHelpSubheadingActivity(): HelpSubheadingActivity

    @HelpHeadingActivityScope
    @ContributesAndroidInjector(modules = [HelpHeadingModule::class])
    abstract fun contributeHelpHeadingActivity(): HelpHeadingActivity

    @ReorderActivityScope
    @ContributesAndroidInjector(modules = [ReorderViewModule::class])
    abstract fun contributeReorderActivity(): ReorderActivity

    @PaymentActivityScope
    @ContributesAndroidInjector(modules = [PaymentModule::class])
    abstract fun contributePaymentActivity(): PaymentActivity

    @ScratchActivityScope
    @ContributesAndroidInjector(modules = [ScratchModule::class])
    abstract fun contributeScratchActivity(): ScratchCardActivity

    @LocationActivityScope
    @ContributesAndroidInjector(modules = [LocationViewModule::class])
    abstract fun contributeLocationSearchActivity(): LocationSearchActivity

    @LocationActivityScope
    @ContributesAndroidInjector(modules = [LocationViewModule::class])
    abstract fun contributeLocationPickrActivity(): LocationPickrActivity

    @LocationActivityScope
    @ContributesAndroidInjector(modules = [LocationViewModule::class])
    abstract fun contributeLocationInfoActivity(): LocationInfoActivity

    @InviteActivityScope
    @ContributesAndroidInjector(modules = [InviteViewModule::class])
    abstract fun contributeInviteActivity(): InviteActivity

    @PaymentActivityScope
    @ContributesAndroidInjector(modules = [PaymentModule::class])
    abstract fun contributePaymentConfirmActivity(): PaymentConfirmActivity

    @LoginActivityScope
    @ContributesAndroidInjector(modules = [LoginViewModule::class])
    abstract fun contributeTempLoginActivity(): TempLoginActivity


}
