package com.yumzy.orderfood.di.module.offer

import javax.inject.Scope

@Scope
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class OfferActivityScope