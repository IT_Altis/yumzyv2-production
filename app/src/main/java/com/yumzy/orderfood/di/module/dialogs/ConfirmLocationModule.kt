/*
 * Created By Shriom Tripathi 2 - 5 - 2020
 */

package com.yumzy.orderfood.di.module.dialogs

import com.yumzy.orderfood.api.ProfileServiceAPI
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.module.OTPVerifyViewModel
import com.yumzy.orderfood.ui.screens.module.address.ConfirmLocationViewModel
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRemoteDataSource
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class ConfirmLocationModule {

    companion object {

        @JvmStatic
        @ConfirmLocationScope
        @Provides
        fun provideUserDao(db: AppDatabase): UserInfoDao = db.userInfoDao()


        @JvmStatic
        @ConfirmLocationScope
        @Provides
        fun provideAppApi(retrofit: Retrofit): ProfileServiceAPI {
            return retrofit.create(ProfileServiceAPI::class.java)
        }

        @JvmStatic
        @ConfirmLocationScope
        @Provides
        fun provideProfileRemoteDataSource(apiServiceAPI: ProfileServiceAPI) =
            ProfileRemoteDataSource(apiServiceAPI)

        @JvmStatic
        @ConfirmLocationScope
        @Provides
        fun provideProfileRepository(dao: UserInfoDao, dataSource: ProfileRemoteDataSource) =
            ProfileRepository(dao, dataSource)

    }

    @Binds
    @IntoMap
    @ConfirmLocationScope
    @ViewModelKey(ConfirmLocationViewModel::class)
    abstract fun bindConfirmLocationViewModel(viewModel: ConfirmLocationViewModel?): ConfirmLocationViewModel?
}