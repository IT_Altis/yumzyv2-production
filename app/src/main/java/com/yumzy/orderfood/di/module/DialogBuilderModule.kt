package com.yumzy.orderfood.di.module

import com.yumzy.orderfood.di.module.dialogs.*
import com.yumzy.orderfood.ui.screens.login.fragment.OTPVerifyFragment
import com.yumzy.orderfood.ui.screens.module.ApplyReferralDialog
import com.yumzy.orderfood.ui.screens.module.address.AddAddressDialog
import com.yumzy.orderfood.ui.screens.module.address.ConfirmLocationDialog
import com.yumzy.orderfood.ui.screens.module.address.OrderTrackingDialog
import com.yumzy.orderfood.ui.screens.module.address.SelectDeliveryLocationDialog
import com.yumzy.orderfood.ui.screens.module.appRating.RatingDialog
import com.yumzy.orderfood.ui.screens.module.dishes.TopDishesDialog
import com.yumzy.orderfood.ui.screens.module.favourites.FavouritesDialog
import com.yumzy.orderfood.ui.screens.module.foodpersonalisation.FoodPersonalisationDialog
import com.yumzy.orderfood.ui.screens.module.foodpreference.FoodPreferenceDialog
import com.yumzy.orderfood.ui.screens.module.restaurant.OrderCustomizationDialog
import com.yumzy.orderfood.ui.screens.module.review.RateOrderDialog
import com.yumzy.orderfood.ui.screens.module.seeall.SeeAllDialog
import com.yumzy.orderfood.ui.screens.module.toprestaurant.TopRestaurantsDialog
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module()
abstract class DialogBuilderModule {


    @OtpVerifyDialogScope
    @ContributesAndroidInjector(modules = [OtpVerifyModule::class])
    abstract fun contributeOtpDialog(): OTPVerifyFragment

    @ReferralDialogScope
    @ContributesAndroidInjector(modules = [CouponModule::class])
    abstract fun contributeReferralDialog(): ApplyReferralDialog

    @AddAddressDialogScope
    @ContributesAndroidInjector(modules = [AddAddressModule::class])
    abstract fun contributeAddAddressDialog(): AddAddressDialog

    @FavouritesScope
    @ContributesAndroidInjector(modules = [FavouriteModule::class])
    abstract fun contributeFavouriteDialog(): FavouritesDialog

    @RatingDialogScope
    @ContributesAndroidInjector(modules = [RatingDialogModule::class])
    abstract fun contributeRatingDialog(): RatingDialog

    @FoodPreferenceDialogScope
    @ContributesAndroidInjector(modules = [FoodPreferenceModule::class])
    abstract fun contributeFoodPreferenceDialog(): FoodPreferenceDialog

    @SelectDeliverLocationScope
    @ContributesAndroidInjector(modules = [SelectDeliverLocationModule::class])
    abstract fun contributeSelectDeliverLocationDialog(): SelectDeliveryLocationDialog

    @OrderCustomizationScope
    @ContributesAndroidInjector(modules = [OrderCustomizationModule::class])
    abstract fun contributeOrderCustomizationDialogDialog(): OrderCustomizationDialog

    @ConfirmLocationScope
    @ContributesAndroidInjector(modules = [ConfirmLocationModule::class])
    abstract fun contributeConfirmLocationDialog(): ConfirmLocationDialog


    @FoodPersonalisationScope
    @ContributesAndroidInjector(modules = [FoodPersonalisationModule::class])
    abstract fun contributeFoodPersonalisationDialog(): FoodPersonalisationDialog

    @OrderTrackingScope
    @ContributesAndroidInjector(modules = [OrderTrackingModule::class])
    abstract fun contributeOrderTrackingDialog(): OrderTrackingDialog

//    @SavedAddressDialogScope
//    @ContributesAndroidInjector(modules = [SavedAddressModule::class])
//    abstract fun contributeSavedAddressDialog(): SavedAddressDialog

    @TopDishesScope
    @ContributesAndroidInjector(modules = [TopDishesModule::class])
    abstract fun contributeTopDishesDialog(): TopDishesDialog

    @TopRestaurantsScope
    @ContributesAndroidInjector(modules = [TopRestaurantsModule::class])
    abstract fun contributeTopRestaurantsDialog(): TopRestaurantsDialog

    @RateOrderScope
    @ContributesAndroidInjector(modules = [RateOrderModule::class])
    abstract fun contributeRateOrderDialog(): RateOrderDialog

    @ContributesAndroidInjector(modules = [])
    abstract fun contributeSeeAllDialog(): SeeAllDialog

    /*@RestaurantSearchDialogScope
    @ContributesAndroidInjector
    abstract fun contributeRestaurantSearchDialog(): RestaurantSearchDialog*/

}
