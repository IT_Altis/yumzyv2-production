
/*
 * Created By Shriom Tripathi 2 - 5 - 2020
 */

package com.yumzy.orderfood.di.module.dialogs

import com.yumzy.orderfood.api.OrderServiceAPI
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.dao.OrdersDao
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.module.address.OrderTrackingDialogViewModel
import com.yumzy.orderfood.ui.screens.orderhistory.data.OrderRemoteDataSource
import com.yumzy.orderfood.ui.screens.orderhistory.data.OrderRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class OrderTrackingModule {

    companion object {
        @JvmStatic
        @OrderTrackingScope
        @Provides
        fun provideAppApi(retrofit: Retrofit): OrderServiceAPI {
            return retrofit.create(OrderServiceAPI::class.java)
        }

        @JvmStatic
        @OrderTrackingScope
        @Provides
        fun provideLocalDao(appDatabase: AppDatabase) : OrdersDao {
            return appDatabase.ordersDao()
        }

        @JvmStatic
        @OrderTrackingScope
        @Provides
        fun provideOrderRemoteDataSource(orderServiceApi: OrderServiceAPI) =
            OrderRemoteDataSource(orderServiceApi)

        @JvmStatic
        @OrderTrackingScope
        @Provides
        fun provideOrderRepository(remoteSource: OrderRemoteDataSource, localSource: OrdersDao ) =
            OrderRepository(remoteSource, localSource)

    }

    @Binds
    @IntoMap
    @OrderTrackingScope
    @ViewModelKey(OrderTrackingDialogViewModel::class)
    abstract fun bindOrderTrackingViewModel(viewModel: OrderTrackingDialogViewModel?): OrderTrackingDialogViewModel?
}