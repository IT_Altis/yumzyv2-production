package com.yumzy.orderfood.di.module.reorder

import javax.inject.Scope

@Scope
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class ReorderActivityScope