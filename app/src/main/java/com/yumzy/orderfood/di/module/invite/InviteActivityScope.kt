package com.yumzy.orderfood.di.module.invite

import javax.inject.Scope

@Scope
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class InviteActivityScope