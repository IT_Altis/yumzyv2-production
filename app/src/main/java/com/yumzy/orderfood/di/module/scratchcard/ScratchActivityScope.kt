package com.yumzy.orderfood.di.module.scratchcard

import javax.inject.Scope

@Scope
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class ScratchActivityScope