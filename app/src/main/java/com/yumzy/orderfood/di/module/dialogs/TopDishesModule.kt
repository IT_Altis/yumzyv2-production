package com.yumzy.orderfood.di.module.dialogs

import com.yumzy.orderfood.api.HomeServiceAPI
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.module.dishes.TopDishesViewModel
import com.yumzy.orderfood.ui.screens.module.dishes.data.TopDishesRemoteDataSource
import com.yumzy.orderfood.ui.screens.module.dishes.data.TopDishesRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class TopDishesModule {
    companion object {
        @JvmStatic
        @TopDishesScope
        @Provides
        fun provideAppApi(retrofit: Retrofit): HomeServiceAPI {
            return retrofit.create(HomeServiceAPI::class.java)
        }

        @JvmStatic
        @TopDishesScope
        @Provides
        fun provideTopDishesRemoteDataSource(apiServiceAPI: HomeServiceAPI) =
            TopDishesRemoteDataSource(apiServiceAPI)

        @JvmStatic
        @TopDishesScope
        @Provides
        fun provideTopDishesRepository(dataSource: TopDishesRemoteDataSource) =
            TopDishesRepository(dataSource)

    }

    @Binds
    @IntoMap
    @TopDishesScope
    @ViewModelKey(TopDishesViewModel::class)
    abstract fun bindTopDishesViewModel(viewModel: TopDishesViewModel?): TopDishesViewModel?
}
