package com.yumzy.orderfood.di.module.home


import androidx.lifecycle.ViewModel
import com.yumzy.orderfood.api.ProfileServiceAPI
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.dao.SelectedAddressDao
import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.profile.ProfileViewModel
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRemoteDataSource
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class ProfileFragmentModule {

    companion object {
        @ProfileFragmentScope
        @Provides
        fun provideUserDao(db: AppDatabase): UserInfoDao = db.userInfoDao()


        @ProfileFragmentScope
        @Provides
        fun provideAppApi(retrofit: Retrofit): ProfileServiceAPI {
            return retrofit.create(ProfileServiceAPI::class.java)
        }

        @ProfileFragmentScope
        @Provides
        fun provideSelectedAddress(db: AppDatabase): SelectedAddressDao = db.selectedAddressDao()

        @ProfileFragmentScope
        @Provides
        fun provideProfileRemoteDataSource(
            apiServiceAPI: ProfileServiceAPI,
            selectedAddressDao: SelectedAddressDao,
        ) =
            ProfileRemoteDataSource(apiServiceAPI, selectedAddressDao)

        @ProfileFragmentScope
        @Provides
        fun provideProfileRepository(dao: UserInfoDao, dataSource: ProfileRemoteDataSource) =
            ProfileRepository(dao, dataSource)

    }

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    abstract fun bindProfileViewModel(viewModel: ProfileViewModel): ViewModel
}