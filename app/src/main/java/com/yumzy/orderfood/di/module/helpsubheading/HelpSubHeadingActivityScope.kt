/*
 * Created By Shriom Tripathi 10 - 5 - 2020
 */

package com.yumzy.orderfood.di.module.helpsubheading

import javax.inject.Scope

@Scope
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class HelpSubHeadingActivityScope