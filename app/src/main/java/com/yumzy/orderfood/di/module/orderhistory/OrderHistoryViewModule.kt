package com.yumzy.orderfood.di.module.orderhistory

import androidx.lifecycle.ViewModel
import com.yumzy.orderfood.api.OrderServiceAPI
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.dao.OrdersDao
import com.yumzy.orderfood.di.module.reorder.ReorderActivityScope
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.orderhistory.OrderHistoryViewModel
import com.yumzy.orderfood.ui.screens.orderhistory.data.OrderRemoteDataSource
import com.yumzy.orderfood.ui.screens.orderhistory.data.OrderRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

/**
 * Created by Bhupendra Kumar Sahu.
 */
@Module
abstract class OrderHistoryViewModule {
    companion object
    {
        @JvmStatic
        @OrderHistoryActivityScope
        @Provides
        fun provideAppApi(retrofit: Retrofit): OrderServiceAPI {
            return retrofit.create(OrderServiceAPI::class.java)
        }

        @JvmStatic
        @OrderHistoryActivityScope
        @Provides
        fun provideOrderRemoteDataSource(orderServiceApi: OrderServiceAPI) =
            OrderRemoteDataSource(orderServiceApi)

        @JvmStatic
        @OrderHistoryActivityScope
        @Provides
        fun provideLocalDao(appDatabase: AppDatabase) : OrdersDao {
            return appDatabase.ordersDao()
        }

        @JvmStatic
        @OrderHistoryActivityScope
        @Provides
        fun provideOrderRepository(remoteSource: OrderRemoteDataSource, localSource: OrdersDao) =
            OrderRepository(remoteSource, localSource)

    }
    @Binds
    @IntoMap
    @ViewModelKey(OrderHistoryViewModel::class)
    abstract fun bindOrderHistoryViewModel(viewModel: OrderHistoryViewModel): ViewModel
}