package com.yumzy.orderfood.di.module.login

import androidx.lifecycle.ViewModel
import com.yumzy.orderfood.api.LoginServiceAPI
import com.yumzy.orderfood.api.ProfileServiceAPI
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.dao.SelectedAddressDao
import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.login.LoginViewModel
import com.yumzy.orderfood.ui.screens.login.data.LoginRemoteDataSource
import com.yumzy.orderfood.ui.screens.login.data.LoginRepository
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRemoteDataSource
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class LoginViewModule {

    companion object{
        @JvmStatic
        @LoginActivityScope
        @Provides
        fun provideUserDao(db: AppDatabase): UserInfoDao = db.userInfoDao()

        @JvmStatic
        @LoginActivityScope
        @Provides
        fun provideSelectedAddressDao(db: AppDatabase): SelectedAddressDao = db.selectedAddressDao()

        @JvmStatic
        @LoginActivityScope
        @Provides
        fun provideLoginApi(retrofit: Retrofit): LoginServiceAPI =
            retrofit.create(LoginServiceAPI::class.java)


        @JvmStatic
        @LoginActivityScope
        @Provides
        fun provideProfileServiceApi(retrofit: Retrofit): ProfileServiceAPI =
            retrofit.create(ProfileServiceAPI::class.java)


        @JvmStatic
        @LoginActivityScope
        @Provides
        fun provideLoginRemoteDataSource(loginServiceAPI: LoginServiceAPI) =
            LoginRemoteDataSource(loginServiceAPI)

        @JvmStatic
        @LoginActivityScope
        @Provides
        fun provideProfileRemoteDataSource(profileServiceAPI: ProfileServiceAPI) =
            ProfileRemoteDataSource(profileServiceAPI)

        @JvmStatic
        @LoginActivityScope
        @Provides
        fun provideLoginRepository(
            userInfoDao: UserInfoDao,
            selectedAddressDao: SelectedAddressDao,
            dataSource: LoginRemoteDataSource,
            profileRemoteDataSource: ProfileRemoteDataSource
        ) =
            LoginRepository(userInfoDao, selectedAddressDao, dataSource, profileRemoteDataSource)

    }
    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel?): ViewModel?

}