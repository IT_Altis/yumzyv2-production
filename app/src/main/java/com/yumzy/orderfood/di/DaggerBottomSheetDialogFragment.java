package com.yumzy.orderfood.di;

import android.app.Dialog;
import android.os.Bundle;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.jetbrains.annotations.NotNull;

import dagger.android.support.DaggerAppCompatDialogFragment;
import dagger.internal.Beta;

@Beta
public abstract class DaggerBottomSheetDialogFragment extends DaggerAppCompatDialogFragment {

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new BottomSheetDialog(requireContext(), getTheme());
    }
}