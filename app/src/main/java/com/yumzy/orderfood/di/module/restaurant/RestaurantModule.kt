package com.yumzy.orderfood.di.module.restaurant

import com.yumzy.orderfood.api.CartServiceAPI
import com.yumzy.orderfood.ui.screens.restaurant.data.RestaurantRemoteDataSource
import com.yumzy.orderfood.ui.screens.restaurant.data.RestaurantRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * Created by Bhupendra Kumar Sahu.
 */
@Module
object RestaurantModule {

    @JvmStatic
    @RestaurantActivityScope
    @Provides
    fun provideAppApi(retrofit: Retrofit): CartServiceAPI {
        return retrofit.create(CartServiceAPI::class.java)
    }

    @JvmStatic
    @RestaurantActivityScope
    @Provides
    fun provideRestaurantRemoteDataSource(cartServiceApi: CartServiceAPI) =
        RestaurantRemoteDataSource(cartServiceApi)

    @JvmStatic
    @RestaurantActivityScope
    @Provides
    fun provideRestaurantRepository(dataSource: RestaurantRemoteDataSource) =
        RestaurantRepository(dataSource, null, null, null)

}