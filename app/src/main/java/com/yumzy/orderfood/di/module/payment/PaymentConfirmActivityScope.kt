package com.yumzy.orderfood.di.module.payment

import javax.inject.Scope

@Scope
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class PaymentConfirmActivityScope