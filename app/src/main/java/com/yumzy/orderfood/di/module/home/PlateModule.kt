package com.yumzy.orderfood.di.module.home

import com.yumzy.orderfood.api.CartServiceAPI
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.dao.CartResDao
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.home.fragment.PlateRemoteDataSource
import com.yumzy.orderfood.ui.screens.home.fragment.PlateViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit


@Module
abstract class PlateModule {
    companion object {

        @JvmStatic
        @PlateFragmentScope
        @Provides
        fun providePlateService(retrofit: Retrofit): CartServiceAPI {
            return retrofit.create(CartServiceAPI::class.java)
        }

        @JvmStatic
        @PlateFragmentScope
        @Provides
        fun providePlateDataSource(apiService: CartServiceAPI) =
            PlateRemoteDataSource(apiService)


        @JvmStatic
        @PlateFragmentScope
        @Provides
        fun provideUserDao(db: AppDatabase): CartResDao = db.cartResDao()


    }

    @Binds
    @IntoMap
    @PlateFragmentScope
    @ViewModelKey(PlateViewModel::class)
    abstract fun bindPlateViewModel(viewModel: PlateViewModel?): PlateViewModel?

}
