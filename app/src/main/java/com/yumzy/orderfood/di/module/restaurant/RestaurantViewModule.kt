package com.yumzy.orderfood.di.module.restaurant

import androidx.lifecycle.ViewModel
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.restaurant.RestaurantViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by Bhupendra Kumar Sahu.
 */
@Module
abstract class RestaurantViewModule {
    @Binds
    @IntoMap
    @ViewModelKey(RestaurantViewModel::class)
    abstract fun bindRestaurantViewModel(viewModel: RestaurantViewModel): ViewModel
}