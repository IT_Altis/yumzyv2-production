/*
 * Created By Shriom Tripathi 11 - 5 - 2020
 */

package com.yumzy.orderfood.di.module.dialogs

import javax.inject.Scope


@Scope
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class FoodPersonalisationScope