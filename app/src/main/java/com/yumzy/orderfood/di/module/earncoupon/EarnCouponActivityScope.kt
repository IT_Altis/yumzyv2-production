package com.yumzy.orderfood.di.module.earncoupon

import javax.inject.Scope

@Scope
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class EarnCouponActivityScope