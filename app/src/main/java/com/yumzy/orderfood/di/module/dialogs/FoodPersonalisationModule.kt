/*
 * Created By Shriom Tripathi 11 - 5 - 2020
 */

package com.yumzy.orderfood.di.module.dialogs

import com.yumzy.orderfood.api.ProfileServiceAPI
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.module.foodpersonalisation.FoodPersonalisationViewModel
import com.yumzy.orderfood.ui.screens.module.foodpersonalisation.data.FoodPersonalisationRemoteDataSource
import com.yumzy.orderfood.ui.screens.module.foodpersonalisation.data.FoodPersonalisationRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class FoodPersonalisationModule {

    companion object {
        @JvmStatic
        @FoodPersonalisationScope
        @Provides
        fun provideAppApi(retrofit: Retrofit): ProfileServiceAPI {
            return retrofit.create(ProfileServiceAPI::class.java)
        }

        @JvmStatic
        @FoodPersonalisationScope
        @Provides
        fun provideFoodPersonalisationRemoteDataSource(apiServiceAPI: ProfileServiceAPI) =
            FoodPersonalisationRemoteDataSource(apiServiceAPI)

        @JvmStatic
        @FoodPersonalisationScope
        @Provides
        fun provideFoodPersonalisationRepository(dataSource: FoodPersonalisationRemoteDataSource) =
            FoodPersonalisationRepository(dataSource)

    }

    @Binds
    @IntoMap
    @FoodPersonalisationScope
    @ViewModelKey(FoodPersonalisationViewModel::class)
    abstract fun bindFoodPersonalisationViewModel(viewModel: FoodPersonalisationViewModel): FoodPersonalisationViewModel
}