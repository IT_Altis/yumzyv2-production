package com.yumzy.orderfood.di.module.dialogs

import com.yumzy.orderfood.api.ProfileServiceAPI
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.module.address.SelectDeliveryLocationViewModel
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRemoteDataSource
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class SelectDeliverLocationModule {

    companion object {

        @JvmStatic
        @SelectDeliverLocationScope
        @Provides
        fun provideUserDao(db: AppDatabase): UserInfoDao = db.userInfoDao()

        @JvmStatic
        @SelectDeliverLocationScope
        @Provides
        fun provideAppApi(retrofit: Retrofit): ProfileServiceAPI {
            return retrofit.create(ProfileServiceAPI::class.java)
        }

        @JvmStatic
        @SelectDeliverLocationScope
        @Provides
        fun provideSavedAddressRemoteDataSource(apiServiceAPI: ProfileServiceAPI) =
            ProfileRemoteDataSource(apiServiceAPI)

        @JvmStatic
        @SelectDeliverLocationScope
        @Provides
        fun provideProfileRepository(dao: UserInfoDao, dataSource: ProfileRemoteDataSource) =
            ProfileRepository(dao, dataSource)

    }

    @Binds
    @IntoMap
    @SelectDeliverLocationScope
    @ViewModelKey(SelectDeliveryLocationViewModel::class)
    abstract fun bindSelectDeliveryLocationViewModel(viewModel: SelectDeliveryLocationViewModel?): SelectDeliveryLocationViewModel?
}