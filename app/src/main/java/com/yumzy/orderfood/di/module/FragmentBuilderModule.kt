package com.yumzy.orderfood.di.module

import com.yumzy.orderfood.di.module.fragment.*
import com.yumzy.orderfood.di.module.home.ProfileFragmentModule
import com.yumzy.orderfood.di.module.home.ProfileFragmentScope

import com.yumzy.orderfood.ui.screens.login.fragment.*
import com.yumzy.orderfood.ui.screens.profile.ProfileFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module()
abstract class FragmentBuilderModule {

    @StartedFragmentScope
    @ContributesAndroidInjector(modules = [StartedFragmentModule::class])
    abstract fun contributeStartedFragment(): StartedFragment

    @SplashFragmentScope
    @ContributesAndroidInjector(modules = [SplashFragmentModule::class])
    abstract fun contributeSplashFragment(): SplashFragment

    @OnBoardFragmentScope
    @ContributesAndroidInjector(modules = [OnBoardFragmentModule::class])
    abstract fun contributeOnBoardFragment(): OnBoardFragment

    @LoginFragmentScope
    @ContributesAndroidInjector(modules = [LoginFragmentModule::class])
    abstract fun contributeLoginFragment(): LoginFragment

    @RegisterFragmentScope
    @ContributesAndroidInjector(modules = [RegisterFragmentModule::class])
    abstract fun contributeRegisterFragment(): RegisterFragment

    @SetLocationFragmentScope
    @ContributesAndroidInjector(modules = [SetLocationFragmentModule::class])
    abstract fun contributeSetLocationFragment(): SetLocationFragment

    @FetchLocationAnimFragmentScope
    @ContributesAndroidInjector(modules = [FetchLocationAnimFragmentModule::class])
    abstract fun contributeFetchLocationAnimFragment(): FetchLocationAnimFragment

    @ProfileFragmentScope
    @ContributesAndroidInjector(modules = [ProfileFragmentModule::class])
    abstract fun contributeProfileFragment(): ProfileFragment


}
