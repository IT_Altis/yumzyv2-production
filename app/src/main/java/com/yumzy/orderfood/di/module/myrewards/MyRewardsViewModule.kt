package com.yumzy.orderfood.di.module.myrewards

import androidx.lifecycle.ViewModel
import com.yumzy.orderfood.api.RewardServiceAPI
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.rewards.RewardsViewModel
import com.yumzy.orderfood.ui.screens.rewards.data.RewardsRemoteDataSource
import com.yumzy.orderfood.ui.screens.rewards.data.RewardsRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

/**
 * Created by Bhupendra Kumar Sahu.
 */
@Module
abstract class MyRewardsViewModule {

    companion object
    {
        @JvmStatic
        @RewardsActivityScope
        @Provides
        fun provideUserDao(db: AppDatabase): UserInfoDao = db.userInfoDao()

        @JvmStatic
        @RewardsActivityScope
        @Provides
        fun provideApi(retrofit: Retrofit): RewardServiceAPI =
            retrofit.create(RewardServiceAPI::class.java)

        @JvmStatic
        @RewardsActivityScope
        @Provides
        fun provideRemoteDataSource(rewardServiceAPI: RewardServiceAPI) =
            RewardsRemoteDataSource(rewardServiceAPI)

        @JvmStatic
        @RewardsActivityScope
        @Provides
        fun provideRewardRepository(dao: UserInfoDao, dataSource: RewardsRemoteDataSource) =
            RewardsRepository(dao, dataSource)

    }
    @Binds
    @IntoMap
    @ViewModelKey(RewardsViewModel::class)
    abstract fun bindMyRewardsViewModel(viewModel: RewardsViewModel): ViewModel
}