package com.yumzy.orderfood.di.module.dialogs

import com.yumzy.orderfood.api.OrderServiceAPI
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.module.review.RateOrderViewModel
import com.yumzy.orderfood.ui.screens.module.review.data.RateOrderRemoteDataSource
import com.yumzy.orderfood.ui.screens.module.review.data.RateOrderRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class RateOrderModule {


    companion object {
        @JvmStatic
        @RateOrderScope
        @Provides
        fun provideAppApi(retrofit: Retrofit): OrderServiceAPI {
            return retrofit.create(OrderServiceAPI::class.java)
        }

        @JvmStatic
        @RateOrderScope
        @Provides
        fun provideRateOrderDataSource(apiServiceAPI: OrderServiceAPI) =
            RateOrderRemoteDataSource(apiServiceAPI)

        @JvmStatic
        @RateOrderScope
        @Provides
        fun provideRateOrderRepository(dataSource: RateOrderRemoteDataSource) =
            RateOrderRepository(dataSource)

    }


    @Binds
    @IntoMap
    @RateOrderScope
    @ViewModelKey(RateOrderViewModel::class)
    abstract fun bindRateOrderViewModel(viewModelAdd: RateOrderViewModel?): RateOrderViewModel?
}