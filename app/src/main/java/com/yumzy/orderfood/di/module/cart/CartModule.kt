package com.yumzy.orderfood.di.module.cart

import com.yumzy.orderfood.api.CartServiceAPI
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.AppDatabase_Impl
import com.yumzy.orderfood.data.dao.CartResDao
import com.yumzy.orderfood.ui.screens.cart.CartRemoteDataSource
import com.yumzy.orderfood.ui.screens.cart.CartRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

//@Module
//abstract class CartModule {
//
//    companion object {
//
//    }
//
//}