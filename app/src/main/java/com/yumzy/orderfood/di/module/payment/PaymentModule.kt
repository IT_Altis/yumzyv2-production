package com.yumzy.orderfood.di.module.payment

import com.yumzy.orderfood.api.CartServiceAPI
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.dao.CartResDao
import com.yumzy.orderfood.data.dao.OrdersDao
import com.yumzy.orderfood.di.module.reorder.ReorderActivityScope
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.home.fragment.PlateRemoteDataSource
import com.yumzy.orderfood.ui.screens.home.fragment.PlateViewModel
import com.yumzy.orderfood.ui.screens.payment.PaymentRemoteDataSource
import com.yumzy.orderfood.ui.screens.payment.PaymentRepository
import com.yumzy.orderfood.ui.screens.payment.PaymentViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit


@Module
abstract class PaymentModule {
    companion object {

        @JvmStatic
        @PaymentActivityScope
        @Provides
        fun providePlateService(retrofit: Retrofit): CartServiceAPI {
            return retrofit.create(CartServiceAPI::class.java)
        }

        @JvmStatic
        @PaymentActivityScope
        @Provides
        fun providePaymentRemoteDataSource(apiService: CartServiceAPI) =
            PaymentRemoteDataSource(apiService)


        @JvmStatic
        @PaymentActivityScope
        @Provides
        fun provideLocalDao(appDatabase: AppDatabase) : OrdersDao {
            return appDatabase.ordersDao()
        }


        @JvmStatic
        @PaymentActivityScope
        @Provides
        fun providePaymentRepository(remoteDataSource: PaymentRemoteDataSource, localDataSource: OrdersDao) =
            PaymentRepository(remoteDataSource, localDataSource)

    }

    @Binds
    @IntoMap
    @PaymentActivityScope
    @ViewModelKey(PaymentViewModel::class)
    abstract fun bindPlateViewModel(viewModel: PaymentViewModel?): PaymentViewModel?

}
