/*
 * Created By Shriom Tripathi 10 - 5 - 2020
 */

package com.yumzy.orderfood.di.module.helpsubheading

import androidx.lifecycle.ViewModel
import com.yumzy.orderfood.api.ProfileServiceAPI
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.helpfaq.faq.HelpSubHeadingViewModel
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRemoteDataSource
import com.yumzy.orderfood.ui.screens.profile.data.ProfileRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class HelpSubHeadingModule {


    companion object {

        @HelpSubHeadingActivityScope
        @Provides
        fun provideUserDao(db: AppDatabase): UserInfoDao = db.userInfoDao()


        @HelpSubHeadingActivityScope
        @Provides
        fun provideAppApi(retrofit: Retrofit): ProfileServiceAPI {
            return retrofit.create(ProfileServiceAPI::class.java)
        }


        @HelpSubHeadingActivityScope
        @Provides
        fun provideProfileRemoteDataSource(apiServiceAPI: ProfileServiceAPI) =
            ProfileRemoteDataSource(apiServiceAPI)


        @HelpSubHeadingActivityScope
        @Provides
        fun provideProfileRepository(dao: UserInfoDao, dataSource: ProfileRemoteDataSource) =
            ProfileRepository(dao, dataSource)
    }

    @Binds
    @IntoMap
    @ViewModelKey(HelpSubHeadingViewModel::class)
    abstract fun bindHelpSubHeadingViewModel(viewModel: HelpSubHeadingViewModel): ViewModel
}