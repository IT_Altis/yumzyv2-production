package com.yumzy.orderfood.di.module.home

import com.yumzy.orderfood.data.dao.UserInfoDao
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.home.data.ExploreRepository
import com.yumzy.orderfood.ui.screens.home.data.HomeRemoteDataSource
import com.yumzy.orderfood.ui.screens.home.fragment.ExploreViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap


@Module
abstract class ExploreModule {

    companion object {
//        @JvmStatic
//        @ExploreFragmentScope
//        @Provides
//        fun provideUserDao(db: AppDatabase): LocalSearchItemDao = db.localSearchItemDao()

        @JvmStatic
        @ExploreFragmentScope
        @Provides
        fun provideLoginRepository(
            userInfoDao: UserInfoDao,
//            localSearchItemDao: LocalSearchItemDao,
            homeRemoteDataSource: HomeRemoteDataSource
        ) =
            ExploreRepository(
                userInfoDao
//                , localSearchItemDao
                , homeRemoteDataSource
            )

    }

    @Binds
    @IntoMap
    @ExploreFragmentScope
    @ViewModelKey(ExploreViewModel::class)
    abstract fun bindExploreViewModel(viewModel: ExploreViewModel?): ExploreViewModel?

}
