package com.yumzy.orderfood.di.module.reorder

import androidx.lifecycle.ViewModel
import com.yumzy.orderfood.api.OrderServiceAPI
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.dao.OrdersDao
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.orderhistory.data.OrderRemoteDataSource
import com.yumzy.orderfood.ui.screens.orderhistory.data.OrderRepository
import com.yumzy.orderfood.ui.screens.reorder.ReorderViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

/**
 * Created by Bhupendra Kumar Sahu.
 */
@Module
abstract class ReorderViewModule {
    companion object
    {
        @JvmStatic
        @ReorderActivityScope
        @Provides
        fun provideAppApi(retrofit: Retrofit): OrderServiceAPI {
            return retrofit.create(OrderServiceAPI::class.java)
        }

        @JvmStatic
        @ReorderActivityScope
        @Provides
        fun provideOrderRemoteDataSource(orderServiceApi: OrderServiceAPI) =
            OrderRemoteDataSource(orderServiceApi)


        @JvmStatic
        @ReorderActivityScope
        @Provides
        fun provideLocalDao(appDatabase: AppDatabase) : OrdersDao {
            return appDatabase.ordersDao()
        }


        @JvmStatic
        @ReorderActivityScope
        @Provides
        fun provideOrderRepository(remoteSource: OrderRemoteDataSource, localSource: OrdersDao) =
            OrderRepository(remoteSource, localSource)

    }
    @Binds
    @IntoMap
    @ViewModelKey(ReorderViewModel::class)
    abstract fun bindReorderViewModel(viewModel: ReorderViewModel): ViewModel
}