package com.yumzy.orderfood.di.module.earncoupon

import androidx.lifecycle.ViewModel
import com.yumzy.orderfood.api.CartServiceAPI
import com.yumzy.orderfood.di.scope.ViewModelKey
import com.yumzy.orderfood.ui.screens.earncoupon.EarnCouponViewModel
import com.yumzy.orderfood.ui.screens.earncoupon.data.EarnCouponRemoteDataSource
import com.yumzy.orderfood.ui.screens.earncoupon.data.EarnCouponRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

/**
 * Created by Bhupendra Kumar Sahu.
 */
@Module
abstract class EarnCouponViewModule {

    companion object {

        @JvmStatic
        @EarnCouponActivityScope
        @Provides
        fun provideAppApi(retrofit: Retrofit): CartServiceAPI {
            return retrofit.create(CartServiceAPI::class.java)
        }


        @JvmStatic
        @EarnCouponActivityScope
        @Provides
        fun provideEarnAndCouponsRemoteDataSource(cartServiceApi: CartServiceAPI) =
            EarnCouponRemoteDataSource(cartServiceApi)

        @JvmStatic
        @EarnCouponActivityScope
        @Provides
        fun provideEarnAndCouponsRepository(dataSource: EarnCouponRemoteDataSource) =
            EarnCouponRepository(dataSource)

    }

    @Binds
    @IntoMap
    @ViewModelKey(EarnCouponViewModel::class)
    abstract fun bindEarnAndCouponsViewModel(viewModel: EarnCouponViewModel): ViewModel
}