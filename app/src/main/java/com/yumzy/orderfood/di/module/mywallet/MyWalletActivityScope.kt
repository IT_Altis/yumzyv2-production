package com.yumzy.orderfood.di.module.mywallet

import javax.inject.Scope

@Scope
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class MyWalletActivityScope