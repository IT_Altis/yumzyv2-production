package com.yumzy.orderfood.tools.appstart

import android.content.Context
import androidx.startup.Initializer
import com.yumzy.orderfood.AppGlobalObjects


class AppStartInitializer : Initializer<AppGlobalObjects> {

    override fun create(context: Context): AppGlobalObjects {
        return AppGlobalObjects(context)
    }

    override fun dependencies() = emptyList<Nothing>()

}

