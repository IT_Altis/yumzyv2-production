package com.yumzy.orderfood.tools.update;

import android.content.Context;
import android.content.pm.PackageManager;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.yumzy.orderfood.BuildConfig;
import com.yumzy.orderfood.util.IConstants;

import timber.log.Timber;

import static com.yumzy.orderfood.AppGlobalObjectsKt.appRemoteConfig;

public class RemoteConfigUpdate {

    public static final String KEY_UPDATE_REQUIRED = "force_update_required";
    public static final String KEY_CURRENT_VERSION = "force_update_current_version";
    public static final String KEY_UPDATE_URL = "force_update_store_url";
    public static final String TRACKING_FREQUENCY = "tracking_frequency";
    public static final String INVITE_TEXT = "invite_text";
    public static final String SCRATCH_CARD_INVITE_TEXT = "scratch_card_invite_text";

    private static final String TAG = RemoteConfigUpdate.class.getSimpleName();
    private OnRemoteConfigListener onRemoteConfigListener;
    private Context context;

    public RemoteConfigUpdate(@NonNull Context context,
                              OnRemoteConfigListener onRemoteConfigListener) {
        this.context = context;
        this.onRemoteConfigListener = onRemoteConfigListener;
    }

    public static Builder with(@NonNull Context context) {
        return new Builder(context);
    }

    public void check() {

        appRemoteConfig.fetchAndActivate()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {

                        //check for update only when release
                        if (!BuildConfig.DEBUG && appRemoteConfig.getBoolean(KEY_UPDATE_REQUIRED)) {
                            String currentVersion = appRemoteConfig.getString(KEY_CURRENT_VERSION);
                            String appVersion = getAppVersion(context);
                            String updateUrl = appRemoteConfig.getString(KEY_UPDATE_URL);

                            if (!TextUtils.equals(currentVersion, appVersion) && onRemoteConfigListener != null) {
                                onRemoteConfigListener.onUpdateNeeded(updateUrl);
                            }
                        }

                        //update order tracking frequency
                        long tracking_frequency = appRemoteConfig.getLong(TRACKING_FREQUENCY);
                        if (tracking_frequency != 0L) {
                            IConstants.OrderTracking.Companion.setORDER_REFRESH_RATE(tracking_frequency);
                        }

                        //update invite string
                        String invite_text = appRemoteConfig.getString(INVITE_TEXT);
                        if (!invite_text.equals("")) {
                            IConstants.AppConstant.Companion.setINVITE_TEXT(invite_text);
                        }

                        //update scratch card invite string
                        String scratch_card_invite_text = appRemoteConfig.getString(SCRATCH_CARD_INVITE_TEXT);
                        if (!scratch_card_invite_text.equals("")) {
                            IConstants.AppConstant.Companion.setSCRATCH_CARD_INVITE_TEXT(scratch_card_invite_text);
                        }
                    }
                });
    }

    private String getAppVersion(Context context) {
        String result = "";

        try {
            result = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0)
                    .versionName;
            result = result.replaceAll("[a-zA-Z]|-", "");
        } catch (PackageManager.NameNotFoundException e) {
            Timber.e(e);
        }

        return result;
    }

    public interface OnRemoteConfigListener {
        void onUpdateNeeded(String updateUrl);
    }

    public static class Builder {

        private Context context;
        private OnRemoteConfigListener onRemoteConfigListener;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder onUpdateNeeded(OnRemoteConfigListener onRemoteConfigListener) {
            this.onRemoteConfigListener = onRemoteConfigListener;
            return this;
        }

        public RemoteConfigUpdate build() {
            return new RemoteConfigUpdate(context, onRemoteConfigListener);
        }

        public RemoteConfigUpdate check() {
            RemoteConfigUpdate forceUpdateChecker = build();
            forceUpdateChecker.check();

            return forceUpdateChecker;
        }
    }
}
