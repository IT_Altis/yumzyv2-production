package com.yumzy.orderfood.tools

import androidx.appcompat.app.AppCompatActivity
import com.yumzy.orderfood.appRemoteConfig


object RemoteConfigHelper {
    var display_limit: Int = 10
    var tracking_frequency: Long = 20000L
    var search_threshold: Int = 2

    var explore_initial_title = "Search &\nExplore"
    var explore_initial_subtitle = "Search dishes and restaurant"
    var explore_threshold_title = "Search &\nExplore"
    var explore_threshold_subtitle = "Search dishes and restaurant"
    var explore_result_empty = "Sorry, Unable to find"

    var empty_cart_title = ""
    var empty_cart_subtitle = ""
    var non_empty_cart_title = ""
    var non_empty_cart_subtitle = ""

    private var configUpdated = false

    private fun trackingHitTime(activity: AppCompatActivity) {
        appRemoteConfig.fetchAndActivate()
            .addOnCompleteListener(activity) { task ->
                tracking_frequency = if (task.isSuccessful) {
                    appRemoteConfig.getLong("tracking_frequency")
                } else
                    20000L
            }

    }

    private fun displayLimit(activity: AppCompatActivity) {
        appRemoteConfig.fetchAndActivate()
            .addOnCompleteListener(activity) { task ->
                display_limit = if (task.isSuccessful) {
                    val toInt = appRemoteConfig.getLong("display_limit").toInt()
                    if (toInt > 0)
                        toInt
                    else
                        10
                } else
                    10
            }

    }

    private fun searchThreshold(activity: AppCompatActivity) {
        appRemoteConfig.fetchAndActivate()
            .addOnCompleteListener(activity) { task ->
                if (task.isSuccessful) {
                    tracking_frequency = appRemoteConfig.getLong("search_threshold")
                } else
                    tracking_frequency = 20000L
            }
    }

    private fun exploreMessage(activity: AppCompatActivity) {
        appRemoteConfig.fetchAndActivate()
            .addOnCompleteListener(activity) { task ->
                if (task.isSuccessful) {
                    explore_initial_title = appRemoteConfig.getString("explore_initial_title")
                    explore_initial_subtitle = appRemoteConfig.getString("explore_initial_subtitle")
                    explore_threshold_title = appRemoteConfig.getString("explore_threshold_title")
                    explore_threshold_subtitle =
                        appRemoteConfig.getString("explore_threshold_subtitle")
                    explore_result_empty = appRemoteConfig.getString("explore_result_empty")
                }
            }
    }


    private fun emptyCartMessage(activity: AppCompatActivity) {
        appRemoteConfig.fetchAndActivate()
            .addOnCompleteListener(activity) { task ->
                if (task.isSuccessful) {
                    empty_cart_title = appRemoteConfig.getString("empty_cart_title")
                    empty_cart_subtitle = appRemoteConfig.getString("empty_cart_subtitle")
                    non_empty_cart_title = appRemoteConfig.getString("non_empty_cart_title")
                    non_empty_cart_subtitle = appRemoteConfig.getString("non_empty_cart_subtitle")
                }
            }
    }


    fun fetchRemoteData(baseActivity: AppCompatActivity) {
        if (configUpdated) return
        trackingHitTime(baseActivity)
        searchThreshold(baseActivity)
        displayLimit(baseActivity)
        exploreMessage(baseActivity)
        emptyCartMessage(baseActivity)
        configUpdated = true
    }
}