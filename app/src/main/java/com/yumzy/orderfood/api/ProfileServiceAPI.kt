package com.yumzy.orderfood.api

import com.yumzy.orderfood.data.models.*
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.ResponseDTO
import retrofit2.Response
import retrofit2.http.*

interface ProfileServiceAPI {
    @GET(APIConstant.URL.PROFILE)
    suspend fun profileUser(): Response<ResponseDTO<ProfileResponseDTO>>

    @POST(APIConstant.URL.PROFILE)
    suspend fun updateProfileUser(@Query("action") update: String, @Body map: Map<String, String>): Response<ResponseDTO<Any>>

    @POST(APIConstant.URL.PROFILE)
    suspend fun userUpdateProfileImage(@Query("action") update: String, @Body pro: MutableMap<String, Any>): Response<ResponseDTO<Any>>

    @GET(APIConstant.URL.ADDRESS)
    suspend fun getAddressList(): Response<ResponseDTO<List<AddressDTO>>>

    @GET(APIConstant.URL.FOOD_PREFERENCES)
    suspend fun getlikedPrefTags(): Response<ResponseDTO<List<FoodPreferenceDTO>>>

    @PUT(APIConstant.URL.ADDRESS)
    suspend fun updateAddress(@Body address: AddressDTO): Response<ResponseDTO<Any>>

    @POST(APIConstant.URL.ADDRESS)
    suspend fun addAddress(@Body address: AddressDTO): Response<ResponseDTO<Any>>

    @DELETE(APIConstant.URL.ADDRESS)
    suspend fun deleteAddress(@Query("id") id: String): Response<ResponseDTO<Any>>

    @GET(APIConstant.URL.HELP)
    suspend fun getHelpHeadings(): Response<ResponseDTO<List<HelpHeadingDTO>>>

    @GET(APIConstant.URL.HELP)
    suspend fun getHelpSubheadings(@Query("headingId") headingId: Int): Response<ResponseDTO<SubHeadingDTO>>

    @POST(APIConstant.URL.FOOD_PREFERENCES_TAG)
    suspend fun getFoodPreferenceTags(@Body dto: PersonalisationDTO): Response<ResponseDTO<ArrayList<PersonalisationResponseDTO>>>

    @POST(APIConstant.URL.PROFILE)
    suspend fun foodPres(@Query("action") update: String, @Body dto: FoodPersReqDTO): Response<ResponseDTO<FoodPersResDTO>>

    @GET(APIConstant.URL.GET_FAV)
    suspend fun getFavouriteRestaurant(): Response<ResponseDTO<FavRestaurantResDTO>>

    @GET(APIConstant.URL.WALLET_TRANSACTIONS)
    suspend fun getWalletTransactions(@Query("skip") skip: Int, @Query("limit") limit: Int, @Query("mobile") mobile: Long ): Response<ResponseDTO<List<WalletTransactionDTO>>>

    @GET(APIConstant.URL.WALLET_DETAIL)
    suspend fun getUserWallet(@Query("mobile") mobile: Long): Response<ResponseDTO<WalletDTO>>

    @GET(APIConstant.URL.VALIDATE_VPA)
    suspend fun verifyVPA(@Query("vpa") vpa: String): Response<ResponseDTO<VPIDTO>>

    @POST(APIConstant.URL.WITHDRAW_INITIATE)
    suspend fun withdrawBalance(@Body withDraw: MutableMap<String, Any>): Response<ResponseDTO<WithdrawDTO>>

    @POST(APIConstant.URL.LOGOUT)
    suspend fun logoutUser(): Response<ResponseDTO<Any>>
}