/*
 *   Created by Sourav Kumar Pandit  28 - 4 - 2020
 */

package com.yumzy.orderfood.api

import com.yumzy.orderfood.data.ExploreSearchDTO
import com.yumzy.orderfood.data.cart.CartResponseDTO
import com.yumzy.orderfood.data.models.*
import com.yumzy.orderfood.data.models.appstart.AppStartDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.ResponseDTO
import retrofit2.Response
import retrofit2.http.*

@JvmSuppressWildcards
interface HomeServiceAPI {

    @GET(APIConstant.URL.PROFILE)
    suspend fun getProfile(): Response<ResponseDTO<Any>>

    @GET(APIConstant.URL.PROFILE)
    suspend fun profileUser(): Response<ResponseDTO<ProfileResponseDTO>>

    @GET(APIConstant.URL.HOME_FOOD)
    suspend fun homeData(
        @Query("lat") lat: String,
        @Query("lng") lng: String,
        @Query("skip") skip: Int,
        @Query("limit") limit: Int
    ): Response<ResponseDTO<ArrayList<HomeSectionDTO>>>

    @Headers("Cache-Control: no-cache")
    @GET(APIConstant.URL.HOME_FOOD)
    suspend fun getFlatHomeData(
        @Query("lat") lat: String,
        @Query("lng") lng: String,
        @Query("skip") skip: Int,
        @Query("limit") limit: Int
    ): Response<ResponseDTO<ArrayList<HomeSectionDTO>>>

    @GET(APIConstant.URL.SEARCH)
    suspend fun getSearchSuggestion(@Query("searchText") text: String): Response<ResponseDTO<ExploreSearchDTO>>

    @POST(APIConstant.URL.LOGOUT)
    suspend fun logoutUser(): Response<ResponseDTO<Any>>


    @POST(APIConstant.URL.UPDATE_LOCATION)
    suspend fun updateLocation(@Body updateLocation: UpdateLocationDTO): Response<ResponseDTO<Any>>

    @GET(APIConstant.URL.DISHES)
    suspend fun getTopDishes(
        @Query("promoId") promoID: String,
        @Query("lat") lat: String,
        @Query("lng") lng: String,
        @Query("skip") skip: Int,
        @Query("limit") limit: Int
    ): Response<ResponseDTO<DishesResDTO>>

    @GET(APIConstant.URL.RESTAURANTS)
    suspend fun getRestaurants(
        @Query("promoId") promoID: String,
        @Query("lat") lat: String,
        @Query("lng") lng: String,
        @Query("skip") skip: Int,
        @Query("limit") limit: Int
    ): Response<ResponseDTO<RestaurantsResDTO>>

    @POST(APIConstant.URL.CART)
    suspend fun addToCart(@Body addcart: NewAddItemDTO): Response<ResponseDTO<AddCartResDTO>>

    //    {dev}}/food/expandSearch?itemId=56dc1f23-7898-11ea-be47-81370ee7cace&skip=0&limit=20
    /*@GET(APIConstant.URL.EXPAND_SEARCH)
    suspend fun expandSearch(
        @Query("itemId") itemId: String,
        @Query("skip") skip: Int,
        @Query("limit") limit: Int
    ): Response<ResponseDTO<ExploreResultDTO>>*/

    @Headers("Cache-Control: no-cache")
    @PATCH(APIConstant.URL.ITEM_COUNT + "/{position}/{count}")
    suspend fun itemCount(
        @Path("position") position: Int, @Path("count") count: Int, @Body itemCount: ItemCountDTO
    ): Response<ResponseDTO<CartResDTO>>

    @Headers("Cache-Control: no-cache")
    @GET(APIConstant.URL.POST_CHECKOUT + "/{orderId}")
    suspend fun postCheckout(@Query("orderId") orderId: String): Response<ResponseDTO<Any>>

    @HTTP(method = "DELETE", path = APIConstant.URL.CLEAR_CART, hasBody = true)
    suspend fun clearCart(@Body cartId: ClearCartReqDTO): Response<ResponseDTO<Any>>

    @Headers("Cache-Control: no-cache")
    @GET(APIConstant.URL.CART)
    suspend fun getCart(@Query("cartId") cartID: String): Response<ResponseDTO<CartResponseDTO>>

    @Headers("Cache-Control: no-cache")
    @GET(APIConstant.URL.APP_START)
    suspend fun getAppStart(): Response<ResponseDTO<AppStartDTO>>

    @GET(APIConstant.URL.ADDRESS)
    suspend fun getAddressList(): Response<ResponseDTO<List<AddressDTO>>>

    @Headers("Cache-Control: no-cache")
    @PATCH(APIConstant.URL.UPDATE_FCM)
    suspend fun updateUserFcmToken(@Body token: Map<String, String>): Response<ResponseDTO<Any>>

    @Headers("Cache-Control: no-cache")
    @POST(APIConstant.URL.PRE_CHECKOUT)
    suspend fun preCheckout(@Body preCheckout: PreCheckoutReqDTO): Response<ResponseDTO<PreCheckoutResDTO>>

    @POST(APIConstant.URL.APPLY_REFERRAL)
    suspend fun applyReferral(
        @Query("userId") userId: String,
        @Body referralMap: Map<String, String>
    ): Response<ResponseDTO<Any>>

    @POST(APIConstant.URL.CONTACTS)
    suspend fun saveUserContacts(@Body userContacts: Map<String, Any>): Response<ResponseDTO<Any>>

    @GET(APIConstant.URL.SCRATCH_CARD_SUMMARY)
    suspend fun getScratchCardInfo(): Response<ResponseDTO<ScratchSummaryDTO>>
}
