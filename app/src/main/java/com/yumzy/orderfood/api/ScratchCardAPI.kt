package com.yumzy.orderfood.api

import com.yumzy.orderfood.data.models.ScratchCardDTO
import com.yumzy.orderfood.data.models.ScratchCardDetailDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.ResponseDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Query

interface ScratchCardAPI {

    /*
        @POST(APIConstant.URL.APPLY_REFERRAL)
        suspend fun applyReferral(
            @Query("userId")
            userId: String,
            @Body mapReferral: Map<String, String>
        ): Response<ResponseDTO<Any>>*/
    @GET(APIConstant.URL.SCRATCH_CARD_LIST)
    suspend fun fetchScratchCardList(
        @Query("skip") skip: Int,
        @Query("limit") limit: Int
    ): Response<ResponseDTO<List<ScratchCardDTO>>>

    @GET(APIConstant.URL.SCRATCH_CARD_INFO)
    suspend fun getScratchCardDetails(): Response<ResponseDTO<ScratchCardDetailDTO>>

    @PUT(APIConstant.URL.REDEEM_SCRATCH)
    suspend fun redeemScratchCard(
        @Query("scratchId") scratchId: String,
        @Query("mobile") phone: String?
    ): Response<ResponseDTO<Any>>

}
