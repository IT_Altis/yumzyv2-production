package com.yumzy.orderfood.api

import com.yumzy.orderfood.data.models.*
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.ResponseDTO
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.PATCH
import retrofit2.http.POST

interface LoginServiceAPI {


    @POST(APIConstant.URL.SIGN_UP)
    suspend fun registerUser(@Body registerDTO: RegisterDTO?): Response<ResponseDTO<LoginDTO>>

    @POST(APIConstant.URL.SEND_OTP)
    suspend fun setOtpTo(@Body sendOtpDTO: SendOtpDTO): Response<ResponseDTO<Any>>

    @POST(APIConstant.URL.VERIFY_OTP)
    suspend fun verifyOtp(@Body verifyOtpDTO: VerifyOtpDTO): Response<ResponseDTO<LoginDTO>>

    @POST(APIConstant.URL.EXPLORE_USER)
    suspend fun exploreUser(@Body exploreUser: ExploreUser): Response<ResponseDTO<LoginDTO>>

    @POST(APIConstant.URL.RESEND_OTP)
    suspend fun resendOtp(@Body verifyOtpDTO: Map<String, String>): Response<ResponseDTO<Any>>

    @POST(APIConstant.URL.SIGN_OUT)
    suspend fun signOut(): Response<ResponseDTO<Any>>

    @PATCH(APIConstant.URL.UPDATE_FCM)
    suspend fun updateUserFcmToken(@Body token: Map<String, String>): Response<ResponseDTO<Any>>

    @POST(APIConstant.URL.UPDATE_LOCATION)
    suspend fun updateLocation(@Body updateLocation: UpdateLocationDTO): Response<ResponseDTO<Any>>

}
