package com.yumzy.orderfood.api

import com.yumzy.orderfood.appGson
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.ResponseDTO
import com.yumzy.orderfood.util.IConstants
import retrofit2.Response
import timber.log.Timber

/**
 * Abstract Base Data source class with error handling
 */
@Suppress("UNCHECKED_CAST")
abstract class BaseDataSource {
    protected suspend fun <T : ResponseDTO<*>> getResult(call: suspend () -> Response<T>): T {
        try {
            val response = call()
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) return body
            } else if (response.body() is ResponseDTO<*>) {
                return ResponseDTO.error(
                    title = response.body()?.title,
                    message = response.body()?.message,
                    data = response.body()?.data,
                    code = response.body()?.code,
                    displayType = response.body()?.displayType
                ) as T

            }
            val errorBody = response.errorBody()
            if (errorBody != null) {
                val responseDTO = appGson.fromJson(errorBody.string(), ResponseDTO::class.java)
                return ResponseDTO.error(
                    title = responseDTO.title,
                    message = responseDTO.message,
                    data = responseDTO.data,
                    code = responseDTO.code,
                    displayType = responseDTO.displayType
                ) as T
            }
            /*Your app should never come at this point*/
            return ResponseDTO.error(
                title = IConstants.ResponseError.UNKNOWN_ERROR,
                message = IConstants.ResponseError.SOMTHING_HAPPENED,
                data = null,
                code = APIConstant.Status.UNKNOWN,
                displayType = ""
            ) as T
        } catch (e: java.net.UnknownHostException) {
            return ResponseDTO.error(
                title = IConstants.ResponseError.NO_INTERNET,
                message = IConstants.ResponseError.NOT_CONNECTED,
                data = null,
                code = APIConstant.Status.NO_NETWORK,
                displayType = ""
            ) as T
        } catch (e: java.net.ConnectException) {
            return ResponseDTO.error(
                title = IConstants.ResponseError.NO_INTERNET,
                message = IConstants.ResponseError.NOT_CONNECTED,
                data = null,
                code = APIConstant.Status.NO_NETWORK,
                displayType = ""
            ) as T
        } catch (e: Exception) {
            Timber.e(e)
            /*Try to  never Reach this point*/
            return ResponseDTO.error(
                type = APIConstant.Code.UNKNOWN,
                title = IConstants.ResponseError.OOPS_SOMTHING_HAPPENED,
                message = e.message,
                data = null,
                code = 0,
                displayType = ""
            ) as T
        }
    }
}

