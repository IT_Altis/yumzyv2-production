package com.yumzy.orderfood.api

import com.yumzy.orderfood.data.models.*
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.ResponseDTO
import com.yumzy.orderfood.data.models.orderdetails.NewOrderDetailsDTO
import retrofit2.Response
import retrofit2.http.*
import javax.inject.Singleton

@Singleton
interface CartServiceAPI {
    @GET(APIConstant.URL.FOOD_MENU)
    suspend fun foodMenuDetails(
        @Query("outletId") outletId: String,
        @Query("userId") userId: String
    ): Response<ResponseDTO<RestaurantDTO>>

    @Headers("Cache-Control: no-cache")
    @POST(APIConstant.URL.CART)
    suspend fun addToCart(@Body addcart: NewAddItemDTO): Response<ResponseDTO<CartResDTO>>

    @POST(APIConstant.URL.PRE_CHECKOUT)
    suspend fun preCheckout(@Body preCheckout: PreCheckoutReqDTO): Response<ResponseDTO<PreCheckoutResDTO>>

    @POST(APIConstant.URL.APPLY_COUPON)
    suspend fun applyCoupon(@Body applyCoupon: MutableMap<String, Any>): Response<ResponseDTO<CartResDTO>>

    @Headers("Cache-Control: no-cache")
    @POST(APIConstant.URL.NEW_ORDER)
    suspend fun newOrder(@Body newOrder: NewOrderReqDTO): Response<ResponseDTO<Any>>

    @Headers("Cache-Control: no-cache")
    @GET(APIConstant.URL.POST_CHECKOUT)
    suspend fun postCheckout(@Query("orderId") orderId: String) : Response<ResponseDTO<PostCheckoutResDTO>>

    @Headers("Cache-Control: no-cache")
    @GET(APIConstant.URL.COUPON_LIST)
    suspend fun getFetchCouponList(
        @Query("userId") userID: String,
        @Query("cartId") cartId: String?,
        @Query("outletId") outletId: String?
    ): Response<ResponseDTO<List<CouponListResDTO>>>

    @Headers("Cache-Control: no-cache")
    @GET(APIConstant.URL.CART)
    suspend fun getCart(@Query("cartId") cartID: String): Response<ResponseDTO<CartResDTO>>

    @Headers("Cache-Control: no-cache")
    @POST(APIConstant.URL.ADD_FAV)
    suspend fun addFav(@Body addFavReqDTO:  MutableMap<String, Any>): Response<ResponseDTO<Any>>

    @Headers("Cache-Control: no-cache")
    @PATCH(APIConstant.URL.ITEM_COUNT + "/{position}/{count}")
    suspend fun itemCount(
        @Path("position") position: Int, @Path("count") count: Int, @Body itemCount: ItemCountDTO
    ): Response<ResponseDTO<CartResDTO>>

    @Headers("Cache-Control: no-cache")
    @PUT(APIConstant.URL.ITEM_COUNT + "/{position}")
    suspend fun updateItem(
        @Path("position") position: Int, @Body addCart: NewAddItemDTO
    ): Response<ResponseDTO<CartResDTO>>

    @HTTP(method = "DELETE", path = APIConstant.URL.CLEAR_CART, hasBody = true)
    suspend fun clearCart(@Body cartId: Map<String, String>): Response<ResponseDTO<Any>>

    @HTTP(method = "DELETE", path = APIConstant.URL.REMOVE_COUPON, hasBody = true)
    suspend fun removeCoupon(@Body removeCouponReqDTO: MutableMap<String, Any>): Response<ResponseDTO<CartResDTO>>

    @GET(APIConstant.URL.ADDRESS)
    suspend fun getAddressList(): Response<ResponseDTO<List<AddressDTO>>>

    @GET(APIConstant.URL.ORDER_DETAILS)
    suspend fun orderDetails(@Query("orderId") id: String): Response<ResponseDTO<NewOrderDetailsDTO>>
    @GET(APIConstant.URL.WALLET_DETAIL)
    suspend fun getUserWallet(@Query("mobile") mobile: Long): Response<ResponseDTO<WalletDTO>>

    @POST(APIConstant.URL.USE_WALLET)
    suspend fun useWallet(@Body map: HashMap<String, String>): Response<ResponseDTO<PreCheckoutResDTO>>

    @POST(APIConstant.URL.UNUSE_WALLET)
    suspend fun unUseWallet(@Body map: HashMap<String, String>): Response<ResponseDTO<PreCheckoutResDTO>>
}
