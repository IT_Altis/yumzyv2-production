package com.yumzy.orderfood.ui.screens.fastitems

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.ItemLoadingBinding

class LoadingItem : AbstractBindingItem<ItemLoadingBinding>() {
    override val type: Int
        get() = R.id.progress_loading_item

    override fun bindView(binding: ItemLoadingBinding, payloads: List<Any>) {
    }

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ItemLoadingBinding {
        return ItemLoadingBinding.inflate(inflater, parent, false)
    }
}

