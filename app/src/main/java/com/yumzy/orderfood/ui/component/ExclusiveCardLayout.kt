package com.yumzy.orderfood.ui.component

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.common.imageview.RoundedImageView
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.setInterFont
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.YUtils
import com.yumzy.orderfood.util.viewutils.YumUtil
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView
import com.laalsa.laalsalib.ui.VUtil


/**
 *    Created by Sourav Kumar Pandit
 * */
class ExclusiveCardLayout : CardView {


    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {


        initAttrs(attrs, context, defStyleAttr)
        initView()


    }

    lateinit var discountDrawable: Drawable
    var availableRestaurant: Boolean = true
        set(value) {
            field = value
            if (value)
                this.alpha = 0.5f
            else
                this.alpha = 1f

        }
    var icon: String = ""
        set(value) {
            this.findViewById<TextView>(R.id.font_icon_view)?.text = value
            field = value
        }
    var itemTitle: String = ""
        set(value) {
            this.findViewById<TextView>(R.id.tv_item_name)?.text = value
            field = value

        }
    var itemSubtitle: String = ""
        set(value) {
            this.findViewById<TextView>(R.id.tv_item_tag)?.text = value
            field = value
        }

    var coupon: String = ""
        set(value) {
            this.findViewById<TextView>(R.id.tv_coupon_code)?.text = value
            field = value
        }
    var imageUrl: String = ""
        set(value) {
            field = value

            val imageView = this.findViewById<ImageView>(R.id.restaurant_image)
//            Glide.with(imageView).load(value).into(imageView)
            YUtils.setImageInView(imageView,imageUrl)

        }

    var isVeg: Boolean = true
        set(value) {
            field = value
            val view = this.findViewWithTag<FontIconView>("icon_veg")
            if (value) {
                view.setTextColor(ThemeConstant.greenAccent)
            } else {
                view.setTextColor(ThemeConstant.redAccent)
            }
        }

    var removeAddQuantity: Boolean = false
        set(value) {
            field = value
            val view = this.findViewById<AddQuantityView>(R.id.add_quantity_view)

            if (removeAddQuantity)
                view.visibility = View.GONE
            else
                view.visibility = View.VISIBLE

        }

    var removeVegIcon: Boolean = false
        set(value) {
            field = value
            val view = this.findViewWithTag<FontIconView>("icon_veg")
            if (value) {
                view.visibility = View.GONE
            } else {
                view.visibility = View.VISIBLE
            }
        }

    var price: String = ""
        set(value) {
            this.findViewById<TextView>(R.id.tv_time_price)?.text = value
            field = value
        }
//    private var restaurantCardListener: RestaurantCardListener? = null


    private fun initAttrs(
        attrs: AttributeSet?,
        context: Context?,
        defStyleAttr: Int
    ) {
        if (attrs != null) {
            val a =
                context!!.obtainStyledAttributes(
                    attrs,
                    R.styleable.VerticalCardLayout,
                    defStyleAttr,
                    0
                )

            itemTitle = a.getString(R.styleable.VerticalCardLayout_verticalCardTitle) ?: ""
            itemSubtitle = a.getString(R.styleable.VerticalCardLayout_verticalCardSubtitle) ?: ""
            coupon = a.getString(R.styleable.VerticalCardLayout_verticalCardCouponCode) ?: ""
            price = a.getString(R.styleable.VerticalCardLayout_verticalCardPrice) ?: ""
            discountDrawable =
                YumUtil.getIcon(context, R.drawable.ic_discount, ThemeConstant.pinkies)!!

            a.recycle()

        }
//        else {
//        }
    }

    private fun initView() {

        this.layoutParams =

            ViewGroup.LayoutParams(ViewConst.WRAP_CONTENT, ViewConst.WRAP_CONTENT)
        val constraintLayout = ConstraintLayout(context)

        val roundImage = restaurantImageView()

        val itemNameLayout = LinearLayout(context).apply {
            id = R.id.card_top_layout
            orientation = LinearLayout.VERTICAL
            val layoutParams = ConstraintLayout.LayoutParams(0, ViewConst.WRAP_CONTENT).apply {
                endToEnd = R.id.restaurant_image
                startToStart = R.id.restaurant_image
                topToBottom = R.id.restaurant_image
            }
            setPadding(0, UIHelper.i3px, 0, UIHelper.i25px)
        }

        itemNameLayout.addView(getLabeledTextView(itemTitle, 14f, R.id.tv_item_name))
        itemNameLayout.addView(getTextView(itemSubtitle, 12f, R.id.tv_item_tag))


        val priceQuantityView: LinearLayout = getPriceQuantityView(price, 12f, R.id.tv_item_price)
        val bottomParam =
            ConstraintLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        priceQuantityView.setPadding(0, UIHelper.i3px, 0, 0)

        bottomParam.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
        bottomParam.topToBottom = R.id.card_top_layout
        priceQuantityView.layoutParams = bottomParam



        constraintLayout.addView(roundImage)
        constraintLayout.addView(itemNameLayout)
        constraintLayout.addView(priceQuantityView)
        this.addView(constraintLayout)

    }

    private fun getLabeledTextView(itemTag: String, fl: Float, tvItemTag: Int): View? {

        val linearLayout = LinearLayout(context)
        linearLayout.gravity = Gravity.CENTER_VERTICAL
        val layoutParams =
            LayoutParams(LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        layoutParams.marginEnd = VUtil.dpToPx(5)
        linearLayout.layoutParams = layoutParams
        val fontIconView = FontIconView(context)
        fontIconView.tag = "icon_veg"
        fontIconView.layoutParams =
            LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        fontIconView.setPadding(0, 0, VUtil.dpToPx(5), 0)
        fontIconView.textSize = 16f
        fontIconView.setTextColor(context.resources.getColor(R.color.colorGreen))
        fontIconView.text = context.resources.getText(R.string.icon_veg_non_veg)

        linearLayout.addView(fontIconView)
        linearLayout.addView(getTextView(itemTag, fl, tvItemTag))
        return linearLayout
    }


    private fun getPriceQuantityView(itemTag: String, fl: Float, tvItemTag: Int): LinearLayout {

        val linearLayout = LinearLayout(context)
        linearLayout.gravity = Gravity.CENTER_VERTICAL
        val layoutParams =
            LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f)
        layoutParams.marginEnd = VUtil.dpToPx(5)
        linearLayout.layoutParams = layoutParams
        val addQuantityView = AddQuantityView(context)
        addQuantityView.id = R.id.add_quantity_view


        linearLayout.addView(getTextView(itemTag, fl, tvItemTag), layoutParams)
        linearLayout.addView(addQuantityView)
        return linearLayout
    }
/*
    private fun getRattingView(context: Context): View {

        val rattingView = RattingView(context)
        val rattingParam = LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT
        )

        rattingParam.marginStart = VUtil.dpToPx(5)
        rattingParam.bottomMargin = VUtil.dpToPx(5)
        rattingParam.bottomToBottom = R.id.restaurant_image
        rattingParam.startToStart = LayoutParams.PARENT_ID
        rattingView.layoutParams = rattingParam

        return rattingView
    }*/

    private fun restaurantImageView(): ImageView {
        val imageParam =
            ConstraintLayout.LayoutParams((VUtil.screenWidth * 0.45).toInt(), 0)
        imageParam.dimensionRatio = "2:1.4"

        imageParam.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
        imageParam.startToStart = ConstraintLayout.LayoutParams.PARENT_ID
        imageParam.topToTop = ConstraintLayout.LayoutParams.PARENT_ID

        val roundImage = RoundedImageView(context)
        roundImage.layoutParams = imageParam
        roundImage.id = R.id.restaurant_image
        roundImage.cornerRadius = VUtil.dpToPx(10).toFloat()
        roundImage.setImageResource(R.drawable.ic_default_img)
        roundImage.scaleType = ImageView.ScaleType.CENTER_CROP
        return roundImage
    }


    private fun getTextView(text: String, textSize: Float, tvId: Int): View {
        val textView = TextView(context)
        textView.setInterFont()
        textView.text = text
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
        textView.id = tvId
        if (tvId == R.id.tv_item_name) {
//            textView.setInterBoldFont()
            textView.maxLines = 2
            textView.ellipsize = TextUtils.TruncateAt.END
            textView.setTextColor(ThemeConstant.textBlackColor)
        } else if (tvId == R.id.tv_item_tag) {
            textView.setTextColor(ThemeConstant.textDarkGrayColor)
        } else if (tvId == R.id.tv_item_price) {
            textView.setTextColor(ThemeConstant.textBlackColor)

        }
//        textView.setOnClickListener(this)
        return textView

    }


    /*override fun onClick(view: View) {
        when (view.id) {
            R.id.iv_rounded_image -> this.restaurantCardListener?.onRestaurantImageClick()
            R.id.tv_title -> this.restaurantCardListener?.onRestaurantImageClick()
            R.id.tv_subtitle -> this.restaurantCardListener?.onRestaurantImageClick()
            else -> this.restaurantCardListener?.onRestaurantClick()
        }
    }
*/

}