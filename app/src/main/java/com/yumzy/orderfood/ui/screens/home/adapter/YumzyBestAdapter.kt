package com.yumzy.orderfood.ui.screens.home.adapter

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.utilcode.util.ClickUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.ui.common.imageview.RoundedImageView
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.withPrecision
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.YUtils

class YumzyBestAdapter(
    val context: Context,
    private val homeLayoutList: ArrayList<HomeListDTO>?,
    val callerFun: (View, HomeListDTO) -> Unit
) : RecyclerView.Adapter<YumzyBestAdapter.YumzyExclusiveHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): YumzyExclusiveHolder {
        val itemFeatured =
            LayoutInflater.from(context).inflate(R.layout.adapter_best_food, parent, false)
        val tvAddButton: Button = itemFeatured.findViewById(R.id.btn_add_plate)
        tvAddButton.visibility = View.INVISIBLE
        return YumzyExclusiveHolder(itemFeatured)
    }

    override fun getItemCount() = homeLayoutList?.size ?: 0


    inner class YumzyExclusiveHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val roundImageView: RoundedImageView = itemView.findViewById(R.id.rounded_image)
        val tvTitle: TextView = itemView.findViewById(R.id.food_title)
        val tvSubTitle: TextView = itemView.findViewById(R.id.tv_food_subtitle)
        val tvPrice: TextView = itemView.findViewById(R.id.tv_food_price)
        val tvVeg: TextView = itemView.findViewById(R.id.icon_veg_non)
        val started_at: TextView = itemView.findViewById(R.id.tv_starting_at)
//        val tvAddButton: Button = itemView.findViewById(R.id.btn_add_plate)


        fun bindView(homeItem: HomeListDTO?) {
            homeItem?.run {
//                Glide.with(roundImageView).load(image.url).into(roundImageView)
                YUtils.setImageInView(roundImageView, image?.url ?: "")

                tvTitle.text = title.capitalize()
                tvTitle.maxLines = 2
                tvTitle.minLines = 1
                tvTitle.ellipsize = TextUtils.TruncateAt.END
                tvPrice.text = "₹" + meta?.price?.withPrecision(0)
                tvSubTitle.text = subTitle
                if (homeItem.meta?.costForTwo.isNullOrEmpty()) {
                    started_at.visibility = View.VISIBLE
                    started_at.text = context.resources.getString(R.string.started_at)

                } else {
                    started_at.text = context.resources.getString(R.string.started_at)
                    started_at.visibility = View.VISIBLE
                }
                if (homeItem.meta?.isVeg == true) tvVeg.setTextColor(ThemeConstant.greenAccent) else tvVeg.setTextColor(
                    ThemeConstant.pinkies
                )
//            tvVeg.text=title
                ClickUtils.applyGlobalDebouncing(
                    itemView,
                    IConstants.AppConstant.CLICK_DELAY
                ) {
                    callerFun(itemView, homeItem)

                }
            }
        }

    }

    override fun onBindViewHolder(holder: YumzyExclusiveHolder, position: Int) {
        val homeItem = homeLayoutList?.get(position)
        holder.bindView(homeItem)
    }

}

