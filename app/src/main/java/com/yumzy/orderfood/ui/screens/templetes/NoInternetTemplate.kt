package com.yumzy.orderfood.ui.screens.templetes

import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.utilcode.util.NetworkUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.setInterBoldFont
import com.yumzy.orderfood.ui.helper.setInterFont
import com.yumzy.orderfood.ui.screens.module.infodialog.IDetailsActionTemplate
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView

class NoInternetTemplate(
    override val context: Context,
    val fontText: String,
    val title: String,
    val subTitle: String
) : IDetailsActionTemplate {
    override val headerHeight: Int = VUtil.dpToPx(100)
    override val showClose: Boolean = true
    override fun heading(): View? {
        val layout = LinearLayout(context)
        layout.minimumHeight = headerHeight
        val fontIcon = FontIconView(context)
        fontIcon.setTextSize(TypedValue.COMPLEX_UNIT_SP, 75f)
        fontIcon.setTextColor(ThemeConstant.greenAccent)
        fontIcon.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, headerHeight)
        val string = fontText
//        fontIcon.text= fontText
        fontIcon.text = string
        layout.addView(fontIcon)
        return layout
    }

    override fun body(): View? {
        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.VERTICAL
        layout.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        layout.addView(getTextView(R.id.tv_info_header, title))
        layout.addView(getTextView(R.id.tv_info_sub_header, subTitle))

        return layout
    }

    override fun actions(): View? {
        val layout = LinearLayout(context)
        layout.setPadding(0, UIHelper.i20px, 0, UIHelper.i20px)
        layout.gravity = Gravity.CENTER
        layout.minimumHeight = headerHeight
        val button = UIHelper.filledButton(context, "Open Wifi Setting", null)
        button.setPadding(UIHelper.i20px, UIHelper.i10px, UIHelper.i20px, UIHelper.i10px)
        button.setOnClickListener {
            NetworkUtils.openWirelessSettings()
        }
        layout.addView(button)
        return layout
    }

    override fun onDialogVisible(view: View?, visible: Boolean?, dialog: DialogInterface?) {
    }


    fun getTextView(tvId: Int, msg: String): View? {
        val textView = TextView(context)
        textView.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        textView.text = msg
        textView.id = tvId
        textView.gravity = Gravity.CENTER
        textView.setPadding(0, UIHelper.i10px, 0, 0)
        if (tvId == R.id.tv_info_header) {
            textView.setInterBoldFont()
            textView.setTextColor(Color.BLACK)
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
        } else if (tvId == R.id.tv_info_sub_header) {
            textView.setTextColor(Color.DKGRAY)
            textView.setInterFont()
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)

        }
        return textView
    }

}