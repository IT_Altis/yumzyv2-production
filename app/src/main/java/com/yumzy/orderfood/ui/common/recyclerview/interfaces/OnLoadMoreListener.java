package com.yumzy.orderfood.ui.common.recyclerview.interfaces;

public interface OnLoadMoreListener {
    void onLoadMore();
}
