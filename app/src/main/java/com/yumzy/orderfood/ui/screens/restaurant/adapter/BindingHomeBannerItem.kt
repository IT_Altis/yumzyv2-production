package com.yumzy.orderfood.ui.screens.restaurant.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.ModelAbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.databinding.AdapterHomeOffersBinding
import com.yumzy.orderfood.databinding.ViewHomeBannerTopBinding
import java.util.*

class BindingHomeBannerItem(item: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, ViewHomeBannerTopBinding>(item) {

//    private var item: HomeListDTO? = null

    override val type: Int = R.id.home_banner_item_id
    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ViewHomeBannerTopBinding {
        return ViewHomeBannerTopBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: ViewHomeBannerTopBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)
        binding.imageUrl = model.image?.url.toString()
        binding.title = model.title.toString()
    }

    override fun unbindView(binding: ViewHomeBannerTopBinding) {
        binding.title = null
    }

}


class HomeOfferItem(item: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, AdapterHomeOffersBinding>(item) {


    override val type: Int = R.id.home_offer_item

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterHomeOffersBinding {
        return AdapterHomeOffersBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: AdapterHomeOffersBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)
        binding.imageUrl = model.image?.url.toString()
        binding.title = model.title.capitalize(Locale.getDefault())
    }

    override fun unbindView(binding: AdapterHomeOffersBinding) {
        binding.title = null
    }



}