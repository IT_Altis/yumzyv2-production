package com.yumzy.orderfood.ui.common.recyclerview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.util.Log;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.solver.state.State;
import androidx.core.view.GestureDetectorCompat;
import androidx.core.view.MotionEventCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.ItemTouchUIUtil;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.yumzy.orderfood.R;
import com.yumzy.orderfood.ui.common.recyclerview.interfaces.AnimatorCompatHelper;
import com.yumzy.orderfood.ui.common.recyclerview.interfaces.AnimatorListenerCompat;
import com.yumzy.orderfood.ui.common.recyclerview.interfaces.AnimatorUpdateListenerCompat;
import com.yumzy.orderfood.ui.common.recyclerview.interfaces.ValueAnimatorCompat;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * This is a utility class to add swipe to dismiss and drag & drop support to RecyclerView.
 * <p>
 * It works with a RecyclerView and a Callback class, which configures what type of interactions
 * are enabled and also receives events when user performs these actions.
 * <p>
 * Depending on which functionality you support, you should override
 * {@link Callback#onMove(RecyclerView, RecyclerView.ViewHolder, RecyclerView.ViewHolder)} and / or
 * {@link Callback#onSwiped(RecyclerView.ViewHolder, int)}.
 * <p>
 * This class is designed to work with any LayoutManager but for certain situations, it can be
 * optimized for your custom LayoutManager by extending methods in the
 * {@link ItemTouchHelper.Callback} class or implementing {@link ItemTouchHelper.ViewDropHandler}
 * interface in your LayoutManager.
 * <p>
 * By default, ItemTouchHelper moves the items' translateX/Y properties to reposition them. On
 * platforms older than Honeycomb, ItemTouchHelper uses canvas translations and View's visibility
 * property to move items in response to touch events. You can customize these behaviors by
 * overriding {@link Callback#onChildDraw(Canvas, RecyclerView, RecyclerView.ViewHolder, float, float, int,
 * boolean)}
 * or {@link Callback#onChildDrawOver(Canvas, RecyclerView, RecyclerView.ViewHolder, float, float, int,
 * boolean)}.
 * <p/>
 * Most of the time, you only need to override <code>onChildDraw</code> but due to limitations of
 * platform prior to Honeycomb, you may need to implement <code>onChildDrawOver</code> as well.
 */
public class ItemTouchHelperExtension extends RecyclerView.ItemDecoration
        implements RecyclerView.OnChildAttachStateChangeListener {

    /**
     * Up direction, used for swipe & drag control.
     */
    public static final int UP = 1;
    /**
     * Down direction, used for swipe & drag control.
     */
    public static final int DOWN = 2;

    /**
     * Left direction, used for swipe & drag control.
     */
    public static final int LEFT = 4;

    /**
     * Right direction, used for swipe & drag control.
     */
    public static final int RIGHT = 8;

    // If you change these relative direction values, update Callback#convertToAbsoluteDirection,
    // Callback#convertToRelativeDirection.
    /**
     * Horizontal start direction. Resolved to LEFT or RIGHT depending on RecyclerView's layout
     * direction. Used for swipe & drag control.
     */
    public static final int START = 16;

    /**
     * Horizontal end direction. Resolved to LEFT or RIGHT depending on RecyclerView's layout
     * direction. Used for swipe & drag control.
     */
    public static final int END = 32;

    /**
     * ItemTouchHelper is in idle state. At this state, either there is no related motion event by
     * the user or latest motion events have not yet triggered a swipe or drag.
     */
    public static final int ACTION_STATE_IDLE = 0;

    /**
     * A View is currently being swiped.
     */
    public static final int ACTION_STATE_SWIPE = 1;

    /**
     * A View is currently being dragged.
     */
    public static final int ACTION_STATE_DRAG = 2;

    /**
     * Animation type for views which are swiped successfully.
     */
    public static final int ANIMATION_TYPE_SWIPE_SUCCESS = 2;

    /**
     * Animation type for views which are not completely swiped thus will animate back to their
     * original position.
     */
    public static final int ANIMATION_TYPE_SWIPE_CANCEL = 4;

    /**
     * Animation type for views that were dragged and now will animate to their final position.
     */
    public static final int ANIMATION_TYPE_DRAG = 8;
    private static final String TAG = "ItemTouchHelper";
    private static final boolean DEBUG = false;
    private static final int ACTIVE_POINTER_ID_NONE = -1;
    private static final int DIRECTION_FLAG_COUNT = 8;
    private static final int ACTION_MODE_IDLE_MASK = 255;
    private static final int ACTION_MODE_SWIPE_MASK = 65280;
    private static final int ACTION_MODE_DRAG_MASK = 16711680;

    /**
     * Views, whose state should be cleared after they are detached from RecyclerView.
     * This is necessary after swipe dismissing an item. We wait until animator finishes its job
     * to clean these views.
     */
    private static final int PIXELS_PER_SECOND = 1000;
    final List<View> mPendingCleanup = new ArrayList();

    /**
     * Re-use array to calculate dx dy for a ViewHolder
     */
    private final float[] mTmpPosition = new float[2];

    /**
     * Currently selected view holder
     */
    RecyclerView.ViewHolder mSelected = null;

    /**
     * The reference coordinates for the action start. For drag & drop, this is the time long
     * press is completed vs for swipe, this is the initial touch point.
     */
    RecyclerView.ViewHolder mPreOpened = null;
    float mInitialTouchX;
    float mInitialTouchY;

    /**
     * The diff between the last event and initial touch.
     */
    float mSwipeEscapeVelocity;
    float mMaxSwipeVelocity;
    float mDx;
    float mDy;

    /**
     * The coordinates of the selected view at the time it is selected. We record these values
     * when action starts so that we can consistently position it even if LayoutManager moves the
     * View.
     */
    float mSelectedStartX;
    float mSelectedStartY;

    /**
     * The pointer we are tracking.
     */
    int mActivePointerId = -1;

    /**
     * Developer callback which controls the behavior of ItemTouchHelper.
     */
    Callback mCallback;

    /**
     * Current mode.
     */
    int mActionState = 0;

    /**
     * The direction flags obtained from unmasking
     * for the current
     * action state.
     */
    int mSelectedFlags;

    /**
     * When a View is dragged or swiped and needs to go back to where it was, we create a Recover
     * Animation and animate it to its location using this custom Animator, instead of using
     * framework Animators.
     * Using framework animators has the side effect of clashing with ItemAnimator, creating
     * jumpy UIs.
     */
    List<RecoverAnimation> mRecoverAnimations = new ArrayList<RecoverAnimation>();
    private int mSlop;
    private RecyclerView mRecyclerView;
    /**
     * Used for detecting fling swipe
     */
    private VelocityTracker mVelocityTracker;
    //re-used list for selecting a swap target
    private List<RecyclerView.ViewHolder> mSwapTargets;
    //re used for for sorting swap targets
    private List<Integer> mDistances;
    /**
     * If drag & drop is supported, we use child drawing order to bring them to front.
     */
    private RecyclerView.ChildDrawingOrderCallback mChildDrawingOrderCallback = null;
    /**
     * This keeps a reference to the child dragged by the user. Even after user stops dragging,
     * until view reaches its final position (end of recover animation), we keep a reference so
     * that it can be drawn above other children.
     */
    private View mOverdrawChild = null;
    /**
     * We cache the position of the overdraw child to avoid recalculating it each time child
     * position callback is called. This value is invalidated whenever a child is attached or
     * detached.
     */
    private int mOverdrawChildPosition = -1;
    /**
     * Used to detect long press.
     */
    private GestureDetectorCompat mGestureDetector;
    /**
     * Temporary rect instance that is used when we need to lookup Item decorations.
     */
    private Rect mTmpRect;
    /**
     * When user started to drag scroll. Reset when we don't scroll
     */
    private long mDragScrollStartTimeInMs;
    /**
     * When user drags a view to the edge, we start scrolling the LayoutManager as long as View
     * is partially out of bounds.
     */
    private final Runnable mScrollRunnable = new Runnable() {
        public void run() {
            if (ItemTouchHelperExtension.this.mSelected != null && ItemTouchHelperExtension.this.scrollIfNecessary()) {
                if (ItemTouchHelperExtension.this.mSelected != null) {
                    ItemTouchHelperExtension.this.moveIfNecessary(ItemTouchHelperExtension.this.mSelected);
                }

                ItemTouchHelperExtension.this.mRecyclerView.removeCallbacks(ItemTouchHelperExtension.this.mScrollRunnable);
                ViewCompat.postOnAnimation(ItemTouchHelperExtension.this.mRecyclerView, this);
            }

        }
    };
    private final RecyclerView.OnItemTouchListener mOnItemTouchListener = new RecyclerView.OnItemTouchListener() {
        boolean mClick = false;
        float mLastX = 0.0F;

        public boolean onInterceptTouchEvent(@NotNull RecyclerView recyclerView, @NotNull MotionEvent event) {
            ItemTouchHelperExtension.this.mGestureDetector.onTouchEvent(event);
            int action = event.getActionMasked();
            if (action == 0) {
                ItemTouchHelperExtension.this.mActivePointerId = MotionEventCompat.getPointerId(event, 0);
                ItemTouchHelperExtension.this.mInitialTouchX = event.getX();
                ItemTouchHelperExtension.this.mInitialTouchY = event.getY();
                this.mClick = true;
                this.mLastX = event.getX();
                ItemTouchHelperExtension.this.obtainVelocityTracker();
                if (ItemTouchHelperExtension.this.mSelected == null) {
                    RecoverAnimation index = ItemTouchHelperExtension.this.findAnimation(event);
                    if (index != null) {
                        ItemTouchHelperExtension.this.mInitialTouchX -= index.mX;
                        ItemTouchHelperExtension.this.mInitialTouchY -= index.mY;
                        ItemTouchHelperExtension.this.endRecoverAnimation(index.mViewHolder, true);
                        if (ItemTouchHelperExtension.this.mPendingCleanup.remove(index.mViewHolder.itemView)) {
                            ItemTouchHelperExtension.this.mCallback.clearView(ItemTouchHelperExtension.this.mRecyclerView, index.mViewHolder);
                        }

                        ItemTouchHelperExtension.this.select(index.mViewHolder, index.mActionState);
                        ItemTouchHelperExtension.this.updateDxDy(event, ItemTouchHelperExtension.this.mSelectedFlags, 0);
                    }
                }
            } else if (action != 3 && action != 1) {
                if (ItemTouchHelperExtension.this.mActivePointerId != -1) {
                    int index1 = event.findPointerIndex(ItemTouchHelperExtension.this.mActivePointerId);
                    if (index1 >= 0) {
                        ItemTouchHelperExtension.this.checkSelectForSwipe(action, event, index1);
                    }
                }
            } else {
                ItemTouchHelperExtension.this.mActivePointerId = -1;
                if (this.mClick) {
                    ItemTouchHelperExtension.this.doChildClickEvent(event.getRawX(), event.getRawY());
                }

                ItemTouchHelperExtension.this.select(null, 0);
            }

            if (ItemTouchHelperExtension.this.mVelocityTracker != null) {
                ItemTouchHelperExtension.this.mVelocityTracker.addMovement(event);
            }

            return ItemTouchHelperExtension.this.mSelected != null;
        }

        public void onTouchEvent(@NonNull RecyclerView recyclerView, @NonNull MotionEvent event) {
            ItemTouchHelperExtension.this.mGestureDetector.onTouchEvent(event);
            if (ItemTouchHelperExtension.this.mVelocityTracker != null) {
                ItemTouchHelperExtension.this.mVelocityTracker.addMovement(event);
            }

            if (ItemTouchHelperExtension.this.mActivePointerId != -1) {
                int action = event.getActionMasked();
                int activePointerIndex = event.findPointerIndex(ItemTouchHelperExtension.this.mActivePointerId);
                if (activePointerIndex >= 0) {
                    ItemTouchHelperExtension.this.checkSelectForSwipe(action, event, activePointerIndex);
                }

                ViewHolder viewHolder = ItemTouchHelperExtension.this.mSelected;
                if (viewHolder != null) {
                    switch (action) {
                        case 2:
                            if (activePointerIndex >= 0) {
                                ItemTouchHelperExtension.this.updateDxDy(event, ItemTouchHelperExtension.this.mSelectedFlags, activePointerIndex);
                                if (Math.abs(event.getX() - this.mLastX) > (float) ItemTouchHelperExtension.this.mSlop) {
                                    this.mClick = false;
                                }

                                this.mLastX = event.getX();
                                ItemTouchHelperExtension.this.moveIfNecessary(viewHolder);
                                ItemTouchHelperExtension.this.mRecyclerView.removeCallbacks(ItemTouchHelperExtension.this.mScrollRunnable);
                                ItemTouchHelperExtension.this.mScrollRunnable.run();
                                ItemTouchHelperExtension.this.mRecyclerView.invalidate();
                            }
                            break;
                        case 3:
                            if (ItemTouchHelperExtension.this.mVelocityTracker != null) {
                                ItemTouchHelperExtension.this.mVelocityTracker.clear();
                            }
                        case 1:
                            if (this.mClick) {
                                ItemTouchHelperExtension.this.doChildClickEvent(event.getRawX(), event.getRawY());
                            }

                            this.mClick = false;
                            ItemTouchHelperExtension.this.select(null, 0);
                            ItemTouchHelperExtension.this.mActivePointerId = -1;
                            break;
                        case 4:
                        case 5:
                        default:
                            this.mClick = false;
                            break;
                        case 6:
                            this.mClick = false;
                            int pointerIndex = event.getActionIndex();
                            int pointerId = event.getPointerId(pointerIndex);
                            if (pointerId == ItemTouchHelperExtension.this.mActivePointerId) {
                                int newPointerIndex = pointerIndex == 0 ? 1 : 0;
                                ItemTouchHelperExtension.this.mActivePointerId = event.getPointerId(newPointerIndex);
                                ItemTouchHelperExtension.this.updateDxDy(event, ItemTouchHelperExtension.this.mSelectedFlags, pointerIndex);
                            }
                    }

                }
            }
        }

        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
            if (disallowIntercept) {
                ItemTouchHelperExtension.this.select(null, 0);
            }
        }
    };

    public ItemTouchHelperExtension(Callback callback) {
        this.mCallback = callback;
    }

    private static boolean hitTest(View child, float x, float y, float left, float top) {
        return x >= left && x <= left + (float) child.getWidth() && y >= top && y <= top + (float) child.getHeight();
    }

    private void closeOpenedPreItem() {
        View view = this.mCallback.getItemFrontView(this.mPreOpened);
        if (this.mPreOpened != null && view != null) {
            ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view, "translationX", view.getTranslationX(), 0.0F);
            objectAnimator.addListener(new AnimatorListenerAdapter() {
                public void onAnimationStart(Animator animation) {
                    super.onAnimationStart(animation);
                    if (ItemTouchHelperExtension.this.mPreOpened != null) {
                        ItemTouchHelperExtension.this.mCallback.clearView(ItemTouchHelperExtension.this.mRecyclerView, ItemTouchHelperExtension.this.mPreOpened);
                    }

                    if (ItemTouchHelperExtension.this.mPreOpened != null) {
                        ItemTouchHelperExtension.this.mPendingCleanup.remove(ItemTouchHelperExtension.this.mPreOpened.itemView);
                    }

                    ItemTouchHelperExtension.this.endRecoverAnimation(ItemTouchHelperExtension.this.mPreOpened, true);
                    ItemTouchHelperExtension.this.mPreOpened = ItemTouchHelperExtension.this.mSelected;
                }

                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                }
            });
            objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
            objectAnimator.start();
        }
    }

    private void doChildClickEvent(float x, float y) {
        if (this.mSelected != null) {
            View view = this.mSelected.itemView;
            View consumeEventView = this.findConsumeView((ViewGroup) view, x, y);
            if (consumeEventView != null) {
                consumeEventView.performClick();
            }

        }
    }

    private View findConsumeView(ViewGroup parent, float x, float y) {
        for (int i = 0; i < parent.getChildCount(); ++i) {
            View child = parent.getChildAt(i);
            if (child instanceof ViewGroup && child.getVisibility() == 0) {
                return this.findConsumeView((ViewGroup) child, x, y);
            }

            if (this.isInBoundsClickable((int) x, (int) y, child)) {
                return child;
            }
        }

        if (this.isInBoundsClickable((int) x, (int) y, parent)) {
            return parent;
        } else {
            return null;
        }
    }

    private boolean isInBoundsClickable(int x, int y, View child) {
        int[] location = new int[2];
        child.getLocationOnScreen(location);
        Rect rect = new Rect(location[0], location[1], location[0] + child.getWidth(), location[1] + child.getHeight());
        return rect.contains(x, y) && ViewCompat.hasOnClickListeners(child) && child.getVisibility() == 0;
    }

    /**
     * Attaches the ItemTouchHelper to the provided RecyclerView. If TouchHelper is already
     * attached
     * to a RecyclerView, it will first detach from the previous one.
     *
     * @param recyclerView The RecyclerView instance to which you want to add this helper.
     */
    public void attachToRecyclerView(@Nullable RecyclerView recyclerView) {
        if (this.mRecyclerView != recyclerView) {
            if (this.mRecyclerView != null) {
                this.destroyCallbacks();
            }

            this.mRecyclerView = recyclerView;
            if (this.mRecyclerView != null) {
                Resources resources = recyclerView.getResources();
                this.mSwipeEscapeVelocity = resources.getDimension(R.dimen.item_touch_helper_swipe_escape_velocity);
                this.mMaxSwipeVelocity = resources.getDimension(R.dimen.item_touch_helper_swipe_escape_max_velocity);
                this.setupCallbacks();
                this.mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        if (newState == 1 && ItemTouchHelperExtension.this.mPreOpened != null) {
                            ItemTouchHelperExtension.this.closeOpenedPreItem();
                        }

                    }
                });
            }

        }
    }

    private void setupCallbacks() {
        ViewConfiguration vc = ViewConfiguration.get(this.mRecyclerView.getContext());
        this.mSlop = vc.getScaledTouchSlop();
        this.mRecyclerView.addItemDecoration(this);
        this.mRecyclerView.addOnItemTouchListener(this.mOnItemTouchListener);
        this.mRecyclerView.addOnChildAttachStateChangeListener(this);
        this.initGestureDetector();
    }

    private void destroyCallbacks() {
        this.mRecyclerView.removeItemDecoration(this);
        this.mRecyclerView.removeOnItemTouchListener(this.mOnItemTouchListener);
        this.mRecyclerView.removeOnChildAttachStateChangeListener(this);
        // clean all attached
        int recoverAnimSize = this.mRecoverAnimations.size();

        for (int i = recoverAnimSize - 1; i >= 0; --i) {
            RecoverAnimation recoverAnimation = this.mRecoverAnimations.get(0);
            this.mCallback.clearView(this.mRecyclerView, recoverAnimation.mViewHolder);
        }

        this.mRecoverAnimations.clear();
        this.mOverdrawChild = null;
        this.mOverdrawChildPosition = -1;
        this.releaseVelocityTracker();
    }

    private void initGestureDetector() {
        if (this.mGestureDetector == null) {
            this.mGestureDetector = new GestureDetectorCompat(this.mRecyclerView.getContext(), new ItemTouchHelperGestureListener());
        }
    }

    private void getSelectedDxDy(float[] outPosition) {
        if ((this.mSelectedFlags & 12) != 0) {
            outPosition[0] = this.mSelectedStartX + this.mDx - (float) this.mSelected.itemView.getLeft();
        } else {
            outPosition[0] = this.mSelected.itemView.getTranslationX();
        }

        if ((this.mSelectedFlags & 3) != 0) {
            outPosition[1] = this.mSelectedStartY + this.mDy - (float) this.mSelected.itemView.getTop();
        } else {
            outPosition[1] = this.mSelected.itemView.getTranslationY();
        }

    }

    public void onDrawOver(Canvas c, RecyclerView parent, State state) {
        float dx = 0.0F;
        float dy = 0.0F;
        if (this.mSelected != null) {
            this.getSelectedDxDy(this.mTmpPosition);
            dx = this.mTmpPosition[0];
            dy = this.mTmpPosition[1];
        }

        this.mCallback.onDrawOver(c, parent, this.mSelected, this.mRecoverAnimations, this.mActionState, dx, dy);
    }

    public void onDraw(Canvas c, RecyclerView parent, State state) {
        this.mOverdrawChildPosition = -1;
        float dx = 0.0F;
        float dy = 0.0F;
        if (this.mSelected != null) {
            this.getSelectedDxDy(this.mTmpPosition);
            dx = this.mTmpPosition[0];
            dy = this.mTmpPosition[1];
        }

        this.mCallback.onDraw(c, parent, this.mSelected, this.mRecoverAnimations, this.mActionState, dx, dy);
    }

    /**
     * Starts dragging or swiping the given View. Call with null if you want to clear it.
     *
     * @param selected    The ViewHolder to drag or swipe. Can be null if you want to cancel the
     *                    current action
     * @param actionState The type of action
     */
    private void select(RecyclerView.ViewHolder selected, int actionState) {
        if (selected != this.mSelected || actionState != this.mActionState) {
            this.mDragScrollStartTimeInMs = -9223372036854775808L;
            final int prevActionState = this.mActionState;
            this.endRecoverAnimation(selected, true);
            this.mActionState = actionState;
            if (actionState == 2) {
                // we remove after animation is complete. this means we only elevate the last drag
                // child but that should perform good enough as it is very hard to start dragging a
                // new child before the previous one settles.
                this.mOverdrawChild = selected.itemView;
                this.addChildDrawingOrderCallback();
            }

            int actionStateMask = (1 << 8 + 8 * actionState) - 1;
            boolean preventLayout = false;
            if (this.mSelected != null) {
                final ViewHolder rvParent = this.mSelected;
                if (rvParent.itemView.getParent() != null) {
                    final int swipeDir = prevActionState == 2 ? 0 : this.swipeIfNecessary(rvParent);
                    this.releaseVelocityTracker();
                    final float targetTranslateX;
                    final float targetTranslateY;
                    switch (swipeDir) {
                        case 1:
                        case 2:
                            targetTranslateX = 0.0F;
                            targetTranslateY = Math.signum(this.mDy) * (float) this.mRecyclerView.getHeight();
                            break;
                        case 4:
                        case 8:
                        case 16:
                            targetTranslateY = 0.0F;
                            targetTranslateX = Math.signum(this.mDx) * this.getSwipeWidth();
                            break;
                        case 32:
                            targetTranslateY = 0.0F;
                            targetTranslateX = Math.signum(this.mDx) * (float) this.mRecyclerView.getWidth();
                            break;
                        default:
                            targetTranslateX = 0.0F;
                            targetTranslateY = 0.0F;
                    }

                    final byte animationType;
                    if (prevActionState == 2) {
                        animationType = 8;
                    } else if (swipeDir > 0) {
                        animationType = 2;
                    } else {
                        animationType = 4;
                    }

                    this.getSelectedDxDy(this.mTmpPosition);
                    final float currentTranslateX = this.mTmpPosition[0];
                    final float currentTranslateY = this.mTmpPosition[1];
                    RecoverAnimation rv = new RecoverAnimation(rvParent, animationType, prevActionState, currentTranslateX, currentTranslateY, targetTranslateX, targetTranslateY) {
                        public void onAnimationEnd(ValueAnimatorCompat animation) {
                            super.onAnimationEnd(animation);
                            if (!this.mOverridden) {
                                if (swipeDir <= 0) {
                                    ItemTouchHelperExtension.this.mCallback.clearView(ItemTouchHelperExtension.this.mRecyclerView, rvParent);
                                } else {
                                    ItemTouchHelperExtension.this.mPendingCleanup.add(rvParent.itemView);
                                    ItemTouchHelperExtension.this.mPreOpened = rvParent;
                                    this.mIsPendingCleanup = true;
                                    if (swipeDir > 0) {
                                        ItemTouchHelperExtension.this.postDispatchSwipe(this, swipeDir);
                                    }
                                }

                                if (ItemTouchHelperExtension.this.mOverdrawChild == rvParent.itemView) {
                                    ItemTouchHelperExtension.this.removeChildDrawingOrderCallbackIfNecessary(rvParent.itemView);
                                }

                            }
                        }
                    };
                    long duration = this.mCallback.getAnimationDuration(this.mRecyclerView, animationType, targetTranslateX - currentTranslateX, targetTranslateY - currentTranslateY);
                    rv.setDuration(duration);
                    this.mRecoverAnimations.add(rv);
                    rv.start();
                    preventLayout = true;
                } else {
                    this.removeChildDrawingOrderCallbackIfNecessary(rvParent.itemView);
                    this.mCallback.clearView(this.mRecyclerView, rvParent);
                }

                this.mSelected = null;
            }

            if (selected != null) {
                this.mSelectedFlags = (this.mCallback.getAbsoluteMovementFlags(this.mRecyclerView, selected) & actionStateMask) >> this.mActionState * 8;
                this.mSelectedStartX = (float) selected.itemView.getLeft();
                this.mSelectedStartY = (float) selected.itemView.getTop();
                this.mSelected = selected;
                if (actionState == 2) {
                    this.mSelected.itemView.performHapticFeedback(0);
                }
            }

            ViewParent rvParent1 = this.mRecyclerView.getParent();
            if (rvParent1 != null) {
                rvParent1.requestDisallowInterceptTouchEvent(this.mSelected != null);
            }

            if (!preventLayout) {
                Objects.requireNonNull(this.mRecyclerView.getLayoutManager()).requestSimpleAnimationsInNextLayout();
            }

            this.mCallback.onSelectedChanged(this.mSelected, this.mActionState);
            this.mRecyclerView.invalidate();
        }
    }

    private float getSwipeWidth() {
        return this.mSelected instanceof Extension ? ((Extension) this.mSelected).getActionWidth() : (float) this.mRecyclerView.getWidth();
    }

    private void postDispatchSwipe(final RecoverAnimation anim, final int swipeDir) {
        // wait until animations are complete.
        this.mRecyclerView.post(new Runnable() {
            public void run() {
                if (ItemTouchHelperExtension.this.mRecyclerView != null && ItemTouchHelperExtension.this.mRecyclerView.isAttachedToWindow() && !anim.mOverridden && anim.mViewHolder.getAdapterPosition() != -1) {
                    RecyclerView.ItemAnimator animator = ItemTouchHelperExtension.this.mRecyclerView.getItemAnimator();
                    // if animator is running or we have other active recover animations, we try
                    // not to call onSwiped because DefaultItemAnimator is not good at merging
                    // animations. Instead, we wait and batch.
                    if ((animator == null || !animator.isRunning(null)) && !ItemTouchHelperExtension.this.hasRunningRecoverAnim()) {
                        ItemTouchHelperExtension.this.mCallback.onSwiped(anim.mViewHolder, swipeDir);
                    } else {
                        ItemTouchHelperExtension.this.mRecyclerView.post(this);
                    }
                }

            }
        });
    }

    private boolean hasRunningRecoverAnim() {
        int size = this.mRecoverAnimations.size();

        for (int i = 0; i < size; ++i) {
            if (!this.mRecoverAnimations.get(i).mEnded) {
                return true;
            }
        }

        return false;
    }

    /**
     * If user drags the view to the edge, trigger a scroll if necessary.
     */
    private boolean scrollIfNecessary() {
        if (this.mSelected == null) {
            this.mDragScrollStartTimeInMs = -9223372036854775808L;
            return false;
        } else {
            long now = System.currentTimeMillis();
            long scrollDuration = this.mDragScrollStartTimeInMs == -9223372036854775808L ? 0L : now - this.mDragScrollStartTimeInMs;
            RecyclerView.LayoutManager lm = this.mRecyclerView.getLayoutManager();
            if (this.mTmpRect == null) {
                this.mTmpRect = new Rect();
            }

            int scrollX = 0;
            int scrollY = 0;
            Objects.requireNonNull(lm).calculateItemDecorationsForChild(this.mSelected.itemView, this.mTmpRect);
            int curY;
            int topDiff;
            int bottomDiff;
            if (lm.canScrollHorizontally()) {
                curY = (int) (this.mSelectedStartX + this.mDx);
                topDiff = curY - this.mTmpRect.left - this.mRecyclerView.getPaddingLeft();
                if (this.mDx < 0.0F && topDiff < 0) {
                    scrollX = topDiff;
                } else if (this.mDx > 0.0F) {
                    bottomDiff = curY + this.mSelected.itemView.getWidth() + this.mTmpRect.right - (this.mRecyclerView.getWidth() - this.mRecyclerView.getPaddingRight());
                    if (bottomDiff > 0) {
                        scrollX = bottomDiff;
                    }
                }
            }

            if (lm.canScrollVertically()) {
                curY = (int) (this.mSelectedStartY + this.mDy);
                topDiff = curY - this.mTmpRect.top - this.mRecyclerView.getPaddingTop();
                if (this.mDy < 0.0F && topDiff < 0) {
                    scrollY = topDiff;
                } else if (this.mDy > 0.0F) {
                    bottomDiff = curY + this.mSelected.itemView.getHeight() + this.mTmpRect.bottom - (this.mRecyclerView.getHeight() - this.mRecyclerView.getPaddingBottom());
                    if (bottomDiff > 0) {
                        scrollY = bottomDiff;
                    }
                }
            }

            if (scrollX != 0) {
                scrollX = this.mCallback.interpolateOutOfBoundsScroll(this.mRecyclerView, this.mSelected.itemView.getWidth(), scrollX, this.mRecyclerView.getWidth(), scrollDuration);
            }

            if (scrollY != 0) {
                scrollY = this.mCallback.interpolateOutOfBoundsScroll(this.mRecyclerView, this.mSelected.itemView.getHeight(), scrollY, this.mRecyclerView.getHeight(), scrollDuration);
            }

            if (scrollX == 0 && scrollY == 0) {
                this.mDragScrollStartTimeInMs = -9223372036854775808L;
                return false;
            } else {
                if (this.mDragScrollStartTimeInMs == -9223372036854775808L) {
                    this.mDragScrollStartTimeInMs = now;
                }

                this.mRecyclerView.scrollBy(scrollX, scrollY);
                return true;
            }
        }
    }

    private List<ViewHolder> findSwapTargets(ViewHolder viewHolder) {
        if (this.mSwapTargets == null) {
            this.mSwapTargets = new ArrayList();
            this.mDistances = new ArrayList();
        } else {
            this.mSwapTargets.clear();
            this.mDistances.clear();
        }

        int margin = this.mCallback.getBoundingBoxMargin();
        int left = Math.round(this.mSelectedStartX + this.mDx) - margin;
        int top = Math.round(this.mSelectedStartY + this.mDy) - margin;
        int right = left + viewHolder.itemView.getWidth() + 2 * margin;
        int bottom = top + viewHolder.itemView.getHeight() + 2 * margin;
        int centerX = (left + right) / 2;
        int centerY = (top + bottom) / 2;
        RecyclerView.LayoutManager lm = this.mRecyclerView.getLayoutManager();
        int childCount = lm.getChildCount();

        for (int i = 0; i < childCount; ++i) {
            View other = lm.getChildAt(i);
            if (other != viewHolder.itemView && other.getBottom() >= top && other.getTop() <= bottom && other.getRight() >= left && other.getLeft() <= right) {
                ViewHolder otherVh = this.mRecyclerView.getChildViewHolder(other);
                if (this.mCallback.canDropOver(this.mRecyclerView, this.mSelected, otherVh)) {
                    int dx = Math.abs(centerX - (other.getLeft() + other.getRight()) / 2);
                    int dy = Math.abs(centerY - (other.getTop() + other.getBottom()) / 2);
                    int dist = dx * dx + dy * dy;
                    int pos = 0;
                    int cnt = this.mSwapTargets.size();

                    for (int j = 0; j < cnt && dist > this.mDistances.get(j); ++j) {
                        ++pos;
                    }

                    this.mSwapTargets.add(pos, otherVh);
                    this.mDistances.add(pos, dist);
                }
            }
        }

        return this.mSwapTargets;
    }

    private void moveIfNecessary(RecyclerView.ViewHolder viewHolder) {
        if (!this.mRecyclerView.isLayoutRequested()) {
            if (this.mActionState == 2) {
                float threshold = this.mCallback.getMoveThreshold(viewHolder);
                int x = (int) (this.mSelectedStartX + this.mDx);
                int y = (int) (this.mSelectedStartY + this.mDy);
                if ((float) Math.abs(y - viewHolder.itemView.getTop()) >= (float) viewHolder.itemView.getHeight() * threshold || (float) Math.abs(x - viewHolder.itemView.getLeft()) >= (float) viewHolder.itemView.getWidth() * threshold) {
                    List swapTargets = this.findSwapTargets(viewHolder);
                    if (swapTargets.size() != 0) {
                        ViewHolder target = this.mCallback.chooseDropTarget(viewHolder, swapTargets, x, y);
                        if (target == null) {
                            this.mSwapTargets.clear();
                            this.mDistances.clear();
                        } else {
                            int toPosition = target.getAdapterPosition();
                            int fromPosition = viewHolder.getAdapterPosition();
                            if (this.mCallback.onMove(this.mRecyclerView, viewHolder, target)) {
                                this.mCallback.onMoved(this.mRecyclerView, viewHolder, fromPosition, target, toPosition, x, y);
                            }

                        }
                    }
                }
            }
        }
    }

    public void onChildViewAttachedToWindow(@NotNull View view) {
    }

    public void onChildViewDetachedFromWindow(View view) {
        this.removeChildDrawingOrderCallbackIfNecessary(view);
        ViewHolder holder = this.mRecyclerView.getChildViewHolder(view);
        if (holder != null) {
            if (this.mSelected != null && holder == this.mSelected) {
                this.select(null, 0);
            } else {
                this.endRecoverAnimation(holder, false);// this may push it into pending cleanup list.
                if (this.mPendingCleanup.remove(holder.itemView)) {
                    this.mCallback.clearView(this.mRecyclerView, holder);
                }
            }

        }
    }

    /**
     * Returns the animation type or 0 if cannot be found.
     */
    private int endRecoverAnimation(RecyclerView.ViewHolder viewHolder, boolean override) {
        int recoverAnimSize = this.mRecoverAnimations.size();

        for (int i = recoverAnimSize - 1; i >= 0; --i) {
            RecoverAnimation anim = this.mRecoverAnimations.get(i);
            if (anim.mViewHolder == viewHolder) {
                anim.mOverridden |= override;
                if (!anim.mEnded) {
                    anim.cancel();
                }

                this.mRecoverAnimations.remove(i);
                return anim.mAnimationType;
            }
        }

        return 0;
    }

    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, State state) {
        outRect.setEmpty();
    }

    private void obtainVelocityTracker() {
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.recycle();
        }

        this.mVelocityTracker = VelocityTracker.obtain();
    }

    private void releaseVelocityTracker() {
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }

    }

    private ViewHolder findSwipedView(MotionEvent motionEvent) {
        RecyclerView.LayoutManager lm = this.mRecyclerView.getLayoutManager();
        if (this.mActivePointerId == -1) {
            return null;
        } else {
            int pointerIndex = motionEvent.findPointerIndex(this.mActivePointerId);
            float dx = motionEvent.getX(pointerIndex) - this.mInitialTouchX;
            float dy = motionEvent.getY(pointerIndex) - this.mInitialTouchY;
            float absDx = Math.abs(dx);
            float absDy = Math.abs(dy);
            if (absDx < (float) this.mSlop && absDy < (float) this.mSlop) {
                return null;
            } else if (absDx > absDy && Objects.requireNonNull(lm).canScrollHorizontally()) {
                return null;
            } else if (absDy > absDx && Objects.requireNonNull(lm).canScrollVertically()) {
                return null;
            } else {
                View child = this.findChildView(motionEvent);
                return child == null ? null : this.mRecyclerView.getChildViewHolder(child);
            }
        }
    }

    /**
     * Checks whether we should select a View for swiping.
     */
    private boolean checkSelectForSwipe(int action, MotionEvent motionEvent, int pointerIndex) {
        if (this.mSelected == null && action == 2 && this.mActionState != 2 && this.mCallback.isItemViewSwipeEnabled()) {
            if (this.mRecyclerView.getScrollState() == 1) {
                return false;
            } else {
                ViewHolder vh = this.findSwipedView(motionEvent);
                if (vh == null) {
                    return false;
                } else {
                    int movementFlags = this.mCallback.getAbsoluteMovementFlags(this.mRecyclerView, vh);
                    int swipeFlags = (movementFlags & '\uff00') >> 8;
                    if (swipeFlags == 0) {
                        return false;
                    } else {
                        // mDx and mDy are only set in allowed directions. We use custom x/y here instead of
                        // updateDxDy to avoid swiping if user moves more in the other direction
                        float x = motionEvent.getX(pointerIndex);
                        float y = motionEvent.getY(pointerIndex);
                        // Calculate the distance moved
                        float dx = x - this.mInitialTouchX;
                        float dy = y - this.mInitialTouchY;
                        // swipe target is chose w/o applying flags so it does not really check if swiping in that
                        // direction is allowed. This why here, we use mDx mDy to check slope value again.
                        float absDx = Math.abs(dx);
                        float absDy = Math.abs(dy);
                        if (absDx < (float) this.mSlop && absDy < (float) this.mSlop) {
                            return false;
                        } else {
                            if (absDx > absDy) {
                                if (dx < 0.0F && (swipeFlags & 4) == 0) {
                                    return false;
                                }

                                if (dx > 0.0F && (swipeFlags & 8) == 0) {
                                    return false;
                                }
                            } else {
                                if (dy < 0.0F && (swipeFlags & 1) == 0) {
                                    return false;
                                }

                                if (dy > 0.0F && (swipeFlags & 2) == 0) {
                                    return false;
                                }
                            }

                            this.mDx = this.mDy = 0.0F;
                            this.mActivePointerId = motionEvent.getPointerId(0);
                            this.select(vh, 1);
                            if (this.mPreOpened != null && this.mPreOpened != vh) {
                                this.closeOpenedPreItem();
                            }

                            return true;
                        }
                    }
                }
            }
        } else {
            return false;
        }
    }

    private View findChildView(MotionEvent event) {
        // first check elevated views, if none, then call RV
        float x = event.getX();
        float y = event.getY();
        if (this.mSelected != null) {
            View i = this.mSelected.itemView;
            if (hitTest(i, x, y, this.mSelectedStartX + this.mDx, this.mSelectedStartY + this.mDy)) {
                return i;
            }
        }

        for (int var7 = this.mRecoverAnimations.size() - 1; var7 >= 0; --var7) {
            RecoverAnimation anim = this.mRecoverAnimations.get(var7);
            View view = anim.mViewHolder.itemView;
            if (hitTest(view, x, y, anim.mX, anim.mY)) {
                return view;
            }
        }

        return this.mRecyclerView.findChildViewUnder(x, y);
    }

    /**
     * Starts dragging the provided ViewHolder. By default, ItemTouchHelper starts a drag when a
     * View is long pressed. You can disable that behavior via
     * {@link ItemTouchHelper.Callback#isLongPressDragEnabled()}.
     * <p>
     * For this method to work:
     * <ul>
     * <li>The provided ViewHolder must be a child of the RecyclerView to which this
     * ItemTouchHelper
     * is attached.</li>
     * <li>{@link ItemTouchHelper.Callback} must have dragging enabled.</li>
     * <li>There must be a previous touch event that was reported to the ItemTouchHelper
     * through RecyclerView's ItemTouchListener mechanism. As long as no other ItemTouchListener
     * grabs previous events, this should work as expected.</li>
     * </ul>
     * <p>
     * For example, if you would like to let your user to be able to drag an Item by touching one
     * of its descendants, you may implement it as follows:
     * <pre>
     *     viewHolder.dragButton.setOnTouchListener(new View.OnTouchListener() {
     *         public boolean onTouch(View v, MotionEvent event) {
     *             if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
     *                 mItemTouchHelper.startDrag(viewHolder);
     *             }
     *             return false;
     *         }
     *     });
     * </pre>
     * <p>
     *
     * @param viewHolder The ViewHolder to start dragging. It must be a direct child of
     *                   RecyclerView.
     * @see ItemTouchHelper.Callback#isItemViewSwipeEnabled()
     */
    public void startDrag(ViewHolder viewHolder) {
        if (!this.mCallback.hasDragFlag(this.mRecyclerView, viewHolder)) {
            Log.e("ItemTouchHelper", "Start drag has been called but swiping is not enabled");
        } else if (viewHolder.itemView.getParent() != this.mRecyclerView) {
            Log.e("ItemTouchHelper", "Start drag has been called with a view holder which is not a child of the RecyclerView which is controlled by this ItemTouchHelper.");
        } else {
            this.obtainVelocityTracker();
            this.mDx = this.mDy = 0.0F;
            this.select(viewHolder, 2);
        }
    }

    /**
     * Starts swiping the provided ViewHolder. By default, ItemTouchHelper starts swiping a View
     * when user swipes their finger (or mouse pointer) over the View. You can disable this
     * behavior
     * by overriding {@link ItemTouchHelper.Callback}
     * <p>
     * For this method to work:
     * <ul>
     * <li>The provided ViewHolder must be a child of the RecyclerView to which this
     * ItemTouchHelper is attached.</li>
     * <li>{@link ItemTouchHelper.Callback} must have swiping enabled.</li>
     * <li>There must be a previous touch event that was reported to the ItemTouchHelper
     * through RecyclerView's ItemTouchListener mechanism. As long as no other ItemTouchListener
     * grabs previous events, this should work as expected.</li>
     * </ul>
     * <p>
     * For example, if you would like to let your user to be able to swipe an Item by touching one
     * of its descendants, you may implement it as follows:
     * <pre>
     *     viewHolder.dragButton.setOnTouchListener(new View.OnTouchListener() {
     *         public boolean onTouch(View v, MotionEvent event) {
     *             if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
     *                 mItemTouchHelper.startSwipe(viewHolder);
     *             }
     *             return false;
     *         }
     *     });
     * </pre>
     *
     * @param viewHolder The ViewHolder to start swiping. It must be a direct child of
     *                   RecyclerView.
     */
    public void startSwipe(ViewHolder viewHolder) {
        if (!this.mCallback.hasSwipeFlag(this.mRecyclerView, viewHolder)) {
            Log.e("ItemTouchHelper", "Start swipe has been called but dragging is not enabled");
        } else if (viewHolder.itemView.getParent() != this.mRecyclerView) {
            Log.e("ItemTouchHelper", "Start swipe has been called with a view holder which is not a child of the RecyclerView controlled by this ItemTouchHelper.");
        } else {
            this.obtainVelocityTracker();
            this.mDx = this.mDy = 0.0F;
            this.select(viewHolder, 1);
        }
    }

    private RecoverAnimation findAnimation(MotionEvent event) {
        if (this.mRecoverAnimations.isEmpty()) {
            return null;
        } else {
            View target = this.findChildView(event);

            for (int i = this.mRecoverAnimations.size() - 1; i >= 0; --i) {
                RecoverAnimation anim = this.mRecoverAnimations.get(i);
                if (anim.mViewHolder.itemView == target) {
                    return anim;
                }
            }

            return null;
        }
    }

    private void updateDxDy(MotionEvent ev, int directionFlags, int pointerIndex) {
        float x = ev.getX(pointerIndex);
        float y = ev.getY(pointerIndex);

        // Calculate the distance moved
        this.mDx = x - this.mInitialTouchX;
        this.mDy = y - this.mInitialTouchY;
        if ((directionFlags & 4) == 0) {
            this.mDx = Math.max(0.0F, this.mDx);
        }

        if ((directionFlags & 8) == 0) {
            this.mDx = Math.min(0.0F, this.mDx);
        }

        if ((directionFlags & 1) == 0) {
            this.mDy = Math.max(0.0F, this.mDy);
        }

        if ((directionFlags & 2) == 0) {
            this.mDy = Math.min(0.0F, this.mDy);
        }

    }

    private int swipeIfNecessary(ViewHolder viewHolder) {
        if (this.mActionState == 2) {
            return 0;
        } else {
            int originalMovementFlags = this.mCallback.getMovementFlags(this.mRecyclerView, viewHolder);
            int absoluteMovementFlags = this.mCallback.convertToAbsoluteDirection(originalMovementFlags, ViewCompat.getLayoutDirection(this.mRecyclerView));
            int flags = (absoluteMovementFlags & '\uff00') >> 8;
            if (flags == 0) {
                return 0;
            } else {
                int originalFlags = (originalMovementFlags & '\uff00') >> 8;
                int swipeDir;
                if (Math.abs(this.mDx) > Math.abs(this.mDy)) {
                    if ((swipeDir = this.checkHorizontalSwipe(viewHolder, flags)) > 0) {
                        // if swipe dir is not in original flags, it should be the relative direction
                        if ((originalFlags & swipeDir) == 0) {
                            // convert to relative
                            return Callback.convertToRelativeDirection(swipeDir, ViewCompat.getLayoutDirection(this.mRecyclerView));
                        }

                        return swipeDir;
                    }

                    if ((swipeDir = this.checkVerticalSwipe(viewHolder, flags)) > 0) {
                        return swipeDir;
                    }
                } else {
                    if ((swipeDir = this.checkVerticalSwipe(viewHolder, flags)) > 0) {
                        return swipeDir;
                    }

                    if ((swipeDir = this.checkHorizontalSwipe(viewHolder, flags)) > 0) {
                        // if swipe dir is not in original flags, it should be the relative direction
                        if ((originalFlags & swipeDir) == 0) {
                            // convert to relative
                            return Callback.convertToRelativeDirection(swipeDir, ViewCompat.getLayoutDirection(this.mRecyclerView));
                        }

                        return swipeDir;
                    }
                }

                return 0;
            }
        }
    }

    private int checkHorizontalSwipe(ViewHolder viewHolder, int flags) {
        if ((flags & 12) != 0) {
            int dirFlag = this.mDx > 0.0F ? 8 : 4;
            float threshold;
            if (this.mVelocityTracker != null && this.mActivePointerId > -1) {
                this.mVelocityTracker.computeCurrentVelocity(1000, this.mCallback.getSwipeVelocityThreshold(this.mMaxSwipeVelocity));
                threshold = this.mVelocityTracker.getXVelocity(this.mActivePointerId);
                float yVelocity = this.mVelocityTracker.getYVelocity(this.mActivePointerId);
                int velDirFlag = threshold > 0.0F ? 8 : 4;
                float absXVelocity = Math.abs(threshold);
                if ((velDirFlag & flags) != 0 && dirFlag == velDirFlag && absXVelocity >= this.mCallback.getSwipeEscapeVelocity(this.mSwipeEscapeVelocity) && absXVelocity > Math.abs(yVelocity)) {
                    return velDirFlag;
                }
            }

            threshold = (float) this.mRecyclerView.getWidth() * this.mCallback.getSwipeThreshold(viewHolder);
            if ((flags & dirFlag) != 0 && Math.abs(this.mDx) > threshold) {
                return dirFlag;
            }
        }

        return 0;
    }

    private int checkVerticalSwipe(ViewHolder viewHolder, int flags) {
        if ((flags & 3) != 0) {
            int dirFlag = this.mDy > 0.0F ? 2 : 1;
            float threshold;
            if (this.mVelocityTracker != null && this.mActivePointerId > -1) {
                this.mVelocityTracker.computeCurrentVelocity(1000, this.mCallback.getSwipeVelocityThreshold(this.mMaxSwipeVelocity));
                threshold = this.mVelocityTracker.getXVelocity(this.mActivePointerId);
                float yVelocity = this.mVelocityTracker.getYVelocity(this.mActivePointerId);
                int velDirFlag = yVelocity > 0.0F ? 2 : 1;
                float absYVelocity = Math.abs(yVelocity);
                if ((velDirFlag & flags) != 0 && velDirFlag == dirFlag && absYVelocity >= this.mCallback.getSwipeEscapeVelocity(this.mSwipeEscapeVelocity) && absYVelocity > Math.abs(threshold)) {
                    return velDirFlag;
                }
            }

            threshold = (float) this.mRecyclerView.getHeight() * this.mCallback.getSwipeThreshold(viewHolder);
            if ((flags & dirFlag) != 0 && Math.abs(this.mDy) > threshold) {
                return dirFlag;
            }
        }

        return 0;
    }

    private void addChildDrawingOrderCallback() {
        if (VERSION.SDK_INT < 21) {
            if (this.mChildDrawingOrderCallback == null) {
                this.mChildDrawingOrderCallback = new RecyclerView.ChildDrawingOrderCallback() {
                    public int onGetChildDrawingOrder(int childCount, int i) {
                        if (ItemTouchHelperExtension.this.mOverdrawChild == null) {
                            return i;
                        } else {
                            int childPosition = ItemTouchHelperExtension.this.mOverdrawChildPosition;
                            if (childPosition == -1) {
                                childPosition = ItemTouchHelperExtension.this.mRecyclerView.indexOfChild(ItemTouchHelperExtension.this.mOverdrawChild);
                                ItemTouchHelperExtension.this.mOverdrawChildPosition = childPosition;
                            }

                            return i == childCount - 1 ? childPosition : (i < childPosition ? i : i + 1);
                        }
                    }
                };
            }

            this.mRecyclerView.setChildDrawingOrderCallback(this.mChildDrawingOrderCallback);
        }
    }

    private void removeChildDrawingOrderCallbackIfNecessary(View view) {
        if (view == this.mOverdrawChild) {
            this.mOverdrawChild = null;
            // only remove if we've added
            if (this.mChildDrawingOrderCallback != null) {
                this.mRecyclerView.setChildDrawingOrderCallback(null);
            }
        }

    }

    public interface ViewDropHandler {
        void prepareForDrop(View var1, View var2, int var3, int var4);
    }

    public interface Extension {
        float getActionWidth();
    }

    public abstract static class SimpleCallback extends Callback {
        private int mDefaultSwipeDirs;
        private int mDefaultDragDirs;

        public SimpleCallback(int dragDirs, int swipeDirs) {
            this.mDefaultSwipeDirs = swipeDirs;
            this.mDefaultDragDirs = dragDirs;
        }

        public void setDefaultSwipeDirs(int defaultSwipeDirs) {
            this.mDefaultSwipeDirs = defaultSwipeDirs;
        }

        public void setDefaultDragDirs(int defaultDragDirs) {
            this.mDefaultDragDirs = defaultDragDirs;
        }

        public int getSwipeDirs(RecyclerView recyclerView, ViewHolder viewHolder) {
            return this.mDefaultSwipeDirs;
        }

        public int getDragDirs(RecyclerView recyclerView, ViewHolder viewHolder) {
            return this.mDefaultDragDirs;
        }

        public int getMovementFlags(RecyclerView recyclerView, ViewHolder viewHolder) {
            return makeMovementFlags(this.getDragDirs(recyclerView, viewHolder), this.getSwipeDirs(recyclerView, viewHolder));
        }
    }

    /**
     * This class is the contract between ItemTouchHelper and your application. It lets you control
     * which touch behaviors are enabled per each ViewHolder and also receive callbacks when user
     * performs these actions.
     * <p>
     * To control which actions user can take on each view, you should override
     * {@link #getMovementFlags(RecyclerView, ViewHolder)} and return appropriate set
     * of direction flags. ({@link #LEFT}, {@link #RIGHT}, {@link #START}, {@link #END},
     * {@link #UP}, {@link #DOWN}). You can use
     * {@link #makeMovementFlags(int, int)} to easily construct it. Alternatively, you can use
     * {@link SimpleCallback}.
     * <p>
     * If user drags an item, ItemTouchHelper will call
     * {@link Callback#onMove(RecyclerView, ViewHolder, ViewHolder)
     * onMove(recyclerView, dragged, target)}.
     * Upon receiving this callback, you should move the item from the old position
     * ({@code dragged.getAdapterPosition()}) to new position ({@code target.getAdapterPosition()})
     * in your adapter and also call .
     * To control where a View can be dropped, you can override
     * {@link #canDropOver(RecyclerView, ViewHolder, ViewHolder)}. When a
     * dragging View overlaps multiple other views, Callback chooses the closest View with which
     * dragged View might have changed positions. Although this approach works for many use cases,
     * if you have a custom LayoutManager, you can override
     * {@link #chooseDropTarget(ViewHolder, List, int, int)} to select a
     * custom drop target.
     * <p>
     * When a View is swiped, ItemTouchHelper animates it until it goes out of bounds, then calls
     * {@link #onSwiped(ViewHolder, int)}. At this point, you should update your
     * adapter (e.g. remove the item) and call related Adapter#notify event.
     */

    public abstract static class Callback {
        public static final int DEFAULT_DRAG_ANIMATION_DURATION = 200;
        public static final int DEFAULT_SWIPE_ANIMATION_DURATION = 250;
        static final int RELATIVE_DIR_FLAGS = 3158064;
        private static final ItemTouchUIUtil sUICallback;
        private static final int ABS_HORIZONTAL_DIR_FLAGS = 789516;
        private static final Interpolator sDragScrollInterpolator = new Interpolator() {
            public float getInterpolation(float t) {
                return t * t * t * t * t;
            }
        };
        private static final Interpolator sDragViewScrollCapInterpolator = new Interpolator() {
            public float getInterpolation(float t) {
                --t;
                return t * t * t * t * t + 1.0F;
            }
        };
        private static final long DRAG_SCROLL_ACCELERATION_LIMIT_TIME_MS = 2000L;

        static {
            if (VERSION.SDK_INT >= 21) {
                sUICallback = new Lollipop();
            } else if (VERSION.SDK_INT >= 11) {
                sUICallback = new Honeycomb();
            } else {
                sUICallback = new Gingerbread();
            }

        }

        private int mCachedMaxScrollSpeed = -1;

        public Callback() {
        }

        public static ItemTouchUIUtil getDefaultUIUtil() {
            return sUICallback;
        }

        /**
         * Replaces a movement direction with its relative version by taking layout direction into
         * account.
         *
         * @param flags           The flag value that include any number of movement flags.
         * @param layoutDirection The layout direction of the View. Can be obtained from
         *                        {@link ViewCompat#getLayoutDirection(View)}.
         * @return Updated flags which uses relative flags ({@link #START}, {@link #END}) instead
         * of {@link #LEFT}, {@link #RIGHT}.
         * @see #convertToAbsoluteDirection(int, int)
         */
        public static int convertToRelativeDirection(int flags, int layoutDirection) {
            int masked = flags & 789516;
            if (masked == 0) {
                return flags;
            } else {
                flags &= ~masked;
                if (layoutDirection == 0) {
                    flags |= masked << 2;
                    return flags;
                } else {
                    flags |= masked << 1 & -789517;
                    flags |= (masked << 1 & 789516) << 2;
                    return flags;
                }
            }
        }

        /**
         * Convenience method to create movement flags.
         * <p>
         * For instance, if you want to let your items be drag & dropped vertically and swiped
         * left to be dismissed, you can call this method with:
         * <code>makeMovementFlags(UP | DOWN, LEFT);</code>
         *
         * @param dragFlags  The directions in which the item can be dragged.
         * @param swipeFlags The directions in which the item can be swiped.
         * @return Returns an integer composed of the given drag and swipe flags.
         */
        public static int makeMovementFlags(int dragFlags, int swipeFlags) {
            return makeFlag(0, swipeFlags | dragFlags) | makeFlag(1, swipeFlags) | makeFlag(2, dragFlags);
        }

        /**
         * Shifts the given direction flags to the offset of the given action state.
         *
         * @param actionState The action state you want to get flags in. Should be one of
         *                    {@link #ACTION_STATE_IDLE}, {@link #ACTION_STATE_SWIPE} or
         *                    {@link #ACTION_STATE_DRAG}.
         * @param directions  The direction flags. Can be composed from {@link #UP}, {@link #DOWN},
         *                    {@link #RIGHT}, {@link #LEFT} {@link #START} and {@link #END}.
         * @return And integer that represents the given directions in the provided actionState.
         */
        public static int makeFlag(int actionState, int directions) {
            return directions << actionState * 8;
        }

        /**
         * Should return a composite flag which defines the enabled move directions in each state
         * (idle, swiping, dragging).
         * <p>
         * Instead of composing this flag manually, you can use {@link #makeMovementFlags(int,
         * int)}
         * or {@link #makeFlag(int, int)}.
         * <p>
         * This flag is composed of 3 sets of 8 bits, where first 8 bits are for IDLE state, next
         * 8 bits are for SWIPE state and third 8 bits are for DRAG state.
         * Each 8 bit sections can be constructed by simply OR'ing direction flags defined in
         * {@link ItemTouchHelper}.
         * <p>
         * For example, if you want it to allow swiping LEFT and RIGHT but only allow starting to
         * swipe by swiping RIGHT, you can return:
         * <pre>
         *      makeFlag(ACTION_STATE_IDLE, RIGHT) | makeFlag(ACTION_STATE_SWIPE, LEFT | RIGHT);
         * </pre>
         * This means, allow right movement while IDLE and allow right and left movement while
         * swiping.
         *
         * @param recyclerView The RecyclerView to which ItemTouchHelper is attached.
         * @param viewHolder   The ViewHolder for which the movement information is necessary.
         * @return flags specifying which movements are allowed on this ViewHolder.
         * @see #makeMovementFlags(int, int)
         * @see #makeFlag(int, int)
         */
        public abstract int getMovementFlags(RecyclerView recyclerView, ViewHolder viewHolder);

        /**
         * Converts a given set of flags to absolution direction which means {@link #START} and
         * {@link #END} are replaced with {@link #LEFT} and {@link #RIGHT} depending on the layout
         * direction.
         *
         * @param flags           The flag value that include any number of movement flags.
         * @param layoutDirection The layout direction of the RecyclerView.
         * @return Updated flags which includes only absolute direction values.
         */
        public int convertToAbsoluteDirection(int flags, int layoutDirection) {
            int masked = flags & 3158064;
            if (masked == 0) {
                return flags;
            } else {
                flags &= ~masked;
                if (layoutDirection == 0) {
                    flags |= masked >> 2;
                    return flags;
                } else {
                    flags |= masked >> 1 & -3158065;
                    flags |= (masked >> 1 & 3158064) >> 2;
                    return flags;
                }
            }
        }

        final int getAbsoluteMovementFlags(RecyclerView recyclerView, ViewHolder viewHolder) {
            int flags = this.getMovementFlags(recyclerView, viewHolder);
            return this.convertToAbsoluteDirection(flags, ViewCompat.getLayoutDirection(recyclerView));
        }

        private boolean hasDragFlag(RecyclerView recyclerView, ViewHolder viewHolder) {
            int flags = this.getAbsoluteMovementFlags(recyclerView, viewHolder);
            return (flags & 16711680) != 0;
        }

        private boolean hasSwipeFlag(RecyclerView recyclerView, ViewHolder viewHolder) {
            int flags = this.getAbsoluteMovementFlags(recyclerView, viewHolder);
            return (flags & '\uff00') != 0;
        }

        /**
         * Return true if the current ViewHolder can be dropped over the the target ViewHolder.
         * <p>
         * This method is used when selecting drop target for the dragged View. After Views are
         * eliminated either via bounds check or via this method, resulting set of views will be
         * passed to {@link #chooseDropTarget(ViewHolder, List, int, int)}.
         * <p>
         * Default implementation returns true.
         *
         * @param recyclerView The RecyclerView to which ItemTouchHelper is attached to.
         * @param current      The ViewHolder that user is dragging.
         * @param target       The ViewHolder which is below the dragged ViewHolder.
         * @return True if the dragged ViewHolder can be replaced with the target ViewHolder, false
         * otherwise.
         */
        public boolean canDropOver(RecyclerView recyclerView, ViewHolder current,
                                   ViewHolder target) {
            return true;
        }

        /**
         * Called when ItemTouchHelper wants to move the dragged item from its old position to
         * the new position.
         * <p>
         * If this method returns true, ItemTouchHelper assumes {@code viewHolder} has been moved
         * to the adapter position of {@code target} ViewHolder
         * ({@link ViewHolder#getAdapterPosition()
         * ViewHolder#getAdapterPosition()}).
         * <p>
         * If you don't support drag & drop, this method will never be called.
         *
         * @param recyclerView The RecyclerView to which ItemTouchHelper is attached to.
         * @param viewHolder   The ViewHolder which is being dragged by the user.
         * @param target       The ViewHolder over which the currently active item is being
         *                     dragged.
         * @return True if the {@code viewHolder} has been moved to the adapter position of
         * {@code target}.
         * @see #onMoved(RecyclerView, ViewHolder, int, ViewHolder, int, int, int)
         */
        public abstract boolean onMove(RecyclerView recyclerView,
                                       ViewHolder viewHolder, ViewHolder target);

        /**
         * Returns whether ItemTouchHelper should start a drag and drop operation if an item is
         * long pressed.
         * <p>
         * Default value returns true but you may want to disable this if you want to start
         * dragging on a custom view touch using {@link #startDrag(ViewHolder)}.
         *
         * @return True if ItemTouchHelper should start dragging an item when it is long pressed,
         * false otherwise. Default value is <code>true</code>.
         * @see #startDrag(ViewHolder)
         */
        public boolean isLongPressDragEnabled() {
            return true;
        }

        /**
         * Returns whether ItemTouchHelper should start a swipe operation if a pointer is swiped
         * over the View.
         * <p>
         * Default value returns true but you may want to disable this if you want to start
         * swiping on a custom view touch using {@link #startSwipe(ViewHolder)}.
         *
         * @return True if ItemTouchHelper should start swiping an item when user swipes a pointer
         * over the View, false otherwise. Default value is <code>true</code>.
         * @see #startSwipe(ViewHolder)
         */
        public boolean isItemViewSwipeEnabled() {
            return true;
        }

        /**
         * When finding views under a dragged view, by default, ItemTouchHelper searches for views
         * that overlap with the dragged View. By overriding this method, you can extend or shrink
         * the search box.
         *
         * @return The extra margin to be added to the hit box of the dragged View.
         */
        public int getBoundingBoxMargin() {
            return 0;
        }

        /**
         * Returns the fraction that the user should move the View to be considered as swiped.
         * The fraction is calculated with respect to RecyclerView's bounds.
         * <p>
         * Default value is .5f, which means, to swipe a View, user must move the View at least
         * half of RecyclerView's width or height, depending on the swipe direction.
         *
         * @param viewHolder The ViewHolder that is being dragged.
         * @return A float value that denotes the fraction of the View size. Default value
         * is .5f .
         */
        public float getSwipeThreshold(ViewHolder viewHolder) {
            return 0.5F;
        }

        /**
         * Returns the fraction that the user should move the View to be considered as it is
         * dragged. After a view is moved this amount, ItemTouchHelper starts checking for Views
         * below it for a possible drop.
         *
         * @param viewHolder The ViewHolder that is being dragged.
         * @return A float value that denotes the fraction of the View size. Default value is
         * .5f .
         */
        public float getMoveThreshold(ViewHolder viewHolder) {
            return 0.5F;
        }

        public float getSwipeEscapeVelocity(float defaultValue) {
            return defaultValue;
        }

        public float getSwipeVelocityThreshold(float defaultValue) {
            return defaultValue;
        }

        public ViewHolder chooseDropTarget(ViewHolder selected, List<ViewHolder> dropTargets, int curX, int curY) {
            int right = curX + selected.itemView.getWidth();
            int bottom = curY + selected.itemView.getHeight();
            ViewHolder winner = null;
            int winnerScore = -1;
            int dx = curX - selected.itemView.getLeft();
            int dy = curY - selected.itemView.getTop();
            int targetsSize = dropTargets.size();

            for (int i = 0; i < targetsSize; ++i) {
                ViewHolder target = dropTargets.get(i);
                int diff;
                int score;
                if (dx > 0) {
                    diff = target.itemView.getRight() - right;
                    if (diff < 0 && target.itemView.getRight() > selected.itemView.getRight()) {
                        score = Math.abs(diff);
                        if (score > winnerScore) {
                            winnerScore = score;
                            winner = target;
                        }
                    }
                }

                if (dx < 0) {
                    diff = target.itemView.getLeft() - curX;
                    if (diff > 0 && target.itemView.getLeft() < selected.itemView.getLeft()) {
                        score = Math.abs(diff);
                        if (score > winnerScore) {
                            winnerScore = score;
                            winner = target;
                        }
                    }
                }

                if (dy < 0) {
                    diff = target.itemView.getTop() - curY;
                    if (diff > 0 && target.itemView.getTop() < selected.itemView.getTop()) {
                        score = Math.abs(diff);
                        if (score > winnerScore) {
                            winnerScore = score;
                            winner = target;
                        }
                    }
                }

                if (dy > 0) {
                    diff = target.itemView.getBottom() - bottom;
                    if (diff < 0 && target.itemView.getBottom() > selected.itemView.getBottom()) {
                        score = Math.abs(diff);
                        if (score > winnerScore) {
                            winnerScore = score;
                            winner = target;
                        }
                    }
                }
            }

            return winner;
        }

        public abstract void onSwiped(ViewHolder var1, int var2);

        public View getItemFrontView(ViewHolder viewHolder) {
            if (viewHolder == null) {
                return null;
            }
            ViewGroup viewGroup = (ViewGroup) viewHolder.itemView;
            if (viewHolder.itemView instanceof ViewGroup && ((ViewGroup) viewHolder.itemView).getChildCount() > 1) {
                return viewGroup.getChildAt(viewGroup.getChildCount() - 1);
            } else if (viewGroup.getChildAt(viewGroup.getChildCount() - 1) instanceof CardView) {
                CardView cardView = (CardView) viewGroup.getChildAt(viewGroup.getChildCount() - 1);
                return cardView.getChildAt(cardView.getChildCount() - 1);
            } else if (viewGroup.getChildAt(viewGroup.getChildCount() - 1) instanceof FrameLayout) {
                FrameLayout cardView = (FrameLayout) viewGroup.getChildAt(viewGroup.getChildCount() - 1);
                return cardView.getChildAt(cardView.getChildCount() - 1);
            } else {
                return viewHolder.itemView;
            }


        }

        public void onSelectedChanged(ViewHolder viewHolder, int actionState) {
            if (viewHolder != null) {
                sUICallback.onSelected(viewHolder.itemView);
            }

        }

        private int getMaxDragScroll(RecyclerView recyclerView) {
            if (this.mCachedMaxScrollSpeed == -1) {
                this.mCachedMaxScrollSpeed = recyclerView.getResources().getDimensionPixelSize(R.dimen.item_touch_helper_max_drag_scroll_per_frame);
            }

            return this.mCachedMaxScrollSpeed;
        }

        public void onMoved(RecyclerView recyclerView, ViewHolder viewHolder, int fromPos, ViewHolder target, int toPos, int x, int y) {
            RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
            if (layoutManager instanceof ViewDropHandler) {
                ((ViewDropHandler) layoutManager).prepareForDrop(viewHolder.itemView, target.itemView, x, y);
            } else {
                int minTop;
                int maxBottom;
                if (layoutManager.canScrollHorizontally()) {
                    minTop = layoutManager.getDecoratedLeft(target.itemView);
                    if (minTop <= recyclerView.getPaddingLeft()) {
                        recyclerView.scrollToPosition(toPos);
                    }

                    maxBottom = layoutManager.getDecoratedRight(target.itemView);
                    if (maxBottom >= recyclerView.getWidth() - recyclerView.getPaddingRight()) {
                        recyclerView.scrollToPosition(toPos);
                    }
                }

                if (layoutManager.canScrollVertically()) {
                    minTop = layoutManager.getDecoratedTop(target.itemView);
                    if (minTop <= recyclerView.getPaddingTop()) {
                        recyclerView.scrollToPosition(toPos);
                    }

                    maxBottom = layoutManager.getDecoratedBottom(target.itemView);
                    if (maxBottom >= recyclerView.getHeight() - recyclerView.getPaddingBottom()) {
                        recyclerView.scrollToPosition(toPos);
                    }
                }

            }
        }

        private void onDraw(Canvas c, RecyclerView parent, ViewHolder selected, List<RecoverAnimation> recoverAnimationList, int actionState, float dX, float dY) {
            int recoverAnimSize = recoverAnimationList.size();

            int count;
            for (count = 0; count < recoverAnimSize; ++count) {
                RecoverAnimation anim = recoverAnimationList.get(count);
                anim.update();
                int count1 = c.save();
                this.onChildDraw(c, parent, anim.mViewHolder, anim.mX, anim.mY, anim.mActionState, false);
                c.restoreToCount(count1);
            }

            if (selected != null) {
                count = c.save();
                this.onChildDraw(c, parent, selected, dX, dY, actionState, true);
                c.restoreToCount(count);
            }

        }

        private void onDrawOver(Canvas c, RecyclerView parent, ViewHolder selected, List<RecoverAnimation> recoverAnimationList, int actionState, float dX, float dY) {
            int recoverAnimSize = recoverAnimationList.size();

            int hasRunningAnimation;
            for (hasRunningAnimation = 0; hasRunningAnimation < recoverAnimSize; ++hasRunningAnimation) {
                RecoverAnimation i = recoverAnimationList.get(hasRunningAnimation);
                int anim = c.save();
                this.onChildDrawOver(c, parent, i.mViewHolder, i.mX, i.mY, i.mActionState, false);
                c.restoreToCount(anim);
            }

            if (selected != null) {
                hasRunningAnimation = c.save();
                this.onChildDrawOver(c, parent, selected, dX, dY, actionState, true);
                c.restoreToCount(hasRunningAnimation);
            }

            boolean var12 = false;

            for (int var13 = recoverAnimSize - 1; var13 >= 0; --var13) {
                RecoverAnimation var14 = recoverAnimationList.get(var13);
                if (var14.mEnded && !var14.mIsPendingCleanup) {
                    recoverAnimationList.remove(var13);
                } else if (!var14.mEnded) {
                    var12 = true;
                }
            }

            if (var12) {
                parent.invalidate();
            }

        }

        public void clearView(RecyclerView recyclerView, ViewHolder viewHolder) {
            sUICallback.clearView(viewHolder.itemView);
        }

        public void onChildDraw(Canvas c, RecyclerView recyclerView, ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            sUICallback.onDraw(c, recyclerView, viewHolder.itemView, dX, dY, actionState, isCurrentlyActive);
        }

        public void onChildDrawOver(Canvas c, RecyclerView recyclerView, ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            sUICallback.onDrawOver(c, recyclerView, viewHolder.itemView, dX, dY, actionState, isCurrentlyActive);
        }

        public long getAnimationDuration(RecyclerView recyclerView, int animationType, float animateDx, float animateDy) {
            RecyclerView.ItemAnimator itemAnimator = recyclerView.getItemAnimator();
            return itemAnimator == null ? (animationType == 8 ? 200L : 250L) : (animationType == 8 ? itemAnimator.getMoveDuration() : itemAnimator.getRemoveDuration());
        }

        public int interpolateOutOfBoundsScroll(RecyclerView recyclerView, int viewSize, int viewSizeOutOfBounds, int totalSize, long msSinceStartScroll) {
            int maxScroll = this.getMaxDragScroll(recyclerView);
            int absOutOfBounds = Math.abs(viewSizeOutOfBounds);
            int direction = (int) Math.signum((float) viewSizeOutOfBounds);
            float outOfBoundsRatio = Math.min(1.0F, 1.0F * (float) absOutOfBounds / (float) viewSize);
            int cappedScroll = (int) ((float) (direction * maxScroll) * sDragViewScrollCapInterpolator.getInterpolation(outOfBoundsRatio));
            float timeRatio;
            if (msSinceStartScroll > 2000L) {
                timeRatio = 1.0F;
            } else {
                timeRatio = (float) msSinceStartScroll / 2000.0F;
            }

            int value = (int) ((float) cappedScroll * sDragScrollInterpolator.getInterpolation(timeRatio));
            return value == 0 ? (viewSizeOutOfBounds > 0 ? 1 : -1) : value;
        }
    }

    static class Gingerbread implements ItemTouchUIUtil {
        Gingerbread() {
        }

        private void draw(Canvas c, RecyclerView parent, View view, float dX, float dY) {
            c.save();
            c.translate(dX, dY);
            parent.drawChild(c, view, 0L);
            c.restore();
        }

        public void clearView(View view) {
            view.setVisibility(0);
        }

        public void onSelected(View view) {
            view.setVisibility(4);
        }

        public void onDraw(Canvas c, RecyclerView recyclerView, View view, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            if (actionState != 2) {
                this.draw(c, recyclerView, view, dX, dY);
            }

        }

        public void onDrawOver(Canvas c, RecyclerView recyclerView, View view, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            if (actionState == 2) {
                this.draw(c, recyclerView, view, dX, dY);
            }

        }
    }

    static class Honeycomb implements ItemTouchUIUtil {
        Honeycomb() {
        }

        public void clearView(View view) {
            view.setTranslationX(0.0F);
            view.setTranslationY(0.0F);
        }

        public void onSelected(View view) {
        }

        public void onDraw(Canvas c, RecyclerView recyclerView, View view, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            view.setTranslationX(dX);
            view.setTranslationY(dY);
        }

        public void onDrawOver(Canvas c, RecyclerView recyclerView, View view, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        }
    }

    static class Lollipop extends Honeycomb {
        Lollipop() {
        }

        public void onDraw(Canvas c, RecyclerView recyclerView, View view, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            if (isCurrentlyActive) {
                Object originalElevation = view.getTag(R.id.item_touch_helper_previous_elevation);
                if (originalElevation == null) {
                    Float originalElevation1 = Float.valueOf(ViewCompat.getElevation(view));
                    float newElevation = 1.0F + this.findMaxElevation(recyclerView, view);
                    ViewCompat.setElevation(view, newElevation);
                    view.setTag(R.id.item_touch_helper_previous_elevation, originalElevation1);
                }
            }

            super.onDraw(c, recyclerView, view, dX, dY, actionState, isCurrentlyActive);
        }

        private float findMaxElevation(RecyclerView recyclerView, View itemView) {
            int childCount = recyclerView.getChildCount();
            float max = 0.0F;

            for (int i = 0; i < childCount; ++i) {
                View child = recyclerView.getChildAt(i);
                if (child != itemView) {
                    float elevation = ViewCompat.getElevation(child);
                    if (elevation > max) {
                        max = elevation;
                    }
                }
            }

            return max;
        }

        public void clearView(View view) {
            Object tag = view.getTag(R.id.item_touch_helper_previous_elevation);
            if (tag != null && tag instanceof Float) {
                ViewCompat.setElevation(view, ((Float) tag).floatValue());
            }

            view.setTag(R.id.item_touch_helper_previous_elevation, null);
            super.clearView(view);
        }
    }

    private class RecoverAnimation implements AnimatorListenerCompat {
        final float mStartDx;
        final float mStartDy;
        final float mTargetX;
        final float mTargetY;
        final RecyclerView.ViewHolder mViewHolder;
        final int mActionState;
        private final ValueAnimatorCompat mValueAnimator;
        private final int mAnimationType;
        public boolean mIsPendingCleanup;
        float mX;
        float mY;
        boolean mOverridden = false;
        private boolean mEnded = false;
        private float mFraction;

        public RecoverAnimation(RecyclerView.ViewHolder viewHolder, int animationType, int actionState, float startDx, float startDy, float targetX, float targetY) {
            this.mActionState = actionState;
            this.mAnimationType = animationType;
            this.mViewHolder = viewHolder;
            this.mStartDx = startDx;
            this.mStartDy = startDy;
            this.mTargetX = targetX;
            this.mTargetY = targetY;
            this.mValueAnimator = AnimatorCompatHelper.emptyValueAnimator();
            this.mValueAnimator.addUpdateListener(new AnimatorUpdateListenerCompat() {
                public void onAnimationUpdate(ValueAnimatorCompat animation) {
                    RecoverAnimation.this.setFraction(animation.getAnimatedFraction());
                }
            });
            this.mValueAnimator.setTarget(viewHolder.itemView);
            this.mValueAnimator.addListener(this);
            this.setFraction(0.0F);
        }

        public void setDuration(long duration) {
            this.mValueAnimator.setDuration(duration);
        }

        public void start() {
            this.mViewHolder.setIsRecyclable(false);
            this.mValueAnimator.start();
        }

        public void cancel() {
            this.mValueAnimator.cancel();
        }

        public void setFraction(float fraction) {
            this.mFraction = fraction;
        }

        public void update() {
            if (this.mStartDx == this.mTargetX) {
                this.mX = this.mViewHolder.itemView.getTranslationX();
            } else {
                this.mX = this.mStartDx + this.mFraction * (this.mTargetX - this.mStartDx);
            }

            if (this.mStartDy == this.mTargetY) {
                this.mY = this.mViewHolder.itemView.getTranslationY();
            } else {
                this.mY = this.mStartDy + this.mFraction * (this.mTargetY - this.mStartDy);
            }

        }

        public void onAnimationStart(ValueAnimatorCompat animation) {
        }

        public void onAnimationEnd(ValueAnimatorCompat animation) {
            if (!this.mEnded) {
                this.mViewHolder.setIsRecyclable(true);
            }

            this.mEnded = true;
        }

        public void onAnimationCancel(ValueAnimatorCompat animation) {
            this.setFraction(1.0F);
        }

        public void onAnimationRepeat(ValueAnimatorCompat animation) {
        }
    }

    private class ItemTouchHelperGestureListener extends SimpleOnGestureListener {
        private ItemTouchHelperGestureListener() {
        }

        public boolean onDown(MotionEvent e) {
            return true;
        }

        public void onLongPress(MotionEvent e) {
            View child = ItemTouchHelperExtension.this.findChildView(e);
            if (child != null) {
                RecyclerView.ViewHolder vh = ItemTouchHelperExtension.this.mRecyclerView.getChildViewHolder(child);
                if (vh != null) {
                    if (!ItemTouchHelperExtension.this.mCallback.hasDragFlag(ItemTouchHelperExtension.this.mRecyclerView, vh)) {
                        return;
                    }

                    int pointerId = e.getPointerId(0);
                    if (pointerId == ItemTouchHelperExtension.this.mActivePointerId) {
                        int index = e.findPointerIndex(ItemTouchHelperExtension.this.mActivePointerId);
                        float x = e.getX(index);
                        float y = e.getY(index);
                        ItemTouchHelperExtension.this.mInitialTouchX = x;
                        ItemTouchHelperExtension.this.mInitialTouchY = y;
                        ItemTouchHelperExtension.this.mDx = ItemTouchHelperExtension.this.mDy = 0.0F;
                        if (ItemTouchHelperExtension.this.mCallback.isLongPressDragEnabled()) {
                            ItemTouchHelperExtension.this.select(vh, 2);
                        }
                    }
                }
            }

        }

        public boolean onContextClick(MotionEvent e) {
            return super.onContextClick(e);
        }
    }


}
