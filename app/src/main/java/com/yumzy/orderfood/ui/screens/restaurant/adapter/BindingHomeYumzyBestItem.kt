package com.yumzy.orderfood.ui.screens.restaurant.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.binding.ModelAbstractBindingItem
import com.mikepenz.fastadapter.listeners.ClickEventHook
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.databinding.AdapterHomeYumzyBestBinding
import com.yumzy.orderfood.ui.helper.UIHelper

class BindingHomeYumzyBestItem(private val item: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, AdapterHomeYumzyBestBinding>(item) {

//    private var item: HomeListDTO? = null

    override val type: Int = R.id.item_yumzy_best_id
/*
    fun withNewLaunchItem(item: HomeListDTO): BindingHomeYumzyBestItem {
        this.item = item
        return this
    }*/


    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterHomeYumzyBestBinding {
        return AdapterHomeYumzyBestBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: AdapterHomeYumzyBestBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)
        binding.materialCardview.radius = UIHelper.cornerRadius

        binding.title = item.title.toString().capitalize()
        binding.description = item.subTitle.toString()
        binding.imageUrl = item.image?.url

        /*TODO add ribbon layout*/
//        binding.ribbonText = homeList.bannerText
    }

    override fun unbindView(binding: AdapterHomeYumzyBestBinding) {
        binding.title = null
        binding.description = null
//        binding.imageUrl = null
    }


    class HomeYumzyBestClickEvent(val callBack: (item: HomeListDTO, view: View) -> Unit) :
        ClickEventHook<BindingHomeYumzyBestItem>() {

        override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
            return viewHolder.itemView
        }

        override fun onClick(
            v: View,
            position: Int,
            fastAdapter: FastAdapter<BindingHomeYumzyBestItem>,
            item: BindingHomeYumzyBestItem
        ) {
            callBack(item.item, v)
        }
    }
}