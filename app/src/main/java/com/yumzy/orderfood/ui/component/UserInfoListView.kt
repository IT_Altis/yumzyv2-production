package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.widget.LinearLayout
import android.widget.TextView
import com.yumzy.orderfood.data.models.ProfileResponseDTO
import com.yumzy.orderfood.ui.helper.setInterBoldFont
import com.yumzy.orderfood.util.IConstants
import com.laalsa.laalsalib.ui.VUtil

class UserInfoListView : LinearLayout, UserItemInfoView.OnUserItemInfoListener {

    private lateinit var mOnUserInfoClickListener: OnUserInfoClickListener

    private val i18px = VUtil.dpToPx(18)
    private val i10px = VUtil.dpToPx(10)

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, atr: AttributeSet?) : this(context, atr, 0)
    constructor(context: Context, atr: AttributeSet?, style: Int) : super(context, atr, style) {

        this.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        this.orientation = VERTICAL

    }

    private fun String.getSectionTitleView(): TextView {
        val titleTextView = TextView(context)
        titleTextView.setPadding(
            i10px,
            i18px,
            i10px,
            i18px
        )
        titleTextView.setInterBoldFont()
        titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)
        titleTextView.text = this
        return titleTextView
    }

    fun setUserInfo(user: ProfileResponseDTO) {

        this.addView("Basic Information".getSectionTitleView())

        val userNameView = UserItemInfoView(context)
        userNameView.setPadding(
            i10px,
            i18px,
            i10px,
            i18px
        )

        user.name?.let {
            userNameView.setUserItemInfo(
                IConstants.UserModule.USER_NAME,
                "Name",
                it, IConstants.UserUploadModule.USER_NAME_KEY
            )
        }
        userNameView.setUserItemClickListener(this)
        this.addView(userNameView)


        val userNumberView = UserItemInfoView(context)
        userNumberView.setPadding(
            i10px,
            i18px,
            i10px,
            i18px
        )

        userNumberView.setUserItemInfo(
            IConstants.UserModule.USER_NUMBER,
            "Mobile",
            user.mobile, IConstants.UserUploadModule.USER_NUMBER_KEY
        )
        userNumberView.setUserItemClickListener(this)
        this.addView(userNumberView)


        val userEmailView = UserItemInfoView(context)
        userEmailView.setPadding(
            i10px,
            i18px,
            i10px,
            i18px
        )
        userEmailView.setUserItemInfo(
            IConstants.UserModule.USER_EMAIL,
            "Email",
            user.email,
            IConstants.UserUploadModule.USER_EMAIL_KEY
        )
        userEmailView.setUserItemClickListener(this)
        this.addView(userEmailView)

    }

    fun setUserInfoClickListener(onUserInfoClickListener: OnUserInfoClickListener) {
        mOnUserInfoClickListener = onUserInfoClickListener
    }


    interface OnUserInfoClickListener {
        fun onUserInfoClicked(
            moduleId: Int,
            userInfoTitle: String = "",
            userInfoValue: String = "",
            userKey: String
        )
    }

    override fun onUserItemInfoClicked(
        moduleId: Int,
        userInfoTitle: String,
        userInfoValue: String,
        userKey: String
    ) {
        mOnUserInfoClickListener.onUserInfoClicked(moduleId, userInfoTitle, userInfoValue, userKey)
    }

}