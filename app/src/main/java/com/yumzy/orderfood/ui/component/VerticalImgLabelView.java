package com.yumzy.orderfood.ui.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.yumzy.orderfood.R;
import com.laalsa.laalsalib.ui.VUtil;

/**
 * Created by Bhupendra Kumar Sahu
 */
public class VerticalImgLabelView extends LinearLayout {

    private String smallTitle;
    private String title;
    private String subTitle;
    private Drawable ivLocation;


    public VerticalImgLabelView(Context context) {
        this(context, null);
    }

    public VerticalImgLabelView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VerticalImgLabelView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);


        if (attrs != null)
            initAttrs(context, attrs, defStyleAttr);
        if (title == null) {
            title = "";
        }
        if (smallTitle == null) {
            smallTitle = "";
        }
        if (subTitle == null) {
            subTitle = "";
        }
        if (ivLocation == null) {
        }

        init();
    }

    private void initAttrs(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.VerticalImageLabelView, defStyleAttr, 0);
        smallTitle = typedArray.getString(R.styleable.VerticalImageLabelView_order_status);
        subTitle = typedArray.getString(R.styleable.VerticalImageLabelView_address1);
        title = typedArray.getString(R.styleable.VerticalImageLabelView_house_name);
        ivLocation = typedArray.getDrawable(R.styleable.VerticalImageLabelView_location_logo);
        typedArray.recycle();
    }


    private void init() {
        this.setOrientation(HORIZONTAL);
        this.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        LinearLayout containerLayout = new LinearLayout(getContext());
        containerLayout.setOrientation(VERTICAL);

        int iv40px = VUtil.dpToPx(40);
        int tv2px = VUtil.dpToPx(2);
        int ll10px = VUtil.dpToPx(10);

        ImageView ivLocation = new ImageView(getContext());
        ivLocation.setImageDrawable(ivLocation.getDrawable());
        ivLocation.setId(R.id.location_logo_address_details);
        ivLocation.setLayoutParams(new LayoutParams(iv40px, iv40px));


        LayoutParams layoutParamsStatus = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParamsStatus.setMargins(0, tv2px, 0, 0);
        TextView tvCurrentStatus = new TextView(getContext());
        tvCurrentStatus.setTextSize(14f);
        tvCurrentStatus.setMaxLines(1);
        tvCurrentStatus.setText(smallTitle);
        tvCurrentStatus.setId(R.id.tv_current_status_addressdetails);
        tvCurrentStatus.setLayoutParams(layoutParamsStatus);
        tvCurrentStatus.setGravity(Gravity.START);
        tvCurrentStatus.setTextColor(ContextCompat.getColor(getContext(), android.R.color.holo_red_dark));


        LayoutParams layoutParamsHouseName = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParamsHouseName.setMargins(0, tv2px, 0, 0);
        TextView tvHouseName = new TextView(getContext());
        tvHouseName.setTextSize(22f);
        tvHouseName.setMaxLines(2);
        tvHouseName.setText(title);
        tvHouseName.setId(R.id.tv_house_name_address_details);
        tvHouseName.setTypeface(Typeface.DEFAULT_BOLD);
        tvHouseName.setLayoutParams(layoutParamsHouseName);
        tvHouseName.setGravity(Gravity.START);
        tvHouseName.setTextColor(ContextCompat.getColor(getContext(), android.R.color.black));


        LayoutParams layoutParamsAddress = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParamsAddress.setMargins(0, tv2px, 0, 0);

        TextView tvAddress = new TextView(getContext());
        tvAddress.setTextSize(16f);
        tvAddress.setMaxLines(2);
        tvAddress.setText(subTitle);
        tvAddress.setId(R.id.tv_address_address_details);
        tvAddress.setLayoutParams(layoutParamsAddress);
        tvAddress.setGravity(Gravity.START);
        tvAddress.setTextColor(ContextCompat.getColor(getContext(), android.R.color.darker_gray));


        containerLayout.addView(ivLocation);
        containerLayout.addView(tvCurrentStatus);
        containerLayout.addView(tvHouseName);
        containerLayout.addView(tvAddress);

        LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.setMargins(ll10px, ll10px, ll10px, ll10px);
        this.addView(containerLayout, layoutParams);
    }

    public String getSmallTitle() {
        return smallTitle;
    }

    public void setSmallTitle(String smallTitle) {
        this.smallTitle = smallTitle;
        ((TextView) this.findViewById(R.id.tv_current_status_addressdetails)).setText(this.smallTitle);

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        ((TextView) this.findViewById(R.id.tv_house_name_address_details)).setText(this.title);

    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
        ((TextView) this.findViewById(R.id.tv_address_address_details)).setText(this.subTitle);

    }

    public Drawable getIvLocation() {
        return ivLocation;
    }

    public void setIvLocation(Drawable ivLocation) {
        this.ivLocation = ivLocation;
        ((ImageView) findViewById(R.id.location_logo_address_details)).setImageDrawable(ivLocation);

    }
}

