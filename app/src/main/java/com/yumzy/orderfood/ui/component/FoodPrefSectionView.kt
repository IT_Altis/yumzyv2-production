package com.yumzy.orderfood.ui.component

import android.content.Context
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import android.widget.Toast
import androidx.annotation.IntRange
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.ui.px
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.ISelectionListener
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.select.SelectExtension
import com.mikepenz.fastadapter.select.getSelectExtension
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.CustomizeOrderSectionDTO
import com.yumzy.orderfood.data.models.FoodPrefSectionDTO
import com.yumzy.orderfood.ui.common.GridItemDecoration
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.screens.module.foodpreference.FoodPreferenceDialog
import com.yumzy.orderfood.ui.screens.module.restaurant.OrderCustomizationDialog
import com.yumzy.orderfood.util.IConstants

class FoodPrefSectionView : RecyclerView {

    //save our FastAdapter
    private var fastItemAdapter: FastItemAdapter<FoodPrefCuisineItem>
    private var selectExtension: SelectExtension<FoodPrefCuisineItem>

    private var maximumSelection: Int = NO_LIMIT
    private var minimumSelection: Int = NO_LIMIT
    private var onGoingInitialSelection: Boolean = true

    internal var section: FoodPrefSectionDTO? = null
    private var listener: FoodPreferenceDialog.FoodPrefChangeListener? = null

    init {

        //create our FastAdapter which will manage everything
        fastItemAdapter = FastItemAdapter()
        selectExtension = fastItemAdapter.getSelectExtension()
        selectExtension.isSelectable = true

        //configure our fastAdapter
        fastItemAdapter.onClickListener =
            { v: View?, _: IAdapter<FoodPrefCuisineItem>, item: FoodPrefCuisineItem, _: Int ->
                v?.let {
                    Toast.makeText(v.context, item.data?.displayName, Toast.LENGTH_LONG)
                        .show()
                }
                false
            }
        fastItemAdapter.onPreClickListener =
            { _: View?, _: IAdapter<FoodPrefCuisineItem>, _: FoodPrefCuisineItem, _: Int ->
                true // consume otherwise radio/checkbox will be deselected
            }


        fastItemAdapter.addEventHook(FoodPrefCuisineItem.FoodPrefClickEvent())

        selectExtension.selectionListener =
            object : ISelectionListener<FoodPrefCuisineItem> {
                override fun onSelectionChanged(
                    item: FoodPrefCuisineItem,
                    selected: Boolean
                ) {
                    onItemSelectionChanged(item, selected, selectExtension.selectedItems)
                }
            }

    }


    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        //get our recyclerView and do basic setup
        this.layoutManager = GridLayoutManager(context, 3)
        this.addItemDecoration(GridItemDecoration(12.px, 12.px))
        this.itemAnimator = DefaultItemAnimator()
        this.overScrollMode = OVER_SCROLL_NEVER
        this.adapter = fastItemAdapter

    }

    private fun setSelectionConstraints(
        @IntRange(from = 1) minimumSelection: Int,
        @IntRange(from = 1) maximumSelection: Int
    ) {
        if (maximumSelection < minimumSelection) {
            throw IllegalArgumentException("maximumsSelction: ${maximumSelection} cannot be less than minimumSelection: ${minimumSelection}")
        }
        this.minimumSelection = minimumSelection
        this.maximumSelection = maximumSelection

        if (selectExtension.selectedItems.size >= minimumSelection) return
        //Select initial items to the min value
        for (i in 0 until minimumSelection) {
            selectExtension.toggleSelection(i)
        }

    }

    private fun onItemSelectionChanged(
        actionItem: FoodPrefCuisineItem,
        selected: Boolean,
        selectedItems: MutableSet<FoodPrefCuisineItem>
    ) {

        if (onGoingInitialSelection) return

        //check the conditions for max selection and min selections
        if (selectedItems.size < minimumSelection) {
            //handling for selection less than @minimumSelection
            var addSelectionCount = minimumSelection - selectedItems.size
            for (i in 0 until fastItemAdapter.itemCount) {
                if (fastItemAdapter.getItem(i)?.isSelected == false) {
                    selectExtension.toggleSelection(i)
                    addSelectionCount--
                } else {
                    continue
                }
                if (addSelectionCount <= 0) break
            }

        } else if (selectedItems.size > maximumSelection) {
            //handling for selection greater than @maximumSelection
            var removeSelectionCount = selectedItems.size - maximumSelection
            for (i in 0 until fastItemAdapter.itemCount) {
                val item = fastItemAdapter.getItem(i)
                if (item?.isSelected == false) {
                    continue
                } else if (item?.identifier != actionItem.identifier) {
                    selectExtension.toggleSelection(i)
                    removeSelectionCount--
                }
                if (removeSelectionCount <= 0) break
            }
        }

        listener?.onCustomisationChange(
            section?.sectionIdentifier.toString(),
            selectExtension.selectedItems.map { item -> item.data!! }.toList()
        )
    }

    fun withAddonSection(
        listener: FoodPreferenceDialog.FoodPrefChangeListener,
        section: FoodPrefSectionDTO
    ): FoodPrefSectionView {

        this.section = section
        this.listener = listener

        val items = ArrayList<FoodPrefCuisineItem>()
        for (data in section.items) {
            items.add(
                FoodPrefCuisineItem().withData(data, section.isMultiSelect)
            )
        }

        fastItemAdapter.add(items)

        /*
        NOTE: Use below code when we get pre selected food prefs.
        section.items.forEachIndexed { index, customizeOrderItemDTO ->
            if (customizeOrderItemDTO.isSelected) selectExtension.toggleSelection(index)
        }*/

        /*NOTE: set below selection constriat when we get from backend.*/
        if(section.isMultiSelect) {
            setSelectionConstraints(1, 100)
        }else{
            setSelectionConstraints(1,1)
        }
        onGoingInitialSelection = false

//        selectExtension.select(0)

        listener.onCustomisationChange(
            section.sectionIdentifier,
            selectExtension.selectedItems.map { item -> item.data!! }.toList()
        )

        return this
    }

    fun getSelectedOptionsByName(): List<String> {
        return selectExtension.selectedItems.map { item ->
            item.data?.displayName.toString()
        }
    }

    fun getSelectedOptionsByPrefId(): List<String> {
        return selectExtension.selectedItems.map { item ->
            item.data?.prefId.toString()
        }
    }

    fun restoreOnConfigChange(savedInstanceState: Bundle?) {
        //restore selections (this has to be done after the items were added
        fastItemAdapter.withSavedInstanceState(savedInstanceState)
    }

    companion object {
        const val NO_LIMIT = -1
    }


}