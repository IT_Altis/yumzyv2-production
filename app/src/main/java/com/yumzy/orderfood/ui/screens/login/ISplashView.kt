package com.yumzy.orderfood.ui.screens.login

import com.google.android.gms.maps.model.LatLng
import com.yumzy.orderfood.ui.base.IView

/**
 * Created by Bhupendra Kumar Sahu on 03-Dec-20.
 */
interface ISplashView : IView {
    fun navHomeActivity()

}