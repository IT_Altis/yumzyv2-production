package com.yumzy.orderfood.ui.screens.home.fragment

import com.yumzy.orderfood.ui.base.IView

interface IAccountView : IView {
    fun accountOptionList()
    fun tempAccountOptionList()
//    fun signOutUser()

}
