package com.yumzy.orderfood.ui.screens.restaurant

import android.view.View

/**
 * Created by Bhupendra Kumar Sahu.
 */
interface RecyclerviewCallbacks<T> {
    fun onItemClick(view: View, position: Int, item: T)

}