package com.yumzy.orderfood.ui.common.search;

import android.text.Editable;
import android.text.TextWatcher;


public abstract class SimpleTextWatcher implements TextWatcher {

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // No action
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // No action
    }

    @Override
    public void afterTextChanged(Editable s) {
        // No action
    }
}
