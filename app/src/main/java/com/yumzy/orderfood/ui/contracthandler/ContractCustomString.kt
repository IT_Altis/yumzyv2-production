package com.yumzy.orderfood.ui.contracthandler

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract

class ContractCustomString : ActivityResultContract<Int, String>() {

    companion object {
        const val ACTION = "com.laalsa.yumzy.MY_ACTION"
        const val INPUT_INT = "input_int"
        const val OUTPUT_STRING = "output_string"
    }


    override fun parseResult(resultCode: Int, intent: Intent?): String? {
        return when (resultCode) {
            Activity.RESULT_OK -> intent?.getStringExtra(OUTPUT_STRING)
            else -> null
        }
    }

    override fun createIntent(p0: Context, p1: Int?): Intent {
        return Intent(ACTION)
            .apply { putExtra(INPUT_INT, p1) }
    }
}