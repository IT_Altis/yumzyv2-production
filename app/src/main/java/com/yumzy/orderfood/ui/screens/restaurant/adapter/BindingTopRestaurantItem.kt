package com.yumzy.orderfood.ui.screens.restaurant.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.mikepenz.fastadapter.listeners.ClickEventHook
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.databinding.AdapterTopRestaurantBinding
import com.yumzy.orderfood.databinding.AdapterWinterSpecailBinding
import com.yumzy.orderfood.util.viewutils.YumUtil

class BindingTopRestaurantItem : AbstractBindingItem<AdapterTopRestaurantBinding>() {

    internal var item: HomeListDTO? = null

    override val type: Int = R.id.item_top_restaurant_id

    fun withTopRestaurantItem(item: HomeListDTO): BindingTopRestaurantItem {
        this.item = item
        return this
    }


    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterTopRestaurantBinding {
        return AdapterTopRestaurantBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: AdapterTopRestaurantBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)


        binding.title = item?.title ?: "-"
        binding.subtitle = item?.subTitle.toString()
        binding.cuisines =
            item?.cuisines?.joinToString(separator = ", ", postfix = "") ?: "New Arrivals"
        val zoneOffer = YumUtil.setZoneOfferString(item?.meta?.offers)
        binding.offer = zoneOffer

//        binding.isVeg = homeList?.meta?.isVeg ?: true
        val ratting = item?.rating.toString()
        if (!ratting.isNullOrEmpty() && !ratting.equals("null")) {
            /*TODO handle rating here*/
//            binding.rating = ratting
        }

        binding.duration = YumUtil.getDurationAndCostForTwo(item!!)

        binding.imageUrl = item?.image?.url

    }

    override fun unbindView(binding: AdapterTopRestaurantBinding) {
//        binding.imageUrl = null

        binding.title = null
        binding.subtitle = null
        binding.cuisines = null
        binding.offer = null
//        binding.rating = null
        binding.duration = null
    }


    class TopRestaurantClickEventHook(val callBack: (item: HomeListDTO, view: View) -> Unit) :
        ClickEventHook<BindingTopRestaurantItem>() {

        override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
            return viewHolder.itemView
        }

        override fun onClick(
            v: View,
            position: Int,
            fastAdapter: FastAdapter<BindingTopRestaurantItem>,
            item: BindingTopRestaurantItem
        ) {
            callBack(item.item!!, v)
        }
    }
}