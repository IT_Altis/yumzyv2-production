package com.yumzy.orderfood.ui.base

interface IView {
    fun showProgressBar()
    fun hideProgressBar()
    fun showCodeError(code: Int?, title: String? = "", message: String = "")
    fun onResponse(code: Int?, response: Any?)
}