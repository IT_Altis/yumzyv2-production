package com.yumzy.orderfood.ui.common.toolbar;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.appcompat.widget.ActionMenuView;
import androidx.appcompat.widget.Toolbar;

import com.yumzy.orderfood.R;

public class YSearchToolbar extends Toolbar {
    private View view;
    private Context clContext;
    private int iMenu;
    private String sTitle;
    private boolean bActionModeStart = false;
    private int iToolbarChildColor;

    public YSearchToolbar(Context context) {
        super(context);
        this.clContext = context;
        this.iToolbarChildColor = Color.WHITE;
        initToolberChlidColors(this, iToolbarChildColor, (Activity) clContext);
    }

    public YSearchToolbar(Context context, int iPrimaryColor, int iToolbarChildColor) {
        super(context);
        this.clContext = context;
        this.iToolbarChildColor = iToolbarChildColor;
        this.setBackgroundColor(iPrimaryColor);
        initToolberChlidColors(this, iToolbarChildColor, (Activity) clContext);
    }

    public YSearchToolbar(Context context, int iMenu, String sTitle) {
        super(context);
        this.clContext = context;
        this.iMenu = iMenu;
        this.sTitle = sTitle;
        super.setTitle(sTitle);
        this.iToolbarChildColor = Color.WHITE;
        initToolberChlidColors(this, iToolbarChildColor, (Activity) clContext);
    }

    public YSearchToolbar(Context context, int iMenu, String sTitle, int iPrimaryColor, int iToolbarChildColor) {
        super(context);
        this.clContext = context;
        this.iToolbarChildColor = iToolbarChildColor;
        this.iMenu = iMenu;
        this.sTitle = sTitle;
        super.setTitle(sTitle);
        this.setBackgroundColor(iPrimaryColor);
        initToolberChlidColors(this, iToolbarChildColor, (Activity) clContext);
    }


    public int getDefaultMenu() {
        return iMenu;
    }

    public void setDefaultMenu(int iMenu) {
        this.iMenu = iMenu;
    }

    public String getTitle() {
        return (String) super.getTitle();
    }

    public void setTitle(String sTitle) {
        super.setTitle(sTitle);
    }

    public void startToolbarActionMode() {
        bActionModeStart = true;
//        getMenu().clear();
        setNavigationIcon(R.drawable.ic_left_arrow);
    }

    public void destroyActionMode() {
        bActionModeStart = false;
//        getMenu().clear();
        super.setTitle(sTitle);
        setNavigationOnClickListener(null);
        setNavigationIcon(null);

    }

    public boolean isActionModeEnabled() {
        return bActionModeStart;
    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        initToolberChlidColors(this, iToolbarChildColor, (Activity) clContext);
    }


    /**
     * Use this method to colorize toolbar icons to the desired target color
     *
     * @param toolbarView       toolbar view being colored
     * @param toolbarIconsColor the target color of toolbar icons
     * @param activity          reference to activity needed to register observers
     */
    private void initToolberChlidColors(Toolbar toolbarView, int toolbarIconsColor, Activity activity) {

        final PorterDuffColorFilter clPorterDuffColorFilter = new PorterDuffColorFilter(toolbarIconsColor, PorterDuff.Mode.SRC_IN);

        for (int i = 0; i < toolbarView.getChildCount(); i++) {
            final View clView = toolbarView.getChildAt(i);

            doColorizing(clView, clPorterDuffColorFilter, toolbarIconsColor);
        }

        //Step 3: Changing the color of title and subtitle .

        toolbarView.setTitleTextColor(toolbarIconsColor);
        toolbarView.setSubtitleTextColor(toolbarIconsColor);


        if (getChildAt(0) != null && getChildAt(0) instanceof TextView) {

            TextView clTextView1 = (TextView) getChildAt(0);
            clTextView1.setPadding(0, 0, 0, 0);
            LayoutParams clParams = (LayoutParams) clTextView1.getLayoutParams();
            clParams.setMargins(0, 0, 0, 0);
            if (clTextView1 != null)
                clTextView1.setTypeface(clTextView1.getTypeface(), Typeface.BOLD);

        }
        if (getChildAt(1) != null && getChildAt(1) instanceof ImageButton) {

            ImageButton clImageButton = (ImageButton) getChildAt(1);
            clImageButton.setPadding(0, 0, 0, 0);
            LayoutParams clParams = (LayoutParams) clImageButton.getLayoutParams();
            clParams.setMargins(0, 0, 0, 0);

        }


    }

    private void doColorizing(View clView, final ColorFilter colorFilter, int toolbarIconsColor) {
        if (clView instanceof ImageButton) {
            Drawable clDrawable = ((ImageButton) clView).getDrawable();
            if (clDrawable != null) {
                clDrawable.setAlpha(255);
                clDrawable.setColorFilter(colorFilter);
            }
        }

        if (clView instanceof ImageView) {
            Drawable clDrawable = ((ImageView) clView).getDrawable();
            if (clDrawable != null) {
                clDrawable.setAlpha(255);
                clDrawable.setColorFilter(colorFilter);
            }
        }
        if (clView instanceof TextView) {
            ((TextView) clView).setTextColor(toolbarIconsColor);


            for (Drawable drawable : ((TextView) clView).getCompoundDrawables()) {
                if (drawable != null) {
                    drawable.setColorFilter(colorFilter);
                }
            }

        }

        if (clView instanceof EditText) {
            ((EditText) clView).setTextColor(toolbarIconsColor);

            for (Drawable drawable : ((EditText) clView).getCompoundDrawables()) {
                if (drawable != null) {
                    drawable.setColorFilter(colorFilter);
                }
            }
        }

        if (clView instanceof AutoCompleteTextView) {
            ((AutoCompleteTextView) clView).setTextColor(Color.BLACK);
        }


        if (clView instanceof ViewGroup) {
            for (int lli = 0; lli < ((ViewGroup) clView).getChildCount(); lli++) {
                doColorizing(((ViewGroup) clView).getChildAt(lli), colorFilter, toolbarIconsColor);
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            if (clView instanceof ActionMenuView) {
                for (int j = 0; j < ((ActionMenuView) clView).getChildCount(); j++) {

                    //Step 2: Changing the color of any ActionMenuViews - icons that
                    //are not back button, nor text, nor overflow menu icon.
                    final View innerView = ((ActionMenuView) clView).getChildAt(j);

                    if (innerView instanceof ActionMenuItemView) {
                        int drawablesCount = ((ActionMenuItemView) innerView).getCompoundDrawables().length;
                        for (int k = 0; k < drawablesCount; k++) {
                            if (((ActionMenuItemView) innerView).getCompoundDrawables()[k] != null) {
                                final int finalK = k;

                                //Important to set the color filter in seperate thread,
                                //by adding it to the message queue
                                //Won't work otherwise.
                                //Works fine for my case but needs more testing

                                ((ActionMenuItemView) innerView).getCompoundDrawables()[finalK].setColorFilter(colorFilter);

//                              innerView.post(new Runnable() {
//                                  @Override
//                                  public void run() {
//                                      ((ActionMenuItemView) innerView).getCompoundDrawables()[finalK].setColorFilter(colorFilter);
//                                  }
//                              });
                            }
                        }
                    }
                }
            }
    }

    public int getToolbarChildColor() {
        return iToolbarChildColor;
    }

}
