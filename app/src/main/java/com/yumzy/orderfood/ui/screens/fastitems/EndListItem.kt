package com.yumzy.orderfood.ui.screens.fastitems

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.ItemListEndBinding
import com.yumzy.orderfood.databinding.ItemSeeAllEndBinding
import com.yumzy.orderfood.databinding.ItemSheetEndBinding

class EndListItem : AbstractBindingItem<ItemListEndBinding>() {
    override val type: Int
        get() = R.id.endlist_item_id

    override fun bindView(binding: ItemListEndBinding, payloads: List<Any>) {
    }

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ItemListEndBinding {
        return ItemListEndBinding.inflate(inflater, parent, false)
    }
}


class BottomSheetListItem : AbstractBindingItem<ItemSheetEndBinding>() {
    override val type: Int
        get() = R.id.endlist_item_id

    override fun bindView(binding: ItemSheetEndBinding, payloads: List<Any>) {
    }

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ItemSheetEndBinding {
        return ItemSheetEndBinding.inflate(inflater, parent, false)
    }
}


class SeeAllEndListItem : AbstractBindingItem<ItemSeeAllEndBinding>() {
    override val type: Int
        get() = R.id.see_all_item_id

    override fun bindView(binding: ItemSeeAllEndBinding, payloads: List<Any>) {
    }

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ItemSeeAllEndBinding {
        return ItemSeeAllEndBinding.inflate(inflater, parent, false)
    }
}

