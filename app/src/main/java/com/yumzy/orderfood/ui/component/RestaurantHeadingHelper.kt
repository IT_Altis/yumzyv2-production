package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.setInterBoldFont
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.ViewConst
import com.laalsa.laalsalib.ui.VUtil
import java.util.*

class RestaurantHeadingHelper(
    private val context: Context,
    private val title: String?
) {
    fun build(): View? {
        val title = (title ?: "").toLowerCase(Locale.getDefault())

        if (title.isEmpty())
            return View(context).apply {
                layoutParams =
                    LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
                        .apply {
                            setMargins(
                                UIHelper.i15px,
                                UIHelper.i25px,
                                UIHelper.i15px,
                                UIHelper.i8px
                            )
                        }
            }

        val view = getHeadingLayout(context, title)
        val layout = LinearLayout(context).apply {
            layoutParams =
                LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT).apply {
                    setMargins(UIHelper.i15px, UIHelper.i25px, UIHelper.i15px, UIHelper.i8px)
                }
        }
        layout.setPadding(UIHelper.i3px, UIHelper.i8px, UIHelper.i3px, UIHelper.i8px)

        layout.addView(view)
        layout.gravity = Gravity.CENTER_VERTICAL
        layout.tag = title.toLowerCase(Locale.getDefault())
        return layout
    }

    private fun getHeadingLayout(context: Context, title: String): View {
        return LinearLayout(context).apply {

            layoutParams =
                LinearLayout.LayoutParams(
                    0,
                    ViewConst.WRAP_CONTENT
                    , 1f
                )

            orientation = LinearLayout.VERTICAL
            val tvHeading = TextView(context)
            tvHeading.text = title.capitalize()
            tvHeading.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f)
            tvHeading.setInterBoldFont()
            addView(tvHeading)

        }
    }
}