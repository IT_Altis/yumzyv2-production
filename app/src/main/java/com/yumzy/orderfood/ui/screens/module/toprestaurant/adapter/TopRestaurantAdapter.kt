package com.yumzy.orderfood.ui.screens.module.toprestaurant.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yumzy.orderfood.data.models.ListElement
import com.yumzy.orderfood.databinding.AdapterRestaurantBinding
import com.yumzy.orderfood.ui.helper.setClickScaleEffect

class TopRestaurantAdapter(
    context: Context,
    private val list: List<ListElement>,
    private val clickListener: (outletID: String) -> Unit
) : RecyclerView.Adapter<TopRestaurantAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: AdapterRestaurantBinding =
            AdapterRestaurantBinding.inflate(layoutInflater, parent, false)

        return ItemViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(list[position])

    }

    inner class ItemViewHolder(itemView: AdapterRestaurantBinding) :
        RecyclerView.ViewHolder(itemView.root) {

        var adapterBinding: AdapterRestaurantBinding = itemView

        fun bind(listElement: ListElement) {

/*
            adapterBinding.horizontalView.imageUrl = listElement.imageUrl
            adapterBinding.horizontalView.title = listElement.outletName
            //   adapterBinding.horizontalView.colorText =zoneOffer
            adapterBinding.horizontalView.cusisines =
                listElement.cuisines.joinToString(separator = ", ", postfix = " ")
            adapterBinding.horizontalView.setClickScaleEffect()
            adapterBinding.horizontalView.setOnClickListener {
                clickListener(listElement.outletId)
            }
*/
        }

    }

}