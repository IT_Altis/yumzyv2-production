package com.yumzy.orderfood.ui.screens.module.seeall

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.databinding.AdapterRestaurantBinding
import com.yumzy.orderfood.ui.helper.setClickScaleEffect

class SeeAllAdapter(
    context: Context,
    private val list: ArrayList<HomeListDTO>,
    private val clickListener: (outletID: String) -> Unit
) : RecyclerView.Adapter<SeeAllAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: AdapterRestaurantBinding =
            AdapterRestaurantBinding.inflate(layoutInflater, parent, false)

        return ItemViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(list[position])

    }

    inner class ItemViewHolder(itemView: AdapterRestaurantBinding) :
        RecyclerView.ViewHolder(itemView.root) {

        var adapterBinding: AdapterRestaurantBinding = itemView

        fun bind(listElement: HomeListDTO) {
           /* adapterBinding.horizontalView.imageUrl = listElement.image.url
            adapterBinding.horizontalView.title = listElement.title
            adapterBinding.horizontalView.cusisines =
                listElement.cuisines?.joinToString(separator = ", ", postfix = " ") ?: "New Arrivals"

            adapterBinding.horizontalView.setClickScaleEffect()
            adapterBinding.horizontalView.setOnClickListener {
                clickListener(listElement.outletId)
            }*/
        }

    }

}