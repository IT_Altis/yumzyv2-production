package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.widget.LinearLayout
import com.google.android.material.textview.MaterialTextView
import com.laalsa.laalsaui.drawable.DrawableUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView

class TimelineHeaderView : LinearLayout {

    private var mCurrentState: TimelineState = TimelineState.Upcoming
        set(value) {
            when (value) {
                TimelineState.Upcoming -> {
                    //update icon
                    this.findViewById<FontIconView>(IConstants.TimelineHeaderView.VIEW_ICON).apply {
                        this.setTextColor(ThemeConstant.textGrayColor)
                        this.background =
                            DrawableUtils.getRoundDrawable(ThemeConstant.lightAlphaGray, 16f)
                    }

                    //update title
                    this.findViewById<MaterialTextView>(IConstants.TimelineHeaderView.VIEW_TITLE).apply {
                        this.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                            this.setTextAppearance(R.style.TextAppearance_MyTheme_SemiBold)
                        } else {
                            this.setTextAppearance(context, R.style.TextAppearance_MyTheme_SemiBold)
                        }
                    }

                }
                TimelineState.Ongoing -> {
                    //update icon
                    this.findViewById<FontIconView>(IConstants.TimelineHeaderView.VIEW_ICON).apply {
                        this.setTextColor(ThemeConstant.pinkies)
                        this.background =
                            DrawableUtils.getRoundDrawable(ThemeConstant.lightAlphaGray, 16f)
                    }

                    //update title
                    this.findViewById<MaterialTextView>(IConstants.TimelineHeaderView.VIEW_TITLE)
                        .apply {
                            this.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                                this.setTextAppearance(R.style.TextAppearance_MyTheme_SemiBold)
                            } else {
                                this.setTextAppearance(
                                    context,
                                    R.style.TextAppearance_MyTheme_SemiBold
                                )
                            }
                        }


                }
                TimelineState.Completed -> {
                    //update icon
                    this.findViewById<FontIconView>(IConstants.TimelineHeaderView.VIEW_ICON).apply {
                        this.setTextColor(ThemeConstant.greenAccent)
                        this.background = DrawableUtils.getRoundDrawable(ThemeConstant.lightGreen, 16f)
                    }

                    //update title
                    this.findViewById<MaterialTextView>(IConstants.TimelineHeaderView.VIEW_TITLE).apply {
                        this.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                            this.setTextAppearance(R.style.TextAppearance_MyTheme_SemiBold)
                        } else {
                            this.setTextAppearance(context, R.style.TextAppearance_MyTheme_SemiBold)
                        }                    }

                }
            }
            field = value
        }

    private var mIcon: String = context.resources.getString(R.string.icon_doc)
        set(value) {
            if (value.isNotEmpty()) {
                this.findViewById<FontIconView>(IConstants.TimelineHeaderView.VIEW_ICON).text =
                    value
            }
            field = value
        }

    private var mHeader: String = ""
        set(value) {
            if (value.isNotEmpty()) {
                this.findViewById<MaterialTextView>(IConstants.TimelineHeaderView.VIEW_TITLE).text = value
            }
            field = value
        }


    constructor(context: Context) : this(context, null, 0)

    constructor(context: Context, atr: AttributeSet?) : this(context, atr, 0)

    constructor(context: Context, atr: AttributeSet?, style: Int) : super(context, atr, style) {
        this.apply {
            orientation = HORIZONTAL
            gravity = Gravity.CENTER_VERTICAL
            layoutParams = LayoutParams(ViewConst.WRAP_CONTENT, ViewConst.WRAP_CONTENT).apply {
                setMargins(UIHelper.i4px, UIHelper.i4px, UIHelper.i4px, UIHelper.i4px)
            }
        }
        initViews()
        initAttr(atr, style)
    }


    private fun initViews() {

        this.addView(FontIconView(context).apply {
            this.id = IConstants.TimelineHeaderView.VIEW_ICON
            this.layoutParams =
                LayoutParams(UIHelper.i45px, UIHelper.i45px)
            this.setPadding(UIHelper.i6px, UIHelper.i6px, UIHelper.i6px, UIHelper.i6px)
            this.text = mIcon
        })

        this.addView(MaterialTextView(context).apply {
            this.id = IConstants.TimelineHeaderView.VIEW_TITLE
            this.layoutParams = LayoutParams(0, ViewConst.WRAP_CONTENT, 1f)
            this.setPadding(UIHelper.i12px, UIHelper.i4px, UIHelper.i4px, UIHelper.i4px)
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                this.setTextAppearance(R.style.TextAppearance_MaterialComponents_Subtitle2)
            } else {
                this.setTextAppearance(context, R.style.TextAppearance_MaterialComponents_Subtitle2)
            }
            this.text = mHeader
        })

    }

    private fun initAttr(atr: AttributeSet?, style: Int) {

        if (atr != null) {
            val a =
                context.obtainStyledAttributes(
                    atr,
                    R.styleable.TimelineHeaderView,
                    style,
                    0
                )
            mIcon = a.getString(R.styleable.TimelineHeaderView_view_icon).toString()
            mHeader = a.getString(R.styleable.TimelineHeaderView_view_name).toString()
            mCurrentState = when (a.getInt(R.styleable.TimelineHeaderView_view_state, 0)) {
                1 -> TimelineState.Ongoing
                2 -> TimelineState.Completed
                else -> TimelineState.Upcoming
            }
            a.recycle()

        }

    }

    fun setState(mState: TimelineState, title: String = mHeader) {
        mCurrentState = mState
        mHeader = title
    }

    sealed class TimelineState {

        object Upcoming : TimelineState()

        object Ongoing : TimelineState()

        object Completed : TimelineState()

    }

}