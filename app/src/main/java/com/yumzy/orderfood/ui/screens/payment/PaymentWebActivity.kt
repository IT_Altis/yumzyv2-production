package com.yumzy.orderfood.ui.screens.payment

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import com.laalsa.laalsalib.utilcode.util.ActivityUtils
import com.yumzy.orderfood.BuildConfig
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.AppIntentUtil
import com.yumzy.orderfood.ui.screens.login.StartedActivity
import com.yumzy.orderfood.util.IConstants
import org.json.JSONObject
import java.lang.Exception
import java.net.URL


class PaymentWebActivity : AppCompatActivity() {

    companion object {
        const val REQ_CODE = 8272
    }

    private lateinit var webView: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)
        if (intent.hasExtra(IConstants.ActivitiesFlags.PAYMENT_FORM)) {
            val stringExtra = intent.getStringExtra(IConstants.ActivitiesFlags.PAYMENT_FORM)
            if (stringExtra == "") cancelActivity()

            webView = findViewById(R.id.webView)

              webView.webViewClient = object : WebViewClient() {
                  override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {

                      val parsedUrl = URL(url)
                      if(parsedUrl.host ==  URL(BuildConfig.BASE_URL).host && parsedUrl.path.contains("payment")) {
                          val intent = Intent()
                          intent.putExtra(IConstants.ActivitiesFlags.PAYMENT_REDIRECT, url)
                          setResult(Activity.RESULT_OK, intent)
                          this@PaymentWebActivity.finish()
                          return false //will not be handled by default action
                      }

                      return true // will be handled by default action
                  }
              }

            webView.settings.javaScriptEnabled = true
            webView.settings.loadWithOverviewMode = true
            webView.settings.useWideViewPort = true
            webView.settings.cacheMode = WebSettings.LOAD_DEFAULT
            webView.settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.NORMAL
            webView.webChromeClient = WebChromeClient()

            stringExtra?.let { webView.loadData(it, "text/html", "UTF-8") };

        } else {
            cancelActivity()
        }
    }

    override fun onBackPressed() {
        if (webView.copyBackForwardList().currentIndex > 0) {
            webView.goBack()
        } else {
            cancelActivity()
        }
    }

    private fun cancelActivity() {
        val returnIntent = Intent()
        setResult(Activity.RESULT_CANCELED, returnIntent)
        finish()
    }

}