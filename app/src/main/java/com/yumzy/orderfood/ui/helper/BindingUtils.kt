package com.yumzy.orderfood.ui.helper

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.text.Layout
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.widget.ImageView
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.core.text.bold
import androidx.core.text.color
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.laalsa.laalsalib.ui.px
import com.laalsa.laalsalib.utilcode.util.BarUtils
import com.laalsa.laalsaui.utils.ColorHelper
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.common.ItemType
import com.yumzy.orderfood.util.YUtils
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView
import com.yumzy.orderfood.util.viewutils.fontutils.IconFontDrawable
import io.cabriole.decorator.LinearBoundsMarginDecoration
import io.cabriole.decorator.LinearDividerDecoration
import java.text.SimpleDateFormat
import java.util.*


/* NOTE   https://android.jlelse.eu/android-data-binding-expressions-part-3-3a01dbd594e3
 android:onClick="@{(view) -> presenter.onSaveProfile(view, user)}"
 android:onCheckedChanged="@{(cb, isChecked) -> presenter.onFollow(isChecked)}"
 //To display first and last name with concatenation
android:text="@{user.firstName + ' ' + user.lastName}
//To concate string value with variable
android:text='@{"Welcome, " + user.firstName}'
//To use Ternary operator for setting visibility
android:visibility="@{user.age < 18 ? View.GONE : View.VISIBLE}"
//To access value from array
android:text="@{user.followersList[index]}"
//To access value from map
android:text="@{user.settingsMap[key]}"

android:text="@{user.displayName ?? user.firstName}"
//Equivalent to below
android:text="@{user.displayName != null ? user.displayName : user.firstName}"

//To convert into string with method call
android:text="@{String.valueOf(user.age)}"
//To calculate age from static method and display it
<import type="com.example.Util"/>
...
android:text="@{String.valueOf(Util.calculateAge(user.dob))}"
//To call method of data model to convert date to string
android:text="@{data.convertToString(data.dob)}"



//In string.xml
<string name="str_your_follower_count">You have %1$d followers</string>
//In layout
android:text="@{String.format(@string/msg_your_follower_count, data.followerCount)}"

//To use
android:text="@{@string/val_hi}"
android:text="@{@string/val_hi + @string/msg_welcome}"
android:layout_height="@{@dimen/img_height"}"
android:layout_height="@{data.isThumbnail ? @dimen/img_tumb_height : @dimen/img_full_height}"
android:background="@{data.isReadMsg ? @color/dray : @color/white}"

@BindingAdapter("greetings")
public static void setName(TextView view, String text) {
    view.setText("Welcome, " + text);
}
...
app:greetings="@{name}" // name is passed as binding variable

 */
//--------------------------------------------------------------------------------------------


/*
app:imageUrl="@{user.picUrl}"
app:placeholder="@{@drawable/ic_profile}"
 */
@SuppressLint("CheckResult")
@BindingAdapter(value = ["imageUrl", "placeholder"], requireAll = false)
fun setImageUrl(imageView: ImageView, imageUrl: String?, placeholder: Drawable?) {


    val target =
        Glide.with(imageView.context).load(
            imageUrl
        )
    if (placeholder != null) {
        target.placeholder(placeholder).error(placeholder)
        target.into(imageView)
    }
}

@BindingAdapter("isGone")
fun View.bindIsGone(hide: Boolean) {
    this.visibility = if (hide) {
        View.GONE
    } else {
        View.VISIBLE
    }
}

/*
fun forgotPasswordClicked() {
    showToast("ForgotPasswordClicked")
}
 */
//app:onClick="@{viewModel::forgotPasswordClicked}"
@BindingAdapter("onClick")
fun onClick(view: View, onClick: () -> Unit) {
    view.setOnClickListener {
        onClick()
    }
}

@BindingAdapter("isInvisible")
fun bindIsInvisible(view: View, isInvisible: Boolean) {
    view.visibility = if (isInvisible) {
        View.INVISIBLE
    } else {
        View.VISIBLE
    }
}

@BindingAdapter("isFloatingButtonGone")
fun bindIsGone(view: FloatingActionButton, isGone: Boolean?) {
    if (isGone == null || isGone) {
        view.hide()
    } else {
        view.show()
    }
}

@BindingAdapter("imageFromUrl")
fun ImageView.bindImageFromUrl(imageUrl: String?) {
    if (!imageUrl.isNullOrEmpty()) {
        Glide.with(this.context)
            .load(if (imageUrl.contains("content://")) Uri.parse(imageUrl) else imageUrl)
            .placeholder(YUtils.getPlaceHolder())
            .thumbnail(0.1f)
            .apply(RequestOptions().override(this.width, this.height))
//            .transition(DrawableTransitionOptions.withCrossFade())
            .into(this)

    } else {
        this.setImageResource(YUtils.getPlaceHolder())
    }
}

@BindingAdapter("userimageFromUrl")
fun ImageView.bindUserImageFromUrl(imageUrl: String?) {
    if (!imageUrl.isNullOrEmpty()) {
        Glide.with(this)
            .load(imageUrl)
            .skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .apply(
                RequestOptions()
                    .circleCrop()
            )
            .into(this)

    } else {
        this.setImageResource(R.drawable.user_placeholder)
    }
}

@BindingAdapter("imageFromUrl", "placeIndex")
fun ImageView.bindImageFromUrl(imageUrl: String?, placeIndex: Int) {
    if (!imageUrl.isNullOrEmpty()) {
        Glide.with(this.context)
            .load(if (imageUrl.contains("content://")) Uri.parse(imageUrl) else imageUrl)
            .placeholder(YUtils.getPlaceHolder(placeIndex))
            .thumbnail(0.1f)
            .apply(RequestOptions().override(this.width, this.height))
//            .transition(DrawableTransitionOptions.withCrossFade())
            .into(this)

    } else {
        this.setImageResource(YUtils.getPlaceHolder())
    }
}

@SuppressLint("ResourceAsColor")
@BindingAdapter("cashBackText")
fun cashBackText(view: TextView, text: String) {
    val ss1 = SpannableString("₹ $text")
    ss1.setSpan(RelativeSizeSpan(1.5f), 0, ss1.length, 0)
    val myCustomizedString = SpannableStringBuilder()
        .color(R.color.darkGrayDark) { append("Cashback Won\n") }
        .append(ss1)
    view.text = myCustomizedString
}

@SuppressLint("ResourceAsColor")
fun outletNameDescription(text1: String, text2: String): CharSequence {
    val ss1 = SpannableString(text1)
    ss1.setSpan(RelativeSizeSpan(1.2f), 0, ss1.length, 0)
    ss1.setSpan(ForegroundColorSpan(ThemeConstant.logoText), 0, ss1.length, 0)
//    val myCustomizedString = SpannableStringBuilder()
//        .append(ss1)
//        .color(R.color.darkGrayDark) { append(text2) }

    return TextUtils.concat(ss1, text2)
}

@BindingAdapter("appliedCoupon")
fun TextView.appliedCoupon(text: String?) {
    if (text.isNullOrEmpty()) {
        this.text = "Apply Coupon"
        return
    }
    val ss1 = SpannableString(text)
    ss1.setSpan(RelativeSizeSpan(1.2f), 0, ss1.length, 0)
    ss1.setSpan(ForegroundColorSpan(ThemeConstant.pinkies), 0, ss1.length, 0)
    this.text = TextUtils.concat(ss1, "  Applied!")
}

@BindingAdapter("verificationCode")
fun TextView.verificationCode(text: String?) {
    val ss1 = SpannableString("+91 " + text)
    val s1 = "Verification code sent to "
    ss1.setSpan(RelativeSizeSpan(1.1f), 0, ss1.length, 0)
    ss1.setSpan(ForegroundColorSpan(ThemeConstant.blueLinkColor), 0, ss1.length, 0)
    this.text = TextUtils.concat(s1, ss1)
}

@BindingAdapter("changeHere")
fun TextView.changeHere(text: String?) {
    val ss1 = SpannableString(text)
    val s1 = "Wrong number? "
    ss1.setSpan(RelativeSizeSpan(1.1f), 0, ss1.length, 0)
    ss1.setSpan(ForegroundColorSpan(ThemeConstant.blueLinkColor), 0, ss1.length, 0)
    this.text = TextUtils.concat(s1, ss1)
}

@BindingAdapter("visibleOrGone")
fun View.setVisibleOrGone(show: Boolean) {
    visibility = if (show) View.VISIBLE else View.GONE
}

@BindingAdapter("clipChildToBackground")
fun ViewGroup.clipLayoutToBackground(show: Boolean) {
    if (show) {
        this.outlineProvider = ViewOutlineProvider.BACKGROUND
        this.clipToOutline = true
    }
}

@BindingAdapter("roundedShadeDrawable")
fun View.roundedBackground(color1: String) {
    val colorOne = Color.parseColor(color1)
    val darkenColor = ColorHelper.darkenColor(colorOne, 0.5f)

    this.background = UIHelper.roundedShadeDrawable(colorOne, darkenColor)
}


@BindingAdapter("isGone")
fun invisibleTextView(view: View, text: String) {
    view.visibility = if (text.isNotEmpty()) {
        View.GONE
    } else {
        View.VISIBLE
    }
}


@BindingAdapter("hideEmptyTextView")
fun TextView.hideEmptyTextView(text: CharSequence?) {
    this.text = text ?: ""
    this.visibility = if (text.isNullOrEmpty()) {
        View.GONE
    } else {
        View.VISIBLE
    }
}


@BindingAdapter("itemType")
fun TextView.itemType(itemType: Boolean?) {
//    while (itemType){}
    if (itemType == null) {
        this.visibility = View.GONE
    } else if (itemType == true) {
        this.visibility = View.VISIBLE
        this.setTextColor(ThemeConstant.vegColor)
    } else if (itemType == false) {
        this.visibility = View.VISIBLE
        this.setTextColor(ThemeConstant.nonVegColor)
    }
}

@BindingAdapter("ratingTextView")
fun TextView.ratingTextView(text: CharSequence?) {
    this.visibility = if (text.isNullOrEmpty()) {
        View.GONE
    } else {

//        this.text = text?:""
        val leftIcon = IconFontDrawable(context)
        leftIcon.text = this.context.getString(R.string.icon_star_2)
        leftIcon.setTextColor(ThemeConstant.pinkies)
        leftIcon.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize)
        this.compoundDrawablePadding = 3.px
        this.setCompoundDrawablesWithIntrinsicBounds(leftIcon, null, null, null)

        val spanText = SpannableString(text)
        val byFive = SpannableString("/5")
        spanText.setSpan(RelativeSizeSpan(1.2f), 0, spanText.length, 0)
        spanText.setSpan(ForegroundColorSpan(ThemeConstant.textBlackColor), 0, spanText.length, 0)
        byFive.setSpan(ForegroundColorSpan(ThemeConstant.textGrayColor), 0, byFive.length, 0)
        this.text = TextUtils.concat(spanText, byFive)
        View.VISIBLE
    }

//    return myCustomizedString

}


@BindingAdapter("invisibleEmptyTextView")
fun TextView.invisibleEmptyTextView(text: CharSequence?) {
    this.text = text ?: ""
    this.visibility = if (text.isNullOrEmpty()) {
        View.INVISIBLE
    } else {
        View.VISIBLE
    }
}


@BindingAdapter("leftDrawableIcon")
fun TextView.leftDrawableIcon(iconString: String) {
    val leftIcon = IconFontDrawable(context)
    leftIcon.text = iconString
    leftIcon.setTextColor(textColors)
    leftIcon.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize)
    this.setCompoundDrawablesWithIntrinsicBounds(leftIcon, null, null, null)
}


@BindingAdapter("rightDrawableIcon")
fun TextView.rightDrawableIcon(iconString: String) {
    val icon = IconFontDrawable(context)
    icon.text = iconString
    icon.setTextColor(textColors)
    icon.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize)
    this.setCompoundDrawablesWithIntrinsicBounds(null, null, null, icon)
}


@BindingAdapter("priceValue")
fun TextView.priceValue(price: Any?) {
    val priceValue = when (price) {
        is Double -> price.withPrecision(2)
        is String -> price
        is Int -> "$price"
        else -> ""
    }
    val leftIcon = IconFontDrawable(context)
    leftIcon.text = this.context.getString(R.string.icon_rupee)
    leftIcon.setTextColor(textColors)
    leftIcon.textAlign = Layout.Alignment.ALIGN_CENTER
//    gravity=Gravity.CENTER_VERTICAL
    leftIcon.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize)
    this.setCompoundDrawablesWithIntrinsicBounds(leftIcon, null, null, null)
    this.text = priceValue
    this.visibility = if (priceValue.isEmpty()) {
        View.INVISIBLE
    } else {
        View.VISIBLE
    }

}

@SuppressLint("SetTextI18n")
@BindingAdapter("appendPriceValue")
fun TextView.appendPriceValue(price: Any?) {
    val priceValue = when (price) {
        is Double -> price.withPrecision(2)
        is String -> price
        is Int -> "$price"
        else -> ""
    }
    this.applyLotoFont()
    this.text = "₹$priceValue"
    this.visibility = if (priceValue.isEmpty()) {
        View.GONE
    } else {
        View.VISIBLE
    }

}

@BindingAdapter("bind:leftDrawable", requireAll = false)
fun TextView.leftDrawableColorIcon(iconString: String) {
    val leftIcon = IconFontDrawable(context)
    leftIcon.text = iconString
    leftIcon.setTextColor(textColors)
    this.compoundDrawablePadding = 3.px
    this.setCompoundDrawablesWithIntrinsicBounds(leftIcon, null, null, null)
}
/*
@BindingAdapter("leftDrawableIcon")
fun TextView.leftDrawableIcon(iconInt: Int) {
    val leftIcon = IconFontDrawable(context, iconInt)
    leftIcon.setTextColor(ThemeConstant.blueJeans)
    leftIcon.textSize = textSize
    this.setCompoundDrawablesWithIntrinsicBounds(leftIcon, null, null, null)
}*/

@BindingAdapter("renderHtml")
fun bindRenderHtml(view: TextView, description: String?) {
    if (description != null) {
        view.text = HtmlCompat.fromHtml(description, HtmlCompat.FROM_HTML_MODE_COMPACT)
        view.movementMethod = LinkMovementMethod.getInstance()
    } else {
        view.text = ""
    }
}

@BindingAdapter("imageResource")
fun setImageResource(view: ImageView, imageUrl: Int) {
    val context: Context = view.context
    val option: RequestOptions = RequestOptions()
        .placeholder(android.R.drawable.ic_menu_gallery)
        .error(android.R.drawable.ic_menu_gallery)
    Glide.with(context)
        .setDefaultRequestOptions(option)
        .load(imageUrl)
        .into(view)
}


@BindingAdapter("imageResource")
fun setImageResource(view: ImageView, imageUrl: String?) {
    val context: Context = view.context
    val option: RequestOptions = RequestOptions()
        .placeholder(android.R.drawable.ic_menu_gallery)
        .error(android.R.drawable.ic_menu_gallery)
    Glide.with(context)
        .setDefaultRequestOptions(option)
        .load(imageUrl)
        .into(view)
}

fun unifiedCoupon(offer: String?, unifiedCode: String?): CharSequence? {
    val ss1 = SpannableString(unifiedCode)
    ss1.setSpan(RelativeSizeSpan(1.1f), 0, ss1.length, 0)
    ss1.setSpan(ForegroundColorSpan(ThemeConstant.pinkies), 0, ss1.length, 0)
    val s = SpannableStringBuilder()
        .bold { append(ss1) }
    return TextUtils.concat(offer, s)

}

/*
<TextView
    app:age="@{user.dob}" />
*/
@BindingAdapter("age")
fun setAge(textView: TextView, dob: String?) {
    try {
        val format = SimpleDateFormat("dd-MM-yyyy")
        val today: Calendar = Calendar.getInstance()
        val dobCal: Calendar = Calendar.getInstance()
        dobCal.time = format.parse(dob)
        var age: Int = today.get(Calendar.YEAR) - dobCal.get(Calendar.YEAR)
        if (today.get(Calendar.DAY_OF_YEAR) < dobCal.get(Calendar.DAY_OF_YEAR) && age > 0) {
            age--
        }
        textView.text = age.toString()
    } catch (ignored: Exception) {
    }
}

@BindingAdapter("paddingStatusBarTop")
fun ViewGroup.bindPaddingStatusBarTop(setHeight: Boolean) {
    this.setPadding(0, BarUtils.getStatusBarHeight(), 0, 0)
}

/*@BindingAdapter("app:barrierDirection")
fun setBarrier(barrier: Barrier,  ) {

}*/

@BindingAdapter("itemDishType")
fun FontIconView.itemDishType(dishType: ItemType) {
//    val iconFont = IconFontDrawable(context)
    this.text = context.getText(R.string.icon_veg_non_veg)

    when (dishType) {
        ItemType.VEG -> {
            this.setTextColor(ThemeConstant.vegColor)
        }
        ItemType.EGG -> {
            this.setTextColor(ThemeConstant.eggColor)
        }
        else -> {
            this.setTextColor(ThemeConstant.nonVegColor)
        }

    }

    /* Glide.with(imageView)
         .load(if (ItemType.VEG == dishType) R.drawable.ic_veg else if (ItemType.EGG == dishType) R.drawable.ic_egg else R.drawable.ic_non_veg)
         .into(imageView)*/
}

@BindingAdapter("dishType")
fun setVegOrNonVegResource(imageView: ImageView, dishType: ItemType) {
    val context = imageView.context
    val iconFont = IconFontDrawable(context)
    when (dishType) {
        ItemType.VEG -> {
            iconFont.text = context.getText(R.string.icon_leaf_1)
            iconFont.setTextColor(ThemeConstant.vegColor)
        }
        ItemType.EGG -> {
            iconFont.text = context.getText(R.string.icon_avocado_half_fruit_shape)
            iconFont.setTextColor(ThemeConstant.sunglowYellow)
        }
        else -> {
            iconFont.text = context.getText(R.string.icon_024_chicken_1)
            iconFont.setTextColor(ThemeConstant.nonVegColor)
        }
    }

    imageView.load(iconFont)
    /* Glide.with(imageView)
         .load(if (ItemType.VEG == dishType) R.drawable.ic_veg else if (ItemType.EGG == dishType) R.drawable.ic_egg else R.drawable.ic_non_veg)
         .into(imageView)*/
}
/*
@BindingAdapter("blobTitleStyle")
fun BlobHeaderLayout.blobTitleStyle(blobTitleStyleDTO: BlobTitleStyleDTO?) {
    if (blobTitleStyleDTO == null) return
    this.titleText = blobTitleStyleDTO.title
    this.titleColor = blobTitleStyleDTO.color
    this.style = blobTitleStyleDTO.style
    this.maxLine = blobTitleStyleDTO.maxLine
    this.showShadow = blobTitleStyleDTO.shadow
    this.shadowColor = blobTitleStyleDTO.shadowColor

    this.binding.blobTitle.setTextColor(titleColor)

    this.binding.blobTitle.text = titleText
    if (blobTitleStyleDTO.shadow)
        this.binding.blobTitle.setShadowLayer(5f, 2f, 2f, shadowColor)
    this.binding.blobTitle.hideEmptyTextView(titleText)

  *//*  if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
        this.binding.blobTitle.setTextAppearance(style)
    } else {
        this.binding.blobTitle.setTextAppearance(context, style)
    }*//*

    this.binding.blobTitle.maxLines = maxLine

}*/
/*
@BindingAdapter("blobSubTitleStyle")
fun BlobHeaderLayout.blobSubTitleStyle(blobSubTitleStyleDTO: BlobTitleStyleDTO?) {
    if (blobSubTitleStyleDTO == null) return
    this.titleText = blobSubTitleStyleDTO.title
    this.titleColor = blobSubTitleStyleDTO.color
    this.style = blobSubTitleStyleDTO.style
    this.maxLine = blobSubTitleStyleDTO.maxLine
    this.showShadow = blobSubTitleStyleDTO.shadow
    this.shadowColor = blobSubTitleStyleDTO.shadowColor

    this.binding.blobSubtitle.setTextColor(titleColor)

    this.binding.blobSubtitle.text = titleText
    if (blobSubTitleStyleDTO.shadow)
        this.binding.blobSubtitle.setShadowLayer(2f, 1f, 1f, shadowColor)
    this.binding.blobSubtitle.hideEmptyTextView(titleText)
*//*
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
        this.binding.blobSubtitle.setTextAppearance(style)
    } else {
        this.binding.blobSubtitle.setTextAppearance(context, style)
    }*//*

    this.binding.blobSubtitle.maxLines = maxLine

}


@BindingAdapter("blobHeaderStyle")
fun BlobHeaderLayout.blobHeaderStyle(blobHeaderStyleDTO: BlobTitleStyleDTO?) {
    if (blobHeaderStyleDTO == null) return
    this.titleText = blobHeaderStyleDTO.title
    this.titleColor = blobHeaderStyleDTO.color
    this.style = blobHeaderStyleDTO.style
    this.maxLine = blobHeaderStyleDTO.maxLine
    this.showShadow = blobHeaderStyleDTO.shadow
    this.shadowColor = blobHeaderStyleDTO.shadowColor

  *//*  this.binding.blobHeading.setTextColor(titleColor)

    this.binding.blobHeading.text = titleText
    if (blobHeaderStyleDTO.shadow)
        this.binding.blobHeading.setShadowLayer(2f, 1f, 1f, shadowColor)
    this.binding.blobHeading.hideEmptyTextView(titleText)

    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
        this.binding.blobHeading.setTextAppearance(style)
    } else {
        this.binding.blobHeading.setTextAppearance(context, style)
    }*//*

    this.binding.blobHeading.maxLines = maxLine

}*/


fun Double.withPrecision(digits: Int) = "%.${digits}f".format(this)

fun RecyclerView.setHorizontalListDecoration() {

    val decoration = LinearDividerDecoration.create(
        color = ThemeConstant.white,
        size = 10.px,
        leftMargin = 0.px,
        topMargin = 0.px,
        rightMargin = 0.px,
        bottomMargin = 0.px,
        orientation = RecyclerView.HORIZONTAL,
    )

    val marginDecorator = LinearBoundsMarginDecoration.create(
        margin = 12.px,
        orientation = RecyclerView.HORIZONTAL
    )

    this.addItemDecoration(decoration)
    this.addItemDecoration(marginDecorator)
}


