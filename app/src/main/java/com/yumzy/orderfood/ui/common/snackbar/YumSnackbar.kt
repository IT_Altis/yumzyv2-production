package com.yumzy.orderfood.ui.common.snackbar

/*

class YumSnackbar(
    parent: ViewGroup,
    content: YumzySnackbarView
) : BaseTransientBottomBar<YumSnackbar>(parent, content, content) {

    init {
        getView().setBackgroundColor(
            ContextCompat.getColor(
                view.context,
                android.R.color.transparent
            )
        )
        getView().setPadding(0, 0, 0, 0)
    }

    companion object {

        fun make(view: View): YumSnackbar {

            // First we find a suitable parent for our custom view
            val parent = view.findSuitableParent() ?: throw IllegalArgumentException(
                "No suitable parent found from the given view. Please provide a valid view."
            )

            // We inflate our custom view
            val customView = YumzySnackbarView(parent.context)
            customView.layoutParams =
                ViewGroup.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
            customView.setPadding(UIHelper.i5px, UIHelper.i5px, UIHelper.i5px, UIHelper.i5px)

            // We create and return our Snackbar
            return YumSnackbar(
                parent,
                customView
            )
        }

    }

}*/
