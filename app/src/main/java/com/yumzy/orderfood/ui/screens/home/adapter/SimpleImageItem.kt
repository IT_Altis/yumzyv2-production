package com.yumzy.orderfood.ui.screens.home.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.google.android.material.card.MaterialCardView
import com.laalsa.laalsalib.ui.px
import com.laalsa.laalsalib.ui.pxf
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.ModelAbstractItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.util.viewutils.YumUtil

/**
 * Created by mikepenz on 28.12.15.
 */
open class SimpleImageItem(imageUrl: String) :
    ModelAbstractItem<String, SimpleImageItem.ViewHolder>(imageUrl) {


    override val type: Int = R.id.simple_image_item

    /**
     * defines the layout which will be used for this item in the list
     *
     * @return the layout for this item
     */
    override val layoutRes: Int
        get() = R.layout.sample_item

    override fun getViewHolder(v: View): ViewHolder {
        val context = v.context
        val materialCardView = MaterialCardView(context)
        val layoutParams = LinearLayout.LayoutParams(100.px, 100.px)
//        layoutParams.setMargins(12.px, 5.px, 0, 5.px)
        materialCardView.layoutParams = layoutParams
        materialCardView.cardElevation = 0f
        materialCardView.radius = 4.pxf
        val imageView = ImageView(context)
        imageView.scaleType = ImageView.ScaleType.CENTER_CROP
        imageView.id = R.id.imageView
        materialCardView.addView(imageView)
        val linearLayout = LinearLayout(context)
        linearLayout.addView(materialCardView)
        return ViewHolder(linearLayout)
    }

    /**
     * our ViewHolder
     */
    class ViewHolder(view: ViewGroup) : FastAdapter.ViewHolder<SimpleImageItem>(view) {
        var imageView: ImageView = view.findViewById(R.id.imageView)
        override fun bindView(item: SimpleImageItem, payloads: List<Any>) {
            val model = item.model
            YumUtil.setImageInView(imageView, model)
        }

        override fun unbindView(item: SimpleImageItem) {
            YumUtil.setImageInView(imageView, "")
        }
    }
}
