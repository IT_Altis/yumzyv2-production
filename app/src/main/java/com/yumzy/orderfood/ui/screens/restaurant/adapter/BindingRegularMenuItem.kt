package com.yumzy.orderfood.ui.screens.restaurant.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.RestaurantItem
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.databinding.AdapterOutletRegularItemBinding
import com.yumzy.orderfood.ui.component.AddItemView
import com.yumzy.orderfood.ui.screens.module.ICartHandler

class BindingRegularMenuItem(val outletInteraction: OutletInteraction) :
    AbstractBindingItem<AdapterOutletRegularItemBinding>() {

    var restaurantItem: RestaurantItem? = null

    override val type: Int = R.id.item_regular_menu

    fun withRestaurantAddItem(item: RestaurantItem): BindingRegularMenuItem {
        this.restaurantItem = item
        return this
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterOutletRegularItemBinding {
        return AdapterOutletRegularItemBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: AdapterOutletRegularItemBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)

        restaurantItem?.let {
            binding.outletRegularItem.setMenuItem(it)

            binding.outletRegularItem.addItemListener = object : AddItemView.AddItemListener {
                override fun onItemAdded(
                    itemId: String,
                    count: Int,
                    priced: String,
                    outletId: String
                ) {
                    outletInteraction.onItemCountChange(
                        object : ICartHandler {
                            override fun itemAdded(totalItemCount: Int) {
                                it.isItemAdding = false
//                                it.prevQuantity = totalItemCount
                            }

                            override fun itemRepeated() {}

                            override fun failedToAdd(message: String, gotoPlate: Boolean) {
                                it.isItemAdding = false
                                binding.outletRegularItem.getQuantityView().quantityCount =
                                    it.prevQuantity

                                if (gotoPlate) {
                                    outletInteraction.gotoPlate()
                                }
                            }

                            override fun showProgressBar() {
                                it.isItemAdding = true
                                binding.outletRegularItem.showItemAdding(true)
                            }

                            override fun hideProgressBar() {
                                it.isItemAdding = false
                                binding.outletRegularItem.showItemAdding(false)
                            }


                            override fun showCodeError(
                                code: Int?,
                                title: String?,
                                message: String
                            ) {
                                it.isItemAdding = false
                                when (code) {
                                    APIConstant.Status.SUCCESS -> {
                                    }
                                    else -> {
                                        failedToAdd(gotoPlate = false)
                                        outletInteraction.onItemFailed("${title}\n${message}")
                                    }
                                }
                            }

                            override fun onResponse(code: Int?, response: Any?) {}
                        },
                        count,
                        it,
                        binding.outletRegularItem.getQuantityView(),
                        it.prevQuantity < count
                    )
                }
            }
        }
    }

    override fun unbindView(binding: AdapterOutletRegularItemBinding) {
        binding.outletRegularItem.setMenuItem(null)
    }
}