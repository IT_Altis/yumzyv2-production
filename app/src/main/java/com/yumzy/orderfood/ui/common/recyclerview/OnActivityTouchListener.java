package com.yumzy.orderfood.ui.common.recyclerview;

import android.view.MotionEvent;

public interface OnActivityTouchListener {
    void getTouchCoordinates(MotionEvent ev);
}