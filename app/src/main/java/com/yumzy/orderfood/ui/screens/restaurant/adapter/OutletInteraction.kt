package com.yumzy.orderfood.ui.screens.restaurant.adapter

import com.yumzy.orderfood.data.models.RestaurantItem
import com.yumzy.orderfood.ui.component.AddQuantityView
import com.yumzy.orderfood.ui.screens.module.ICartHandler

interface OutletInteraction {

        fun onItemSelected(position: Int, item: RestaurantItem)

        fun onItemCountChange(
                cartHandler: ICartHandler,
                count: Int,
                item: RestaurantItem,
                quantityView: AddQuantityView,
                isIncremented: Boolean?
        )

        fun onItemFailed(message: String)
        fun adapterSizeChange(itemSize: Int)

        fun onNoteClicked(item: RestaurantItem)

        fun gotoPlate()
}
