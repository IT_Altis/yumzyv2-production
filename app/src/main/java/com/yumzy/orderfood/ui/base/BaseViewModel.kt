package com.yumzy.orderfood.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

abstract class BaseViewModel<T : IView> : ViewModel() {
    var view: T? = null

    fun attachView(view: T) {
        this.view = view
    }

    fun detachView() {
        this.view = null
    }

    fun changeUI(uiScope: () -> Unit) {
        viewModelScope.launch(Dispatchers.Main) { uiScope.invoke() }
    }

    fun launch(networkScope: suspend () -> Unit) {
        viewModelScope.launch(Dispatchers.Main) { view?.showProgressBar() }
        viewModelScope.launch(Dispatchers.IO) { networkScope.invoke() }
        viewModelScope.launch(Dispatchers.Main) { view?.hideProgressBar() }
    }


}
