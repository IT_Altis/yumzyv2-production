package com.yumzy.orderfood.ui.screens.fastitems

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.ItemRetryBinding

class RetryListItem : AbstractBindingItem<ItemRetryBinding>() {
    override val type = R.id.retry_item_id
    override fun bindView(binding: ItemRetryBinding, payloads: List<Any>) {}
    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ItemRetryBinding {
        return ItemRetryBinding.inflate(inflater, parent, false)
    }
}

