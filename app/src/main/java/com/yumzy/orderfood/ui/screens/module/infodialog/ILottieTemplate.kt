package com.yumzy.orderfood.ui.screens.module.infodialog

import android.content.Context
import android.content.DialogInterface
import android.view.View
import com.yumzy.orderfood.ui.common.YumLottieView

interface ILottieTemplate {
    val context: Context
    val bodySize: Int
    val showClose: Boolean
    val titleText: String?

    //    val bodyText: String?
    val footerText: String?
    val imgUrl: String
    fun heading(): View?
    fun body(): YumLottieView
    fun footer(): View?
    fun onDialogVisible(view: View?, visible: Boolean?, dialog: DialogInterface?)
}