package com.yumzy.orderfood.ui.component

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.yumzy.orderfood.util.ViewConst

class HorizontalPagerAdapter(val context: Context) : RecyclerView.Adapter<PagerVH>() {

    //array of colors to change the background color of screen
    private val colors = intArrayOf(
        android.R.color.black,
        android.R.color.holo_red_light,
        android.R.color.holo_blue_dark,
        android.R.color.holo_purple
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PagerVH {
        val layout = LinearLayout(context)
        layout.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.MATCH_PARENT)
        layout.background = ColorDrawable(Color.GRAY)
        return PagerVH(layout)
    }

    //get the size of color array
    override fun getItemCount(): Int = Int.MAX_VALUE

    //binding the screen with view
    override fun onBindViewHolder(holder: PagerVH, position: Int) = holder.itemView.run {
        if (position == 0) {
            this.setBackgroundResource(colors[position])
        }
        if (position == 1) {

            this.setBackgroundResource(colors[position])
        }
        if (position == 2) {

            this.setBackgroundResource(colors[position])
        }
        if (position == 3) {

            this.setBackgroundResource(colors[position])
        }
    }
}

class PagerVH(itemView: View) : RecyclerView.ViewHolder(itemView)