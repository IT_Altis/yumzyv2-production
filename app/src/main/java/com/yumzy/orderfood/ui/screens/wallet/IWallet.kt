package com.yumzy.orderfood.ui.screens.wallet

import com.yumzy.orderfood.ui.base.IView

/**
 * Created by Bhupendra Kumar Sahu.
 */
interface IWallet :IView {
    fun fetchWalletTransactions()
    fun getUserWallet()
}