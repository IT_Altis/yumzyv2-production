package com.yumzy.orderfood.ui.screens.payment

import android.content.Intent
import com.yumzy.orderfood.data.models.NewOrderResDTO
import com.yumzy.orderfood.ui.common.IModuleHandler

interface IPayment : IModuleHandler {

//    fun setupActionBar()
    fun getIntentData(intent: Intent?)
    fun setUpData()
    fun newOrder()
    fun startPayment(order: NewOrderResDTO)
    fun postCheckout(orderId: String)

}