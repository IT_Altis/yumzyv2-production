package com.yumzy.orderfood.ui.screens.restaurant.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.yumzy.orderfood.data.models.RestaurantItem
import com.yumzy.orderfood.databinding.AdapterRestaurentMenuBinding
import com.yumzy.orderfood.ui.screens.restaurant.MenuItemDiff
import com.yumzy.orderfood.ui.screens.restaurant.RestaurantViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class OutletSearchAdapter(
    val context: Context,
    val viewModel: RestaurantViewModel,
    var menuList: List<RestaurantItem>,

    private val interaction: Interaction? = null,
    private val lifecycleOwner: LifecycleOwner,
    private val selectedNotes: LiveData<ArrayList<ContactsContract.CommonDataKinds.Note>>,
    private val dateUtil: DateUtil
) : RecyclerView.Adapter<OutletSearchAdapter.RestaurantItemHolder>() {

    private var items: MutableList<RestaurantItem> = menuList.toMutableList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantItemHolder {
        val layoutInflater = LayoutInflater.from(context)
        val itemBinding: AdapterRestaurentMenuBinding =
            AdapterRestaurentMenuBinding.inflate(layoutInflater, parent, false)
        return RestaurantItemHolder(itemBinding)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: RestaurantItemHolder, position: Int) =
        holder.bindData(items[position])

    @SuppressLint("CheckResult")
    fun updateList(query: String) {
        //using rx to filter the list and update the list by diffing.
        Observable
            .fromCallable {
                return@fromCallable menuList.filter { item ->
                    item.name.contains(query, ignoreCase = true)
                            || item.description.contains(query, ignoreCase = true)
                            || when (query.trim()) {
                        "veg", "vegetarian", "Veg", "Vegetarian" -> item.isVeg
                        "nonveg", "non veg", "Nonveg", "Non Veg", "Non veg", "non" -> !item.isVeg
                        "egg" -> item.containsEgg
                        else -> false
                    }
                }
            }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { list ->
                val diffCallback = MenuItemDiff(menuList, items)
                val diffResult = DiffUtil.calculateDiff(diffCallback)
                items.clear()
                items.addAll(list)
                diffResult.dispatchUpdatesTo(this)
            }
    }

    fun resetList() {
        val diffCallback = MenuItemDiff(items, menuList)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        items.clear()
        items.addAll(menuList)
        diffResult.dispatchUpdatesTo(this)
    }


    inner class RestaurantItemHolder(adapterBinding: AdapterRestaurentMenuBinding) :
        RecyclerView.ViewHolder(adapterBinding.root) {

        var adapterBinding: AdapterRestaurentMenuBinding = adapterBinding


        private val restaurantItem: MutableLiveData<RestaurantItem> =
            MutableLiveData()

        fun bindData(restaurantDTO: RestaurantItem) {
            restaurantItem.value = restaurantDTO
        }

        init {
            /* val name: LiveData<RestaurantItem> = Transformations.switchMap(
                 restaurantItem,
                 Function<RestaurantItem, LiveData<RestaurantItem>> {
                     viewModel.getRestaurantItem()
                 })
             name.observeForever {

             }*/
        }
    }
}

interface Interaction {

    fun onItemSelected(position: Int, restaurantItem: RestaurantItem)

    fun restoreListPosition()

    fun isMultiSelectionModeEnabled(): Boolean

    fun activateMultiSelectionMode()

    fun isNoteSelected(restaurantItem: RestaurantItem): Boolean
}
