/*
 * Created By Shriom Tripathi 5 - 5 - 2020
 */

package com.yumzy.orderfood.ui.screens.module.address

import com.yumzy.orderfood.data.models.AddressDTO

sealed class LocationMapAction {

    class MapActionUpdate(val mAddress: AddressDTO) : LocationMapAction()

    class MapActionAdd : LocationMapAction()

    class MapActionFetch : LocationMapAction()


}