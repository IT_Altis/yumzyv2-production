package com.yumzy.orderfood.ui.screens.home

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.SearchItemDTO
import com.yumzy.orderfood.databinding.AdapterExploreBinding
import com.yumzy.orderfood.ui.screens.home.fragment.ExploreItemListener

import java.util.*


class ExploreItem : AbstractBindingItem<AdapterExploreBinding>() {

    internal var data: SearchItemDTO? = null
    var itemListener: ExploreItemListener? = null
    override val type: Int
        get() = R.id.explore_list

    override fun bindView(binding: AdapterExploreBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)

        binding.srvComponent.itemClick = { it ->
            itemListener?.onItemClick(data?.outletId, data?.itemId)
        }
        if (!data?.itemId.isNullOrEmpty()) {
            binding.srvComponent.title = data?.name.toString().capitalize(Locale.getDefault())
            binding.srvComponent.subTitle =
                data?.outletName.toString().capitalize(Locale.getDefault())
            binding.srvComponent.typeOf = "Dish"


        } else {

            binding.srvComponent.title = data?.outletName.toString().capitalize(Locale.getDefault())
            binding.srvComponent.typeOf = "Restaurant"
            if (!data?.name.isNullOrEmpty())
                binding.srvComponent.subTitle =
                    data?.name.toString().capitalize(Locale.getDefault())
            else
                binding.srvComponent.subTitle = "Explore this restaurant"
        }
        binding.srvComponent.imageUrl = data?.imageUrl.toString()


    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterExploreBinding {
        return AdapterExploreBinding.inflate(inflater, parent, false)
    }

    fun withListener(listener: ExploreItemListener): ExploreItem {
        this.itemListener = listener
        return this
    }

    fun withOrder(order: SearchItemDTO): ExploreItem {
        this.data = order
        return this
    }


}