package com.yumzy.orderfood.ui.screens.manageaddress

import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.ui.base.IView

interface IManageAddress : IView {
    fun fetchAddressList();
    fun editAddress(address: AddressDTO)
    fun deleteRemoteAddress(addressId: String)
    fun setupAdapter()
}