package com.yumzy.orderfood.ui.screens.earncoupon

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.utilcode.util.ShadowUtils
import com.yumzy.orderfood.databinding.AdapterEarnPointsBinding
import com.yumzy.orderfood.ui.helper.UIHelper

/**
 * Created by Bhupendra Kumar Sahu.
 */
class EarnListAdapter(
    val context: Context,
    private val clickedListener: (adapterPosition: Int) -> Unit
) :
    RecyclerView.Adapter<EarnListAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: AdapterEarnPointsBinding =
            AdapterEarnPointsBinding.inflate(layoutInflater, parent, false)
        ShadowUtils.apply(
            itemBinding.root,
            ShadowUtils.Config().setShadowSize(UIHelper.i1px).setShadowRadius(UIHelper.i10px.toFloat()).setShadowColor(
                0XBFCECECE.toInt()
            )
        )


        return ItemViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return 5
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {

    }

    inner class ItemViewHolder(itemView: AdapterEarnPointsBinding) :
        RecyclerView.ViewHolder(itemView.root) {


        var adapterBinding: AdapterEarnPointsBinding = itemView



        init {

            adapterBinding.earnPointView.header = "Order payment done by HDFC Credit card "
            adapterBinding.earnPointView.subHeader = "On January 15 2020, 3:47pm"
            adapterBinding.earnPointView.points = "720"


        }

    }
}
