package com.yumzy.orderfood.ui.screens.module.foodpreference

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.mikepenz.fastadapter.items.AbstractItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.FoodPreferenceDTO


class VeganItem(
    val context: Context,
) :
    AbstractItem<VeganItem.ViewHolder>() {

    var data: FoodPreferenceDTO? = null
    var prefName: String = ""
    var prefImage: String = ""

    override val type: Int
        get() = R.id.item_vegan_id

    override val layoutRes: Int
        get() = R.layout.adapter_vegan_selection

    fun withData(data: FoodPreferenceDTO): VeganItem {
        this.data = data
        this.prefName = data.displayName
        this.prefImage = data.imageurl
        return this
    }


    override fun bindView(holder: ViewHolder, payloads: List<Any>) {
        super.bindView(holder, payloads)
        this.isSelected
        holder.name.text = prefName.toString()
        GlideToVectorYou.justLoadImage(
            context as Activity,
            Uri.parse(prefImage.toString()),
            holder.img
        )


    }

    override fun unbindView(holder: ViewHolder) {
        super.unbindView(holder)
        holder.name.text = null
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var img: ImageView = view.findViewById(R.id.img_vegan)
        var cl: ConstraintLayout = view.findViewById(R.id.cl)
        var name: TextView = view.findViewById(R.id.tv_vegan_name)
    }

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

}

