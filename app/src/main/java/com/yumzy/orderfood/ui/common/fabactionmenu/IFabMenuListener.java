package com.yumzy.orderfood.ui.common.fabactionmenu;

import android.view.View;

public interface IFabMenuListener {
    void onFabItemClick(View v, Object clActionItem);
}
