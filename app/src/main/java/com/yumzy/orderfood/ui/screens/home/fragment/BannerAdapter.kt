/*
 * Created By Shriom Tripathi 9 - 5 - 2020
 */

package com.yumzy.orderfood.ui.screens.home.fragment

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.utilcode.util.ClickUtils
import com.laalsa.laalsaui.utils.dpf
import com.yumzy.orderfood.ui.common.imageview.RoundedImageView
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.YUtils


abstract class BannerAdapter<T>(
    val context: Context,
    var items: ArrayList<T>
) :
    RecyclerView.Adapter<BannerAdapter<T>.BannerHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BannerAdapter<T>.BannerHolder {
        val imageView = RoundedImageView(context).apply {
            layoutParams = LinearLayout.LayoutParams(
                ViewConst.MATCH_PARENT,
                ViewConst.MATCH_PARENT
            )
            cornerRadius = 10.dpf
            scaleType = ImageView.ScaleType.CENTER_CROP


        }
//        Glide.with(imageView).load(onLoadImage(items[bannerPosition])).into(imageView)

//        container.addView(imageView)
        return BannerHolder(imageView)
    }

    abstract fun onLoadImage(item: T): String

    override fun onBindViewHolder(holder: BannerHolder, position: Int) {
        holder.bindData(items[position])
    }

    override fun getItemCount(): Int = items.size

    abstract fun onItemClick(view: View, item: T, bannerPosition: Int)

    inner class BannerHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(bannerItem: T) {
//            itemView.
            if (itemView is ImageView) {
                val imageView = itemView
//                Glide.with(imageView).load(onLoadImage(items[adapterPosition])).into(imageView)
                YUtils.setImageInView(imageView, onLoadImage(bannerItem))
            }
            ClickUtils.applyGlobalDebouncing(
                itemView,
                IConstants.AppConstant.CLICK_DELAY
            )
            {
                onItemClick(itemView, items[adapterPosition], adapterPosition)
            }
        }

    }
    /* override fun getCount(): Int {
        return items.size
    }

    abstract fun onLoadImage(item: T): String

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, bannerPosition: Int): Any {
        val imageView = RoundedImageView(context).apply {
            layoutParams = LinearLayout.LayoutParams(
                ViewConst.MATCH_PARENT,
                ViewConst.MATCH_PARENT
            )
            cornerRadius = 10.dpf
            scaleType = ImageView.ScaleType.CENTER_CROP


            ClickUtils.applyGlobalDebouncing(
                this,
                IConstants.AppConstant.CLICK_DELAY
            ) {
                onItemClick(items[bannerPosition], bannerPosition)

            }

        }
//        Glide.with(imageView).load(onLoadImage(items[bannerPosition])).into(imageView)
        YUtils.setImageInView(imageView,onLoadImage(items[bannerPosition]))

        container.addView(imageView)
        return imageView
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }*/


}