package com.yumzy.orderfood.ui.screens.module.seeall

import android.content.Intent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeSectionDTO
import com.yumzy.orderfood.databinding.DialogSeeAllBinding
import com.yumzy.orderfood.ui.base.BaseDialogFragment
import com.yumzy.orderfood.ui.screens.restaurant.OutletActivity
import com.yumzy.orderfood.util.IConstants

/**
 * Created by Bhupendra Kumar Sahu on 15-Jun-20.
 */
class SeeAllDialog(val filter: HomeSectionDTO) : BaseDialogFragment<DialogSeeAllBinding>() {

    var title: String = ""
    override fun getLayoutId(): Int = R.layout.dialog_see_all

    override fun onDialogReady(view: View) {
        (activity as AppCompatActivity).run {
            setSupportActionBar(bd.tbSeeallToolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeButtonEnabled(true)
        }
        bd.collapseToolbar.title = title
        bd.tbSeeallToolbar.setNavigationOnClickListener { this.dismiss() }

        setTopDishesList()

    }


    private fun setTopDishesList() {
        if (filter.list.isNullOrEmpty()) {
            bd.rvSeeAll.visibility = View.GONE
            bd.lottieSeeAllList.visibility = View.VISIBLE

        } else {
            /*context?.let {
                YUtils.setImageInView(bd.ivSeeAllHeader,filter.list.get(0).heroImage.url)

            }*/
            bd.rvSeeAll.visibility = View.VISIBLE
            bd.lottieSeeAllList.visibility = View.GONE

//            val subList = filter.list.subList(0, 10)
//           val list =ArrayList<HomeList>()
//            list.addAll(subList)
//            list.add(subList)
            activity?.let {
                bd.rvSeeAll.adapter =
                    SeeAllAdapter(
                        it,
                        filter.list,
                        clickListener = { outletId: String ->
                            onPostSelected(
                                outletId
                            )
                        })
                bd.rvSeeAll.layoutManager = LinearLayoutManager(it)

            }
        }
    }


    private fun onPostSelected(outletId: String) {
        val intent = Intent(context, OutletActivity::class.java)
        intent.putExtra(IConstants.ActivitiesFlags.OUTLET_ID, outletId)
        startActivityForResult(intent, OutletActivity.REStAURANT_REQ_CODE)
        dismiss()
    }

    override fun onSuccess(actionId: Int, data: Any?) {

    }


}
