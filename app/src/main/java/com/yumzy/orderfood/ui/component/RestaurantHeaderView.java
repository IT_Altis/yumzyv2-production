package com.yumzy.orderfood.ui.component;

/**
 * Created by Bhupendra Kumar Sahu
 */
/*
public class RestaurantHeaderView extends LinearLayout {
    private String title;
    private String subTitle;
    private Drawable ivRestaurent;

    public RestaurantHeaderView(Context context) {
        this(context, null);
    }

    public RestaurantHeaderView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RestaurantHeaderView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            initAttrs(context, attrs, defStyleAttr);
        if (ivRestaurent == null) {
        }
        if (title == null) {
            title = "";
        }
        if (subTitle == null) {
            subTitle = "";
        }
        init();

    }

    private void initAttrs(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.RestaurantHeaderView, defStyleAttr, 0);
        ivRestaurent = ta.getDrawable(R.styleable.RestaurantHeaderView_restaurant_image);
        title = ta.getString(R.styleable.RestaurantHeaderView_restaurant_title);
        subTitle = ta.getString(R.styleable.RestaurantHeaderView_restaurant_sub_title);
        ta.recycle();
    }

    private void init() {
        this.setOrientation(VERTICAL);
        this.setGravity(Gravity.CENTER);

        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(VERTICAL);
        linearLayout.setGravity(Gravity.CENTER);
        int i15px = VUtil.dpToPx(15);
        int l200px = VUtil.dpToPx(200);


        ImageView iv_restaurant = new ImageView(getContext());
        iv_restaurant.setId(R.id.iv_restaurant);

        iv_restaurant.setPadding(0, i15px, 0, i15px);
        linearLayout.addView(iv_restaurant, new LayoutParams(l200px, l200px));

        linearLayout.addView(getTextView(true, title));
        linearLayout.addView(getTextView(false, subTitle));
        linearLayout.setBackground(this.getResources().getDrawable(R.drawable.restra_gradiant));
        this.addView(linearLayout);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        ((TextView) findViewById(R.id.tv_restaurant_title)).setText(this.title);

    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
        ((TextView) findViewById(R.id.tv_restaurant_subtitle)).setText(this.subTitle);

    }

    public Drawable getIvRestaurent() {
        return ivRestaurent;
    }

    public void setIvRestaurent(Drawable ivRestaurent) {
        this.ivRestaurent = ivRestaurent;
        ((ImageView) findViewById(R.id.iv_restaurant)).setImageDrawable(ivRestaurent);

    }

    private View getTextView(boolean isHeader, String msg) {
        TextView textView = new TextView(getContext());
        textView.setGravity(Gravity.CENTER);
        textView.setText(msg);
        int tv15px = VUtil.dpToPx(15);
        int tv2px = VUtil.dpToPx(2);
        int tv40px = VUtil.dpToPx(40);
        if (isHeader) {
            textView.setId(R.id.tv_restaurant_title);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f);
            textView.setTypeface(Typeface.DEFAULT_BOLD);
            textView.setTextColor(Color.BLACK);
        } else {
            textView.setPadding(tv40px, tv2px, tv40px, tv15px);
            textView.setId(R.id.tv_restaurant_subtitle);
        }
        return textView;
    }

}
*/
