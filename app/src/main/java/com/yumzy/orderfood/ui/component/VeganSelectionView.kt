package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.utilcode.util.ToastUtils
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.ISelectionListener
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.select.SelectExtension
import com.mikepenz.fastadapter.select.getSelectExtension
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.FoodPreferenceDTO
import com.yumzy.orderfood.ui.common.GridItemDecoration
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.screens.module.foodpreference.VeganItem


class VeganSelectionView : RecyclerView {

    //save our FastAdapter
    private var fastItemAdapter: FastItemAdapter<VeganItem>
    private var selectExtension: SelectExtension<VeganItem>

    internal var section: FoodPreferenceDTO? = null

    init {

        //create our FastAdapter which will manage everything
        fastItemAdapter = FastItemAdapter()
        selectExtension = fastItemAdapter.getSelectExtension()
        selectExtension.isSelectable = true
        selectExtension.multiSelect = false
        selectExtension.selectionListener = object : ISelectionListener<VeganItem> {
            override fun onSelectionChanged(item: VeganItem, selected: Boolean) {
                ToastUtils.showShort(item.prefName)

            }

        }

        //configure our fastAdapter
        fastItemAdapter.onClickListener =
            { v: View?, _: IAdapter<VeganItem>, item: VeganItem, _: Int ->
                v?.let {
                    Toast.makeText(v.context, item.prefName.toString(), Toast.LENGTH_LONG)
                        .show()
                }
                false
            }
        fastItemAdapter.onPreClickListener =
            { _: View?, _: IAdapter<VeganItem>, _: VeganItem, _: Int ->
                true // consume otherwise radio/checkbox will be deselected
            }
    }


    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        //get our recyclerView and do basic setup
        val gridLayoutManager = GridLayoutManager(context, 3)
        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return when (fastItemAdapter.getItemViewType(position)) {
                    R.id.item_vegan_id -> 1
                    else -> -1
                }
            }
        }

        this.layoutManager = gridLayoutManager
        /*this.itemAnimator = AlphaCrossFadeAnimator().apply {
            addDuration = 100
            removeDuration = 100
        }*/
        this.adapter = fastItemAdapter
        this.addItemDecoration(GridItemDecoration(UIHelper.i10px, UIHelper.i8px))

    }


    fun veganSection(veganList: ArrayList<FoodPreferenceDTO>): VeganSelectionView {
        val items = veganList.map { item -> VeganItem(context).withData(item) }
        fastItemAdapter.add(items)

        selectExtension.toggleSelection(0)
        return this

    }


}