package com.yumzy.orderfood.ui.component;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.yumzy.orderfood.R;
import com.yumzy.orderfood.ui.helper.ThemeConstant;
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView;
import com.laalsa.laalsalib.ui.VUtil;

/**
 * Created by Bhupendra Kumar Sahu.
 */
public class DishesHeaderInfoView extends LinearLayout {
    private String restraName;
    private String restraAddress;
    private String viewMenu;

    private String menuIcon;


    public DishesHeaderInfoView(Context context) {
        this(context, null);
    }

    public DishesHeaderInfoView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DishesHeaderInfoView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            initAttrs(context, attrs, defStyleAttr);
        if (restraName == null) {
            restraName = "";
        }
        if (restraAddress == null) {
            restraAddress = "";
        }
        if (viewMenu == null) {
            viewMenu = "";
        }
        if (menuIcon == null) {
            menuIcon = "";
        }

        init();
    }

    private void initAttrs(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.DishesHeaderInfoView, defStyleAttr, 0);
        restraName = a.getString(R.styleable.DishesHeaderInfoView_tv_restra_name);
        restraAddress = a.getString(R.styleable.DishesHeaderInfoView_tv_restra_address);
        viewMenu = a.getString(R.styleable.DishesHeaderInfoView_tv_view_menu);
        menuIcon = a.getString(R.styleable.DishesHeaderInfoView_menuIcon);
        a.recycle();
    }

    @SuppressLint("SetTextI18n")
    public void init() {
        this.setOrientation(HORIZONTAL);
        int i10px = VUtil.dpToPx(10);
        int i20px = VUtil.dpToPx(20);
        int i5px = VUtil.dpToPx(5);
        this.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        this.setWeightSum(10);
        this.setPadding(i10px, i10px, i10px, i10px);

        LinearLayout leftcontainer = new LinearLayout(getContext());
        LinearLayout.LayoutParams containerLayoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 3f);
        leftcontainer.setLayoutParams(containerLayoutParams);
        leftcontainer.setOrientation(VERTICAL);




        /*Restaurant Name*/

        TextView resName = new TextView(getContext());
        LayoutParams addressParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        addressParams.setMargins(0, i5px, 0, i5px);
        resName.setText(restraName);
        resName.setId(R.id.tv_res_restraName);
        resName.setTypeface(Typeface.DEFAULT_BOLD);
        resName.setTextColor(ThemeConstant.blueJeans);
        resName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10f);

        /*res Address*/
        TextView resAddress = new TextView(getContext());
        LayoutParams addressParam = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        addressParam.setMargins(0, 0, 0, i5px);
        resAddress.setTextColor(ContextCompat.getColor(getContext(), R.color.colorGrey));
        resAddress.setText(restraAddress);
        resAddress.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f);
        resAddress.setId(R.id.tv_restra_address);
        resAddress.setLayoutParams(addressParam);

        leftcontainer.addView(resName);
        leftcontainer.addView(resAddress);


        LinearLayout rightcontainer = new LinearLayout(getContext());
        LinearLayout.LayoutParams containerLayoutParam = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 7f);
        rightcontainer.setLayoutParams(containerLayoutParam);
        rightcontainer.setOrientation(HORIZONTAL);
        rightcontainer.setGravity(Gravity.CENTER | VERTICAL | Gravity.END);


        FontIconView menuicon = new FontIconView(getContext());
        LayoutParams iconParams = new LayoutParams(VUtil.dpToPx(15), VUtil.dpToPx(20));
        menuicon.setText(menuIcon);
        menuicon.setId(R.id.tv_menu_icon);
        menuicon.setTextColor(ContextCompat.getColor(getContext(), R.color.blueJeans));
        menuicon.setLayoutParams(iconParams);


        TextView viewmenu = new TextView(getContext());
        LayoutParams viewParam = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        viewmenu.setTextColor(ContextCompat.getColor(getContext(), R.color.blueJeans));
        viewParam.setMargins(VUtil.dpToPx(5), 0, 0, 0);
        viewmenu.setText(viewMenu);
        viewmenu.setTypeface(Typeface.DEFAULT_BOLD);
        viewmenu.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f);
        viewmenu.setId(R.id.tv_view_menu);
        viewmenu.setLayoutParams(viewParam);

        rightcontainer.addView(menuicon);
        rightcontainer.addView(viewmenu);


        this.addView(leftcontainer);
        this.addView(rightcontainer);
    }

    public Button getViewMenu() {
        return this.findViewById(R.id.tv_view_menu);
    }

    public void setViewMenu(String viewMenu) {
        this.viewMenu = viewMenu;
        ((TextView) findViewById(R.id.tv_view_menu)).setText(this.viewMenu);

    }

    public String getRestraName() {
        return restraName;
    }

    public void setRestraName(String restraName) {
        this.restraName = restraName;
        ((TextView) findViewById(R.id.tv_res_restraName)).setText(this.restraName);

    }

    public String getRestraAddress() {
        return restraAddress;
    }

    public void setRestraAddress(String restraAddress) {
        restraAddress = restraAddress;
        ((TextView) findViewById(R.id.tv_restra_address)).setText(this.restraAddress);

    }

    public String getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
        ((FontIconView) findViewById(R.id.tv_menu_icon)).setText(this.menuIcon);

    }
}
