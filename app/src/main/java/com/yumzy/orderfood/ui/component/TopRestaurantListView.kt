package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.ui.px
import com.mikepenz.fastadapter.GenericItem
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.itemanimators.SlideUpAlphaAnimator
import com.yumzy.orderfood.data.models.CustomizeOrderSectionDTO
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.tools.RemoteConfigHelper
import com.yumzy.orderfood.ui.common.VerticalItemDecoration
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.screens.restaurant.adapter.BindingSeeMoreItem
import com.yumzy.orderfood.ui.screens.restaurant.adapter.BindingTopRestaurantItem

class TopRestaurantListView : RecyclerView {

    //save our FastAdapter
    private var fastItemAdapter: FastItemAdapter<GenericItem>

    internal var section: CustomizeOrderSectionDTO? = null

    private var callBack: ((HomeListDTO, View, Int) -> Unit?)? = null

    init {

        //create our FastAdapter which will manage everything
        fastItemAdapter = FastItemAdapter()

        /* //configure our fastAdapter
         fastItemAdapter.onClickListener =
             { v: View?, _: IAdapter<BindingTopRestaurantItem>, item: BindingTopRestaurantItem, _: Int ->
                 v?.let { }
                 false
             }
         fastItemAdapter.onPreClickListener =
             { _: View?, _: IAdapter<BindingTopRestaurantItem>, _: BindingTopRestaurantItem, _: Int ->
                 true // consume otherwise radio/checkbox will be deselected
             }
 */
        fastItemAdapter.onClickListener =
            { v: View?, _: IAdapter<GenericItem>, item: GenericItem, _: Int ->
                v?.let {
                    when (item) {
                        is BindingTopRestaurantItem -> {
                            callBack?.let { it(item.item!!, v, -1) }
                        }
                        is BindingSeeMoreItem -> {
                            callBack?.let { it(item.item!!, v, -2) }
                        }
                        else -> {
                            false
                        }
                    }
                }
                true
            }


    }


    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        //get our recyclerView and do basic setup
        this.addItemDecoration(VerticalItemDecoration(23.px, false))
        this.layoutManager = LinearLayoutManager(context)
        this.isNestedScrollingEnabled = false
        this.itemAnimator = SlideUpAlphaAnimator().apply {
            addDuration = 600
            removeDuration = 200
        }
        this.overScrollMode = OVER_SCROLL_NEVER
        this.adapter = fastItemAdapter
        this.setPadding(
            UIHelper.i15px,
            UIHelper.i25px,
            UIHelper.i15px,
            UIHelper.i15px
        )
        this.clipToPadding = false
        this.clipChildren = false
        /*OverScrollDecoratorHelper.setUpOverScroll(
            this,
            OverScrollDecoratorHelper.ORIENTATION_HORIZONTAL
        )*/

    }

    fun withItem(
        section: List<HomeListDTO>,
        listener: (HomeListDTO, View, Int) -> Unit?
    ): TopRestaurantListView {
        this.callBack = listener

        val items = ArrayList<GenericItem>()
        section.forEach { homeItem ->
            homeItem.let { item ->
                items.add(
                    BindingTopRestaurantItem().withTopRestaurantItem(item)
                )
            }
        }

        if (section.size > RemoteConfigHelper.display_limit) {
            items.add(BindingSeeMoreItem().withSeeMoreItem(section[0]))
        }

        fastItemAdapter.add(items)

        return this
    }

    fun setData(nestedList: ArrayList<HomeListDTO>) {
        val items = ArrayList<GenericItem>()
        nestedList.forEach { homeItem ->
            homeItem.let { item ->
                items.add(
                    BindingTopRestaurantItem().withTopRestaurantItem(item)
                )
            }
        }
    }

}