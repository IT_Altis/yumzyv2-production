package com.yumzy.orderfood.ui.contracthandler

import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.ActivityResultRegistry
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.yumzy.orderfood.ui.base.IView

class ContractContentPicker(
    private val registry: ActivityResultRegistry,
    private val iview: IView
) :
    DefaultLifecycleObserver {
    lateinit var getContent: ActivityResultLauncher<String>

    override fun onCreate(owner: LifecycleOwner) {
        getContent = registry.register("key", owner, ActivityResultContracts.GetContent()) { uri ->
            iview.onResponse(234, uri)
        }
    }

    fun selectImage(contentType: String? = "image/*") {
        getContent.launch(contentType)
    }

}
