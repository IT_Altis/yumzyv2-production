package com.yumzy.orderfood.ui.component;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.laalsa.laalsalib.ui.VUtil;
import com.laalsa.laalsaui.drawable.DrawableUtils;
import com.yumzy.orderfood.R;
import com.yumzy.orderfood.ui.helper.ThemeConstant;
import com.yumzy.orderfood.util.viewutils.fontutils.IconFontDrawable;

/**
 * Created by Bhupendra Kumar Sahu.
 */
public class CartCountView extends LinearLayout {
    private String item_count;
    private String restaurant_name;


    public CartCountView(Context context) {
        this(context, null);
    }

    public CartCountView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CartCountView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            initAttrs(context, attrs, defStyleAttr);
        if (item_count == null) {
            item_count = "";
        }
        if (restaurant_name == null) {
            restaurant_name = "";
        }

        init();
    }

    private void initAttrs(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CartCountView, defStyleAttr, 0);
        item_count = a.getString(R.styleable.CartCountView_tv_item_count);
        restaurant_name = a.getString(R.styleable.CartCountView_tv_restaurant_name);

        a.recycle();
    }

    @SuppressLint("SetTextI18n")
    public void init() {

        int i10px = VUtil.dpToPx(10);
        int i30px = VUtil.dpToPx(40);
        int i5px = VUtil.dpToPx(5);

        this.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        this.setOrientation(HORIZONTAL);
        this.setBackground(DrawableUtils.getRoundDrawable(ThemeConstant.white, VUtil.dpToPx(8)));
        LinearLayout container = new LinearLayout(getContext());
        LinearLayout.LayoutParams containerLayoutParams = new LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
        container.setLayoutParams(containerLayoutParams);

        container.setOrientation(VERTICAL);
        container.setGravity(Gravity.CENTER | Gravity.START);
        container.setPadding(i5px, i5px, i5px, i5px);
        containerLayoutParams.setMargins(i5px, i5px, i5px, i5px);



        /*Count Text*/

        TextView itemCount = new TextView(getContext());
        itemCount.setText(item_count);
        itemCount.setId(R.id.tv_item_count);
        itemCount.setPadding(i10px, i5px, i5px, 0);
        itemCount.setTypeface(Typeface.DEFAULT_BOLD);
        itemCount.setTextColor(ContextCompat.getColor(getContext(), android.R.color.black));
        itemCount.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f);

        /*restaurant name*/
        TextView rsetName = new TextView(getContext());
        rsetName.setTextColor(ContextCompat.getColor(getContext(), R.color.colorGrey));
        rsetName.setText(restaurant_name);
        rsetName.setPadding(i10px, 0, i5px, 0);
        rsetName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10f);
        rsetName.setId(R.id.tv_rest_name);


        LinearLayout btnContainer = new LinearLayout(getContext());
        LayoutParams btnContainerParam = new LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
        btnContainer.setLayoutParams(btnContainerParam);
        btnContainer.setGravity(Gravity.CENTER);


        Button btnPlate = new Button(getContext());
        LayoutParams btnPlateParam = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        btnPlateParam.setMargins(i5px, i5px, i5px, i5px);
        btnPlate.setPadding(i30px, i5px, i30px, i5px);
        btnPlate.setText(R.string.view_plate);
        btnPlate.setId(R.id.btnPlate);
        btnPlate.setGravity(Gravity.CENTER);
        btnPlate.setClickable(false);

        IconFontDrawable plateDrawable = new IconFontDrawable(getContext(), R.string.icon_dish);
        plateDrawable.setTextSize(24);
        plateDrawable.setTextColor(R.color.white);
        btnPlate.setCompoundDrawablesWithIntrinsicBounds(null, null, plateDrawable, null);
        btnPlate.setAllCaps(false);
        btnPlate.setTypeface(Typeface.DEFAULT_BOLD);
        btnPlate.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f);

        btnPlate.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
        btnPlate.setLayoutParams(btnPlateParam);


        container.addView(itemCount);
        container.addView(rsetName);
        btnContainer.addView(btnPlate);
        this.addView(container);
        this.addView(btnContainer);


    }

    public String getItem_count() {
        return item_count;
    }

    public void setItem_count(String item_count) {
        this.item_count = item_count;
        ((TextView) findViewById(R.id.tv_item_count)).setText(this.item_count);

    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
        ((TextView) findViewById(R.id.tv_rest_name)).setText(this.restaurant_name);

    }


}
