package com.yumzy.orderfood.ui.screens.restaurant

import android.content.Context
import com.yumzy.orderfood.data.models.RestaurantItem
import com.yumzy.orderfood.ui.common.cart.CartHelper


class RestaurantRecyclerHelper(
    private val context: Context,
    private val homeLayoutList: ArrayList<RestaurantItem>,
    private val layoutType: Int,
    private val cartHelper: CartHelper,
    private val sectionIndex: Int
) /*{

    fun build(): View {
        return when (layoutType) {
            13 -> {
                getGridRecyclerView(homeLayoutList)
            }
            else -> getVerticalRecyclerView(homeLayoutList)
        }
    }

    private fun getGridRecyclerView(trimList: ArrayList<RestaurantItem>): RecyclerView {
        val recyclerView = RecyclerView(context)
        recyclerView.setPadding(
            UIHelper.i15px,
            UIHelper.i8px,
            UIHelper.i15px,
            UIHelper.i8px
        )
        recyclerView.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        val spanCount = if (VUtil.isMobile(context)) 2 else 3

        recyclerView.addItemDecoration(
            GridSpacingItemDecoration(
                spanCount,
                VUtil.dpToPx(13),
                false
            )
        )
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.overScrollMode = View.OVER_SCROLL_NEVER
        recyclerView.layoutManager = GridLayoutManager(context, spanCount)

        val outletTopAdapter = OutletTopAdapter(context, object : OutletInteraction {
            override fun onItemSelected(position: Int, item: RestaurantItem) {}

            override fun onItemCountChange(
                cartHandler: ICartHandler,
                count: Int,
                item: RestaurantItem,
                quantityView: AddQuantityView
            ) {
                cartHelper.changeItemCountInCart(
                    (context as OutletActivity),
                    context,
                    count,
                    item,
                    quantityView,
                    context.vm.addonsMap
                ) {

                    when {
                        !it -> {
                            cartHandler.failedToAdd(count, quantityView)
                        }
                        else -> {
                            cartHandler.itemAdded(count)
                            when {

                                count <= 0 -> {
                                    context.vm.removeSelectedItem(item.itemId)
                                        .observe(context, Observer {})
                                }
                                count > 0 -> {
                                    context.vm.addSelectedItemList(item.itemId, count)
                                        .observe(context, Observer { })
                                }
                            }
                        }
                    }
                }
            }

            override fun onItemFailed(message: String) {}

            override fun adapterSizeChange(itemSize: Int) {}

            override fun onNoteClicked(item: RestaurantItem) {}
        })
        outletTopAdapter.setListData(homeLayoutList)
        outletTopAdapter.setVegOnly()



        (context as OutletActivity).let {
            it.vm.viewModelScope.launch(Dispatchers.Main) {
                it.vm.getSelectedItem()?.observe(it, Observer { selectedList ->
                    outletTopAdapter.setSelectedItemData(selectedList)

                })
            }

        }
        outletTopAdapter.setListData(homeLayoutList)
        outletTopAdapter.setVegOnly()

        recyclerView.adapter = outletTopAdapter
        return recyclerView
    }


    private fun getVerticalRecyclerView(trimList: ArrayList<RestaurantItem>): RecyclerView {
        val recyclerView = RecyclerView(context)
        recyclerView.setPadding(
            UIHelper.i15px,
            UIHelper.i8px,
            UIHelper.i15px,
            UIHelper.i8px
        )
        recyclerView.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        recyclerView.addItemDecoration(LayoutMarginDecoration(1, UIHelper.i20px))
        recyclerView.overScrollMode = View.OVER_SCROLL_NEVER
        recyclerView.clipToPadding = false
        recyclerView.isNestedScrollingEnabled = false
        recyclerView.overScrollMode = RecyclerView.OVER_SCROLL_NEVER
        recyclerView.layoutManager = LinearLayoutManagerWrapper(context)
        recyclerView.itemAnimator = DefaultItemAnimator()
        val outletMenuAdapter = OutletMenuAdapter(context, object : OutletInteraction {

            override fun onItemSelected(position: Int, item: RestaurantItem) {}

            override fun onItemCountChange(
                cartHandler: ICartHandler,
                count: Int,
                item: RestaurantItem,
                quantityView: AddQuantityView
            ) {
                cartHelper.changeItemCountInCart(
                    (context as OutletActivity),
                    context,
                    count,
                    item,
                    quantityView,
                    context.vm.addonsMap
                ) {

                    when {
                        !it -> {
                            cartHandler.failedToAdd(count, quantityView)
                        }
                        else -> {
                            when {

                                count <= 0 -> {
                                    context.vm.removeSelectedItem(item.itemId)
                                        .observe(context, Observer {response ->
                                            when(response.data) {
                                                true -> {
                                                    cartHandler.itemAdded(count)
                                                }
                                            }
                                        })
                                }
                                count > 0 -> {
                                    context.vm.addSelectedItemList(item.itemId, count)
                                        .observe(context, Observer {response ->
                                            when(response.data) {
                                                true -> {
                                                    cartHandler.itemAdded(count)
                                                }
                                            }
                                        })
                                }
                            }
                        }
                    }
                }
            }

            override fun onItemFailed(message: String) {}

            override fun adapterSizeChange(itemSize: Int) {}

            override fun onNoteClicked(item: RestaurantItem) {}
        })
        outletMenuAdapter.setListData(homeLayoutList)
        outletMenuAdapter.setVegOnly()
        (context as OutletActivity).let {
//                CoroutineScope(Main)
//                CoroutineScope(CoroutineScope.M)
            it.vm.viewModelScope.launch(Dispatchers.Main) {
                it.vm.getSelectedItem()?.observe(it, Observer { selectedList ->
                    outletMenuAdapter.setSelectedItemData(selectedList)
                })
            }



        }
        recyclerView.adapter = outletMenuAdapter
        return recyclerView
    }

}*/