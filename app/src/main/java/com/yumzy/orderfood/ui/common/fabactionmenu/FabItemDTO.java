package com.yumzy.orderfood.ui.common.fabactionmenu;

import android.graphics.drawable.Drawable;

public class FabItemDTO {
    private String sActionName = null;
    private int iActionId = 0;
    private Drawable clIcon = null;

    public FabItemDTO() {
    }

    public FabItemDTO(String sActionName, int iActionId, Drawable clIcon) {
        this.sActionName = sActionName;
        this.iActionId = iActionId;
        this.clIcon = clIcon;
    }

    public String getActionName() {
        return sActionName;
    }

    public void setActionName(String sActionName) {
        this.sActionName = sActionName;
    }

    public int getActionId() {
        return iActionId;
    }

    public void setActionId(int iActionId) {
        this.iActionId = iActionId;
    }

    public Drawable getIcon() {
        return clIcon;
    }

    public void setIcon(Drawable clIcon) {
        this.clIcon = clIcon;
    }

    @Override
    public String toString() {
        return sActionName;
    }
}
