package com.yumzy.orderfood.ui.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.laalsa.laalsalib.ui.VUtil;
import com.laalsa.laalsalib.utilcode.util.ShadowUtils;
import com.yumzy.orderfood.R;
import com.yumzy.orderfood.util.viewutils.YumUtil;
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView;

/**
 * Created by Bhupendra Kumar Sahu
 */
public class AppliedCouponView extends LinearLayout implements View.OnClickListener {
    public boolean isCouponApplied;
    private Drawable iv, icon;
    private String couponCode, couponAppliedText;
    private CouponViewEvents couponViewEvents;

    public AppliedCouponView(Context context) {
        this(context, null);
    }

    public AppliedCouponView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AppliedCouponView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null) {
            initArray(attrs, defStyleAttr);
        }
        if (icon == null) {
        }
        if (couponCode == null) {
            couponCode = "";
        }
        if (couponAppliedText == null) {
            couponAppliedText = "";
        }

        if (iv == null) {
        }
        init();
    }

    private void initArray(@Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.AppliedCouponView, defStyleAttr, 0);
        couponCode = typedArray.getString(R.styleable.AppliedCouponView_coupon_code);
        couponAppliedText = typedArray.getString(R.styleable.AppliedCouponView_coupon_applied_text);
        icon = typedArray.getDrawable(R.styleable.AppliedCouponView_coupon_cross_icon);
        iv = typedArray.getDrawable(R.styleable.AppliedCouponView_coupon_tag_icon);
        isCouponApplied = typedArray.getBoolean(R.styleable.AppliedCouponView_coupon_applied, false);
        typedArray.recycle();
    }

    private void init() {
        this.setOrientation(VERTICAL);
        this.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        //this.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_gray));
        int i24px = VUtil.dpToPx(20);
        int i20px = VUtil.dpToPx(20);
        int i16px = VUtil.dpToPx(16);
        int i10px = VUtil.dpToPx(10);
        int i1px = VUtil.dpToPx(1);

        /*Coupon view*/
        LinearLayout llCoupon = new LinearLayout(getContext());
        LayoutParams couponLayoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        ShadowUtils.apply(
                llCoupon,
                new ShadowUtils.Config().setShadowSize(i1px).setShadowRadius(i10px).setShadowColor(0XBFCECECE)
        );
        llCoupon.setLayoutParams(couponLayoutParams);
        llCoupon.setOrientation(HORIZONTAL);
        llCoupon.setGravity(Gravity.CENTER | Gravity.START);
        llCoupon.setPadding(i16px, i10px, i10px, i10px);

        ImageView ivCouponIcon = new ImageView(getContext());
        ivCouponIcon.setId(R.id.iv_coupon_icon);
        ivCouponIcon.setImageDrawable(iv);
        ivCouponIcon.setLayoutParams(new LayoutParams(i24px, i24px));

        TextView tvApplyCoupon = new TextView(getContext());
        tvApplyCoupon.setId(R.id.tv_apply_coupon);
        tvApplyCoupon.setText(R.string.apply_coupon);
        tvApplyCoupon.setVisibility(isCouponApplied ? GONE : VISIBLE);
        YumUtil.INSTANCE.setInterFontBold(getContext(), tvApplyCoupon);
        LayoutParams tvApplyParam = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        tvApplyParam.setMargins(i10px, 0, i10px, 0);
        tvApplyCoupon.setGravity(Gravity.CENTER | Gravity.START);
        tvApplyCoupon.setLayoutParams(tvApplyParam);
        tvApplyCoupon.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
        tvApplyCoupon.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f);


        TextView tvCouponCode = new TextView(getContext());
        tvCouponCode.setId(R.id.tv_coupon_code);
        tvCouponCode.setText(couponCode);
        YumUtil.INSTANCE.setInterFontBold(getContext(), tvCouponCode);
        LayoutParams couponCodeParam = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        couponCodeParam.setMargins(i10px, 0, i10px, 0);
        tvCouponCode.setLayoutParams(couponCodeParam);
        tvCouponCode.setGravity(Gravity.CENTER | Gravity.START);
        tvCouponCode.setVisibility(isCouponApplied ? VISIBLE : GONE);
        tvCouponCode.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
        tvCouponCode.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f);

        TextView tvCouponApplied = new TextView(getContext());
        tvCouponApplied.setId(R.id.tv_coupon_applied);
        tvCouponApplied.setText(couponAppliedText);
        tvCouponApplied.setVisibility(isCouponApplied ? VISIBLE : GONE);
        tvCouponApplied.setGravity(Gravity.START);
        if (isCouponApplied) {
            tvCouponApplied.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));

        } else {
            tvCouponApplied.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        }
        tvCouponApplied.setTextColor(ContextCompat.getColor(getContext(), R.color.colorGreen));
        tvCouponApplied.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11f);
        YumUtil.INSTANCE.setInterFontRegular(getContext(), tvCouponApplied);

        ImageView ivCancelCoupon = new ImageView(getContext());
        ivCancelCoupon.setId(R.id.iv_cancel_coupon);
        ivCancelCoupon.setVisibility(isCouponApplied ? VISIBLE : GONE);
        ivCancelCoupon.setImageDrawable(icon);
        ivCancelCoupon.setLayoutParams(new LayoutParams(i20px, i20px));

        FontIconView nextarrow = new FontIconView(getContext());
        nextarrow.setId(R.id.tv_next_arrow);
        nextarrow.setTextColor(ContextCompat.getColor(getContext(), R.color.cool_gray));
        nextarrow.setVisibility(isCouponApplied ? GONE : VISIBLE);
        nextarrow.setText(R.string.icon_angle_right_b1);
        nextarrow.setLayoutParams(new LayoutParams(i16px, i16px));

        llCoupon.addView(ivCouponIcon);
        llCoupon.addView(tvCouponCode);
        llCoupon.addView(tvApplyCoupon);
        llCoupon.addView(tvCouponApplied);
        llCoupon.addView(ivCancelCoupon);
        llCoupon.addView(nextarrow);


        this.addView(llCoupon);


    }

    public Drawable getCouponTagIcon() {
        return iv;
    }

    public void setCouponTagIcon(Drawable iv) {
        this.iv = iv;
        ((ImageView) findViewById(R.id.iv_coupon_icon)).setImageDrawable(this.iv);
    }

    public Drawable getCouponCancelIcon() {
        return icon;
    }

    public void setCouponCancelIcon(Drawable icon) {
        this.icon = icon;
        ((ImageView) findViewById(R.id.iv_cancel_coupon)).setImageDrawable(this.icon);
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
        ((TextView) findViewById(R.id.tv_coupon_code)).setText(this.couponCode);
    }

    public String getCouponApplied() {
        return couponAppliedText;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_cancel_coupon:
                if (couponViewEvents != null) {
                    findViewById(R.id.iv_cancel_coupon).setVisibility(GONE);
                    findViewById(R.id.tv_coupon_applied).setVisibility(GONE);
                    couponViewEvents.removeCoupon();
                }
                break;


        }
    }

    public boolean isCouponApplied() {
        return isCouponApplied;
    }

    public void setCouponApplied(String couponAppliedText) {
        this.couponAppliedText = couponAppliedText;
        ((TextView) findViewById(R.id.tv_coupon_applied)).setText(this.couponAppliedText);

    }

    public void setCouponApplied(boolean couponApplied) {
        isCouponApplied = couponApplied;
        if (isCouponApplied) {
            findViewById(R.id.tv_coupon_applied).setVisibility(VISIBLE);
            findViewById(R.id.iv_cancel_coupon).setVisibility(VISIBLE);
            findViewById(R.id.tv_coupon_code).setVisibility(VISIBLE);
            findViewById(R.id.tv_next_arrow).setVisibility(GONE);
            findViewById(R.id.tv_apply_coupon).setVisibility(GONE);
            findViewById(R.id.tv_coupon_applied).setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        } else {
            findViewById(R.id.tv_coupon_code).setVisibility(GONE);
            findViewById(R.id.tv_coupon_applied).setVisibility(GONE);
            findViewById(R.id.iv_cancel_coupon).setVisibility(GONE);
            findViewById(R.id.tv_next_arrow).setVisibility(VISIBLE);
            findViewById(R.id.tv_apply_coupon).setVisibility(VISIBLE);
            findViewById(R.id.tv_coupon_applied).setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        }
    }

    public TextView applyCouponTextID() {
        return this.findViewById(R.id.tv_apply_coupon);
    }

    public TextView applyCouponArrow() {
        return this.findViewById(R.id.tv_next_arrow);
    }

    public ImageView removeCouponViewID() {
        return this.findViewById(R.id.iv_cancel_coupon);
    }

    public CouponViewEvents getCouponViewEvents() {
        return couponViewEvents;
    }

    public void setCouponViewEvents(CouponViewEvents couponViewEvents) {
        this.couponViewEvents = couponViewEvents;
    }

    public interface CouponViewEvents {
        void applyCoupon();

        void removeCoupon();
    }
}
