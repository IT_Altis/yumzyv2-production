package com.yumzy.orderfood.ui.screens.restaurant.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.ModelAbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.databinding.AdapterPopularCuisineBinding
import com.yumzy.orderfood.databinding.AdapterWinterSpecailBinding
import com.yumzy.orderfood.databinding.ViewTheBrandBinding
import com.yumzy.orderfood.util.IConstants
import java.util.*

class BindingWinterSpecialItem(item: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, AdapterWinterSpecailBinding>(item) {
    override val type: Int = R.id.home_banner_item_id
    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterWinterSpecailBinding {
        return AdapterWinterSpecailBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: AdapterWinterSpecailBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)
        binding.imageUrl = model.image?.url.toString()
        binding.title = model.title.capitalize(Locale.getDefault())
    }

    override fun unbindView(binding: AdapterWinterSpecailBinding) {
//        binding.imageUrl = null
        binding.title = null
    }

}

class TheBrandItem(item: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, ViewTheBrandBinding>(item) {
    override val type: Int = R.id.item_the_brand
    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ViewTheBrandBinding {
        return ViewTheBrandBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: ViewTheBrandBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)
        val title = model.image?.caption ?: ""
        val sDuration = model.meta?.duration ?: 0.0
        binding.imageUrl = model.image?.url.toString()
        binding.title = title
        binding.timeDuration = sDuration.toInt().toString() + IConstants.DeliveryTime.MINS;


    }

    override fun unbindView(binding: ViewTheBrandBinding) {
        binding.title = null
    }

}


class PopularCuisinesItem(item: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, AdapterPopularCuisineBinding>(item) {
    override val type: Int = R.id.popular_cuisines_item
    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterPopularCuisineBinding {
        return AdapterPopularCuisineBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: AdapterPopularCuisineBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)
        val imageUrl = model.image?.url ?: ""
        val title = model.title.capitalize(Locale.getDefault())
        binding.cuisineView.setPopularCuisine(imageUrl, title)
        binding.imageUrl = imageUrl
        binding.title = title
    }

    override fun unbindView(binding: AdapterPopularCuisineBinding) {
        binding.title = null
    }
}

class OutletCategoryItem(item: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, AdapterPopularCuisineBinding>(item) {

    override val type: Int = R.id.popular_cuisines_item

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterPopularCuisineBinding {
        return AdapterPopularCuisineBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: AdapterPopularCuisineBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)
        val imageUrl = model.image?.url ?: ""
        val title = model.title.capitalize(Locale.getDefault())
        binding.cuisineView.setPopularCuisine(imageUrl, title)
        binding.imageUrl = imageUrl
        binding.title = title
    }

    override fun unbindView(binding: AdapterPopularCuisineBinding) {
        binding.title = null
    }
}
