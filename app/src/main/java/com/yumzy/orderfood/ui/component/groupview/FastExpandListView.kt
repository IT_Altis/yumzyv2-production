package com.yumzy.orderfood.ui.component.groupview

/*
class FastExpandListView : LinearLayout {

    private val i14px = VUtil.dpToPx(14)
    private val i8px = VUtil.dpToPx(8)
    private val i3px = VUtil.dpToPx(3)

    private var mListItemId = 8050
    private var mCurrentSelectedItemId = -1

    private var mViewsMap = mutableMapOf<Int, View>()

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        this.apply {
            orientation = VERTICAL
            layoutTransition =
                LayoutTransition().apply { this.enableTransitionType(LayoutTransition.APPEARING) }
        }
    }

    fun buildWithArrayList(
        items: ArrayList<PersonalisationResponseDTO>,
        onBindView: (data: Any) -> View,
        isViewCenter: Boolean = false
    ) {

        items.forEach { item ->
            val view = onBindView(item.values).apply {
                visibility = View.GONE
            }

            mViewsMap[mListItemId] = view
            val container = getContainer(item.title, mListItemId, isViewCenter) { containerId ->

                if (mCurrentSelectedItemId == containerId) {
                    this.findViewById<LinearLayout>(containerId).apply {
                        getChildAt(1).apply {
                            if (this.visibility == View.GONE) {
                                this.visibility = View.VISIBLE
                            } else {
                                this.visibility = View.GONE
                            }
                        }
                    }
                } else if (mCurrentSelectedItemId != -1) {
                    //close previous view
                    this.findViewById<LinearLayout>(mCurrentSelectedItemId).apply {
                        getChildAt(1).visibility = View.GONE
                    }

                    //expand current clicked view
                    this.findViewById<LinearLayout>(containerId).apply {
                        getChildAt(1).visibility = View.VISIBLE
                    }

                    //expanded container id.
                    mCurrentSelectedItemId = containerId

                } else {
                    //expand current clicked view
                    this.findViewById<LinearLayout>(containerId).apply {
                        getChildAt(1).visibility = View.VISIBLE
                    }

                    //expanded container id.
                    mCurrentSelectedItemId = containerId
                }
            }

            this.addView(container)

            //update container id
            mListItemId++
        }

    }

    fun setupList(
        items: Map<String, Any>,
        onBindView: (data: Any) -> View
    ) {

        items.keys.forEach {
            val view = onBindView(items.getValue(it)).apply {
                visibility = View.GONE
            }

            mViewsMap[mListItemId] = view
            val container = getContainer(it, mListItemId, false) { containerId ->

                if (mCurrentSelectedItemId == containerId) {
                    this.findViewById<LinearLayout>(containerId).apply {
                        getChildAt(1).apply {
                            if (this.visibility == View.GONE) {
                                this.visibility = View.VISIBLE
                            } else {
                                this.visibility = View.GONE
                            }
                        }
                    }
                } else if (mCurrentSelectedItemId != -1) {
                    //close previous view
                    this.findViewById<LinearLayout>(mCurrentSelectedItemId).apply {
                        getChildAt(1).visibility = View.GONE
                    }

                    //expand current clicked view
                    this.findViewById<LinearLayout>(containerId).apply {
                        getChildAt(1).visibility = View.VISIBLE
                    }

                    //expanded container id.
                    mCurrentSelectedItemId = containerId

                } else {
                    //expand current clicked view
                    this.findViewById<LinearLayout>(containerId).apply {
                        getChildAt(1).visibility = View.VISIBLE
                    }

                    //expanded container id.
                    mCurrentSelectedItemId = containerId
                }
            }

            this.addView(container)

            //update container id
            mListItemId++
        }

    }

    private fun getContainer(
        title: String,
        containerId: Int,
        isViewCenter: Boolean,
        onContainerClick: (containerId: Int) -> Unit
    ): View {
        return LinearLayout(context).apply {
            layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
            setPadding(i3px, i3px, i3px, i3px)

            id = containerId
            orientation = VERTICAL

            if (isViewCenter)
                gravity = Gravity.CENTER

            //horizontal linear layout
            this.addView(LinearLayout(context).apply {
                layoutParams = if (isViewCenter == false)
                    LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
                else
                    LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)

                orientation = HORIZONTAL
                setPadding(i3px, i3px, i3px, i3px)

                //titleTextview
                this.addView(TextView(context).apply {
                    layoutParams = LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f).apply {
                        setMargins(i3px, i3px, i3px, i3px)
                    }
                    this.setInterBoldFont()
                    this.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
                    this.text = title.capitalize()
                })

                //arrwobutton
                this.addView(FontIconView(context).apply {
                    layoutParams =
                        LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT).apply {
                            setMargins(i3px, VUtil.dpToPx(5), i3px, i3px)
                        }
                    this.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)
                    this.text = context.resources.getString(R.string.icon_angle_down)
                    this.id = containerId + 51
                })

                this.setClickListenerBackground()
                this.setOnClickListener { onContainerClick(containerId) }

            })

            //expandable view
            this.addView(mViewsMap[containerId])

        }
    }

}*/
