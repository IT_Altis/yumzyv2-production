package com.yumzy.orderfood.ui.screens.information

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.DialogFragment
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.common.YumLottieView
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.util.ViewConst


class LottieFullDialog : DialogFragment(), YumLottieView.OnLottieAnimationListener {

    lateinit var imageUrl: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
            STYLE_NO_FRAME,
            R.style.FullScreenDialogStyle
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val layout = LinearLayout(context)
        layout.setBackgroundColor(Color.WHITE)
        layout.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.MATCH_PARENT)
        val buildLottieFrameWithListener = UIHelper.buildLottieFrameWithListener(
            requireContext(),
            imageUrl,
            this
        )




        layout.addView(buildLottieFrameWithListener)
        return layout

    }

    override fun onAnimationEnd() {
        this.dismiss()
    }

    /* override fun onStart() {
         super.onStart()

         // safety check
         if (dialog == null) {
             return
         }

         // set the animations to use on showing and hiding the dialog
         dialog?.window?.setWindowAnimations(
             R.style.DialogTheme
         )

         // alternative way of doing it
         //getDialog().getWindow().getAttributes().
         //    windowAnimations = R.style.dialog_animation_fade;

         // ... other stuff you want to do in your onStart() method
     }*/

}
