package com.yumzy.orderfood.ui.helper

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.view.Gravity
import android.view.View
import android.view.ViewAnimationUtils
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import com.airbnb.lottie.LottieDrawable
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.ui.pxf
import com.laalsa.laalsaui.drawable.DrawableUtils
import com.laalsa.laalsaui.utils.ColorHelper
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.LayoutInviteEarnBinding
import com.yumzy.orderfood.ui.common.YumLottieView
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView
import kotlin.math.hypot


object UIHelper {
    val i1px = VUtil.dpToPx(1)
    val i2px = VUtil.dpToPx(2)
    val i3px = VUtil.dpToPx(3)
    val i4px = VUtil.dpToPx(4)
    val i5px = VUtil.dpToPx(5)
    val i6px = VUtil.dpToPx(6)
    val i7px = VUtil.dpToPx(7)
    val i8px = VUtil.dpToPx(8)
    val i10px = VUtil.dpToPx(10)
    val i12px = VUtil.dpToPx(12)
    val i13px = VUtil.dpToPx(13)
    val i15px = VUtil.dpToPx(15)
    val i18px = VUtil.dpToPx(18)
    val i20px = VUtil.dpToPx(20)
    val i25px = VUtil.dpToPx(25)
    val i30px = VUtil.dpToPx(30)
    val i35px = VUtil.dpToPx(35)
    val i40px = VUtil.dpToPx(40)
    val i42px = VUtil.dpToPx(42)
    val i45px = VUtil.dpToPx(45)
    val i50px = VUtil.dpToPx(50)
    val i60px = VUtil.dpToPx(60)
    val i70px = VUtil.dpToPx(70)
    val i80px = VUtil.dpToPx(80)
    val i100px = VUtil.dpToPx(100)
    val i120px = VUtil.dpToPx(120)
    val i130px = VUtil.dpToPx(130)
    val i150px = VUtil.dpToPx(150)
    val i180px = VUtil.dpToPx(180)
    val i200px = VUtil.dpToPx(200)
    val i300px = VUtil.dpToPx(300)
    val statusBarSize = i12px
    val iconSizeSmall = i25px
    val iconSizeMedium = i25px
    val iconSizeLarge = i35px
    val buttonHeight = i42px
    val cornerRadius = 5.pxf

    fun outlineButton(context: Context, text: String, compoundDrawable: Drawable?): Button {
        val buttonBackground1 = roundColoredDrawable()
        val buttonBackground2 = roundStrokedDrawable()

        val outlineButton = Button(context)

        changeButtonPropety(outlineButton)

        outlineButton.text = text
        outlineButton.setPadding(i20px, i10px, i20px, i10px)
        outlineButton.setTextColor(
            DrawableUtils.getColorStateListDrawable(
                ThemeConstant.pinkies,
                Color.WHITE
            )
        )
        compoundDrawable?.let {
            val tintDrawable =
                compoundDrawable.constantState?.newDrawable()?.mutate()
            outlineButton.setCompoundDrawablesRelativeWithIntrinsicBounds(
                DrawableUtils.getDrawableListState(
                    compoundDrawable,
                    tintDrawable ?: ColorDrawable()
                ), null, null, null
            )
        }
        outlineButton.compoundDrawablePadding = i5px
        outlineButton.setOnClickListener {}
        outlineButton.background = DrawableUtils.getDrawableListState(
            buttonBackground2,
            buttonBackground1
        )
        return outlineButton
    }

    fun filledButton(context: Context, text: String, compoundDrawable: Drawable?): Button {
        val buttonBackground1 = roundColoredDrawable()
        val buttonBackground2 = roundStrokedDrawable()

        val filledButton = Button(context)
        changeButtonPropety(filledButton)
        filledButton.text = text
        filledButton.setPadding(i20px, i10px, i20px, i10px)
        filledButton.setTextColor(
            DrawableUtils.getColorStateListDrawable(
                Color.WHITE, ThemeConstant.pinkies
            )
        )
        compoundDrawable?.let {
            val tintDrawable =
                compoundDrawable.constantState?.newDrawable()?.mutate()
            filledButton.setCompoundDrawablesRelativeWithIntrinsicBounds(
                DrawableUtils.getDrawableListState(
                    compoundDrawable,
                    tintDrawable ?: ColorDrawable()
                ), null, null, null
            )
        }
        filledButton.compoundDrawablePadding = i5px
        filledButton.setOnClickListener {}
        filledButton.background = DrawableUtils.getDrawableListState(
            buttonBackground1,
            buttonBackground2
        )
        return filledButton
    }

    private fun changeButtonPropety(filledButton: Button) {
        val param = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        filledButton.minHeight = 0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            filledButton.stateListAnimator = null
            filledButton.elevation = 0f
        }
        filledButton.minWidth = 0
        filledButton.minimumHeight = 0
        filledButton.minimumWidth = 0
        filledButton.layoutParams = param
    }

    fun roundedShadeDrawable(
        color1: Int = 0xFFFFC04C.toInt(),
    ) = roundedShadeDrawable(
        color1,
        ColorHelper.darkenColor(color1, 0.2f),
    )

    fun roundedShadeDrawable(
        color1: Int = 0xFFFFC04C.toInt(),
        color2: Int = 0xFFFF7C18.toInt(),
    ): GradientDrawable {
        val ovalDrawable =
            GradientDrawable(GradientDrawable.Orientation.BL_TR, intArrayOf(color1, color2))
        ovalDrawable.setGradientCenter(0.0f, 0.0f)
        ovalDrawable.shape = GradientDrawable.OVAL
        ovalDrawable.gradientType = GradientDrawable.LINEAR_GRADIENT
        return ovalDrawable
    }

    fun roundColoredDrawable(): GradientDrawable {
        val buttonBackground1 = GradientDrawable()
        buttonBackground1.cornerRadius = 8.pxf
        buttonBackground1.setStroke(2, ThemeConstant.pinkies)
        buttonBackground1.setColor(ThemeConstant.pinkies)
        return buttonBackground1
    }

    fun roundStrokedDrawable(color: Int = ThemeConstant.pinkies): GradientDrawable {
        val buttonBackground2 = GradientDrawable()
        buttonBackground2.cornerRadius = 8.pxf
        buttonBackground2.setStroke(2, color)
        buttonBackground2.setColor(ThemeConstant.white)
        return buttonBackground2
    }

    fun getStrokeDoted(
        strokeWidth: Int,
        color: Int,
        dashWidth: Float,
        dashGap: Float
    ): GradientDrawable {
        val storkDrawable = GradientDrawable()

        storkDrawable.shape = GradientDrawable.LINE
        storkDrawable.setStroke(strokeWidth, color, dashWidth, dashGap)
        return storkDrawable
    }

    fun roundDotedGreenStrokedDrawable(
        strokeWidth: Int,
        color: Int,
        backColor: Int,
        dashWidth: Float,
        dashGap: Float
    ): GradientDrawable {
        val strokDrawable = GradientDrawable()
        strokDrawable.cornerRadius = 8.pxf
        strokDrawable.setStroke(strokeWidth, color, dashWidth, dashGap)
        strokDrawable.setColor(backColor)
        return strokDrawable
    }

    fun roundDotedRedStrokedDrawable(
        strokeWidth: Int = i1px,
        color: Int = ThemeConstant.pinkies,
        dashWidth: Float = i5px.toFloat(),
        dashGap: Float = i5px.toFloat()
    ): GradientDrawable {
        val strokDrawable = GradientDrawable()
        strokDrawable.cornerRadius = 8.pxf
        strokDrawable.setStroke(strokeWidth, color, dashWidth, dashGap)
        strokDrawable.setColor(ThemeConstant.white)
        return strokDrawable
    }

    fun getStrokeDrawable(strokeColor: Int, rounded: Float, strokeWidth: Int): Drawable {
        val drawable = GradientDrawable()
        drawable.cornerRadius = rounded
        drawable.setStroke(strokeWidth, strokeColor)
        return drawable

    }

    fun showNoDataToDisplay(requireContext: Context): LinearLayout {
        val layout = LinearLayout(requireContext)
        layout.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.MATCH_PARENT)
        layout.orientation = LinearLayout.VERTICAL
        layout.gravity = Gravity.CENTER
        val fontIconView = FontIconView(requireContext)
        fontIconView.layoutParams = LinearLayout.LayoutParams(VUtil.dpToPx(100), VUtil.dpToPx(100))
        fontIconView.setText(R.string.icon_cooking)
        fontIconView.setTextColor(Color.GRAY)

        val textView = TextView(requireContext)
        textView.gravity = Gravity.CENTER
        textView.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        textView.setInterBoldFont()
        textView.text = "Sorry, Nothing Here"
        textView.setTextColor(Color.GRAY)

        layout.addView(fontIconView)
        layout.addView(textView)
        return layout

    }

    fun inviteLayout(requireContext: Context, @DrawableRes drawable: Int): LinearLayout {
        val layout = LinearLayout(requireContext)
        layout.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.MATCH_PARENT)
        layout.orientation = LinearLayout.VERTICAL
        layout.gravity = Gravity.CENTER
        val illsImage = ImageView(requireContext)
        illsImage.layoutParams = LinearLayout.LayoutParams(
            (VUtil.screenWidth * (0.6)).toInt(),
            (VUtil.screenWidth * 0.6).toInt()
        )

        illsImage.setImageResource(drawable)

        layout.addView(illsImage)
        return layout

    }

    fun showMessagedToDisplay(
        context: Context,
        fontIcon: Int,
        color: Int,
        label: String
    ): LinearLayout {
        val layout = LinearLayout(context)
        layout.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.MATCH_PARENT)
        layout.orientation = LinearLayout.VERTICAL
        layout.gravity = Gravity.CENTER
        val fontIconView = FontIconView(context)
        fontIconView.layoutParams = LinearLayout.LayoutParams(VUtil.dpToPx(100), VUtil.dpToPx(100))

        fontIconView.setText(fontIcon)
        fontIconView.setTextColor(color)

        val textView = TextView(context)
        textView.gravity = Gravity.CENTER
        textView.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        textView.setInterBoldFont()
        textView.gravity = Gravity.CENTER
        textView.text = label
        textView.setTextColor(Color.GRAY)
        layout.addView(fontIconView)
        layout.addView(textView)
        return layout

    }

    fun inviteImageLayout(
        activity: AppCompatActivity,
        @DrawableRes resDrawable: Int,
        message: String,
        actionResponse: () -> Any
    ): View {
        val bd = LayoutInviteEarnBinding.inflate(activity.layoutInflater)
        bd.lifecycleOwner = activity
        bd.inviteImage.setImageResource(resDrawable)
        bd.tvInviteMessage.text = message
        bd.btnInvite.setOnClickListener { actionResponse.invoke() }
        return bd.root
    }

    fun showMessageWithImage(
        context: Context,
        fontIcon: Int,
        label: String
    ): LinearLayout {
        val layout = LinearLayout(context)
        layout.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.MATCH_PARENT)
        layout.orientation = LinearLayout.VERTICAL
        layout.gravity = Gravity.CENTER
        val fontIconView = ImageView(context)
        fontIconView.layoutParams = LinearLayout.LayoutParams(VUtil.dpToPx(100), VUtil.dpToPx(100))
        fontIconView.setImageResource(fontIcon)

        val textView = TextView(context)
        textView.gravity = Gravity.CENTER
        textView.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        textView.setInterBoldFont()
        textView.gravity = Gravity.CENTER
        textView.text = label
        textView.setTextColor(Color.GRAY)
        layout.addView(fontIconView)
        layout.addView(textView)
        return layout

    }

    fun buildLottieFrameLoop(context: Context, lottieUrl: String): YumLottieView {
        return YumLottieView(context, lottieUrl).apply {
            layoutParams = ViewGroup.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.MATCH_PARENT)
            repeatCount = LottieDrawable.INFINITE
        }
    }

    fun buildLottieFrame(context: Context, lottieUrl: String): YumLottieView {
        return YumLottieView(context, lottieUrl).apply {
            layoutParams = ViewGroup.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.MATCH_PARENT)
        }
    }

    fun buildLottieFrameWithListener(
        context: Context,
        lottieUrl: String,
        onLottieAnimationListener: YumLottieView.OnLottieAnimationListener
    ): YumLottieView {
        return YumLottieView(context, lottieUrl, onLottieAnimationListener).apply {
            layoutParams = ViewGroup.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.MATCH_PARENT)
        }
    }

    /*fun displayImageRound(ctx: Context, img: ImageView, @DrawableRes drawable: Int) {
        try {
            Glide.with(ctx).load(drawable).asBitmap().centerCrop()
                .into(object : BitmapImageViewTarget(img) {
                    override fun setResource(resource: Bitmap?) {
                        val circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(ctx.resources, resource)
                        circularBitmapDrawable.isCircular = true
                        img.setImageDrawable(circularBitmapDrawable)
                    }
                })
        } catch (e: Exception) {
        }
    }*/
    fun copyToClipboard(context: Context, data: String?) {
        val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("clipboard", data)
        clipboard.setPrimaryClip(clip)
        Toast.makeText(context, "Text copied to clipboard", Toast.LENGTH_SHORT).show()
    }

    fun buildLottieSized(
        context: Context,
        lottieUrl: String, width: Int, height: Int,
        onLottieAnimationListener: YumLottieView.OnLottieAnimationListener?
    ): YumLottieView {
        return YumLottieView(context, lottieUrl, onLottieAnimationListener).apply {
            layoutParams = ViewGroup.LayoutParams(width, height)
        }
    }

    fun buildLottieSizedListener(
        context: Context,
        lottieUrl: String, width: Int, height: Int,
        onLottieAnimationListener: YumLottieView.OnLottieAnimationListener
    ): YumLottieView {
        return YumLottieView(context, lottieUrl, onLottieAnimationListener).apply {
            layoutParams = ViewGroup.LayoutParams(width, height)
        }
    }

    fun revealInvisibleView(
        view: View,
        cx: Int = view.width / 2,
        cy: Int = view.height / 2,
        startRadius: Float = 0f,
        duration: Long = 500
    ) {
        // previously invisible view
        // Check if the runtime version is at least Lollipop
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // get the center for the clipping circle
            // get the final radius for the clipping circle
            val finalRadius = hypot(cx.toDouble(), cy.toDouble()).toFloat()

            // create the animator for this view (the start radius is zero)
            val anim =
                ViewAnimationUtils.createCircularReveal(view, cx, cy, startRadius, finalRadius)
            // make the view visible and start the animation
            view.visibility = View.VISIBLE
            anim.setDuration(duration).start()
        } else {
            // set the view to invisible without a circular reveal animation below Lollipop
            view.visibility = View.INVISIBLE
        }
    }

    fun hideVisibleView(view: View, cx: Int = view.width / 2, cy: Int = view.height / 2) {

// Check if the runtime version is at least Lollipop
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
            // get the center for the clipping circle
//            val cx = view.width / 2
//            val cy = view.height / 2
            // get the initial radius for the clipping circle
            val initialRadius = Math.hypot(cx.toDouble(), cy.toDouble()).toFloat()

            // create the animation (the final radius is zero)
            val anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, initialRadius, 0f)

            // make the view invisible when the animation is done
            anim.addListener(object : AnimatorListenerAdapter() {

                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    view.visibility = View.INVISIBLE
                }
            })

            // start the animation
            anim.start()
        } else {
            // set the view to visible without a circular reveal animation below Lollipop
            view.visibility = View.VISIBLE
        }
    }


}