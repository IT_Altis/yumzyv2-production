package com.yumzy.orderfood.ui.screens.orderhistory

import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.screens.orderhistory.data.OrderRepository
import javax.inject.Inject

/**
 * Created by Bhupendra Kumar Sahu.
 */
class OrderHistoryViewModel @Inject constructor(
    private val repository: OrderRepository
) : BaseViewModel<IOrderHistory>() {
    fun getOrderHistoryList(skip: Int) = repository.getOrderHistoryList(skip)

}