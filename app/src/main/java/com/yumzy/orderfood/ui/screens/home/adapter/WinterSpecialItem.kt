package com.yumzy.orderfood.ui.screens.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.laalsa.laalsalib.ui.px
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.binding.ModelAbstractBindingItem
import com.mikepenz.fastadapter.listeners.ClickEventHook
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.data.models.RestaurantItem
import com.yumzy.orderfood.databinding.ViewBrandListBinding
import com.yumzy.orderfood.databinding.ViewPopularCusinesBinding
import com.yumzy.orderfood.databinding.ViewRecyclerBinding
import com.yumzy.orderfood.databinding.ViewWinterSpecialBinding
import com.yumzy.orderfood.ui.component.AddQuantityView
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.setHorizontalListDecoration
import com.yumzy.orderfood.ui.screens.home.HomePageActivity
import com.yumzy.orderfood.ui.screens.home.fragment.globalHomeItemListener
import com.yumzy.orderfood.ui.screens.module.ICartHandler
import com.yumzy.orderfood.ui.screens.restaurant.adapter.OutletInteraction
import io.cabriole.decorator.LinearDividerDecoration
import io.cabriole.decorator.LinearMarginDecoration

class WinterSpecialItem(private val homeListDTO: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, ViewWinterSpecialBinding>(homeListDTO) {
    override val type = R.id.winter_special_item_id
    override fun bindView(binding: ViewWinterSpecialBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        binding.winterSpecialList.clearData()
        binding.winterSpecialList.setData(homeListDTO.nestedList!!)
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ViewWinterSpecialBinding {
        return ViewWinterSpecialBinding.inflate(inflater, parent, false)
    }
}

class BrandListItem(private val homeListDTO: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, ViewBrandListBinding>(homeListDTO) {
    override val type = R.id.brand_item_id
    override fun bindView(binding: ViewBrandListBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        binding.listItem.clearData()
        binding.listItem.setData(homeListDTO.nestedList!!)
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ViewBrandListBinding {
        return ViewBrandListBinding.inflate(inflater, parent, false)
    }
}

class AdsListItem(private val homeListDTO: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, ViewRecyclerBinding>(homeListDTO) {
    private val items = ArrayList<SimpleImageItem>()
    private val itemAdapter = ItemAdapter<SimpleImageItem>()
    private val fastAdapter: FastAdapter<SimpleImageItem> = FastAdapter.with(itemAdapter)

    override val type = R.id.ads_item_list
    override fun bindView(binding: ViewRecyclerBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
//        val fastAdapter: FastAdapter<SimpleImageItem> = FastAdapter<SimpleImageItem>()
        itemAdapter.clear()
        items.clear()
        homeListDTO.nestedList?.forEach { items.add(SimpleImageItem(it.image?.url ?: "")) }
        itemAdapter.add(items)
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ViewRecyclerBinding {
        val binding = ViewRecyclerBinding.inflate(inflater, parent, false)
        binding.listItem.layoutManager =
            LinearLayoutManager(parent?.context, LinearLayoutManager.HORIZONTAL, false)
        binding.listItem.setHorizontalListDecoration()
        binding.listItem.adapter = fastAdapter
//        binding.listItem.addItemDecoration(
//            LinearMarginDecoration(
//                orientation = RecyclerView.HORIZONTAL,
//                leftMargin = 12.px,
//                rightMargin = 25.px
//            )
//        )
        fastAdapter.onClickListener =
            { v: View?, _: IAdapter<SimpleImageItem>, item: SimpleImageItem, position: Int ->
                if (v != null) {
                    model.nestedList?.get(position)?.let { globalHomeItemListener?.invoke(v, it) }
                }
                false
            }

        return binding
    }
}


class PopularCuisinesItem(private val homeListDTO: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, ViewPopularCusinesBinding>(homeListDTO) {
    override val type = R.id.popular_cusines_view
    override fun bindView(binding: ViewPopularCusinesBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        binding.popularCuisinesList.clearData()
        binding.popularCuisinesList.setData(homeListDTO.nestedList!!)
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ViewPopularCusinesBinding {
        return ViewPopularCusinesBinding.inflate(inflater, parent, false)
    }
}

class PopularFoodListItem(private val homeListDTO: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, ViewRecyclerBinding>(homeListDTO), OutletInteraction {

    private val items = ArrayList<PopularFoodItem>()
    private val itemAdapter = ItemAdapter<PopularFoodItem>()
    private val fastAdapter: FastAdapter<PopularFoodItem> = FastAdapter.with(itemAdapter)


    override val type = R.id.popular_food_list
    override fun bindView(binding: ViewRecyclerBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        itemAdapter.clear()
        homeListDTO.nestedList?.forEach { items.add(PopularFoodItem(it)) }
        itemAdapter.set(items)
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ViewRecyclerBinding {
        val binding = ViewRecyclerBinding.inflate(inflater, parent, false)
        binding.listItem.layoutManager =
            LinearLayoutManager(parent?.context, LinearLayoutManager.HORIZONTAL, false)
        binding.listItem.adapter = fastAdapter
        binding.listItem.setHorizontalListDecoration()
        fastAdapter.addEventHook(object : ClickEventHook<PopularFoodItem>() {
            override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
                val itemView = viewHolder.itemView
                return itemView.findViewById<MaterialButton>(R.id.add_dish_btn)
            }

            override fun onClick(
                v: View,
                position: Int,
                fastAdapter: FastAdapter<PopularFoodItem>,
                item: PopularFoodItem
            ) {
                val model = item.model
                if (item.model.dishId.isNotEmpty()) {
                    val context = v.context
                    if (context is HomePageActivity) {
                        context.let {
                            it.cartHelper.addOrUpdateItemToCart(
                                it,
                                1,
                                RestaurantItem().apply {
                                    outletId = model.outletId
                                    itemId = model.dishId
                                    price = model.meta?.price ?: 0.0
                                    name = model.title
                                },
                                null,
                                true
                            )
                        }
                    }
                }
            }
        })
        return binding
    }

    override fun onItemSelected(position: Int, item: RestaurantItem) {
    }

    override fun onItemCountChange(
        cartHandler: ICartHandler,
        count: Int,
        item: RestaurantItem,
        quantityView: AddQuantityView,
        isIncremented: Boolean?
    ) {
    }

    override fun onItemFailed(message: String) {
    }

    override fun adapterSizeChange(itemSize: Int) {
    }

    override fun onNoteClicked(item: RestaurantItem) {
    }

    override fun gotoPlate() {
    }
}