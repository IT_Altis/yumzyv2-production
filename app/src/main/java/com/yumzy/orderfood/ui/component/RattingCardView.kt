package com.yumzy.orderfood.ui.component

import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.RequiresApi
import com.google.android.material.textview.MaterialTextView
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.setInterFont
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsaui.drawable.DrawableUtils


/**
 *    Created by Sourav Kumar Pandit
 * */
class RattingCardView : LinearLayout {

    val i3px = VUtil.dpToPx(3)
    val i1px = VUtil.dpToPx(1)
    val i5px = VUtil.dpToPx(5)

    private var icon: String = ""

    private var title: String? = ""

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {

        this.addView(
            LinearLayout(context).apply {
                layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
                setPadding(i3px, i3px, i3px, i3px)
                gravity = Gravity.CENTER
                orientation = HORIZONTAL
            })

    }

    private fun getFontIconView(iconColorInt: Int): FontIconView? {
        return FontIconView(context).apply {
            id = R.id.icon_rating
            text = icon
            setTextColor(iconColorInt)
            setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
        }
    }


    private fun getRatingTextView(textColor: Int): MaterialTextView {
        return MaterialTextView(context).apply {
            id = R.id.tv_ratingtitle
            text = title ?: ""
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                setTextAppearance(R.style.TextAppearance_MyTheme_SemiBold)
            } else {
                setTextAppearance(context, R.style.TextAppearance_MyTheme_SemiBold)
            }
            setTextColor(textColor)
            setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
            setPadding(0, 0, 0, 0)
        }
    }


    fun setRating(
        icon: String,
        rating: String?,
        @ColorInt iconColorInt: Int = ThemeConstant.textBlackColor,
        @ColorInt textColor: Int = ThemeConstant.textBlackColor,
        @ColorInt backgroundColorInt: Int = -1,
        cornerRadius: Float = 8f
    ) {
        if (rating.isNullOrEmpty()) {
            this.visibility = View.GONE
        } else {
            this.visibility = View.VISIBLE
        }
        this.icon = icon
        this.title = rating ?: ""

        (this.getChildAt(0) as LinearLayout).apply {
            background = if (backgroundColorInt != -1) DrawableUtils.getRoundDrawable(
                backgroundColorInt,
                cornerRadius
            ) else null
            this.removeAllViews()
            this.addView(getFontIconView(iconColorInt))
            this.addView(getRatingTextView(textColor))
        }
    }


    fun setRatingColor(
        icon: String,
        rating: String?,
        color: Int,
        @ColorInt backgroundColorInt: Int = -1,
        cornerRadius: Float = 8f
    ) {
        if (rating.isNullOrEmpty()) {
            this.visibility = View.GONE
        } else {
            this.visibility = View.VISIBLE
        }
        this.icon = icon
        this.title = rating ?: ""

        (this.getChildAt(0) as LinearLayout).apply {
            background = if (backgroundColorInt != -1) DrawableUtils.getRoundDrawable(
                backgroundColorInt,
                cornerRadius
            ) else null
            this.removeAllViews()

            this.addView(getFontIconView(color))
            this.addView(getRatingTextView(color))
        }
    }

}