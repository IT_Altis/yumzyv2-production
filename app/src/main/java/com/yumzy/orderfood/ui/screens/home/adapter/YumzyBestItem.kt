package com.yumzy.orderfood.ui.screens.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.ModelAbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.databinding.ViewRecyclerBinding

class YumzyBestItem(homeListDTO: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, ViewRecyclerBinding>(homeListDTO) {
    override val type = R.id.new_launch_item_id

    /* override fun bindView(binding: ViewNewLaunchBinding, payloads: List<Any>) {
         binding.homeNewLaunch.setNewLaunchData(homeListDTO.nestedList!!)
 //        binding.homeTopBanner.withItem(homeListDTO.nestedList!!){item,view->
 //
 //            AlerterHelper.showInfo(binding.homeTopBanner.context,"${model.outletId}")
 //        }

     }*/
    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ViewRecyclerBinding {

        val inflate = ViewRecyclerBinding.inflate(inflater, parent, false)
//        inflate.homeNewLaunch.setNewLaunchData(homeListDTO.nestedList!!)
        return inflate
    }
}
