package com.yumzy.orderfood.ui.component;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.google.android.material.textview.MaterialTextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import com.yumzy.orderfood.R;
import com.yumzy.orderfood.util.viewutils.YumUtil;
import com.laalsa.laalsalib.ui.VUtil;

/**
 * Created by Bhupendra Kumar Sahu.
 */
public class OrderPickupView extends LinearLayout {

    private String address;
    private String title;
    private String subTitle;
    private Drawable icon;


    @RequiresApi(api = Build.VERSION_CODES.M)
    public OrderPickupView(Context context) {
        this(context, null);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public OrderPickupView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public OrderPickupView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (attrs != null)
            initAttrs(context, attrs, defStyleAttr);
        if (title == null) {
            title = "";
        }
        if (subTitle == null) {
            subTitle = "";
        }
        if (icon == null) {

        }
        init();

    }

    private void initAttrs(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.OrderPickupView, defStyleAttr, 0);
        title = a.getString(R.styleable.OrderPickupView_address_title_tag);
        subTitle = a.getString(R.styleable.OrderPickupView_address_subtitle_tag);
        address = a.getString(R.styleable.OrderPickupView_address_tags);
        icon = a.getDrawable(R.styleable.OrderPickupView_address_icon);

        a.recycle();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("SetTextI18n")
    public void init() {
        this.setOrientation(VERTICAL);

        this.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


        int i45px = VUtil.dpToPx(40);
        int i8px = VUtil.dpToPx(2);

        /*Addess icon*/


        ImageView iconView = new ImageView(getContext());
        iconView.setLayoutParams(new LayoutParams(i45px, i45px));
        iconView.setId(R.id.iv_address);
        iconView.setImageDrawable(icon);
        iconView.setPadding(i8px, i8px, i8px, i8px);

        /*Address title*/
        MaterialTextView tvAddressTag = new MaterialTextView(getContext());
        LayoutParams addressTagParam = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        addressTagParam.setMargins(i8px, i8px, i8px, i8px);

        tvAddressTag.setText(address);
        tvAddressTag.setId(R.id.tv_addresstag);

       // YumUtil.INSTANCE.setInterFontBold(getContext(), tvAddressTag);
        tvAddressTag.setTextAppearance(R.style.TextAppearance_MyTheme_SemiBold);

        tvAddressTag.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        tvAddressTag.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f);
        tvAddressTag.setLayoutParams(addressTagParam);


        MaterialTextView tvTitleAddress = new MaterialTextView(getContext());
        LayoutParams addressParam = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tvTitleAddress.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
        tvTitleAddress.setText(title);
        addressParam.setMargins(i8px, i8px, i8px, i8px);
        tvTitleAddress.setTextAppearance(R.style.TextAppearance_MyTheme_SemiBold);
        tvTitleAddress.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f);
        tvTitleAddress.setId(R.id.tv_address_title_tag);
        tvTitleAddress.setMaxLines(3);
        tvTitleAddress.setEllipsize(TextUtils.TruncateAt.END);
        tvTitleAddress.setLayoutParams(addressParam);


        MaterialTextView tvSubTitleAddress = new MaterialTextView(getContext());
        LayoutParams addressParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        addressParams.setMargins(i8px, i8px, i8px, i8px);
        tvSubTitleAddress.setTextColor(ContextCompat.getColor(getContext(), R.color.darkGray));
        tvSubTitleAddress.setText(subTitle);
        tvSubTitleAddress.setMaxLines(3);
        tvSubTitleAddress.setEllipsize(TextUtils.TruncateAt.END);
        tvSubTitleAddress.setTextAppearance(R.style.TextAppearance_MyTheme_Caption);

        tvSubTitleAddress.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f);
        tvSubTitleAddress.setId(R.id.tv_address_subtitle_tag);
        tvSubTitleAddress.setLayoutParams(addressParams);


        this.addView(iconView);
        this.addView(tvAddressTag);
        this.addView(tvTitleAddress);
        this.addView(tvSubTitleAddress);
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        ((MaterialTextView) findViewById(R.id.tv_address_title_tag)).setText(this.title);
    }


    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
        ((MaterialTextView) findViewById(R.id.tv_address_subtitle_tag)).setText(this.subTitle);

    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        ((MaterialTextView) findViewById(R.id.tv_addresstag)).setText(this.address);

    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
        ((ImageView) findViewById(R.id.iv_address)).setImageDrawable(icon);

    }
}
