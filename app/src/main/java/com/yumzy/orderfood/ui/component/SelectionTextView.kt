/*
 * Created By Shriom Tripathi 11 - 5 - 2020
 */

package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.view.View
import com.google.android.material.textview.MaterialTextView
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsaui.drawable.DrawableUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.component.groupview.GroupItemActions
import com.yumzy.orderfood.ui.component.groupview.OnGroupItemClickListener
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper

class SelectionTextView : MaterialTextView, GroupItemActions<SelectionTextView>,
    View.OnClickListener {

    private lateinit var mOnGroupItemClickListener: OnGroupItemClickListener<SelectionTextView>

    private val i4px = VUtil.dpToPx(4)

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {

        this.apply {
            setPadding(i4px, i4px, i4px, i4px)
        }
        this.background = DrawableUtils.getRoundStrokeDrawableListState(
            ThemeConstant.white,
            UIHelper.i8px.toFloat(),
            ThemeConstant.lightAlphaGray,
            UIHelper.i8px.toFloat(),
            ThemeConstant.lightAlphaGray,
            UIHelper.i2px
        )
        this.setTextAppearance(context, R.style.TextAppearance_MyTheme_Caption)
        this.setTextColor(ThemeConstant.textBlackColor)


        this.setOnClickListener(this)
    }

    override var mIsSelected: Boolean = false
        set(value) {
            if (value) {
                setSelectedBackground()
            } else {
                setDeselectedBackground()
            }
            field = value
        }

    override fun onClick(v: View?) {
        mOnGroupItemClickListener.onGroupItemClicked(this)
    }

    override fun setSelectedBackground() {
        this.background = DrawableUtils.getRoundDrawableListState(
            ThemeConstant.pinkies,
            UIHelper.i8px.toFloat(),
            ThemeConstant.transparent,
            UIHelper.i8px.toFloat()
        )
        this.setTextAppearance(context, R.style.TextAppearance_MyTheme_Caption)
        this.setTextColor(ThemeConstant.white)
    }

    override fun setDeselectedBackground() {
        this.background = DrawableUtils.getRoundStrokeDrawableListState(
            ThemeConstant.white,
            UIHelper.i8px.toFloat(),
            ThemeConstant.lightAlphaGray,
            UIHelper.i8px.toFloat(),
            ThemeConstant.lightAlphaGray,
            UIHelper.i2px
        )
        this.setTextAppearance(context, R.style.TextAppearance_MyTheme_Caption)
        this.setTextColor(ThemeConstant.textBlackColor)
    }

    override fun setOnItemClickListener(value: OnGroupItemClickListener<SelectionTextView>) {
        mOnGroupItemClickListener = value
    }

    override fun compareTo(other: GroupItemActions<*>): Int {
        val condition = this.text == (other as SelectionTextView).text
        return if (condition) {
            1
        } else {
            0
        }
    }
}