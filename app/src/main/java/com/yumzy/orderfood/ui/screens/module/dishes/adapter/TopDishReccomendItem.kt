package com.yumzy.orderfood.ui.screens.module.dishes.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.ModelAbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.DishesItem
import com.yumzy.orderfood.databinding.AdapterOutletRecommendItemBinding
import com.yumzy.orderfood.ui.component.AddItemView

class TopDishReccomendItem(val listElements: DishesItem) :
    ModelAbstractBindingItem<DishesItem, AdapterOutletRecommendItemBinding>(listElements),
    AddItemView.AddItemListener {
//    var listElements: DishesItem? = null


    lateinit var callerFun: (String?) -> Unit
    lateinit var clickedListener: (listElement: DishesItem?) -> Unit
    override val type: Int
        get() = R.id.item_recommended_menu


    //    var onRestaurantClick={outletId}
    override fun bindView(binding: AdapterOutletRecommendItemBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)
        binding.outletRecommendItem.setTopDishRecommendedItem(listElements)
        binding.outletRecommendItem.addItemListener = this

    }

    override fun unbindView(binding: AdapterOutletRecommendItemBinding) {
        binding.outletRecommendItem.setTopDishRecommendedItem(null)
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterOutletRecommendItemBinding {
        return AdapterOutletRecommendItemBinding.inflate(inflater, parent, false)
    }

    override fun onItemAdded(itemId: String, count: Int, priced: String, outletId: String) {
        clickedListener(model)
    }


}