/*
 * Created By Shriom Tripathi 10 - 5 - 2020
 */

package com.yumzy.orderfood.ui.component.groupview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.annotation.IntRange
import androidx.constraintlayout.helper.widget.Flow
import androidx.constraintlayout.widget.ConstraintLayout
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsaui.utils.dpf
import com.yumzy.orderfood.R

class FlowGroupView : ConstraintLayout, OnGroupItemClickListener<Any> {

    private val i8px = VUtil.dpToPx(8)
    private val i6px = 6.dpf
    private val i4px = 4.dpf
    private val i2px = VUtil.dpToPx(2)

    private val mFlow = Flow(context).apply {
        layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
    }

    private var mViewIds = 7500

    private var mIsMultiSelectEnabled = false

    private var mMinimumSelection = 0
        set(value) {
            if (value < 0) {
                throw IllegalArgumentException(" minimumSelected must be in range 0..len(itemsList) of GroupView ")
            } else {
                field = value
            }
        }

    private var mMaximumSelection = 100
        set(value) {
            if (value < 1) {
                throw IllegalArgumentException("Maximus Selection must not be less than 1")
            } else if (value < mMinimumSelection) {
                throw IllegalArgumentException("Maximus Selection must be greater than minimum selection which is $mMinimumSelection")
            } else {
                field = value
            }
        }

    private var mWrapMode: Int = 2
        set(value) {
            mFlow.setWrapMode(value)
            field = value
        }

    private var mVerticalGap = i4px
    private var mHorzontalGap = i6px
    private var mHorizontalSpreadStyle = 0
        set(value) {
            mFlow.setHorizontalStyle(value)
            field = value
        }

    private var mSelectedItems = mutableListOf<GroupItemActions<*>>()
    private var mItemIds = mutableListOf<Int>()

    private var mOnGroupItemClickListener: OnGroupItemClickListener<*>? = null
    private var mOnSelectedItemChangeListener: OnSelectedItemChangeListener? = null
    private var mCachedGroupItem: GroupItemActions<*>? = null
    private var lastSelected: GroupItemActions<*>? = null

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, atr: AttributeSet?) : this(context, atr, 0)
    constructor(context: Context, atr: AttributeSet?, style: Int) : super(context, atr, style) {

        if (atr != null) {
            val a = context.obtainStyledAttributes(atr, R.styleable.FlowGroupView, style, 0)

            mMinimumSelection = a.getInt(R.styleable.FlowGroupView_item_minimumSelection, 0)
            mIsMultiSelectEnabled =
                a.getBoolean(R.styleable.FlowGroupView_item_multiSelectable, false)
            mWrapMode = a.getInt(R.styleable.FlowGroupView_item_wrapMode, 2)
            mVerticalGap =
                a.getDimension(R.styleable.FlowGroupView_item_verticalGap, 6.dpf)
            mHorzontalGap =
                a.getDimension(R.styleable.FlowGroupView_item_horizontalGap, 6.dpf)
            mHorizontalSpreadStyle = a.getInt(R.styleable.FlowGroupView_item_horizontalStyle, 2)
            a.recycle()
        }

        intiView()

    }

    private fun intiView() {
        this.run {
            layoutParams = LayoutParams(
                LayoutParams(
                    LayoutParams.MATCH_PARENT,
                    LayoutParams.WRAP_CONTENT
                )
            )
        }
        mOnGroupItemClickListener = this

        mFlow.setWrapMode(mWrapMode)
        mFlow.setVerticalGap(mVerticalGap.toInt())
        mFlow.setHorizontalGap(mHorzontalGap.toInt())
        mFlow.setHorizontalStyle(mHorizontalSpreadStyle)
        this.addView(mFlow)
    }

    private fun removeFromSelected(view: GroupItemActions<*>) {

        var existsIndex = -1
        for ((index, item) in mSelectedItems.withIndex()) {
            if (view.compareTo(item) == 1) {
                existsIndex = index
                break
            }
        }

        if (existsIndex != -1) {
            mSelectedItems.removeAt(existsIndex)
            if (mIsMultiSelectEnabled) mOnSelectedItemChangeListener?.onSelectedItemChanged(
                mSelectedItems,
                this.id
            )
        }

    }

    private fun addToSelected(view: GroupItemActions<*>) {

        var isExists = false
        for (item in mSelectedItems) {
            if (view.compareTo(item) == 1) {
                isExists = true
                break
            }
        }

        if (!isExists) {
            mSelectedItems.add(view)
            mOnSelectedItemChangeListener?.onSelectedItemChanged(mSelectedItems, this.id)
        }

    }

    fun setProperties(
        isMultiSelectEnabled: Boolean = false,
        minimumSelection: Int = 0,
        wrapMode: Int = mWrapMode,
        horizontalSpreadStyle: Int = mHorizontalSpreadStyle,
        maximumSelection: Int = 100
    ) {

        mIsMultiSelectEnabled = isMultiSelectEnabled
        mMinimumSelection = minimumSelection
        mWrapMode = wrapMode
        mHorizontalSpreadStyle = horizontalSpreadStyle
        mMaximumSelection = maximumSelection
    }

    fun <T, V : View> setArrayItems(
        list: List<T>,
        onBindData: (parent: OnGroupItemClickListener<V>, value: T) -> V
    ) {

        list.forEach { item ->
            val view = onBindData(mOnGroupItemClickListener as OnGroupItemClickListener<V>, item)
            view.id = mViewIds
            mItemIds.add(mViewIds++)
            this.addView(view)
        }
        mFlow.referencedIds = mItemIds.toIntArray()
    }

    fun <T, V : View> setItems(
        list: List<T>,
        onBindData: (parent: OnGroupItemClickListener<V>, value: T) -> V
    ) {
        mSelectedItems.clear()
        for (i in this.childCount - 1 downTo 0) {
            val view = this.getChildAt(i)
            if (view !is Flow) this.removeView(view)
        }
        list.forEach { item ->
            val view = onBindData(mOnGroupItemClickListener as OnGroupItemClickListener<V>, item)
            view.id = mViewIds
            mItemIds.add(mViewIds++)
            this.addView(view)
        }
        mFlow.referencedIds = mItemIds.toIntArray()
    }

    fun <T, V : View> clearAndAddItem(
        list: List<T>,
        onBindData: (parent: OnGroupItemClickListener<V>, value: T) -> V
    ) {
        this.removeAllViews()
        list.forEach { item ->
            val view = onBindData(mOnGroupItemClickListener as OnGroupItemClickListener<V>, item)
            view.id = mViewIds
            mItemIds.add(mViewIds++)
            this.addView(view)
        }
        mFlow.referencedIds = mItemIds.toIntArray()
    }

    fun setOnSelectedItemChangeListener(
        onSelectedItemChangeListener: OnSelectedItemChangeListener
    ) {
        mOnSelectedItemChangeListener = onSelectedItemChangeListener
    }

    fun setItemSelectedAt(
        @IntRange(from = 0) vararg indices: Int
    ) {
        for (index in indices) {

            val itemPosition = index + 1

            //seeting as clicked
            val findViewById = this.findViewById<View>(mItemIds[index])
            if (findViewById != null)
                onGroupItemClicked((findViewById as GroupItemActions<*>))

        }
    }

    override fun onGroupItemClicked(view: GroupItemActions<*>) {
        synchronized(this) {
            if (mIsMultiSelectEnabled == false) {

                //Multiselect is disabled

                if (mCachedGroupItem != null) {
                    mCachedGroupItem?.setStateDeselected()
                    removeFromSelected(mCachedGroupItem!!)
                }

                mCachedGroupItem = view
                mCachedGroupItem?.setStateSelected()

                addToSelected(this.mCachedGroupItem!!)

            } else {

                if (view.getState() == false) {
                    if (mSelectedItems.size < mMaximumSelection) {
                        view.setStateSelected()
                        lastSelected = view
                        addToSelected(view)
                    } else {
                        if (view === lastSelected) {
                            view.setStateDeselected()
                            removeFromSelected(view)
                            return
                        } else {
                            lastSelected?.let {
                                it.setStateDeselected()
                                removeFromSelected(it)
                            }
                        }
                        view.setStateSelected()
                        addToSelected(view)
                        lastSelected = view
                        mOnSelectedItemChangeListener?.onShowMessage("Please select at most $mMaximumSelection")
                    }
                } else {
                    if (mSelectedItems.size > mMinimumSelection) {
                        view.setStateDeselected()
                        removeFromSelected(view)
                    } else {
/*
                        if (view === lastSelected) {
                            view.setStateDeselected()
                            removeFromSelected(view)
                            return
                        }*//* else {

                            lastSelected?.let {
                                it.setStateDeselected()
                                removeFromSelected(it)
                            }
                        }*//*
                        view.setStateSelected()
                        addToSelected(view)
                        lastSelected = view*/
                        mOnSelectedItemChangeListener?.onShowMessage("Please select at least 3 cuisines!")
                    }
                }

            }
        }
    }


    interface OnSelectedItemChangeListener {
        fun onSelectedItemChanged(list: MutableList<GroupItemActions<*>>, listType: Int)
        fun onShowMessage(message: String)
    }

}