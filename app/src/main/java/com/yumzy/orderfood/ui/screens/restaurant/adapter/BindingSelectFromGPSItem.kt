package com.yumzy.orderfood.ui.screens.restaurant.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.databinding.AdapterSelectFromMapBinding

class BindingSelectFromGPSItem : AbstractBindingItem<AdapterSelectFromMapBinding>() {

    override val type: Int
        get() = R.id.item_select_from_map

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): AdapterSelectFromMapBinding {
        return AdapterSelectFromMapBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: AdapterSelectFromMapBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)
    }

}