package com.yumzy.orderfood.ui.screens.module.review

import com.yumzy.orderfood.ui.base.IView

/**
 * Created by Bhupendra Kumar Sahu.
 */
interface IRateOrder : IView {
    fun skip()
}