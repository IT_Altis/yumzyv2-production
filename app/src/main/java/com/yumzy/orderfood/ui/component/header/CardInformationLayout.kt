package com.yumzy.orderfood.ui.component.header

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.LayoutCardInformationBinding
import com.yumzy.orderfood.ui.helper.hideEmptyTextView

class CardInformationLayout : FrameLayout {
     var cardSubtitle: String? = null
         set(value) {
             field = value
             binding.cardSubtitle.hideEmptyTextView(field)
             if (!cardTitle.isNullOrEmpty() || !cardSubtitle.isNullOrEmpty()) {
                 this.visibility = View.VISIBLE
             } else
                 this.visibility = View.GONE

         }
    var cardTitle: String? = null
        set(value) {
            field = value
            binding.cardTitle.hideEmptyTextView(field)
            if (!cardTitle.isNullOrEmpty() || !cardSubtitle.isNullOrEmpty()) {
                this.visibility = View.VISIBLE
            } else
                this.visibility = View.GONE
        }
    private val binding by lazy {
        val layoutInflater = LayoutInflater.from(context)
        LayoutCardInformationBinding.inflate(layoutInflater, this, true)
    }

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {

        if (attrs != null) {
            val a =
                context.obtainStyledAttributes(
                    attrs,
                    R.styleable.CardInformationLayout,
                    defStyleAttr,
                    0
                )
            this.cardTitle = a.getString(R.styleable.CardInformationLayout_cardInfoTitle)
            this.cardSubtitle = a.getString(R.styleable.CardInformationLayout_cardInfoSubtitle)
//            if (cardTitle.isNullOrEmpty() && cardSubtitle.isNullOrEmpty()) {
//                this.visibility = View.GONE
//            }

            a.recycle()

        }


    }


}