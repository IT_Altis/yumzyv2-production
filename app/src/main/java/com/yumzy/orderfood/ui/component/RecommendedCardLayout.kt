package com.yumzy.orderfood.ui.component

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.Drawable
import android.os.Handler
import android.text.TextUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsaui.drawable.DrawableUtils
import com.laalsa.laalsaui.utils.px
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.common.imageview.RoundedImageView
import com.yumzy.orderfood.ui.helper.*
import com.yumzy.orderfood.ui.helper.UIHelper.i2px
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.YUtils
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView

/**
 * Created by Bhupendra Kumar Sahu on 06-Jul-20.
 */
class RecommendedCardLayout : LinearLayout, AddQuantityView.OnCounterChange {


    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {


        initAttrs(attrs, context, defStyleAttr)
        initView()


    }


    private var addHandler: Handler? = null

    private var update: Runnable? = null

    var itemId: String = ""
    var outletId: String = ""

    var itemEnable: Boolean = true
        set(value) {
            field = value
            val quantityButton = findViewById<AddQuantityView>(R.id.tv_add_quantity)
            val customizeButton = findViewById<TextView>(R.id.tv_customize)
            if (itemEnable) {
//                quantityButton?.buttonColor = 0xFF58D896.toInt()
                quantityButton?.setOnCounterChange(this)
                customizeButton?.visibility = View.VISIBLE
                this.alpha = 1f
            } else {
//                quantityButton?.buttonColor = 0xFFCECECE.toInt()
                quantityButton?.setOnCounterChange(null)
                customizeButton?.visibility = View.GONE
                this.alpha = 0.8f

            }


        }


    var imageRatio: String = "5:4"
        set(value) {
            field = value
            val imageView = this.findViewById<ImageView>(R.id.restaurant_image)

            val imageParam =
                ConstraintLayout.LayoutParams(ViewConst.MATCH_PARENT, 0)
            imageParam.dimensionRatio = this.imageRatio

            imageParam.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
            imageParam.startToStart = ConstraintLayout.LayoutParams.PARENT_ID
            imageParam.topToTop = ConstraintLayout.LayoutParams.PARENT_ID
            imageView.layoutParams = imageParam
        }
    lateinit var discountDrawable: Drawable

    var availableRestaurant: Boolean = true
        set(value) {
            field = value
            if (value)
                this.alpha = 0.5f
            else
                this.alpha = 1f

        }
    var icon: String = ""
        set(value) {
            this.findViewById<TextView>(R.id.font_icon_view)?.text = value
            field = value
        }
    var itemTitle: String = ""
        set(value) {
            this.findViewById<TextView>(R.id.tv_item_name)?.text = value
            field = value

        }
    var displayType: String = ""
        set(value) {
            this.findViewById<TextView>(R.id.tv_item_combo)?.text = value
            field = value

        }
    var itemSubtitle: String = ""
        set(value) {
            this.findViewById<TextView>(R.id.tv_item_tag)?.text = value
            field = value
        }

    var colorText: String = ""
        set(value) {
            field = value

            val view = this.findViewById<TextView>(R.id.tv_coupon_code)
            if (view != null)
                if (value.isBlank()) {
                    view.visibility = View.GONE
                } else {
                    view.visibility = View.VISIBLE

                    view.text = value
                }

        }

/*    var description: String = ""
        set(value) {
            field = value

            val view = this.findViewById<TextView>(R.id.description)
            if (view != null)
                if (value.isBlank()) {
                    view.visibility = View.GONE
                } else {
                    view.visibility = View.VISIBLE

                    view.text = value
                }

        }*/
/*    var costForTwo: String = ""
        set(value) {
            field = value

            val view = this.findViewById<TextView>(R.id.tv_cost_for_two)
            if (view != null)
                if (value.isBlank()) {
                    view.visibility = View.GONE
                } else {
                    view.visibility = View.VISIBLE

                    view.text = value
                }

        }*/

    var imageUrl: String = ""
        set(value) {
            val imageView = this.findViewById<ImageView>(R.id.restaurant_image)
//            Glide.with(imageView).load(value).into(imageView)
            YUtils.setImageInView(imageView, value)

            field = value
        }

    var isVeg: Boolean = true
        set(value) {
            field = value
            val view = this.findViewWithTag<FontIconView>("icon_veg")
            if (value) {
                view.setTextColor(ThemeConstant.greenConfirmColor)
            } else {
                view.setTextColor(ThemeConstant.nonVegColor)
            }
        }
/*    var removeRating: Boolean = false
        set(value) {
            field = value
            val view = this.findViewById<AddQuantityView>(R.id.add_quantity_view)
            if (view != null && removeAddQuantity) {
                this.removeView(view)
            }

        }

    fun removeAddQuantity() {
        val view = this.findViewById<AddQuantityView>(R.id.add_quantity_view)
        val layout = this.findViewById<LinearLayout>(R.id.add_quantity_layot)
        if (view != null && removeAddQuantity) {
            layout.removeView(view)
        }

    }*/

    var removeAddQuantity: Boolean = false
        set(value) {
            field = value
            val view = this.findViewById<AddQuantityView>(R.id.add_quantity_view)
            val layout = this.findViewById<LinearLayout>(R.id.add_quantity_layot)
            if (view != null && removeAddQuantity) {
                layout.removeView(view)
            }

        }

    var removeVegIcon: Boolean = false
        set(value) {
            field = value
            val view = this.findViewWithTag<FontIconView>("icon_veg")
            if (value) {
                view.visibility = View.GONE
            } else {
                view.visibility = View.VISIBLE
            }
        }

    var ribbonText: String? = null
        set(value) {
            field = value
            val findViewById = this.findViewById<RibbonView>(R.id.ribbon_view)
            findViewById?.ribbonText = ribbonText

        }


    var price: String = ""
        set(value) {
            this.findViewById<TextView>(R.id.tv_item_price)?.text = value
            field = value
        }

    var showCustomize: Boolean = false
        set(value) {
            field = value
            if (field)
                this.findViewById<TextView>(R.id.tv_customize)?.visibility = View.VISIBLE
            else
                this.findViewById<TextView>(R.id.tv_customize)?.visibility = View.GONE
        }


    var ignoreCounterChange: Boolean = false
        set(value) {
            field = value
            this.findViewById<AddQuantityView>(R.id.tv_add_quantity).ignoreClickCount = field
        }

    private fun initAttrs(
        attrs: AttributeSet?,
        context: Context?,
        defStyleAttr: Int
    ) {
        if (attrs != null) {
            val a =
                context!!.obtainStyledAttributes(
                    attrs,
                    R.styleable.RecommendedCardLayout,
                    defStyleAttr,
                    0
                )

            itemTitle = a.getString(R.styleable.RecommendedCardLayout_RecommendedCardTitle) ?: ""
            displayType =
                a.getString(R.styleable.RecommendedCardLayout_RecommendedCardDisplayType) ?: ""
            itemSubtitle =
                a.getString(R.styleable.RecommendedCardLayout_RecommendedCardSubtitle) ?: ""
            colorText =
                a.getString(R.styleable.RecommendedCardLayout_RecommendedCardCouponCode) ?: ""
            price = a.getString(R.styleable.RecommendedCardLayout_RecommendedCardPrice) ?: ""
            ribbonText = a.getString(R.styleable.RecommendedCardLayout_RecommendedRibbonText) ?: ""
//            discountDrawable =
//                YumUtil.getIcon(context, R.drawable.ic_discount, ThemeConstant.pinkies)!!

            a.recycle()

        }
//        else {
//        }
    }

    private fun initView() {

        this.layoutParams =

            ViewGroup.LayoutParams(ViewConst.WRAP_CONTENT, ViewConst.WRAP_CONTENT)
        this.background =
            DrawableUtils.getRoundDrawable(ThemeConstant.lightSkyBlue, VUtil.dpToPx(5).toFloat())

//        this.setPadding(UIHelper.i3px, UIHelper.i3px, UIHelper.i3px, UIHelper.i3px)
        val roundImage = restaurantImageView()
        val ribbonView = getRibbonView()
        this.clipChildren = false
        val itemNameLayout = LinearLayout(context).apply {
            id = R.id.card_top_layout
            orientation = LinearLayout.VERTICAL
            layoutParams =
                ConstraintLayout.LayoutParams(0, ViewConst.WRAP_CONTENT)
                    .apply {
                        startToStart = R.id.restaurant_image
                        topToBottom = R.id.restaurant_image
                        endToEnd = R.id.restaurant_image
                    }
        }

        itemNameLayout.addView(getTitleWithIcon(displayType, 12f, R.id.tv_item_combo))
//        itemNameLayout.addView(getTextView(itemTitle, 16f, R.id.tv_item_name))
        itemNameLayout.addView(getTextView(itemSubtitle, 10f, R.id.tv_item_tag))
//        itemNameLayout.addView(getTextView(description, 10f, R.id.tv_descriptions))
        itemNameLayout.addView(getTextView(colorText, 14f, R.id.tv_coupon_code))
//        itemNameLayout.addView(getTextView(costForTwo, 10f, R.id.tv_cost_for_two))

        val priceQuantityView: LinearLayout = getPriceQuantityView(price, 12f, R.id.tv_item_price)
        val bottomParam =
            ConstraintLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        priceQuantityView.setPadding(5.px, 0, 0, 0)
        bottomParam.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
        bottomParam.topToBottom = R.id.card_top_layout
        priceQuantityView.layoutParams = bottomParam

        val constraintLayout = ConstraintLayout(context)
        constraintLayout.layoutParams =
            LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)

        constraintLayout.addView(roundImage)
        constraintLayout.addView(getRattingView(context))
        constraintLayout.addView(itemNameLayout)
        constraintLayout.addView(priceQuantityView)
        constraintLayout.addView(ribbonView)
        this.addView(constraintLayout)

    }

    private fun getRibbonView(): RibbonView {
        val ribbonView = RibbonView(context)
        val ribbonParam =
            ConstraintLayout.LayoutParams(ViewConst.WRAP_CONTENT, ViewConst.WRAP_CONTENT)
        ribbonView.layoutParams = ribbonParam
        ribbonParam.setMargins(0, VUtil.dpToPx(5), 0, 0)
//        ribbonView.x = -UIHelper.i10px.toFloat()
        ribbonView.ribbonTextSize(12f)
//        ribbonView.y = (UIHelper.i5px).toFloat()
//        ribbonView.x = (UIHelper.i5px).toFloat()
        ribbonView.id = R.id.ribbon_view
        ribbonView.ribbonText = ribbonText
        return ribbonView
    }

    private fun getTitleWithIcon(itemTag: String, fl: Float, tvItemTag: Int): View? {

        val linearLayout = LinearLayout(context)

        val layoutParams =
            LayoutParams(LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        linearLayout.layoutParams = layoutParams
        linearLayout.setPadding(0, VUtil.dpToPx(5), 0, 0)
        linearLayout.gravity = Gravity.START
        val fontIconView = FontIconView(context)
        fontIconView.tag = "icon_veg"
        fontIconView.layoutParams =
            LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)

        fontIconView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
        fontIconView.setPadding(0, 0, VUtil.dpToPx(5), 0)
        fontIconView.setTextColor(ThemeConstant.greenConfirmColor)
        fontIconView.gravity = Gravity.BOTTOM
        fontIconView.text = context.resources.getText(R.string.icon_veg_non_veg)

        linearLayout.addView(fontIconView)
        linearLayout.addView(getTextView(itemTitle, 14f, R.id.tv_item_name))
        return linearLayout
    }

    private fun getQuantityButton(): View {
        val layout = LinearLayout(context)
        layout.id = R.id.item_available_frame
        layout.orientation = VERTICAL
        layout.layoutParams.apply {
            layoutParams = LayoutParams(ViewConst.WRAP_CONTENT, ViewConst.WRAP_CONTENT)

        }
        val addQuantityView = addQuantityView()
        layout.addView(addQuantityView)
        layout.addView(addProgressIndicator())
        layout.addView(getCustomizeText())
        return layout
    }

    fun getProgressBar(): ProgressBar? {
        return findViewById(R.id.pb_item_processeing)
    }

    fun showItemAdding(showProgress: Boolean) {
//        if (showProgress == this.clickDisabled) return
        val progressBar = getProgressBar()
        if (progressBar != null)
            if (showProgress) {
                progressBar.isIndeterminate = true
                progressBar.visibility = View.VISIBLE
//                this.isEnabled = false
                /*          getQuantityView()?.deepForEach {
          //                    this.isEnabled = false
                              this.alpha = 0.5f

                          }*/

            } else {
                progressBar.isIndeterminate = false
                progressBar.visibility = View.INVISIBLE
//                this.isEnabled = true
/*                getQuantityView()?.deepForEach {
//                    this.isEnabled = true
                    this.alpha = 1.0f

                }*/
            }
//        clickDisabled = showProgress

    }

    fun getQuantityView(): AddQuantityView? {
        return this.findViewById(R.id.tv_add_quantity)
    }

    private fun addQuantityView(): AddQuantityView {
        val addQuantityView = AddQuantityView(context)
        val param = LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT
        )
        param.setMargins(0, UIHelper.i2px, UIHelper.i1px,  0)
        addQuantityView.layoutParams = param
        addQuantityView.id = R.id.tv_add_quantity
        if (itemEnable)
            addQuantityView.setOnCounterChange(this)
        return addQuantityView
    }


    private fun addProgressIndicator(): ProgressBar {
        val progressBar = ProgressBar(context, null, android.R.attr.progressBarStyleHorizontal)
            .apply {

                val colorFilter = PorterDuffColorFilter(
                    ContextCompat.getColor(context, R.color.blueJeans),
                    PorterDuff.Mode.MULTIPLY
                )

                indeterminateDrawable.colorFilter = colorFilter
            }
        progressBar.id = R.id.pb_item_processeing
        progressBar.minimumHeight = 0
        progressBar.minimumWidth = 0

//    progressBar.drawableState=
//    progressBar.indeterminateDrawable=context.getDrawable(R.drawable.progress_rounded)
        progressBar.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, VUtil.dpToPx(8)).apply {
//        setMargins(0,UIHelper.i2px,0,0)
            gravity = Gravity.CENTER
        }
        progressBar.isIndeterminate = false
        progressBar.visibility = View.INVISIBLE
        return progressBar
    }

    private fun getCustomizeText(): TextView {
        val customizeText = TextView(context)
        customizeText.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        customizeText.text = context.resources.getString(R.string.customization)
        customizeText.id = R.id.tv_customize
        customizeText.setPadding(0,0,0,3.px)
        customizeText.gravity = Gravity.CENTER
        customizeText.setInterFont()
        customizeText.setTextColor(ThemeConstant.textLightGrayColor)
        customizeText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10f)
        return customizeText
    }


    private fun getPriceQuantityView(itemTag: String, fl: Float, tvItemTag: Int): LinearLayout {
        val linearLayout = LinearLayout(context)
        linearLayout.gravity = Gravity.TOP
        linearLayout.id = R.id.add_quantity_layot
        val lp =
            LayoutParams(ViewConst.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        linearLayout.layoutParams = lp

        linearLayout.addView(getHorizontalLayout())
        linearLayout.addView(View(context).apply {
            layoutParams = LayoutParams(0, ViewConst.WRAP_CONTENT, 1f)
        })
        linearLayout.addView(getQuantityButton())

        return linearLayout
    }

    private fun getRattingView(context: Context): View {

        val rattingView = RatingView(context)
        rattingView.id = R.id.rating_card
        val rattingParam = ConstraintLayout.LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT
        )

        rattingParam.marginStart = VUtil.dpToPx(10)
        rattingParam.bottomMargin = VUtil.dpToPx(10)
        rattingParam.bottomToBottom = R.id.restaurant_image
        rattingParam.startToStart = ConstraintLayout.LayoutParams.PARENT_ID
        rattingView.layoutParams = rattingParam

        return rattingView
    }

    private fun restaurantImageView(): ImageView {
        val imageParam =
            ConstraintLayout.LayoutParams(ViewConst.MATCH_PARENT, 0)
        imageParam.dimensionRatio = this.imageRatio

        imageParam.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
        imageParam.startToStart = ConstraintLayout.LayoutParams.PARENT_ID
        imageParam.topToTop = ConstraintLayout.LayoutParams.PARENT_ID

        val roundImage = RoundedImageView(context)
        roundImage.layoutParams = imageParam
        roundImage.id = R.id.restaurant_image
        roundImage.cornerRadius = VUtil.dpToPx(10).toFloat()
        roundImage.setImageResource(R.drawable.ic_default_img)
        roundImage.scaleType = ImageView.ScaleType.CENTER_CROP
        return roundImage
    }

    private fun getTextView(text: CharSequence, textSize: Float, tvId: Int): View {
        val textView = TextView(context)
        textView.text = text
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
        textView.id = tvId
        when (tvId) {
            R.id.tv_item_combo -> {
                textView.setInterFont()

                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13f)
                textView.gravity = Gravity.TOP

                textView.ellipsize = TextUtils.TruncateAt.END
                textView.setTextColor(ThemeConstant.textGrayColor)
            }
            R.id.tv_item_name -> {
                textView.setInterBoldFont()
                textView.setLineSpacing(0f, .8f)
                textView.minLines = 2
                textView.maxLines = 2
                textView.ellipsize = TextUtils.TruncateAt.END
                textView.setTextColor(ThemeConstant.textBlackColor)
            }
            R.id.tv_item_tag -> {
                textView.setTextColor(ThemeConstant.textBlackColor)
                textView.setInterFont()
                textView.setPadding(0, VUtil.dpToPx(3), 0, 0)
                textView.maxLines = 2
                textView.ellipsize = TextUtils.TruncateAt.END

            }
            R.id.tv_coupon_code -> {
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
                textView.setTextColor(ThemeConstant.pinkies)
            }
            R.id.tv_item_price -> {
                textView.setTextColor(ThemeConstant.textBlackColor)
                textView.setInterBoldFont()
                textView.gravity = Gravity.START

            }
            R.id.tv_cost_for_two -> {
                textView.setTextColor(ThemeConstant.textGrayColor)
                textView.setInterRegularFont()
                textView.gravity = Gravity.START

            }
            //        textView.setOnClickListener(this)
        }
//        textView.setOnClickListener(this)
        return textView

    }

    fun getAddQuantityView(): AddQuantityView? {
        return this.findViewById(R.id.add_quantity_view)
    }

    fun setRating(ratting: String) {
        val rattingView = findViewById<RatingView>(R.id.rating_card)
        rattingView?.setRating(ratting)
    }

    fun setQuantitiyListener(countCaller: (Int) -> Unit) {
        this.findViewById<AddQuantityView>(R.id.add_quantity_view)
            .setOnCounterChange(object : AddQuantityView.OnCounterChange {
                override fun onCounterChange(count: Int) {
                    countCaller(count)
                }
            })
    }

    fun updateImageParams(width: Int, height: Int, ratio: String) {
        this.findViewById<RoundedImageView>(R.id.restaurant_image).apply {
            layoutParams = ConstraintLayout.LayoutParams(width, height).apply {
                this.dimensionRatio = ratio
            }

        }
    }

    var addItenListener: AddItemView.AddItemListener? = null


    private var priced: CharSequence = ""
        set(value) {
            field = value
//            field = "₹ $value"
            this.findViewById<TextView>(R.id.tv_item_price)?.text = field
        }

    private var offerpriced: String = ""
        set(value) {
            field = value
//            field = "₹ $value"
            this.findViewById<TextView>(R.id.tv_item_offer_price)?.text = field
        }

    fun getStrikeView(): TextView {
        return findViewById(R.id.tv_item_price)
    }

    fun setItemSpannedPrice(priced: CharSequence?) {
        this.priced = priced!!
        this.findViewById<TextView>(R.id.tv_item_price)
            ?.setText(priced, TextView.BufferType.SPANNABLE)
    }

    fun setItemPrice(priced: String) {
        this.priced = priced
        this.findViewById<TextView>(R.id.tv_item_price)?.text = "₹${this.priced}"
    }

    fun setItemOfferPrice(priced: String) {
        this.offerpriced = priced
        this.findViewById<TextView>(R.id.tv_item_offer_price)?.text = "₹${this.offerpriced}"
    }

    private fun getHorizontalLayout(): View? {
        val linearLayout = LinearLayout(context)
        linearLayout.orientation = HORIZONTAL
        linearLayout.gravity = Gravity.START
        linearLayout.addView(getTextView(priced, 12f, R.id.tv_item_price))
        linearLayout.addView(getTextView(offerpriced, 12f, R.id.tv_item_offer_price))
        return linearLayout
    }

    interface AddItemListener {
        fun onItemAdded(itemId: String, count: Int, priced: String, outletId: String)
    }

    override fun onCounterChange(count: Int) {
        addItenListener?.onItemAdded(itemId, count, price, outletId)
    }


    /*override fun onClick(view: View) {
        when (view.id) {
            R.id.iv_rounded_image -> this.restaurantCardListener?.onRestaurantImageClick()
            R.id.tv_title -> this.restaurantCardListener?.onRestaurantImageClick()
            R.id.tv_subtitle -> this.restaurantCardListener?.onRestaurantImageClick()
            else -> this.restaurantCardListener?.onRestaurantClick()
        }
    }
*/

}