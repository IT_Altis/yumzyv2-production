package com.yumzy.orderfood.ui.screens.module.restaurant

/*
class RestaurnatSearchAdapter(
    val context: Context,
    var menuList: List<RestaurantItem>,
    val callerFun: (cartHandler: ICartHandler, Int, RestaurantItem, AddQuantityView) -> Unit,
    val countFun: (Int) -> Unit
) : RecyclerView.Adapter<RestaurnatSearchAdapter.RestaurantItemHolder>() {

    private var items: MutableList<RestaurantItem> = menuList.toMutableList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantItemHolder {
        val layoutInflater = LayoutInflater.from(context)
        val itemBinding: AdapterRestaurentMenuBinding =
            AdapterRestaurentMenuBinding.inflate(layoutInflater, parent, false)
        return RestaurantItemHolder(itemBinding)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: RestaurantItemHolder, position: Int) =
        holder.bindData(items[position])

    @SuppressLint("CheckResult")
    fun updateList(query: String) {
        //using rx to filter the list and update the list by diffing.
        Observable
            .fromCallable {
                return@fromCallable menuList.filter { item ->
                    item.name.contains(query, ignoreCase = true)
                            || item.description.contains(query, ignoreCase = true)
                            || when (query.trim()) {
                        "veg", "vegetarian", "Veg", "Vegetarian" -> item.isVeg
                        "nonveg", "non veg", "Nonveg", "Non Veg", "Non veg", "non" -> !item.isVeg
                        "egg" -> item.containsEgg
                        else -> false
                    }
                }
            }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { list ->
                val diffCallback = MenuItemDiff(menuList, items)
                val diffResult = DiffUtil.calculateDiff(diffCallback)
                items.clear()
                items.addAll(list)
                countFun(items.size)
                diffResult.dispatchUpdatesTo(this)
            }
    }

    fun resetList() {
        val diffCallback = MenuItemDiff(items, menuList)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        items.clear()
        items.addAll(menuList)
        diffResult.dispatchUpdatesTo(this)
    }

    inner class RestaurantItemHolder(itemBinding: AdapterRestaurentMenuBinding) :
        YItemViewHolder(itemBinding.root) {

        var adapterBinding: AdapterRestaurentMenuBinding = itemBinding

        fun bindData(item: RestaurantItem?) {

            adapterBinding.addItemView.setItemPrice(item?.price.toString())
            item?.name?.let { adapterBinding.addItemView.setTitles(it.capitalize()) }
            item?.description?.let { adapterBinding.addItemView.setSubtitle(it) }
            adapterBinding.addItemView.itemId = item?.itemId.toString()
            adapterBinding.addItemView.setImageShow(false)
            adapterBinding.addItemView.isVeg = item?.isVeg ?: false
            adapterBinding.addItemView.ribbonText = item?.ribbon
            adapterBinding.addItemView.showCustomize = !(item?.addons?.isEmpty() ?: true)
            if (item?.prevQuantity!! > 0) {
                adapterBinding.addItemView.getQuantityView()?.quantityCount =
                    item.prevQuantity
            } else {
                adapterBinding.addItemView.getQuantityView()?.quantityCount = 0
            }
            adapterBinding.addItemView.addItenListener = object : AddItemView.AddItemListener,
                @kotlin.ParameterName(name = "cartHandler") ICartHandler {
                override fun onItemAdded(
                    itemId: String,
                    count: Int,
                    priced: String,
                    outletId: String
                ) {
                    callerFun(this, count, item, adapterBinding.addItemView.getQuantityView()!!)
//                    if (value){
                    */
/*    items[adapterPosition].prevQuantity = count*//*

//                    }
                }

                override fun itemAdded(count: Int) {
                    if (adapterPosition >= 0)
                        items[adapterPosition].prevQuantity = count
                }

                override fun itemRepeated() {
                }

                override fun failedToAdd(
                    count: Int,
                    view: AddQuantityView
                ) {
                    adapterBinding.addItemView.setQuantity(items[adapterPosition].prevQuantity)

                }

            }
        }
    }

}
*/
