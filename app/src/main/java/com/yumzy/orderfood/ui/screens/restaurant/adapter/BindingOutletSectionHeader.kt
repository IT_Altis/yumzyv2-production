package com.yumzy.orderfood.ui.screens.restaurant.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.AdapterOutletSectionHeaderBinding

class BindingOutletSectionHeader :
    AbstractBindingItem<AdapterOutletSectionHeaderBinding>() {

    var mTitle: String? = null
    var hasVeg = false
    var hasEgg = false
    var hasBoth = false
    var itemCount = 0

    override val type: Int
        get() = R.id.item_section_header

    fun withSection(title: String, sectionItemCount: Int): BindingOutletSectionHeader {
        this.mTitle = title
        this.itemCount = sectionItemCount
        return this
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterOutletSectionHeaderBinding {
        return AdapterOutletSectionHeaderBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: AdapterOutletSectionHeaderBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)

        binding.title = mTitle
    }
}