package com.yumzy.orderfood.ui.common.recyclerview.adapter;

import android.content.Context;
import android.util.SparseIntArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.RecyclerView;

import com.yumzy.orderfood.R;
import com.yumzy.orderfood.ui.common.recyclerview.interfaces.IListGroupDTO;
import com.yumzy.orderfood.ui.common.recyclerview.listener.IItemViewHolderListener;
import com.yumzy.orderfood.ui.common.recyclerview.view.YExpandableRecyclerView;
import com.yumzy.orderfood.ui.common.recyclerview.view.YItemViewHolder;
import com.yumzy.orderfood.ui.common.recyclerview.view.YProgressHolder;
import com.yumzy.orderfood.ui.common.recyclerview.view.YRecyclerView;
import com.yumzy.orderfood.util.ViewConst;
import com.laalsa.laalsalib.ui.VUtil;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class YExpandableListAdapter<T extends IListGroupDTO<T>> extends YMultiSelectionViewAdapter<T>//RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    public static final int VIEW_TYPE_SECTION = 1;// R.layout.layout_section;
    public static final int VIEW_TYPE_ITEM = 2;//R.layout.layout_item; //TODO : change this

    public static final byte SINGLE_ARRAY_LIST = 1;
    public static final byte GROUP_ARRAY_LIST = 2;

    public static final int MODE_NORMAL = 0;
    public static final int MODE_ACCORDION = 1;
    public static final int ARROW_ROTATION_DURATION = 150;

    protected ArrayList<T> arrVisibleItems;
    protected List<Integer> indexList = new ArrayList<>();
    protected SparseIntArray expandMap = new SparseIntArray();
    protected int iMode;
    protected View preSelectedView = null;
    private boolean bExpanable = true;
    private boolean isEnableActionMode = true;
    private byte byTypeofData;
    private int iDefaultExpandPos = 0;

    public YExpandableListAdapter(Context context, ArrayList<T> viewHolderList, IItemViewHolderListener itemViewHolderListener) {
        super(context, viewHolderList, itemViewHolderListener);
        arrVisibleItems = new ArrayList<>(viewHolderList);/*getParentChildListItems(viewHolderList)*/
    }

    public YExpandableListAdapter(Context context, ArrayList<T> viewHolderList, IItemViewHolderListener itemViewHolderListener, boolean bExpanable, boolean isEnableActionMode) {
        super(context, viewHolderList, itemViewHolderListener);
        arrVisibleItems = viewHolderList;/*getParentChildListItems(viewHolderList)*/
        this.bExpanable = bExpanable;
        this.isEnableActionMode = isEnableActionMode;
    }

    public YExpandableListAdapter(Context context, ArrayList<T> viewHolderList, IItemViewHolderListener itemViewHolderListener, boolean bExpanable, boolean isEnableActionMode, boolean bLoadMore) {
        super(context, viewHolderList, itemViewHolderListener, bLoadMore);
        arrVisibleItems = new ArrayList<>(viewHolderList);/*getParentChildListItems(viewHolderList)*/
        this.bExpanable = bExpanable;
        this.isEnableActionMode = isEnableActionMode;
        this.hasMoreRecords = bLoadMore;
    }

    public YExpandableListAdapter(Context context, ArrayList<T> viewHolderList, IItemViewHolderListener itemViewHolderListener, boolean bExpanable, boolean isEnableActionMode, boolean bLoadMore, byte byTypeofData) {
        super(context, viewHolderList, itemViewHolderListener, bLoadMore);
        arrVisibleItems = new ArrayList<T>(viewHolderList);/*getParentChildListItems(viewHolderList)*/

        this.bExpanable = bExpanable;
        this.isEnableActionMode = isEnableActionMode;
        this.hasMoreRecords = bLoadMore;
        this.byTypeofData = byTypeofData;
        expandDefPosData(0);
    }

    public static void openArrow(View view) {
        view.animate().setDuration(ARROW_ROTATION_DURATION).rotation(180);

    }

    public static void closeArrow(View view) {
        view.animate().setDuration(ARROW_ROTATION_DURATION).rotation(0);
    }

    private void expandDefPosData(int iExpandIndex) {
        if (byTypeofData == GROUP_ARRAY_LIST) {
            iDefaultExpandPos = iExpandIndex;
            IListGroupDTO<T> clListGroupDTO = itemHolderArrayList.get(iExpandIndex);
            arrVisibleItems.addAll(iExpandIndex + 1, (Collection<? extends T>) clListGroupDTO.getListItems());
        }
    }

    public void setItemClickListener(YRecyclerView.IItemClickListener clItemClickListener) {
        this.clItemClickListener = clItemClickListener;
    }

    @NotNull
    @Override
    public YItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        YItemViewHolder yItemViewHolder;
        if (viewType != TYPE_PROGRESS)
            yItemViewHolder = itemViewHolderListener.onCreateViewHolder(parent, viewType);
        else {
            LinearLayout layout = new LinearLayout(context);
            layout.setLayoutParams(new LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.MATCH_PARENT));
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setGravity(Gravity.CENTER);

            ProgressBar progressBar = new ProgressBar(context);
            progressBar.setLayoutParams(new LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, VUtil.dpToPx(45)));
            progressBar.setId(R.id.progress_indicator);
            layout.addView(progressBar);
            yItemViewHolder = new YProgressHolder(layout);
        }
        return yItemViewHolder;
    }

    @Override
    public void onBindViewHolder(@NotNull final RecyclerView.ViewHolder holder, final int position) {
        if (!(holder instanceof YProgressHolder)) {
            if (bExpanable) {
                if (position == iDefaultExpandPos && byTypeofData == GROUP_ARRAY_LIST && holder.itemView.getTag(R.id.holder_zero) == null) {
                    holder.itemView.setTag(R.id.holder_zero, "expanded");
                    ((YItemViewHolder) holder).setExpanded(true);
                }
                itemViewHolderListener.onBindViewHolder(holder, holder.getAdapterPosition());
                holder.itemView.setActivated(mSelectedItemsIds.get(position, false));
                ((YItemViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (bExpanable) {
                            if (isGroup(holder.getAdapterPosition())) {
                                holder.itemView.setActivated(toggleExpandedItems(holder, false));

                            } else {
                                if (preSelectedView != null)
                                    preSelectedView.setSelected(false);
                                preSelectedView = v;
//                                int iItemPosition = (arrVisibleItems.get(holder.getAdapterPosition())).getMenuId();
                                preSelectedView.setSelected(true);
                                holder.itemView.setSelected(true);

                                //iItemViewEventListener.onSingleTap(v, null, holder,iItemPosition);
                            }

                        }
                        /*else
                        {
                            if (!isGroup(holder.getAdapterPosition()) && clItemClickListener != null)
                            {
                                clItemClickListener.onItemClick(v, holder.getAdapterPosition());
                            }
                        }*/
                        if (clItemClickListener != null)
                            clItemClickListener.onItemClick(v, holder.getAdapterPosition());

                    }
                });
            }

            if (isEnableActionMode)
                super.onBindViewHolder(holder, position);
        }
        if (hasMoreRecords) {
            super.onBindViewHolder(holder, position);
        }
    }

    @Override
    public int getItemCount() {
        if (arrVisibleItems != null && arrVisibleItems.size() > 0) {
            return arrVisibleItems.size();
        }
        return -1;
    }

    @Override
    public int getItemViewType(int position) {
        int itemType = -1;
        if (itemViewHolderListener != null)
            itemType = itemViewHolderListener.getItemViewType(position);
        if (itemType < 0) {
            if (hasMoreRecords && itemHolderArrayList.get(position) == null)
                return TYPE_PROGRESS;
            if (isGroup(position))
                return VIEW_TYPE_SECTION;
            else
                return VIEW_TYPE_ITEM;
        }
        return itemType;
    }

/*
    public interface IActionModeEventListener {
        boolean onLongPress(View view, ActionMode actionMode);
        void onSingleTap(View view, ActionMode actionMode, RecyclerView.ViewHolder clItemViewHolder, int iItemPosition);
        boolean onActionModeItemClicked(ActionMode mode, MenuItem item);
    }*/

    protected boolean isGroup(int position) {
        if (arrVisibleItems.get(position) instanceof IListGroupDTO)
            return (arrVisibleItems.get(position)).isGroup();
        else
            return false;
    }

    public ArrayList<T> getData() {
        return arrVisibleItems;
    }

    public void setData(ArrayList<T> arrayList) {
        setData(arrayList, 0);
    }

    public void expandItems(int position, boolean notify) {
        int count = 0;
//        int index = indexList.get(position);
        int insert = position;
        List<T> listItems = getSectionItems(position, true);   //todo: get child Items
        for (int i = 0; i < listItems.size(); i++) {
            insert++;
            count++;
            arrVisibleItems.add(insert, listItems.get(i));
//            indexList.add(insert, i);
        }

        if (count > 0) {
            notifyItemRangeInserted(position + 1, count);
//            int allItemsPosition = indexList.get(position);
//            expandMap.put(allItemsPosition, 1);
        }

        if (notify) {
            notifyItemChanged(position);
        }
    }

    public ArrayList<T> getSectionItems(int iSecPosition, boolean isExpand) {
        ArrayList<T> arrItems;
        if (arrVisibleItems.get(iSecPosition).isGroup()) {
            if (byTypeofData == GROUP_ARRAY_LIST) {
                return new ArrayList<T>((Collection<? extends T>) arrVisibleItems.get(iSecPosition).getListItems());
            } else {
                if (isExpand)
                    arrItems = itemHolderArrayList;
                else
                    arrItems = arrVisibleItems;
                ArrayList<T> arrSecItems = new ArrayList<>();
                int iParentLevel = arrVisibleItems.get(iSecPosition).getLevel();
                for (int i = iSecPosition + 1; i < arrItems.size(); i++) {
                    int iLevel = arrItems.get(i).getLevel();
                    if (iLevel == iParentLevel && !arrItems.get(i).isGroup())
                        arrSecItems.add(arrItems.get(i));

                }
                return arrSecItems;
            }
        }
        return null;
    }

    public void collapseItems(int position, boolean notify) {
        int count = 0;
//        int index = indexList.get(position);
        ArrayList<T> arrSecItems = getSectionItems(position, false);
        int iChildItemsCount = 0/*getListItems(position,false).size()*/;
        if (arrSecItems != null)
            iChildItemsCount = arrSecItems.size();
        for (int i = 0; i < iChildItemsCount; i++) {
            count++;
            arrVisibleItems.remove(position + 1);
//            indexList.remove(position + 1);
        }

        if (count > 0) {
            notifyItemRangeRemoved(position + 1, count);
//            int allItemsPosition = indexList.get(position);
//            expandMap.delete(allItemsPosition);
        }


        if (notify) {
            notifyItemRangeChanged(position, 1);
        }
    }

    public boolean toggleExpandedItems(int position, boolean notify) {
        if (isExpanded(position)) {
            collapseItems(position, notify);
            return false;
        } else {
            expandItems(position, notify);
            if (iMode == MODE_ACCORDION) {
                collapseAllExcept(position);
            }

            return true;
        }
    }

    public boolean toggleExpandedItems(RecyclerView.ViewHolder viewHolder, boolean notify) {
        if (((YItemViewHolder) viewHolder).isExpanded())
//        if (isExpanded(viewHolder.getAdapterPosition()))
        {
            collapseItems(viewHolder.getAdapterPosition(), notify);
            ((YItemViewHolder) viewHolder).setExpanded(false);
//            viewHolder.itemView.setActivated(false);
            return false;
        } else {
            expandItems(viewHolder.getAdapterPosition(), notify);
//            viewHolder.itemView.setActivated(true);
            ((YItemViewHolder) viewHolder).setExpanded(true);
            if (iMode == MODE_ACCORDION) {
                collapseAllExcept(viewHolder);
            }

            return true;
        }
    }

    protected boolean isExpanded(int position) {
        int allItemsPosition = indexList.get(position);
        return expandMap.get(allItemsPosition, -1) >= 0;
    }

    public void collapseAll() {
        collapseAllExcept(-1);
    }

    public void collapseAllExcept(RecyclerView.ViewHolder clViewHolder) {
        int iSize = arrVisibleItems.size();
        YExpandableRecyclerView clExpandableRecyclerView = (YExpandableRecyclerView) clViewHolder.itemView.getParent();
        for (int i = iSize - 1; i >= 0; i--) {
            if (i != clViewHolder.getAdapterPosition() && getItemViewType(i) == VIEW_TYPE_SECTION) {
                int iSectionLevel = arrVisibleItems.get(i).getLevel();
                if (i < iSize - 1 && iSectionLevel == arrVisibleItems.get(i + 1).getLevel()) {
                    YItemViewHolder yItemViewHolder = (YItemViewHolder) clExpandableRecyclerView.findViewHolderForAdapterPosition(i);
                    collapseItems(i, true);
                    if (yItemViewHolder != null)
                        yItemViewHolder.setExpanded(false);
                }
            }
        }

    }

    public void collapseAllExcept(int position) {
        int iSize = arrVisibleItems.size();
        for (int i = iSize - 1; i >= 0; i--) {
            if (i != position && getItemViewType(i) == VIEW_TYPE_SECTION) {
                int iSectionLevel = arrVisibleItems.get(i).getLevel();
                if (i < iSize - 1 && iSectionLevel == arrVisibleItems.get(i + 1).getLevel()) {
                    collapseItems(i, false);
                }
            }
        }
    }

    public void expandAll() {
        for (int i = arrVisibleItems.size() - 1; i >= 0; i--) {
            if (getItemViewType(i) == VIEW_TYPE_SECTION) {
                if (!isExpanded(i)) {
                    expandItems(i, true);
                }
            }
        }
    }

    public int getMode() {
        return iMode;
    }

    public void setMode(int mode) {
        this.iMode = mode;
    }

    public void removeItemAt(int visiblePosition) {

        arrVisibleItems.remove(visiblePosition);
        if (bExpanable) {
            int allItemsPosition = indexList.get(visiblePosition);
//        itemHolderArrayList.remove(allItemsPosition);
            incrementIndexList(allItemsPosition, visiblePosition, -1);
            incrementExpandMapAfter(allItemsPosition, -1);
        }

        notifyItemRemoved(visiblePosition);
    }

    private void incrementExpandMapAfter(int position, int direction) {
        SparseIntArray newExpandMap = new SparseIntArray();

        for (int i = 0; i < expandMap.size(); i++) {
            int index = expandMap.keyAt(i);
            newExpandMap.put(index < position ? index : index + direction, 1);
        }

        expandMap = newExpandMap;
    }

    private void incrementIndexList(int allItemsPosition, int visiblePosition, int direction) {
        List<Integer> newIndexList = new ArrayList<>();

        for (int i = 0; i < indexList.size(); i++) {
            if (i == visiblePosition) {
                if (direction > 0) {
                    newIndexList.add(allItemsPosition);
                }
            }

            int val = indexList.get(i);
            if (val != allItemsPosition)
                newIndexList.add(val < allItemsPosition ? val : val + direction);
        }

        indexList = newIndexList;
    }

    public void setData(ArrayList<T> arrayList, int iExpandIndex)//expand index work only for group array list
    {
        if (arrayList != null) {
            arrVisibleItems = arrayList;
            expandDefPosData(iExpandIndex);
        }
    }

}
