package com.yumzy.orderfood.ui.common

enum class ItemType {
    VEG,
    EGG,
    NON_VEG
}