package com.yumzy.orderfood.ui.screens.module.toprestaurant

import com.yumzy.orderfood.ui.base.IView

interface ITopRestaurantView : IView {
    //    fun fetchTopRestaurantList()
    fun showFilterOptions()
    fun hideFilterOptions()
    fun applyFilter()
    fun clearAllFilter()
}
