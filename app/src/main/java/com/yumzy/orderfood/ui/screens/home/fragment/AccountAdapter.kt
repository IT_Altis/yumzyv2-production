package com.yumzy.orderfood.ui.screens.home.fragment

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.utilcode.util.ClickUtils
import com.yumzy.orderfood.data.ui.AccountDTO
import com.yumzy.orderfood.databinding.AdapterAccountBinding
import com.yumzy.orderfood.ui.helper.setClickScaleEffect
import com.yumzy.orderfood.util.IConstants

/**
 * Created by Bhupendra Kumar Sahu.
 */
class AccountAdapter(
    val context: Context,
    private val accountList: ArrayList<AccountDTO>,
    private val clickedListener: (view: View, adapterPosition: Int) -> Unit
) : RecyclerView.Adapter<AccountAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: AdapterAccountBinding =
            AdapterAccountBinding.inflate(layoutInflater, parent, false)
        return ItemViewHolder(itemBinding)
    }

    override fun getItemCount() = accountList.size
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.adapterBinding.iconOption.text = accountList[position].icons
        holder.adapterBinding.tvTitle.text = accountList[position].title
        holder.adapterBinding.tvDescription.text = accountList[position].description
    }

    inner class ItemViewHolder(itemView: AdapterAccountBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        var adapterBinding: AdapterAccountBinding = itemView

        init {
            adapterBinding.clAccount.setClickScaleEffect()

            ClickUtils.applyGlobalDebouncing(
                adapterBinding.clAccount,
                IConstants.AppConstant.CLICK_DELAY
            ) {

                accountList.get(adapterPosition).actionId.let { actionId ->
                    clickedListener(
                        adapterBinding.tvTitle,
                        actionId
                    )
                }
            }

        }
    }
}
