package com.yumzy.orderfood.ui.screens.restaurant

import com.yumzy.orderfood.data.models.FoodMenuDTO
import com.yumzy.orderfood.ui.base.IView

/**
 * Created by Bhupendra Kumar Sahu.
 */
interface IRestaurant : IView {
    fun getCart()
    fun fetchMenu()
    fun subscribe()
}