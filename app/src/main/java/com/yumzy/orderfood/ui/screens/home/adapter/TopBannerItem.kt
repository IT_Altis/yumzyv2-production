package com.yumzy.orderfood.ui.screens.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.ModelAbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.databinding.ViewOfferAdsListBinding
import com.yumzy.orderfood.databinding.ViewTopBannerBinding

class TopBannerItem(private val homeListDTO: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, ViewTopBannerBinding>(homeListDTO) {
    override val type = R.id.top_banner_item_id
    override fun bindView(binding: ViewTopBannerBinding, payloads: List<Any>) {
        binding.homeTopBanner.clearData()
        binding.homeTopBanner.setBannerData(homeListDTO.nestedList)
    }

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ViewTopBannerBinding {
        return ViewTopBannerBinding.inflate(inflater, parent, false)
    }
}

class OfferAdsListItem(private val homeListDTO: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, ViewOfferAdsListBinding>(homeListDTO) {
    override val type = R.id.offer_ads_item
    override fun bindView(binding: ViewOfferAdsListBinding, payloads: List<Any>) {
        binding.offersAds.clearData()
        binding.offersAds.setData(homeListDTO.nestedList!!)
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ViewOfferAdsListBinding {
        return ViewOfferAdsListBinding.inflate(inflater, parent, false)
    }
}