package com.yumzy.orderfood.ui.screens.manageaddress

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.ui.sp2Px
import com.yumzy.orderfood.AppGlobalObjects
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.databinding.AdapterSavedAddressBinding
import com.yumzy.orderfood.ui.component.shadowcard.DetailsCardView
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.util.viewutils.YumUtil
import java.util.*

class ManageAddressAdapter(
    private var items: List<AddressDTO>,
    private val detailsCardListener: DetailsCardView.DetailsCardListener?
) :
    RecyclerView.Adapter<ManageAddressAdapter.ItemViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: AdapterSavedAddressBinding =
            AdapterSavedAddressBinding.inflate(layoutInflater, parent, false)
        return ItemViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(items[position])
    }

    fun updateList(list: List<AddressDTO>) {
        items = list
        notifyDataSetChanged()
    }

    inner class ItemViewHolder(itemView: AdapterSavedAddressBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        var adapterBinding: AdapterSavedAddressBinding = itemView
        fun bind(address: AddressDTO) {
            adapterBinding.addressInfoView.tag = address
            val iconHome = getAddressTypeIcon(address.addressTag)
            val iconFontDrawable = YumUtil.getIconFontDrawable(
                adapterBinding.root.context,
                iconHome,
                8f.sp2Px,
                ThemeConstant.textBlackColor
            )
            adapterBinding.addressInfoView.setTitleDrawable(iconFontDrawable)
            adapterBinding.addressInfoView.title = address.name.capitalize(Locale.getDefault())
            adapterBinding.addressInfoView.body =
                "${address.houseNum.capitalize(Locale.getDefault())}, ${
                    if (address.landmark.isNotBlank()) address.landmark.capitalize(
                        Locale.getDefault()
                    ) + ", " else ""
                }${address.fullAddress.capitalize(Locale.getDefault())}"
            adapterBinding.addressInfoView.detailsCardListener = detailsCardListener
            adapterBinding.addressInfoView.isDeliveryAddress = address.addressId == AppGlobalObjects.selectedAddress?.addressId

        }

        private fun getAddressTypeIcon(addressTag: String): Int {
            return when {
                addressTag.trim().equals("home", true) -> R.string.icon_009_address
                addressTag.trim().equals("office", true) -> R.string.icon_briefcase_alt
                else -> R.string.icon_compass

            }

        }
    }
}
