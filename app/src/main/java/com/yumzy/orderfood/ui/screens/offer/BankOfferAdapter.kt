package com.yumzy.orderfood.ui.screens.offer

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.utilcode.util.ShadowUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.AdapterBankOfferBinding
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.screens.module.infodialog.DetailActionDialog
import com.yumzy.orderfood.ui.screens.templetes.DetailsInfoTemplate
import com.yumzy.orderfood.util.YUtils

/**
 * Created by Bhupendra Kumar Sahu.
 */
class BankOfferAdapter(
    val context: Context,
    //private val savedAddressList: ArrayList<SavedAddressDTO>?,
    private val clickedListener: (adapterPosition: Int) -> Unit
) :
    RecyclerView.Adapter<BankOfferAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: AdapterBankOfferBinding =
            AdapterBankOfferBinding.inflate(layoutInflater, parent, false)
        ShadowUtils.apply(
            itemBinding.root,
            ShadowUtils.Config().setShadowSize(UIHelper.i1px).setShadowRadius(UIHelper.i10px.toFloat()).setShadowColor(0XBFCECECE.toInt())
        )


        return ItemViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return 5
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {

    }

    inner class ItemViewHolder(itemView: AdapterBankOfferBinding) :
        RecyclerView.ViewHolder(itemView.root) {


        var adapterBinding: AdapterBankOfferBinding = itemView

//        fun bind(savedAddress: SavedAddressDTO) {
//            //TODO bind the data to layout.
//        }

        init {
            adapterBinding.bankOfferView.setInfoImage(
                ResourcesCompat.getDrawable(
                    context.resources, YUtils.getPlaceHolder(),
                    context.theme
                )
            )

            adapterBinding.bankOfferView.setInfoHeader("Get 20% discount using Citi Bank Cards")
            adapterBinding.bankOfferView.setInfoSubHeader("Use code CIIFOODI & get 20% discount up to Rs 200 on order Rs 600 & above. Offer valid only on Weekends.")
            adapterBinding.bankOfferView.setOnClickListener {

                val detailActionDialog = DetailActionDialog()
                val infoTemplate = DetailsInfoTemplate(
                    context,"https://assets9.lottiefiles.com/packages/lf20_bMIGMp.json",
                    "Get 20% discount using Citi Bank Cards",
                    "Use code CIIFOODI ",
                    arrayListOf(
                        "The maximum discount to avali is Rs 200",
                        "offer valid twice per user ",
                        "Offer Valid on Minmum order value of Rs 600",
                        "Offer valid on all CITI Bank Credit and Debit Cards ",
                        "Other T&Cs may apply",
                        "Offer Valid Till Dec 29"
                    ))
                detailActionDialog.detailsTemplate = infoTemplate
                detailActionDialog.showNow((context as AppCompatActivity).supportFragmentManager, "")

                     }

        }

    }
}
