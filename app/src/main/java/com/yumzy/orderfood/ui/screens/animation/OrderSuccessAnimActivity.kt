package com.yumzy.orderfood.ui.screens.animation

import android.animation.Animator
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.yumzy.orderfood.databinding.ActivityLottiAnimationorBinding

class OrderSuccessAnimActivity : AppCompatActivity(), Animator.AnimatorListener {
    private val bd: ActivityLottiAnimationorBinding by lazy {
        ActivityLottiAnimationorBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(bd.root)
        bd.lottiAnimationView.addAnimatorListener(this)
        bd.lottiAnimationView.playAnimation()

    }

    override fun onAnimationStart(animation: Animator?) {
    }

    override fun onAnimationEnd(animation: Animator?) {
        val handler = Handler(this.mainLooper)
        val runnable = Runnable {
            finish()
        }

        handler.postDelayed(runnable, 1000)
    }

    override fun onAnimationCancel(animation: Animator?) {
    }

    override fun onAnimationRepeat(animation: Animator?) {

    }
}