package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.setClickScaleEffect
import com.yumzy.orderfood.ui.helper.setInterFont
import com.laalsa.laalsalib.ui.VUtil

class UserItemInfoView : ConstraintLayout {

    private var mInfoTitle: String = ""
    private var mInfoValue: String = ""
    private var mKey: String = ""
    private var mModuleId: Int = -1

    private lateinit var mUserItemInfoListener: OnUserItemInfoListener

    private val i18px = VUtil.dpToPx(1)
    private val i10px = VUtil.dpToPx(1)

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, atr: AttributeSet?) : this(context, atr, 0)
    constructor(context: Context, atr: AttributeSet?, style: Int) : super(context, atr, style) {

        if (atr != null) {
            val a = context.obtainStyledAttributes(atr, R.styleable.UserItemInfoView, style, 0)
            mInfoTitle = a.getString(R.styleable.UserItemInfoView_info_title).toString()
            mKey = a.getString(R.styleable.UserItemInfoView_info_key).toString()
            mInfoValue = a.getString(R.styleable.UserItemInfoView_info_value).toString()
            mModuleId = a.getInt(R.styleable.UserItemInfoView_module_id, -1)
            a.recycle()
        }

        init()

    }

    private fun init() {

        this.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)

        this.addView(getTitleTextView(mInfoTitle))
        this.addView(getInfoView(mInfoValue))
        this.addView(getDivider())
        this.id = mModuleId

        this.setClickScaleEffect()
        this.setOnClickListener {
            if (this::mUserItemInfoListener.isInitialized) {
                mUserItemInfoListener.onUserItemInfoClicked(
                    mModuleId,
                    mInfoTitle,
                    mInfoValue, mKey
                )
            }
        }


    }

    private fun getDivider(): View? {
        val v = View(context)
        val layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, 2)
        layoutParams.bottomToBottom = LayoutParams.PARENT_ID

        v.layoutParams = layoutParams
        v.setBackgroundColor(ThemeConstant.lightAlphaGray)

        return v
    }

    private fun getTitleTextView(infoTitle: String): TextView {

        val titleTextView = TextView(context)

        val layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        layoutParams.topToTop = LayoutParams.PARENT_ID
        layoutParams.startToStart = LayoutParams.PARENT_ID
        layoutParams.bottomToBottom = LayoutParams.PARENT_ID

        titleTextView.layoutParams = layoutParams


        titleTextView.setInterFont()
        titleTextView.text = infoTitle
        titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)

        return titleTextView

    }

    private fun getInfoView(infoValue: String): TextView {

        val titleInfoView = TextView(context)

        val layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        layoutParams.topToTop = LayoutParams.PARENT_ID
        layoutParams.endToEnd = LayoutParams.PARENT_ID
        layoutParams.bottomToBottom = LayoutParams.PARENT_ID

        titleInfoView.layoutParams = layoutParams


        titleInfoView.setInterFont()
        titleInfoView.text = infoValue
        titleInfoView.setTextColor(ThemeConstant.textGrayColor)
        titleInfoView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)

        return titleInfoView

    }

    fun setUserItemInfo(
        moduleId: Int,
        userInfoTitle: String,
        userInfoValue: String,
        userKey: String
    ) {
        mModuleId = moduleId
        mInfoTitle = userInfoTitle
        mInfoValue = userInfoValue
        mKey = userKey

        val titleTv = this.getChildAt(0) as TextView
        val infoTv = this.getChildAt(1) as TextView

        titleTv.text = mInfoTitle
        infoTv.text = mInfoValue
    }

    fun setUserItemClickListener(userItemInfoListener: OnUserItemInfoListener) {
        mUserItemInfoListener = userItemInfoListener
    }

    interface OnUserItemInfoListener {
        fun onUserItemInfoClicked(
            moduleId: Int,
            userInfoTitle: String,
            userInfoValue: String,
            userKey: String
        )
    }

}