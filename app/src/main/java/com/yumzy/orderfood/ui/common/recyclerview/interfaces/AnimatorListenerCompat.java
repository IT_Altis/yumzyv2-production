package com.yumzy.orderfood.ui.common.recyclerview.interfaces;

public interface AnimatorListenerCompat {
    void onAnimationStart(ValueAnimatorCompat var1);

    void onAnimationEnd(ValueAnimatorCompat var1);

    void onAnimationCancel(ValueAnimatorCompat var1);

    void onAnimationRepeat(ValueAnimatorCompat var1);
}