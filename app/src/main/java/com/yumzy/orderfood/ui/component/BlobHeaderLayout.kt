package com.yumzy.orderfood.ui.component

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.laalsa.laalsalib.ui.px
import com.laalsa.laalsaui.utils.ColorHelper
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.LayoutBlobheaderBinding
import com.yumzy.orderfood.ui.helper.ShapeHelper
import com.yumzy.orderfood.ui.helper.hideEmptyTextView


class BlobHeaderLayout : ConstraintLayout {

    private val binding by lazy {
        val layoutInflater = LayoutInflater.from(context)
        LayoutBlobheaderBinding.inflate(layoutInflater, this, true)
    }

    fun convertPixelsToDp(px: Float, context: Context): Float {
        return px / (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }

    var heading: CharSequence? = ""
        set(value) {
            field = value
            this.binding.blobHeading.hideEmptyTextView(field)

        }
    var title: CharSequence? = ""
        set(value) {
            field = value
            this.binding.blobTitle.hideEmptyTextView(field)
        }

    var titleColor: Int = Color.WHITE
        set(value) {
            field = value
            this.binding.blobTitle.setTextColor(field)
        }

    var showShadow: Boolean = true
    var shadowColor: Int = 0
    var titleTextSize: Int = 18.px
        set(value) {
            field = value
            binding.blobTitle.setTextSize(
                TypedValue.COMPLEX_UNIT_DIP,
                convertPixelsToDp(field.toFloat(), context)
            )
        }
    /*  var headingTextSize: Float = 12.pxf
          set(value) {
              field = value
              binding.blobHeading.textSize = titleTextSize
          }*/

    var hasGradient = true
        set(value) {
            field = value
            blobColor = blobColor
        }

    var blobColor = 0xFF556AE9.toInt()
        set(value) {
            field = value
            val color1 = blobColor
            val color2 = ColorHelper.darkenColor(blobColor, 0.2f)
            if (hasGradient)
                this.background = ShapeHelper.deformBubbleDrawable(color1, color2, shadowColor)
            else
                this.background =
                    ShapeHelper.deformBubbleDrawable(color1, color1, Color.TRANSPARENT)
        }


    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {


        if (attrs != null) {
            val a =
                context.obtainStyledAttributes(
                    attrs,
                    R.styleable.BlobHeaderLayout,
                    defStyleAttr,
                    0
                )

            heading = a.getString(R.styleable.BlobHeaderLayout_blobHeading) ?: ""
            title = a.getString(R.styleable.BlobHeaderLayout_blobTitle) ?: ""
//            titleColor = a.getColor(R.styleable.BlobHeaderLayout_titleColor, titleColor)
//            hasGradient = a.getBoolean(R.styleable.BlobHeaderLayout_blobGradient, hasGradient)
//            shadowColor = a.getColor(R.styleable.BlobHeaderLayout_shadowColor, shadowColor)
            // val titleBold = a.getBoolean(R.styleable.BlobHeaderLayout_titleBold, true)
//            showShadow = a.getBoolean(R.styleable.BlobHeaderLayout_showShadow, showShadow)
//            blobColor = a.getColor(R.styleable.BlobHeaderLayout_blobColor, blobColor)
            titleTextSize = a.getDimensionPixelSize(
                R.styleable.BlobHeaderLayout_titleTextSize,
                titleTextSize
            )
//            headingTextSize = a.getDimension(R.styleable.BlobHeaderLayout_headingTextSize, 14f.sp2Px)

//            this.binding.blobHeading.hideEmptyTextView(heading)
//            this.binding.blobTitle.hideEmptyTextView(title)
//            this.binding.blobTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, titleTextSize)

//            renderAttribuite(titleBold, a)

            a.recycle()

        }

    }

    private fun renderAttribuite(titleBold: Boolean, a: TypedArray) {
        if (titleBold) {
            binding.blobTitle.setTypeface(Typeface.DEFAULT, Typeface.BOLD)
        } else
            binding.blobTitle.typeface = Typeface.DEFAULT

        /*  if (heading.isNullOrEmpty()) {
                  binding.blobTitle.gravity = Gravity.CENTER_VERTICAL or Gravity.START
              }*/


        val x: Float = a.getFloat(R.styleable.BlobHeaderLayout_titleDx, 0f)
        val y: Float = a.getFloat(R.styleable.BlobHeaderLayout_titleDy, 0f)
        val blurRadius: Float = a.getFloat(R.styleable.BlobHeaderLayout_titleBlurRadius, 0f)
        val color: Int =
            a.getColor(R.styleable.BlobHeaderLayout_titleShadowColor, Color.TRANSPARENT)
        if (color != Color.TRANSPARENT) {
            this.titleShadow(x, y, blurRadius, color)
        }
    }

    fun headingShadow(x: Float, y: Float, blurRadius: Float, color: Int) {
        binding.blobHeading.setShadowLayer(blurRadius, x, y, color)
    }

    fun titleShadow(x: Float, y: Float, blurRadius: Float, color: Int) {
        binding.blobTitle.setShadowLayer(blurRadius, x, y, color)
    }
//
//    fun subtitleShadow(x: Float, y: Float, blurRadius: Float, color: Int) {
//        binding.blobSubtitle.setShadowLayer(blurRadius, x, y, color)
//    }

    override fun onAttachedToWindow() {
        this.alpha = 0f
        super.onAttachedToWindow()
        this.animate().alpha(1f)

    }
}