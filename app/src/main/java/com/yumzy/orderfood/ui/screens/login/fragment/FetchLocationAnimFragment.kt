package com.yumzy.orderfood.ui.screens.login.fragment

import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.navigation.fragment.findNavController
import com.yumzy.orderfood.AppGlobalObjects
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.SharedPrefsHelper
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.data.models.UserDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.databinding.FragmentFetchLocationAnimBinding
import com.yumzy.orderfood.tools.analytics.CleverTapHelper
import com.yumzy.orderfood.tools.notification.YumzyFirebaseMessaging
import com.yumzy.orderfood.ui.base.BaseFragment
import com.yumzy.orderfood.ui.screens.home.HomePageActivity
import com.yumzy.orderfood.ui.screens.home.fragment.IFetchLocAnim
import com.yumzy.orderfood.ui.screens.location.LocationSearchActivity
import com.yumzy.orderfood.ui.screens.login.StartedActivity
import com.yumzy.orderfood.util.IConstants
import java.util.*
import javax.inject.Inject


class FetchLocationAnimFragment :
    BaseFragment<FragmentFetchLocationAnimBinding, FetchLocationAnimViewModel>(),
    IFetchLocAnim {

    @Inject
    override lateinit var fvm: FetchLocationAnimViewModel

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    lateinit var user: UserDTO
    var mUserSavedAddress: AddressDTO? = null
    private var mOutletId = ""

    private val navController by lazy {
        findNavController()
    }

    override fun getLayoutId() = R.layout.fragment_fetch_location_anim
    override fun onFragmentReady(view: View) {
        fvm.attachView(this)
        setUserDTO()
        observeAddress()
        bd.tvCloseApp.setOnClickListener {
            requireActivity().finish()
        }
        bd.btnPickerAddress.setOnClickListener {
            startActivity(Intent(requireActivity(), LocationSearchActivity::class.java))
            requireActivity().finish()
        }


    }

    private fun observeAddress() {

        AppGlobalObjects.getAddressLiveData.observeForever {
            if (it != null) {

                sharedPrefsHelper.saveAddressTag(it.name)
                sharedPrefsHelper.savedCityLocation(it.googleLocation)
                sharedPrefsHelper.saveFullAddress(it.fullAddress)
                updateCurrentLocation(it)
            }
        }

        if (sharedPrefsHelper.getSelectedAddress() == null) {
            //get location from remote database
            fvm.getAddressList().observe(this, { result ->
                RHandler<List<AddressDTO>>(
                    this,
                    result
                ) {
                    if (!it.isNullOrEmpty()) {
                        AppGlobalObjects.selectedAddress = it[0]
                    } else {
                        //start location picker activity
                        startLocationPicker()
                    }
                }
            })
        }

    }

    private fun startLocationPicker() {
        if (this::user.isInitialized && !user.address.isNullOrEmpty()) {
            fvm.setSelectedAddress(user.address?.get((user.address?.size ?: 1) - 1)!!)
        } else {
            bd.tvDeliverTo.text = getString(R.string.oops)
            bd.tvLocationMessage.text =
                getString(R.string.did_not_find_saved_location)
            bd.tvFullLocationMessage.text =
                getString(R.string.pick_again_location)
            bd.llFooter.visibility = View.GONE
            Handler(Looper.getMainLooper()).postDelayed({
                startActivity(Intent(requireActivity(), LocationSearchActivity::class.java))
                requireActivity().finish()
                Handler(Looper.getMainLooper()).postDelayed({
                    bd.llFooter.visibility = View.VISIBLE
                }, 700)
            }, 700)
        }
    }

    private fun updateCurrentLocation(addressDTO: AddressDTO) {

        bd.llFooter.visibility = View.GONE
        bd.tvDeliverTo.text = getString(R.string.delivering_to)
        bd.tvLocationMessage.text =
            " ${addressDTO.name.capitalize(Locale.ROOT)}"
        bd.tvFullLocationMessage.text = "${addressDTO.fullAddress}"

        //updating current location
        fvm.updateCurrentLocation(
            addressDTO.longLat?.coordinates?.get(0).toString(),
            addressDTO.longLat?.coordinates?.get(1).toString(),
            addressDTO.city,
            addressDTO.googleLocation
        ).observe(this, androidx.lifecycle.Observer {
            if (it.code == APIConstant.Code.DATA_UPDATED) {
                CleverTapHelper.updateLatLongUserLocation(
                    addressDTO.googleLocation,
                    addressDTO.city,
                    addressDTO.longLat?.coordinates?.get(0) ?: 0.0,
                    addressDTO.longLat?.coordinates?.get(1) ?: 0.0
                )
//                exploreFragment?.clearSearchResult()
            }


            //this prevent calling fetchhome data multiple times, duplicates the data in ui
            if (it.type == -1) return@Observer

            //fetching home location
            Handler(Looper.getMainLooper()).postDelayed({
                mUserSavedAddress = addressDTO
                val address = sharedPrefsHelper.getSelectedAddress()
                if (address != null) {
                    AppGlobalObjects.selectedAddress = address
                }

                val intent = Intent(requireActivity(), HomePageActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
                intent.putExtra(IConstants.ActivitiesFlags.NEW_USER, false)
                intent.putExtra(
                    YumzyFirebaseMessaging.NOTIFICATION_DESTINATION,
                    StartedActivity.mDestination
                )
                intent.putExtra(
                    YumzyFirebaseMessaging.NOTIFICATION_URL,
                    StartedActivity.mDestinationUrl
                )
                if (mOutletId.isNotBlank()) {
                    intent.putExtra(IConstants.ActivitiesFlags.OUTLET_ID, mOutletId)
                }
                startActivity(intent)
                requireActivity().finish()

            }, 1200)
        })

    }

    private fun setUserDTO() {
        fvm.getAppUser().observe(this, { userDTO ->
            userDTO?.also {
                user = it
                globalUser = it
            }
        })
    }

}
