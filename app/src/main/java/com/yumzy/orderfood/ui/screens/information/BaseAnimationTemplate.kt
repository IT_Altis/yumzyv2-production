package com.yumzy.orderfood.ui.screens.information

import android.content.Context
import android.content.DialogInterface
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.yumzy.orderfood.ui.common.YumLottieView
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.setInterBoldFont
import com.yumzy.orderfood.ui.helper.setInterThineFont
import com.yumzy.orderfood.ui.screens.module.infodialog.ILottieTemplate
import com.yumzy.orderfood.util.ViewConst
import com.laalsa.laalsalib.ui.VUtil

open class BaseAnimationTemplate(
    override val context: Context,
    override var imgUrl: String = "https://assets8.lottiefiles.com/temp/lf20_nXwOJj.json"

) : ILottieTemplate {

    override var showClose: Boolean = true
    override var titleText: String? = null
    override var footerText: String? = null
    override var bodySize: Int = VUtil.dpToPx(100)

    override fun heading(): View? {
        val textView = TextView(context)
        textView.layoutParams =
            ViewGroup.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        textView.text = titleText
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
        textView.setInterBoldFont()
        textView.gravity = Gravity.CENTER
        textView.setPadding(UIHelper.i5px, UIHelper.i5px, UIHelper.i5px, UIHelper.i5px)
        return textView
    }

    override fun body(): YumLottieView {
        val lottieFrame = UIHelper.buildLottieFrame(
            context,
            imgUrl
        )
        lottieFrame.layoutParams =
            ViewGroup.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)

        lottieFrame.minimumHeight = VUtil.dpToPx(bodySize)
        return lottieFrame

    }

    override fun footer(): View? {
        val fotterText = TextView(context)
        fotterText.text = this.footerText
        fotterText.setInterThineFont()
        fotterText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
        fotterText.gravity = Gravity.CENTER
        fotterText.layoutParams =
            ViewGroup.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        fotterText.setPadding(0, 0, 0, VUtil.dpToPx(15))
        return fotterText

    }

    override fun onDialogVisible(view: View?, visible: Boolean?, dialog: DialogInterface?) {

    }


}
