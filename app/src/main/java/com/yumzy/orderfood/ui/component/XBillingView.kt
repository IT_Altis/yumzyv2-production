package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.textview.MaterialTextView
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.ui.px
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.orderdetails.NewBillingItem
import com.yumzy.orderfood.data.models.orderdetails.XBilling
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.setInterBoldFont
import com.yumzy.orderfood.ui.helper.withPrecision
import com.yumzy.orderfood.util.viewutils.YumUtil

class XBillingView : LinearLayout {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initAttrs(attrs, context, defStyleAttr)

        intiViews()

    }

    var hideInfoIcon: Boolean = false
    var title: String = ""
        set(value) {
            field = value
            this.findViewById<MaterialTextView>(R.id.tv_billing_title)?.text = field

        }

    var isBold: Boolean = true
        set(value) {
            field = value
            this.findViewById<MaterialTextView>(R.id.tv_billing_title)?.setInterBoldFont()
            this.findViewById<MaterialTextView>(R.id.tv_billing_subtitle)?.setInterBoldFont()
        }
    var subTitle: String = ""
        set(value) {
            field = value
            this.findViewById<MaterialTextView>(R.id.tv_billing_subtitle)?.text = field
        }


    private fun initAttrs(
        attrs: AttributeSet?,
        context: Context?,
        defStyleAttr: Int
    ) {
        if (attrs != null) {
            val a =
                context!!.obtainStyledAttributes(
                    attrs,
                    R.styleable.BillingDetailsView,
                    defStyleAttr,
                    0
                )

            title = a.getString(R.styleable.BillingDetailsView_billing_title) ?: ""
            subTitle = a.getString(R.styleable.BillingDetailsView_billing_subtitle) ?: ""

            a.recycle()

        }
    }


    private fun intiViews() {
        this.orientation = VERTICAL
        this.setPadding(VUtil.dpToPx(15), VUtil.dpToPx(15), VUtil.dpToPx(15), VUtil.dpToPx(15))
        this.background = ResourcesCompat.getDrawable(
            context.resources,
            R.drawable.rounded_light_gray,
            context.theme
        )

    }


    fun setOutletInfoList(items: List<NewBillingItem>) {
        for (billItem in items) {
            // this.addView(getRow("Billing Details", "", true, 0))

            this.addView(
                getRow(
                    "• " + billItem.name.capitalize() + " X " + billItem.quantity,
                    "₹" + billItem.price.withPrecision(2),
                    true, 0
                )
            )
            if (billItem.addons.isNotEmpty()) {
                this.addView(
                    getRow(
                        billItem.addons.capitalize(),
                        "",
                        false, 2
                    )
                )
//                val dividerView =
//                    YumUtil.dividerView(context, ThemeConstant.lightGray, UIHelper.i3px,UIHelper.i3px,UIHelper.i3px)
//                this.addView(dividerView)

            }
        }
        val dividerView =
            YumUtil.dividerView(context, ThemeConstant.lightAlphaGray, UIHelper.i1px)
        this.addView(dividerView)
    }

//    fun setOutletInfoHeader() {
//
//        this.addView(getRow("Billing Details", "", true, 0))
//        val dividerView =
//            YumUtil.dividerView(context, ThemeConstant.lightAlphaGray, UIHelper.i1px)
//
//        this.addView(dividerView)
//    }


    fun setBillingInfoList(billing: XBilling, onClickListener: OnClickListener) {
        this.addView(
            getRow(
                context.resources.getString(R.string.sun_total),
                "₹" + billing.subTotal.withPrecision(2),
                false,
                0
            )
        )

        for (charges in billing.charges) {
            if (billing.charges.isNotEmpty()) {
                if (!charges.value.toString().equals("0.0")) {
                    this.addView(
                        getRow(
                            charges.displayName,
                            "₹" + charges.value.withPrecision(2),
                            false,
                            0
                        )
                    )
                }
            }
        }

        for (tex in billing.taxes) {
            if (billing.taxes.isNotEmpty()) {
                if (!tex.value.toString().equals("0.0")) {
                    this.addView(
                        getRow(
                            tex.displayName,
                            "₹" + tex.value.withPrecision(2),
                            false,
                            0
                        )
                    )
                }
            }
        }
        if (!billing.discount.toString().equals("0.0")) {
            this.addView(
                getRow(
                    context.resources.getString(R.string.discount),
                    "- ₹" + billing.discount.withPrecision(2),
                    false,
                    1
                )
            )

        }
        /*   val dotedLineDrawable = UIHelper.getStrokeDoted(
               VUtil.dpToPx(1),
               ThemeConstant.lightAlphaGray,
               UIHelper.i5px.toFloat(),
               UIHelper.i5px.toFloat()
           )
           val dividerLine = LinearLayout(context)
           dividerLine.layoutParams = ViewGroup.LayoutParams(ViewConst.MATCH_PARENT, VUtil.dpToPx(2))
           dividerLine.background = dotedLineDrawable
           dividerLine.setPadding(0, UIHelper.i12px, 0, UIHelper.i8px)
           this.addView(dividerLine)*/
        val dividerView =
            YumUtil.dividerView(context, ThemeConstant.lightAlphaGray, UIHelper.i1px)

        this.addView(dividerView)
        var amt: String = ""
        if (billing.totalAmount == 0.0) {
            hideInfoIcon = true
            this.isClickable = false
            amt = "₹" + billing.totalAmount.withPrecision(2) + " "
        } else {
            hideInfoIcon = false
            this.isClickable = true

            amt = "₹" + billing.totalAmount.withPrecision(2) + " "

        }
        val totalRow = getRow(
            context.resources.getString(R.string.total),
            amt,
            true,
            0
        )
        totalRow.setOnClickListener { onClickListener.onClick(totalRow) }
        this.addView(totalRow)

        val dotedLineDrawable = UIHelper.getStrokeDoted(
            VUtil.dpToPx(1),
            ThemeConstant.blueJeans,
            UIHelper.i3px.toFloat(),
            UIHelper.i3px.toFloat()
        )
        val dividerLine = LinearLayout(context)
        dividerLine.layoutParams = ViewGroup.LayoutParams(UIHelper.i40px, UIHelper.i3px)
        dividerLine.background = dotedLineDrawable
        dividerLine.setPadding(0, 0, 0, 0)
        this.addView(dividerLine)

//
//        val ditLine =
//            YumUtil.dotLineView(context, ThemeConstant.blueJeans, UIHelper.i1px)
//
//        this.addView(ditLine)


    }


    fun setPaymentType(paymentType: String) {

        this.addView(getRow(context.resources.getString(R.string.pay_via), paymentType, false, 0))
    }
/*
    private fun addView(dividerView: GradientDrawable) {
        val linearLayout = LinearLayout(context)
        linearLayout.orientation = HORIZONTAL
        linearLayout.background = dividerView

    }*/


    private fun getRow(title: String, amount: String, toBold: Boolean, toChangeColor: Int): View {

        val linearLayout = LinearLayout(context)
        linearLayout.orientation = HORIZONTAL
        linearLayout.setPadding(VUtil.dpToPx(2), VUtil.dpToPx(2), VUtil.dpToPx(2), VUtil.dpToPx(2))

        val lp = LayoutParams(0, LayoutParams.MATCH_PARENT, 1f)

        val titleView = MaterialTextView(context)
        if (toBold) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                titleView.setTextAppearance(R.style.TextAppearance_Lato_Bold)
            } else {
                titleView.setTextAppearance(context, R.style.TextAppearance_Lato_Bold)
            }
            titleView.setPadding(0, 0, UIHelper.i5px, 0)

            // titleView.setPadding(0, VUtil.dpToPx(1), 0, VUtil.dpToPx(1))
        } else {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                titleView.setTextAppearance(R.style.TextAppearance_Lato_Normal)
            } else {
                titleView.setTextAppearance(context, R.style.TextAppearance_Lato_Normal)
            }
            titleView.setPadding(0, 0, UIHelper.i5px, 0)

        }
        if (toChangeColor == 1) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                titleView.setTextAppearance(R.style.TextAppearance_Lato_Normal)
            } else {
                titleView.setTextAppearance(context, R.style.TextAppearance_Lato_Normal)
            }
            titleView.setTextColor(ThemeConstant.greenConfirmColor)
            titleView.setPadding(0, 0, UIHelper.i5px, 0)

        }
        if (toChangeColor == 2) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                titleView.setTextAppearance(R.style.TextAppearance_MyTheme_Caption)
            } else {
                titleView.setTextAppearance(context, R.style.TextAppearance_MyTheme_Caption)
            }
            titleView.setTextColor(ThemeConstant.textGrayColor)
            titleView.setPadding(UIHelper.i10px, 0, 0, UIHelper.i3px)

        }
        if (title.isNotEmpty())
            titleView.text = title
        else
            titleView.visibility = View.GONE
        titleView.id = R.id.tv_billing_title
        titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
        titleView.gravity = Gravity.START
        titleView.layoutParams = lp

//        val icon = IconMaterialTextView(context)
//        icon.text = context.resources.getString(R.string.icon_rupee)
        val lp1 = LayoutParams(100.px, LayoutParams.MATCH_PARENT)

        val priceView = MaterialTextView(context)
        if (toBold) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                priceView.setTextAppearance(R.style.TextAppearance_Lato_Bold)
            } else {
                priceView.setTextAppearance(context, R.style.TextAppearance_Lato_Bold)
            }            // priceView.setPadding(0, VUtil.dpToPx(5), 0, VUtil.dpToPx(5))

        } else {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                priceView.setTextAppearance(R.style.TextAppearance_Lato_Normal)
            } else {
                priceView.setTextAppearance(context, R.style.TextAppearance_Lato_Normal)
            }
        }
        if (toChangeColor == 1) {

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                priceView.setTextAppearance(R.style.TextAppearance_Lato_Normal)
            } else {
                priceView.setTextAppearance(context, R.style.TextAppearance_Lato_Normal)
            }
            priceView.setTextColor(ThemeConstant.greenConfirmColor)
        }

        priceView.text = amount
//        val drawable = context.resources.getDrawable(R.drawable.ic_info_icon, null)
//        if (title.equals("Total")) {
//                priceView.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null)
//        }

        priceView.id = R.id.tv_billing_subtitle
        priceView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)

        priceView.gravity = Gravity.END
        priceView.layoutParams = lp1

        linearLayout.addView(titleView)
        linearLayout.addView(priceView)

        return linearLayout

    }


}