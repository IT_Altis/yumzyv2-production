package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.laalsa.laalsalib.ui.VUtil.dpToPx
import com.laalsa.laalsaui.drawable.DrawableUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.WalletTransactionDTO
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.withPrecision
import com.yumzy.orderfood.util.IConstants


class WalletInfoCardView @JvmOverloads constructor(
    context: Context?,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private val transactionReasonView: TextView

    private val transactionIdView: TextView

    private val transactionAmountView: TextView


    init {

        this.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        this.setPadding(UIHelper.i3px, UIHelper.i3px, UIHelper.i3px, UIHelper.i3px)
        this.gravity = Gravity.CENTER_VERTICAL
        this.background = DrawableUtils.getRoundDrawable(ThemeConstant.lightGray, dpToPx(10).toFloat())

        this.orientation = HORIZONTAL


        val view = View.inflate(context, R.layout.view_wallet_transaction, this)
        transactionReasonView = view.findViewById(R.id.tv_transaction_reason)
        transactionIdView = view.findViewById(R.id.tv_transaction_id)
        transactionAmountView = view.findViewById(R.id.tv_transaction_amount)

        val a = context?.theme?.obtainStyledAttributes(
            attrs,
            R.styleable.WalletInfoCardView,
            defStyleAttr,
            0
        )


        a?.recycle()
    }

    fun setTransaction(transaction: WalletTransactionDTO?) {
        val reasons = transaction?.reason?.split(" ")?.toMutableList()
        reasons?.set(0, reasons.get(0).capitalize())
        transactionReasonView.text = reasons?.joinToString(" ")
        transactionIdView.text = "Transaction ID: ${transaction?.paymentTransactionId}"
        when (transaction?.entryType) {
            IConstants.WalletFlag.CREDIT -> {
                transactionAmountView.text = "+ ${transaction.points.withPrecision(2)}";
                transactionAmountView.setTextColor(ThemeConstant.rainbowBlue)
            }
            IConstants.WalletFlag.DEBIT -> {
                transactionAmountView.text = "- ${transaction.points.withPrecision(2)}";
                transactionAmountView.setTextColor(ThemeConstant.redCancelColor)
            }
        }
    }

}