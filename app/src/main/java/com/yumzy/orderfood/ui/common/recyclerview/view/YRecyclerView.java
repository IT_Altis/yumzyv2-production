package com.yumzy.orderfood.ui.common.recyclerview.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.yumzy.orderfood.ui.common.recyclerview.ItemTouchHelperExtension;
import com.yumzy.orderfood.ui.common.recyclerview.adapter.YMultiSelectionViewAdapter;
import com.yumzy.orderfood.ui.common.recyclerview.adapter.YRecyclerViewAdapter;
import com.yumzy.orderfood.ui.common.recyclerview.interfaces.OnLoadMoreListener;
import com.yumzy.orderfood.ui.common.recyclerview.listener.IItemSwipeListener;
import com.yumzy.orderfood.ui.common.recyclerview.listener.IItemViewHolderListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;

public class YRecyclerView<T> extends RecyclerView implements View.OnCreateContextMenuListener {
    public static final int VIEW_TYPE_SECTION = 1;// R.layout.layout_section;
    public static final int VIEW_TYPE_ITEM = 2;//R.layout.layout_item; //TODO : change this
    public static final int DELETE_ONSWIPE = 1;
    public static final int SHOW_ACTIONS_ONSWIPE = 2;
    public static final int SWIPE_LEFT_ACTION_VIEW = 3;
    public static final int SWIPE_RIGHT_ACTION_VIEW = 4;
    public static final int LEFT_SWIPE = 6;
    public static final int RIGHT_SWIPE = 7;
    public static final int SWIPE_BOTH_DIRECTIONS = 8;
    public static final int TYPE_FULL = 3;
    public static final int TYPE_HALF = 4;
    public static final int TYPE_QUARTER = 5;
    public static int SWIPE_DIRECTION = SWIPE_BOTH_DIRECTIONS;
    boolean bLastNotifiedPos = true;
    private YRecyclerViewAdapter<T> yRecyclerViewAdapter;//RecycleView Adapter Class
    private OnLoadMoreListener onLoadMoreListener;
    private IItemSwipeListener clItemSwipeListener;
    private boolean isLoading = true;
    private boolean isSwipe = false;

    //section map
    //TODO : look for a way to avoid this
    public YRecyclerView(Context context) {
        super(context);
        initRecyclerViewType(context, null, null, false, false, false);
    }

    public YRecyclerView(Context context, AttributeSet attr) {
        super(context, attr);
        initRecyclerViewType(context, null, null, false, false, false);
    }

    public YRecyclerView(Context context, ArrayList<T> viewHolderList, IItemViewHolderListener itemViewListener) {
        super(context);
        initRecyclerViewType(context, viewHolderList, itemViewListener, false, false, false);
    }

    public YRecyclerView(Context context, ArrayList<T> viewHolderList, IItemViewHolderListener itemViewListener, boolean isEnableDrag, boolean isEnableMultiSelection) {
        super(context);
        initRecyclerViewType(context, viewHolderList, itemViewListener, isEnableDrag, isEnableMultiSelection, false);
    }

    public YRecyclerView(Context context, ArrayList<T> viewHolderList, IItemViewHolderListener itemViewListener, boolean isEnableDrag, boolean isEnableMultiSelection, boolean bLoadMore) {
        super(context);
        initRecyclerViewType(context, viewHolderList, itemViewListener, isEnableDrag, isEnableMultiSelection, bLoadMore);
    }


    private void initRecyclerViewType(Context context, final ArrayList<T> mDataArrayList, IItemViewHolderListener itemViewListener, boolean isEnableDrag, boolean isEnableMultiSelection, boolean bLoadMore) {
        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        super.setLayoutManager(mLayoutManager);

        if (isEnableMultiSelection && !isEnableDrag)
            yRecyclerViewAdapter = new YMultiSelectionViewAdapter<T>(context, mDataArrayList, itemViewListener, bLoadMore);
        else
            yRecyclerViewAdapter = new YRecyclerViewAdapter<T>(context, mDataArrayList, itemViewListener, bLoadMore);

        setListAdapter(yRecyclerViewAdapter);

//        CLRecyclerDividerItemDecoration clRecyclerDividerItemDecoration=new CLRecyclerDividerItemDecoration(context, CLRecyclerDividerItemDecoration.VERTICAL_LIST);
//
//        addItemDecoration(clRecyclerDividerItemDecoration);
        setItemAnimator(new DefaultItemAnimator());
    }


    public void setListAdapter(Adapter clAdapter) {
        setAdapter(clAdapter);
    }

    public void enableSwipe(boolean isSwipe) {
        this.isSwipe = isSwipe;
    }

    public void enableSwipe(boolean isSwipe, int iDirection) {
        this.isSwipe = isSwipe;
        SWIPE_DIRECTION = iDirection;
    }


    public void setHasMoreRecords(boolean isLoadMore) {
        isLoading = isLoadMore;
    }


    public void setLayoutManager(LayoutManager layoutManager) {
        if (layoutManager instanceof StaggeredGridLayoutManager)
            yRecyclerViewAdapter.enableStaggeredLayoutManager(true);
        super.setLayoutManager(layoutManager);
    }


    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
        this.addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

//              lastVisibleItem = clRecyclerView.getLayoutManager().findLastVisibleItemPosition();
                LinearLayoutManager mLayoutManager = (LinearLayoutManager) getLayoutManager();
                if (isLoading/* && dy <= 0*/ && findChildViewUnder(dx, dy) != null) {
                    int visibleItemCount = mLayoutManager != null ? mLayoutManager.getChildCount() : 0;
                    int totalItemCount = mLayoutManager != null ? mLayoutManager.getItemCount() : 0;
                    int pastVisibleItems = mLayoutManager != null ? mLayoutManager.findFirstVisibleItemPosition() : 0;
                    if (pastVisibleItems + visibleItemCount + 3 >= getData().size()) {
                        //End of list
                        if (YRecyclerView.this.onLoadMoreListener != null) {
                            YRecyclerView.this.onLoadMoreListener.onLoadMore();
                            isLoading = false;

                        }
                    }
                }
                /*if (isSwipe && lastSwipePosition != -1)
                {
                    clRecyclerViewAdapter.setSwipeIndex(lastSwipePosition, 0);
                    CLItemViewHolder clItemViewHolder = (CLItemViewHolder) getChildViewHolder(getChildAt(lastSwipePosition));
                    clItemViewHolder.foregroundView.setTranslationX(0);
                    lastSwipePosition = -1;
                }*/
            }
        });
    }


    public void setToolbarId(int iId) {
        if (yRecyclerViewAdapter != null && yRecyclerViewAdapter instanceof YMultiSelectionViewAdapter)
            ((YMultiSelectionViewAdapter<T>) yRecyclerViewAdapter).setToolbarId(iId);
    }


    public void setActionModeEventListener(YMultiSelectionViewAdapter.IActionModeEventListener itemViewEventListener) {
        ((YMultiSelectionViewAdapter<T>) yRecyclerViewAdapter).setActionModeEventListener(itemViewEventListener);
    }

    public void setItemClickListener(IItemClickListener clItemClickListener) {
        yRecyclerViewAdapter.setItemClickListener(clItemClickListener);
    }

    public void setItemTouchListener(IItemSwipeListener itemTouchCallback) {
        clItemSwipeListener = itemTouchCallback;
        ItemTouchHelper.Callback itemTouchHelperCallback = new SwipeHelper() {
            @Override
            public void instantiateUnderlayButton(final ViewHolder viewHolder, List<SwipeViewItem> swipeItems, int iDirection) {
                if (clItemSwipeListener != null) {
                    List<YSwipeItemDTO> ySwipeItemDTOS = clItemSwipeListener.getSwipeView(iDirection, viewHolder);
                    for (int i = 0; i < ySwipeItemDTOS.size(); i++) {
                        final YSwipeItemDTO ySwipeItemDTO = ySwipeItemDTOS.get(i);
                        swipeItems.add(i, new SwipeViewItem(ySwipeItemDTO, new UnderlayButtonClickListener() {
                            @Override
                            public void onClick(int pos) {
                                clItemSwipeListener.onSwipeItemClick(ySwipeItemDTO, pos, viewHolder);
                            }
                        }));
                    }
                }
            }
        }; //For Normal swipe and Drag..
        final ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchHelperCallback);
        itemTouchHelper.attachToRecyclerView(YRecyclerView.this);
    }


/*    public List<Integer> getSelectedItems() {
        return clRecyclerViewAdapter.getSelectedItems();
    }*/


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
    }

   /* @Override
    public boolean showContextMenuForChild(View originalView) {
        final int longPressPosition = getChildAdapterPosition(originalView);
        if (longPressPosition >= 0) {
            final long longPressId = getAdapter().getItemId(longPressPosition);
            mContextMenuInfo = new RecyclerViewContextMenuInfo(longPressPosition,longPressId);
            return super.showContextMenuForChild(originalView);
        }
        return false;
    }*/

    private Animation inFromRightAnimation() {

        Animation inFromRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromRight.setDuration(500);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;
    }

/*
    @Override
    public void onStartActionMode(int position) {
        myToggleSelection(position);
    }
*/

    /*public void myToggleSelection(int idx) {
        if (enableMultiSelection) {
            if(actionMode==null) {
                ActionMode.Callback actionModeCallback = new CLActionModeListener(getContext());
                actionMode = startActionMode(actionModeCallback);
            }
            clRecyclerViewAdapter.toggleSelection(idx);
            String title = getResources().getString(R.string.selected_count, clRecyclerViewAdapter.getSelectedItemCount());
            if(clRecyclerViewAdapter.getSelectedItemCount()!=0)*//*if (actionMode != null)*//*
                actionMode.setTitle(title);
            else
                actionMode.finish();
        }
    }*/

    private Animation outToLeftAnimation(final View frontView, final View actionView, final float dX) {
        Animation outtoLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        outtoLeft.setDuration(500);
        outtoLeft.setInterpolator(new AccelerateInterpolator());
        outtoLeft.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                actionView.animate()
                        .translationX(dX)
                        .alpha(1.0f)
                        .setDuration(300)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                actionView.setVisibility(View.VISIBLE);
                            }

                        });
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                frontView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        return outtoLeft;
    }

    private Animation inFromLeftAnimation() {
        Animation inFromLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromLeft.setDuration(500);
        inFromLeft.setInterpolator(new AccelerateInterpolator());
        return inFromLeft;
    }

    private Animation outToRightAnimation() {
        Animation outtoRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        outtoRight.setDuration(500);
        outtoRight.setInterpolator(new AccelerateInterpolator());
        return outtoRight;
    }

    private void slideExit(View currentView, final View comingView, final float dX) {
        currentView.animate().
                translationXBy(-currentView.getWidth()).
                setDuration(1000).
                setInterpolator(new LinearInterpolator()).
                setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        super.onAnimationStart(animation);
                        slideFromRightToLeft(comingView, dX);
                        comingView.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void slideFromRightToLeft(View v, float dX) {
        v.animate().
                translationX(dX).
                setDuration(1000).
                setInterpolator(new LinearInterpolator());
    }

    public void addNewItem(T item) {
        if (yRecyclerViewAdapter != null)
            yRecyclerViewAdapter.addNewItem(item);

    }

    public ArrayList<T> getData() {
        return yRecyclerViewAdapter.getData();
    }

    public void setData(ArrayList<T> arrayList) {
        yRecyclerViewAdapter.setData(arrayList);
    }

    public int getSelectedItemPosition() {
        return yRecyclerViewAdapter.getiSelectedItemPosition();
    }

    public T getSelectedItem() {
        int iSelectedPos = yRecyclerViewAdapter.getiSelectedItemPosition();
        return yRecyclerViewAdapter.getData().get(iSelectedPos);
    }

    @Override
    protected void onConfigurationChanged(Configuration newConfig) {
        if (yRecyclerViewAdapter != null) yRecyclerViewAdapter.notifyDataSetChanged();
    }

    public interface IItemClickListener {
        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }


    public interface UnderlayButtonClickListener {
        void onClick(int pos);
    }

    public static class RecyclerViewContextMenuInfo implements ContextMenu.ContextMenuInfo {

        final public int position;
        final public long id;

        public RecyclerViewContextMenuInfo(int position, long id) {
            this.position = position;
            this.id = id;
        }
    }

    public abstract class SwipeHelper extends ItemTouchHelper.Callback {

        private int BUTTON_WIDTH = 120;

        private GestureDetector gestureDetector;
        private Queue<Integer> recoverQueue;

        private Map<Integer, List<SwipeViewItem>> swipeViewItems = null;

        private int swipedPos = -1;
        private float swipeThreshold = 0.02f;
        private int iLastDirection = -1;

        private boolean ENABLE_DRAG;

        private GestureDetector.SimpleOnGestureListener gestureListener = new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                for (SwipeViewItem button : swipeViewItems.get(swipedPos)) {
                    if (button.onClick(e.getX(), e.getY()))
                        break;
                }

                return true;
            }
        };
        private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent e) {
                if (swipedPos < 0) return false;
                Point point = new Point((int) e.getRawX(), (int) e.getRawY());

                RecyclerView.ViewHolder swipedViewHolder = YRecyclerView.this.findViewHolderForAdapterPosition(swipedPos);
                View swipedItem = swipedViewHolder.itemView;
                Rect rect = new Rect();
                swipedItem.getGlobalVisibleRect(rect);

                if (e.getAction() == MotionEvent.ACTION_DOWN || e.getAction() == MotionEvent.ACTION_UP || e.getAction() == MotionEvent.ACTION_MOVE) {
                    if (rect.top < point.y && rect.bottom > point.y)
                        gestureDetector.onTouchEvent(e);
                    else {
                        recoverQueue.add(swipedPos);
                        swipedPos = -1;
                        recoverSwipedItem();
                    }
                }
                return false;
            }
        };

        public SwipeHelper() {
            swipeViewItems = new HashMap<>();
            this.gestureDetector = new GestureDetector(getContext(), gestureListener);
            YRecyclerView.this.setOnTouchListener(onTouchListener);
            recoverQueue = new LinkedList<Integer>() {
                @Override
                public boolean add(Integer o) {
                    if (contains(o))
                        return false;
                    else
                        return super.add(o);
                }
            };

//            attachSwipe();
        }

        @Override
        public boolean isLongPressDragEnabled() {
            return ENABLE_DRAG;
        }

        @Override
        public boolean isItemViewSwipeEnabled() {
            return isSwipe;
        }

        @Override
        public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
            int dragFlags, swipeFlags;
            {
                dragFlags = ItemTouchHelperExtension.UP | ItemTouchHelperExtension.DOWN;
                if (SWIPE_DIRECTION == YRecyclerView.SWIPE_BOTH_DIRECTIONS)
                    swipeFlags = ItemTouchHelperExtension.START | ItemTouchHelperExtension.END;
                else if (SWIPE_DIRECTION == YRecyclerView.RIGHT_SWIPE)
                    swipeFlags = ItemTouchHelperExtension.START;
                else
                    swipeFlags = ItemTouchHelperExtension.END;
            }

            // final int swipeFlags =  ItemTouchHelper.END | ItemTouchHelper.START; Enable swipe in both direction
            return makeMovementFlags(dragFlags, swipeFlags);
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, RecyclerView.ViewHolder source, RecyclerView.ViewHolder target) {
            if (source.getItemViewType() != target.getItemViewType()) {
                return false;
            }
            // Notify the adapter of the move
            if (recyclerView.getAdapter() instanceof YRecyclerViewAdapter) {
                ((YRecyclerViewAdapter) recyclerView.getAdapter()).onMove(source, target);
            }
            if (clItemSwipeListener != null)
                clItemSwipeListener.onMove(recyclerView, source, target);
            return false;

        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            int pos = viewHolder.getAdapterPosition();

            if (swipedPos != pos)
                recoverQueue.add(swipedPos);

            swipedPos = pos;

        /*if (swipeViewItems.containsKey(swipedPos))
            buttons = swipeViewItems.get(swipedPos);
        else
            buttons.clear();*/

//        swipeViewItems.clear();
            if (swipeViewItems.get(swipedPos) != null)
                swipeThreshold = 0.1f * Objects.requireNonNull(swipeViewItems.get(swipedPos)).size() * BUTTON_WIDTH;
            recoverSwipedItem();
            if (clItemSwipeListener != null)
                clItemSwipeListener.onSwiped((YRecyclerViewAdapter) YRecyclerView.this.getAdapter(), viewHolder, direction);
        }

        @Override
        public float getSwipeThreshold(RecyclerView.ViewHolder viewHolder) {
            return swipeThreshold;
        }

        /*  @Override
          public float getSwipeEscapeVelocity(float defaultValue)
          {
              return 0.1f * defaultValue;
          }

          @Override
          public float getSwipeVelocityThreshold(float defaultValue)
          {
              return 5.0f * defaultValue;
          }
  */
        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            int pos = viewHolder.getAdapterPosition();
            float translationX = dX;
            View itemView = viewHolder.itemView;

            if (pos < 0) {
                swipedPos = pos;
                return;
            }

            if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                int iDirection = -1;

                if (dX > 0) {
                    iDirection = 1;
                } else if (dX < 0) {
                    iDirection = 0;
                }

                if (iDirection != -1) {
                    List<SwipeViewItem> clBuffer = new ArrayList<>();
                    if (!swipeViewItems.containsKey(pos) || iLastDirection != iDirection) {
                        iLastDirection = iDirection;
                        instantiateUnderlayButton(viewHolder, clBuffer, iDirection);
                        swipeViewItems.put(pos, clBuffer);
                    } else
                        clBuffer = swipeViewItems.get(pos);

                    int iItemsWidth = 0;
                    for (SwipeViewItem aClBuffer : clBuffer) {
                        iItemsWidth = iItemsWidth + aClBuffer.iWidth;

                    }
                    if (iItemsWidth > 0) {
                        translationX = dX * clBuffer.size() * iItemsWidth / itemView.getWidth();
                        BUTTON_WIDTH = iItemsWidth;
                    } else
                        translationX = dX * clBuffer.size() * BUTTON_WIDTH / itemView.getWidth();
                    drawButtons(c, itemView, clBuffer, pos, translationX, iDirection);
                    if (swipedPos != pos)
                        swipeThreshold = 0.02f;
                }
            }

            super.onChildDraw(c, recyclerView, viewHolder, translationX, dY, actionState, isCurrentlyActive);
        }

        private synchronized void recoverSwipedItem() {
            while (!recoverQueue.isEmpty()) {
                int pos = recoverQueue.poll();
                if (pos > -1) {
                    swipeViewItems.clear();
                    YRecyclerView.this.getAdapter().notifyItemChanged(pos);
                }
            }
        }

        private void drawButtons(Canvas c, View itemView, List<SwipeViewItem> buffer, int pos, float dX, int iDirection) {
            float left = 0, right = 0;
            float dButtonWidth;


            if (iDirection == 0) {
                dButtonWidth = (-1) * dX / buffer.size();
                right = itemView.getRight();

            } else {
                dButtonWidth = dX / buffer.size();
                left = itemView.getLeft();
            }

            for (SwipeViewItem button : buffer) {
                if (iDirection == 0)
                    left = right - dButtonWidth;
                else
                    right = left + dButtonWidth;

                button.onDraw(
                        c,
                        new RectF(
                                left,
                                itemView.getTop() - 2,
                                right,
                                itemView.getBottom() - 2
                        ),
                        pos
                );

                if (iDirection == 0) right = left;
                else left = right;
            }
        }

        public void attachSwipe() {
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(this);
            itemTouchHelper.attachToRecyclerView(YRecyclerView.this);
        }

        public abstract void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<SwipeViewItem> swipeItems, int iDirection);


    }

    public class SwipeViewItem {
        private String text;
        private int imageResId;
        private int color;
        private int pos;
        private int iWidth;
        private RectF clickRegion;
        private UnderlayButtonClickListener clickListener;

        public SwipeViewItem(YSwipeItemDTO ySwipeItemDTO, UnderlayButtonClickListener clickListener) {
            this.text = ySwipeItemDTO.getText();
            this.imageResId = ySwipeItemDTO.getImageResId();
            this.color = ySwipeItemDTO.getColor();
            this.iWidth = ySwipeItemDTO.getWidth();
            this.clickListener = clickListener;
        }

        public boolean onClick(float x, float y) {
            if (clickRegion != null && clickRegion.contains(x, y)) {
                clickListener.onClick(pos);
                return true;
            }

            return false;
        }

        public void onDraw(Canvas c, RectF rect, int pos) {
            Paint clPaint = new Paint();

            // Draw background
            Typeface tf = Typeface.create("Helvetica", Typeface.BOLD);
            clPaint.setTypeface(tf);
            clPaint.setColor(color);
            c.drawRect(rect, clPaint);
            clPaint.setStyle(Paint.Style.FILL);
            // Draw Text
            clPaint.setColor(Color.WHITE);
            clPaint.setTextSize(18);

            Rect r = new Rect();
            float cHeight = rect.height();
            float cWidth = rect.width();


            float x = cWidth / 2f - r.width() / 2f - r.left;
            float y = cHeight / 2f + r.height() / 2f - r.bottom;

            if (imageResId > 0) {
                Bitmap bitmap = BitmapFactory.decodeResource(getContext().getResources(), imageResId);
                c.drawBitmap(bitmap, rect.left + (x / 2), rect.top + (y / 2), clPaint);
            } else {
                clPaint.getTextBounds(text, 0, text.length(), r);
                clPaint.setTextAlign(Paint.Align.CENTER);
                c.drawText(text, rect.left + x, rect.top + y, clPaint);
            }

            clickRegion = rect;
            this.pos = pos;
        }

    }
}

