package com.yumzy.orderfood.ui.component.shadowcard

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.core.view.isGone
import com.google.android.material.card.MaterialCardView
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.LayoutAddressCardBinding
import com.yumzy.orderfood.ui.helper.hideEmptyTextView
import com.yumzy.orderfood.ui.helper.invisibleEmptyTextView

class DetailsCardView : MaterialCardView, View.OnClickListener {
    var title: CharSequence? = ""
        set(value) {
            field = value
            binding.title.invisibleEmptyTextView(field)

        }
    var action: CharSequence? = ""
        set(value) {
            field = value
            binding.action.hideEmptyTextView(field)

        }
    var body: CharSequence? = ""
        set(value) {
            field = value
            binding.body.hideEmptyTextView(field)
        }
    var rightIcon: Int? = R.string.icon_delete
        set(value) {
            field = value
            if (field == null)
                binding.actionTop.visibility = GONE
            else {
                binding.actionTop.text = context.getString(field!!)
            }
        }

    var isDeliveryAddress: Boolean = false
        set(value) {
            field = value
            binding.tvSelectedAddress.isGone = !field

        }


    private val binding by lazy {
        val layoutInflater = LayoutInflater.from(context)
        LayoutAddressCardBinding.inflate(layoutInflater, this, true)
    }

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {


        if (attrs != null) {
            val a =
                context.obtainStyledAttributes(
                    attrs,
                    R.styleable.DetailsCardView,
                    defStyleAttr,
                    0
                )
            this.body = a.getString(R.styleable.DetailsCardView_cardTitle) ?: ""
            this.title = a.getString(R.styleable.DetailsCardView_cardBody) ?: ""
            this.action = a.getString(R.styleable.DetailsCardView_actionText) ?: ""


            binding.title.invisibleEmptyTextView(title)
            binding.body.hideEmptyTextView(body)
            binding.action.hideEmptyTextView(action)
            binding.root.setOnClickListener(this)
            binding.actionTop.setOnClickListener(this)
            binding.action.setOnClickListener(this)
            a.recycle()
        }
//        this.background = UIHelper.getStrokeDrawable(ThemeConstant.lightGray, 10.pxf, 2.px)
    }

    var detailsCardListener: DetailsCardListener? = null

    interface DetailsCardListener {
        fun onDetailsCard(tag: Any)
        fun onAction(tag: Any)
        fun onRightIcon(tag: Any)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.action -> detailsCardListener?.onAction(this@DetailsCardView.tag)
            R.id.action_top -> detailsCardListener?.onRightIcon(this@DetailsCardView.tag)
            else -> detailsCardListener?.onDetailsCard(this@DetailsCardView.tag)
        }

    }

    fun setTitleDrawable(drawable: Drawable) {
        binding.title.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null)
    }

    fun setBodyDrawableRight(drawable: Drawable) {
        binding.body.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null)
    }

}