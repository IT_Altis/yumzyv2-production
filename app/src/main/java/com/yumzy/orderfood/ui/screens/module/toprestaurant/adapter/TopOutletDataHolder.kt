package com.yumzy.orderfood.ui.screens.module.toprestaurant.adapter

import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.OfferDTO
import com.yumzy.orderfood.ui.component.HorizontalCardLayout
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.mikepenz.fastadapter.ui.utils.StringHolder


open class TopOutletDataHolder : AbstractItem<TopOutletDataHolder.ViewHolder>() {

    var header: String? = null
    var name: StringHolder? = null
    var description: StringHolder? = null

    override val type: Int
        get() = R.id.top_outlet_list

    override val layoutRes: Int
        get() = R.layout.adapter_restaurant

    val offers: List<OfferDTO>?=null
    val outletName: String?=null
    val outletId: String?=null
    val imageUrl: String?=null
    val locality: String?=null
    val haveAvailableItems: Boolean?=null
    val cuisines: List<String>?=null

    fun withName(Name: String): TopOutletDataHolder {
        this.name = StringHolder(Name)
        return this
    }



    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    class ViewHolder(private var view: View) : FastAdapter.ViewHolder<TopOutletDataHolder>(view) {
        var name: TextView = view.findViewById(R.id.tv_title)
        var description: TextView = view.findViewById(R.id.tv_subtitle)

        override fun bindView(item: TopOutletDataHolder, payloads: List<Any>) {
            //get the context
            val ctx = itemView.context
            val constraintLayout = view as ConstraintLayout
//            val horizontalView =
//                constraintLayout.findViewById<HorizontalCardLayout>(R.id.horizontal_view)
//            horizontalView.imageUrl = item.imageUrl ?: ""
//            horizontalView.title = item.outletName ?: ""
////            horizontalView.subTitle =
////                item.cuisines.joinToString(separator = ", ", postfix = " ")
//
////            horizontalView.setClickListenerBackground()
//            horizontalView.setOnClickListener {
////                clickListener(listElement.outletId)
//            }

            StringHolder.applyTo(item.name, name)
            //set the text for the description or hide
            StringHolder.applyToOrHide(item.description, description)
        }

        override fun unbindView(item: TopOutletDataHolder) {
            name.text = null
            description.text = null
        }
    }
}
