package com.yumzy.orderfood.ui.screens.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.ModelAbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.databinding.ViewPopularFoodBinding
import com.yumzy.orderfood.ui.helper.appendPriceValue
import com.yumzy.orderfood.ui.helper.itemType
import com.yumzy.orderfood.util.viewutils.YumUtil
import java.util.*

class PopularFoodItem(homeListDTO: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, ViewPopularFoodBinding>(homeListDTO) {
    override val type = R.id.popular_food_item

    override fun bindView(binding: ViewPopularFoodBinding, payloads: List<Any>) {
        YumUtil.setImageInView(binding.imageView, model.image?.url ?: "")
        binding.viewTitle.text = model.title.capitalize(Locale.getDefault())
        binding.viewSubtitle.text = model.subTitle.capitalize(Locale.getDefault())
        binding.vegNonVeg.itemType(model.meta?.isVeg ?: true)
        binding.tvPrice.appendPriceValue(model.meta?.price)
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ViewPopularFoodBinding {
        return ViewPopularFoodBinding.inflate(inflater, parent, false)
    }


}
