package com.yumzy.orderfood.ui.screens.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.ModelAbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.databinding.ViewSectionTitleBinding
import com.yumzy.orderfood.util.IConstants

class SectionTitleItem(homeListDTO: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, ViewSectionTitleBinding>(homeListDTO) {
    override val type = R.id.secton_title_item_id
    override fun bindView(binding: ViewSectionTitleBinding, payloads: List<Any>) {
        binding.title = model.heading
        binding.subtitle = model.subHeading
        val layoutType = model.layoutType
        val b = !(
//                layoutType == IConstants.LayoutType.Banner1000.THE_BRANDS ||
                layoutType == IConstants.LayoutType.Dish3000.WINTER_SPECIAL ||
                        layoutType == IConstants.LayoutType.Banner1000.POPULAR_CUISINES
                )
        binding.hideSeeAll = if (b) View.VISIBLE else View.GONE
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ViewSectionTitleBinding {
        return ViewSectionTitleBinding.inflate(inflater, parent, false)
    }
}

