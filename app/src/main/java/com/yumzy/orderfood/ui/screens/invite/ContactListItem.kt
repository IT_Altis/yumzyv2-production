package com.yumzy.orderfood.ui.screens.invite

import android.view.LayoutInflater
import android.view.ViewGroup
import com.laalsa.laalsalib.utilcode.util.ClickUtils
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.ContactModelDTO
import com.yumzy.orderfood.databinding.AdapterContactInfoBinding
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.setClickScaleEffect
import com.yumzy.orderfood.util.IConstants

/**
 * Created by Shriom Tripathi on 11-Sept-20.
 */
class ContactListItem : AbstractBindingItem<AdapterContactInfoBinding>() {

    private var colorIndex: Int = 0

    private var inviteListener: IInvite? = null

    var contactModel: ContactModelDTO? = null

    private val colorList = listOf<Int>(
        ThemeConstant.sunglowYellow,
        ThemeConstant.crayolaOrange,
        ThemeConstant.frenchPink,
        ThemeConstant.normalVoilet,
        ThemeConstant.crayolaGreen
    )

    override val type: Int
        get() = R.id.item_contact

    override fun bindView(binding: AdapterContactInfoBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)

        binding.contact = contactModel
        binding.tvInvite.setClickScaleEffect()


        val nameChar = if (contactModel?.name?.length ?: 0 > 0) {
            contactModel?.name?.get(0) ?: '#'
        } else {
            contactModel?.name ?: '#'
        }
        binding.mliContact.letter =
            if (contactModel?.type.equals("referral")) nameChar
                .toString() else nameChar.toString()
        val other = colorList.size
        val index = colorIndex % other
        binding.mliContact.shapeColor = colorList[index]

        ClickUtils.applyGlobalDebouncing(
            binding.tvInvite,
            IConstants.AppConstant.CLICK_DELAY
        ) {
            contactModel?.let {
                inviteListener?.onInviteClick(it)
            }

        }

    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterContactInfoBinding {
        return AdapterContactInfoBinding.inflate(inflater, parent, false)
    }

    fun withContact(contactModel: ContactModelDTO): ContactListItem {
        this.contactModel = contactModel
        return this
    }

    fun withColor(colorIndex: Int): ContactListItem {
        this.colorIndex = colorIndex
        return this
    }

    fun withListener(listener: IInvite): ContactListItem {
        this.inviteListener = listener
        return this
    }

}