/*
 * Created By Shriom Tripathi 1 - 5 - 2020
 */

package com.yumzy.orderfood.ui.component.groupview

import com.yumzy.orderfood.ui.component.groupview.GroupItemActions

interface OnGroupItemClickListener<T> {
    fun onGroupItemClicked(view: GroupItemActions<*>)
}