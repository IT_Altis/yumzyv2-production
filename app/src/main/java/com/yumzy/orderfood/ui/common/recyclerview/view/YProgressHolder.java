package com.yumzy.orderfood.ui.common.recyclerview.view;

import android.view.View;
import android.widget.ProgressBar;

import com.yumzy.orderfood.R;

public class YProgressHolder extends YItemViewHolder {

    public ProgressBar clLoadMoreProgress;

    public YProgressHolder(View v) {
        super(v);
        clLoadMoreProgress = v.findViewById(R.id.progress_indicator);
    }

}