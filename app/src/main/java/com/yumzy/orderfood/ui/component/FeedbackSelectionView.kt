/*
 * Created By Shriom Tripathi 1 - 5 - 2020
 */

package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.view.View
import com.google.android.material.textview.MaterialTextView
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsaui.drawable.DrawableUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.component.groupview.GroupItemActions
import com.yumzy.orderfood.ui.component.groupview.OnGroupItemClickListener
import com.yumzy.orderfood.ui.helper.ThemeConstant


class FeedbackSelectionView :MaterialTextView, GroupItemActions<FeedbackSelectionView>,
    View.OnClickListener {

    private lateinit var mOnGroupItemClickListener: OnGroupItemClickListener<FeedbackSelectionView>

    private val i4px = VUtil.dpToPx(4)
    private val i6px = VUtil.dpToPx(6)
    private val i8px = VUtil.dpToPx(8)


    constructor(context: Context) : this(context, null, 0)

    constructor(context: Context, atr: AttributeSet?) : this(context, atr, 0)

    constructor(context: Context, atr: AttributeSet?, style: Int) : super(context, atr, style) {
        this.setOnClickListener(this)
        this.setPadding(i8px, i4px, i8px, i4px)
        this.background = DrawableUtils.getRoundStrokeDrawableListState(
            ThemeConstant.white,
            10f,
            ThemeConstant.textGrayColor,
            10f,
            ThemeConstant.lightAlphaGray, 2
        )
          if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            this.setTextAppearance(R.style.TextAppearance_MyTheme_Caption)
        } else {
            this.setTextAppearance(context, R.style.TextAppearance_MyTheme_Caption)
        }


    }

    override var mIsSelected: Boolean = false

        set(value) {
            if (value) {
                setSelectedBackground()
            } else {
                setDeselectedBackground()
            }
            field = value
        }


    override fun setSelectedBackground() {

        this.background = DrawableUtils.getRoundStrokeDrawableListState(
            ThemeConstant.greenConfirmColor,
            10f,
            ThemeConstant.lightAlphaGray,
            10f,
            ThemeConstant.greenConfirmColor, 2
        )
          if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            this.setTextAppearance(R.style.TextAppearance_MyTheme_Caption)
        } else {
            this.setTextAppearance(context, R.style.TextAppearance_MyTheme_Caption)
        }
        this.setTextColor(ThemeConstant.white)


    }


    override fun setDeselectedBackground() {

        this.background = DrawableUtils.getRoundStrokeDrawableListState(
            ThemeConstant.white,
            10f,
            ThemeConstant.textGrayColor,
            10f,
            ThemeConstant.lightAlphaGray, 2
        )
          if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            this.setTextAppearance(R.style.TextAppearance_MyTheme_Caption)
        } else {
            this.setTextAppearance(context, R.style.TextAppearance_MyTheme_Caption)
        }
        this.setTextColor(ThemeConstant.textBlackColor)


    }


    override fun setOnItemClickListener(value: OnGroupItemClickListener<FeedbackSelectionView>) {
        mOnGroupItemClickListener = value
    }

    override fun compareTo(other: GroupItemActions<*>): Int {
        val condition = this.text == (other as FeedbackSelectionView).text
        return if(condition) {
            1
        }else {
            0
        }
    }

    override fun onClick(v: View?) {
        mOnGroupItemClickListener.onGroupItemClicked(this)
    }
}