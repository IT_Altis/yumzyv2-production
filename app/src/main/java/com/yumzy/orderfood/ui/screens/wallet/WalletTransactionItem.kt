package com.yumzy.orderfood.ui.screens.wallet

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.WalletTransactionDTO
import com.yumzy.orderfood.databinding.AdapterWalletTransctionBinding
import com.yumzy.orderfood.util.dateformater.UTCtoNormalDate

/**
 * Created by Bhupendra Kumar Sahu on 07-Aug-20.
 */
class WalletTransactionItem : AbstractBindingItem<AdapterWalletTransctionBinding>() {

    var transcation: WalletTransactionDTO? = null
    var context: WalletActivity? = null
    var sectionTitle: String? = ""
    var sectionId: Long = 0L

    lateinit var clickedListener: (transaction: WalletTransactionDTO?) -> Unit

    override val type: Int
        get() = R.id.top_outlet_list

    override fun bindView(binding: AdapterWalletTransctionBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)

        transcation?.createdOn?.let {
            if (UTCtoNormalDate.isToday(it) == true) {
                sectionTitle = context?.getString(R.string.today)
            } else if (UTCtoNormalDate.isYesterday(it) == true) {
                sectionTitle = context?.getString(R.string.yesterday)
            } else {
                sectionTitle =
                    UTCtoNormalDate.getDate(it)
            }

            sectionId = UTCtoNormalDate.getDateMillis(it)
        }


        binding.walletinfoView.setTransaction(transcation)

    }


    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterWalletTransctionBinding {
        return AdapterWalletTransctionBinding.inflate(inflater, parent, false)
    }
}