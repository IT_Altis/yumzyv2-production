package com.yumzy.orderfood.ui.screens.home.adapter

import android.content.Context
import android.database.Cursor
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.cursoradapter.widget.CursorAdapter
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.helper.UIHelper

class ExploreCursorAdapter(
    val context: Context,
    cursor: Cursor?,
    sv: SearchView
) : CursorAdapter(context, cursor, false) {
    private val mLayoutInflater: LayoutInflater
    private val searchView: SearchView
    override fun newView(
        context: Context,
        cursor: Cursor,
        parent: ViewGroup
    ): View {
//        View v = mLayoutInflater.inflate(R.layout.deal_simple_item_layout, parent, false);,
        val suggestionLayout = LinearLayout(context)
        suggestionLayout.orientation = LinearLayout.VERTICAL
        suggestionLayout.setPadding(UIHelper.i8px, UIHelper.i8px, UIHelper.i8px, UIHelper.i8px)

        val itemView = TextView(context)
        itemView.text = "Hello"
        itemView.id = R.id.tv_title

        suggestionLayout.addView(itemView)

        return suggestionLayout
    }

    override fun bindView(
        view: View,
        context: Context,
        cursor: Cursor
    ) {
        val deal = cursor.getString(cursor.getColumnIndexOrThrow("deal"))
        val cashback = cursor.getString(cursor.getColumnIndexOrThrow("cashback"))
        val item = view.findViewById<TextView>(R.id.tv_title)

        view.setOnClickListener { _ ->
            searchView.isIconified = true
            Toast.makeText(
                context, "Selected suggestion " + item.text,
                Toast.LENGTH_LONG
            ).show()
        }
    }

    init {
        mContext = context
        searchView = sv
        mLayoutInflater = LayoutInflater.from(context)
    }
}