package com.yumzy.orderfood.ui.screens.cart

import com.yumzy.orderfood.api.BaseDataSource
import com.yumzy.orderfood.api.CartServiceAPI
import com.yumzy.orderfood.data.models.ItemCountDTO
import com.yumzy.orderfood.data.models.NewAddItemDTO

class CartRemoteDataSource constructor(private val serviceAPI: CartServiceAPI) :
    BaseDataSource() {

    suspend fun addToCart(addCart: NewAddItemDTO) = getResult {
        serviceAPI.addToCart(addCart)
    }

    suspend fun changeItemCount(
        position: Int,
        count: Int,
        itemCount: ItemCountDTO
    ) = getResult {
        serviceAPI.itemCount(position, count, itemCount)
    }

    suspend fun getCart(cartId: String) = getResult {
        serviceAPI.getCart(cartId)
    }


    suspend fun clearCart(cartId: String) = getResult {
        val cartMap = mapOf(
            "cartId" to  cartId
        )
        serviceAPI.clearCart(cartMap)
    }

}