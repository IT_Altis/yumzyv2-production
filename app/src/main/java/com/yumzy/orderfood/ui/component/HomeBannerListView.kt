package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.ui.helper.setHorizontalListDecoration
import com.yumzy.orderfood.ui.screens.home.fragment.globalHomeItemListener
import com.yumzy.orderfood.ui.screens.restaurant.adapter.BindingHomeBannerItem
import com.yumzy.orderfood.ui.screens.restaurant.adapter.HomeOfferItem
import com.yumzy.orderfood.util.SnapHelperOneByOne
import com.yumzy.orderfood.util.ViewConst

//import me.everything.android.ui.overscroll.OverScrollDecoratorHelper

class HomeBannerListView : ConstraintLayout {

    //save our FastAdapter
    private var fastItemAdapter: FastItemAdapter<BindingHomeBannerItem> = FastItemAdapter()
    private var recyclerView: RecyclerView
    init {

        //configure our fastAdapter
        fastItemAdapter.onClickListener =
            { v: View?, _: IAdapter<BindingHomeBannerItem>, item: BindingHomeBannerItem, _: Int ->
                if (v != null) {
                    globalHomeItemListener?.invoke(v, item.model)
                }
                false
            }


        recyclerView = RecyclerView(context)
        //get our recyclerView and do basic setup
        recyclerView.id = R.id.item_home_banner_recycler
        recyclerView.overScrollMode = OVER_SCROLL_NEVER
        recyclerView.adapter = fastItemAdapter
        recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        recyclerView.onFlingListener = null
        recyclerView.setHorizontalListDecoration()
        val linearSpanHelperOneByOne = SnapHelperOneByOne()
        linearSpanHelperOneByOne.attachToRecyclerView(recyclerView)

    }


    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        this.clipToPadding = false
        this.clipChildren = false
        recyclerView.layoutParams =
            LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT).apply {
                startToStart = LayoutParams.PARENT_ID
                endToEnd = LayoutParams.PARENT_ID
                topToTop = LayoutParams.PARENT_ID
            }
//        this.layoutParams = LayoutParams(ViewConst.MATCH_PARENT, 250)
        this.addView(recyclerView)

    }


    fun clearData() {
        fastItemAdapter.clear()
        this.visibility = View.GONE
    }

    fun setBannerData(homeListData: ArrayList<HomeListDTO>?) {
        if (homeListData.isNullOrEmpty()) {
            this.visibility = View.GONE
            return
        }
        this.visibility = View.VISIBLE
        val items = ArrayList<BindingHomeBannerItem>()
        homeListData.forEach { homeItem ->
            homeItem.let { item ->
                items.add(
                    BindingHomeBannerItem(item)
                )
            }
        }

        fastItemAdapter.add(items)
    }

}

class OfferAdsView : ConstraintLayout {

    //save our FastAdapter
    private var fastItemAdapter: FastItemAdapter<HomeOfferItem> = FastItemAdapter()

    private var recyclerView: RecyclerView


    init {

        //configure our fastAdapter
        fastItemAdapter.onClickListener =
            { v: View?, _: IAdapter<HomeOfferItem>, item: HomeOfferItem, _: Int ->
                v?.let {
                    globalHomeItemListener?.invoke(it, item.model)
                }
                false
            }

        recyclerView = RecyclerView(context)
        //get our recyclerView and do basic setup
        recyclerView.id = R.id.item_home_banner_recycler
        recyclerView.overScrollMode = OVER_SCROLL_NEVER
        recyclerView.adapter = fastItemAdapter
        recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        recyclerView.onFlingListener = null
        LinearSnapHelper().attachToRecyclerView(recyclerView)
//        recyclerView.addItemDecoration(LinearMarginDecoration(orientation = RecyclerView.HORIZONTAL, leftMargin = 100.px,rightMargin = 25.px))

//        val linearSpanHelperOneByOne = SnapHelperOneByOne()
//        linearSpanHelperOneByOne.attachToRecyclerView(recyclerView)

    }


    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        this.clipToPadding = false
        this.clipChildren = false
        recyclerView.layoutParams = LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        this.addView(recyclerView)

    }
    fun clearData() {
        fastItemAdapter.clear()
        this.visibility = View.GONE
    }

    fun setData(homeListData: ArrayList<HomeListDTO>?) {
        if (homeListData.isNullOrEmpty()) {
            this.visibility = View.GONE
            return
        }
        this.visibility = View.VISIBLE
        val items = ArrayList<HomeOfferItem>()
        homeListData.forEach { homeItem ->
            homeItem.let { item ->
                items.add(
                    HomeOfferItem(item)
                )
            }
        }

        fastItemAdapter.add(items)
    }

}