package com.yumzy.orderfood.ui.screens.home.adapter;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.yumzy.orderfood.R;
import com.yumzy.orderfood.data.models.HomeListDTO;
import com.yumzy.orderfood.util.YUtils;
import com.yumzy.orderfood.util.viewutils.YumUtil;
import com.zhpan.bannerview.BaseViewHolder;

public class BannerViewHolder extends BaseViewHolder<HomeListDTO> {

    public BannerViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    @Override
    public void bindData(HomeListDTO data, int position, int pageSize) {
        ImageView imageView = findView(R.id.banner_view);
        String imageUrl = data.getImage().getUrl();
        YUtils.INSTANCE.setImageInView(imageView, imageUrl);
    }
}