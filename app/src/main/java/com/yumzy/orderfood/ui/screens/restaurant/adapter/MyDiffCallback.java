package com.yumzy.orderfood.ui.screens.restaurant.adapter;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import com.yumzy.orderfood.data.models.RestaurantItem;

import java.util.List;

public class MyDiffCallback extends DiffUtil.Callback {

    List<RestaurantItem> oldPersons;
    List<RestaurantItem> newPersons;

    public MyDiffCallback(List<RestaurantItem> newPersons, List<RestaurantItem> oldPersons) {
        this.newPersons = newPersons;
        this.oldPersons = oldPersons;
    }

    @Override
    public int getOldListSize() {
        return oldPersons.size();
    }

    @Override
    public int getNewListSize() {
        return newPersons.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldPersons.get(oldItemPosition).getItemId() == newPersons.get(newItemPosition).getItemId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldPersons.get(oldItemPosition).equals(newPersons.get(newItemPosition));
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        //you can return particular field for changed item.
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}