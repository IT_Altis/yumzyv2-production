package com.yumzy.orderfood.ui.component

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.SpannableString
import android.text.TextUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.laalsa.laalsalib.ui.VUtil
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.common.imageview.RoundedImageView
import com.yumzy.orderfood.ui.helper.*
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.YUtils
import com.yumzy.orderfood.util.viewutils.YumUtil
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView


/**
 *    Created by Sourav Kumar Pandit
 * */
class HorizontalCardLayout : ConstraintLayout {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {

        initAttrs(attrs, context, defStyleAttr)
        initView()
    }


    private lateinit var discountDrawable: Drawable
    var colorTextColor = ThemeConstant.pinkies
        set(value) {
            field = value
            this.findViewById<FontIconView>(R.id.tv_coupon_code)?.setTextColor(colorTextColor)
        }

    fun colorTextSize(textSize: Float) {
        val textColor =
            this.findViewById<FontIconView>(R.id.tv_coupon_code)
        textColor?.textSize = textSize

    }

    var availableRestaurant: Boolean = true
        set(value) {
            field = value
            if (value)
                this.alpha = 0.5f
            else
                this.alpha = 1f

        }
    var icon: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.font_icon_view)?.text = field
        }
    var cusisines: String? = ""
        set(value) {
            field = value
            val findViewById = this.findViewById<TextView>(R.id.tv_subtitle)
            if (value.isNullOrEmpty()) {
                findViewById?.visibility = View.GONE
            } else {
                findViewById?.visibility = View.VISIBLE
                findViewById?.text = value

            }


        }
    var locality: String? = ""
        set(value) {
            val tvLocality = this.findViewById<TextView>(R.id.tv_locality)
            field = value
            if (value.isNullOrEmpty()) {
                tvLocality?.visibility = View.GONE
            } else {
                tvLocality?.visibility = View.VISIBLE
                tvLocality?.text = value
            }

        }
    var title: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.tv_title)?.text = field
        }

    var colorText: String? = ""
        set(value) {
            field = value
            val findViewById = this.findViewById<TextView>(R.id.tv_coupon_code)
            if (colorText.isNullOrEmpty()) {
                findViewById?.visibility = View.GONE
            } else {
                findViewById?.visibility = View.VISIBLE
                findViewById?.text = field
            }


        }

    var bottomText: String = ""
        set(value) {
            field = value
            val tvBottomText = this.findViewById<TextView>(R.id.tv_time_price)
            if (value.isEmpty()) {
                tvBottomText?.visibility = View.GONE
            } else {
                tvBottomText?.visibility = View.VISIBLE
                tvBottomText?.text = value

            }
        }

    var imageUrl: String = ""
        set(value) {
            field = value
            val imageView = this.findViewById<ImageView>(R.id.restaurant_image)
//            Glide.with(imageView).load(imageUrl).into(imageView)
            YUtils.setImageInView(imageView, imageUrl)

        }

    var imageSize: Int = 95
        set(value) {
            field = value

            val roundImage = this.findViewById<RoundedImageView>(R.id.restaurant_image) ?: return
            val imageParam =
                LayoutParams(imageSize, 0)
            imageParam.topToTop = LayoutParams.PARENT_ID
            imageParam.startToStart = LayoutParams.PARENT_ID
            imageParam.dimensionRatio = "1:1"

            roundImage.layoutParams = imageParam

        }
    var rating: String = ""
        set(value) {
            field = value
            val ratingView = this.findViewById<RatingView>(R.id.outlet_item_rating)
            ratingView.setRating(value)
        }


    private fun initAttrs(
        attrs: AttributeSet?,
        context: Context?,
        defStyleAttr: Int
    ) {
        if (attrs != null) {
            val a =
                context!!.obtainStyledAttributes(
                    attrs,
                    R.styleable.HorizontalCardLayout,
                    defStyleAttr,
                    0
                )
            this.layoutParams =
                ViewGroup.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)

            title = a.getString(R.styleable.HorizontalCardLayout_horizontalItemTitle) ?: ""
            locality = a.getString(R.styleable.HorizontalCardLayout_horizontalLocality) ?: ""
            cusisines = a.getString(R.styleable.HorizontalCardLayout_horizontalSubtitle) ?: ""
            bottomText = a.getString(R.styleable.HorizontalCardLayout_horizontalBottomText) ?: ""
            colorText = a.getString(R.styleable.HorizontalCardLayout_horizontalColorText) ?: ""
            ribbonText = a.getString(R.styleable.HorizontalCardLayout_horizontalRibbonText) ?: ""
            imageSize =
                (a.getDimension(R.styleable.HorizontalCardLayout_horizontalImageSize, 90f)).toInt()
//            this.imageSize = VUtil.dpToPx(imageSize.toInt())
            discountDrawable =
                YumUtil.getIcon(context, R.drawable.ic_discount, ThemeConstant.pinkies)!!
            a.recycle()

        }
        this.setClickScaleEffect()
    }

    private fun initView() {
        this.layoutParams =
            ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        this.setPadding(0, UIHelper.i2px, UIHelper.i5px, UIHelper.i2px)
        val ribbonView = getRibbonView()
        val roundImage = restaurantImageView()

        val layout = LinearLayout(context)
        val linearParam = LayoutParams(0, ViewConst.WRAP_CONTENT)
        linearParam.endToEnd = LayoutParams.PARENT_ID
        linearParam.topToTop = R.id.restaurant_image
//        linearParam.bottomToBottom = R.id.restaurant_image
        linearParam.startToEnd = R.id.restaurant_image
//        linearParam.bottomToBottom = LayoutParams.PARENT_ID
        layout.layoutParams = linearParam
        layout.orientation = LinearLayout.VERTICAL
        layout.setPadding(VUtil.dpToPx(15), 0, 0, 0)
        layout.addView(getTextView(title, 14f, R.id.tv_title))
        layout.addView(getTextView(locality, 10f, R.id.tv_locality))
        layout.addView(getTextView(cusisines, 11f, R.id.tv_subtitle))
        layout.addView(getTextView(bottomText, 10f, R.id.tv_time_price))
        layout.addView(getTextView(colorText, 11f, R.id.tv_coupon_code))


        val rattingView = getRattingView(context)

        this.addView(roundImage)
        this.addView(rattingView)
        this.addView(ribbonView)
        this.addView(layout)

    }

    var ribbonText: String? = null
        set(value) {
            field = value
            val findViewById = this.findViewById<RibbonView>(R.id.ribbon_view)
            findViewById?.ribbonText = ribbonText

        }

    private fun getRibbonView(): RibbonView {
        val ribbonView = RibbonView(context)
        val ribbonParam = LayoutParams(ViewConst.WRAP_CONTENT, ViewConst.WRAP_CONTENT)
        ribbonParam.setMargins(UIHelper.i3px, UIHelper.i3px, 0, 0)
        ribbonView.layoutParams = ribbonParam
        ribbonView.id = R.id.ribbon_view
        ribbonView.ribbonText = ribbonText
        return ribbonView
    }


    private fun getRattingView(context: Context): View {

        val rattingView = RatingView(context)
        rattingView.id = R.id.outlet_item_rating
        val rattingParam = LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT
        )
        rattingParam.marginStart = VUtil.dpToPx(5)
        rattingParam.bottomMargin = VUtil.dpToPx(5)
        rattingParam.bottomToBottom = R.id.restaurant_image
        rattingParam.startToStart = LayoutParams.PARENT_ID
        rattingView.layoutParams = rattingParam
        return rattingView
    }

    private fun restaurantImageView(): ImageView {
        val imageParam =
            LayoutParams(VUtil.dpToPx(imageSize), 0)
        imageParam.topToTop = LayoutParams.PARENT_ID
        imageParam.startToStart = LayoutParams.PARENT_ID
        imageParam.dimensionRatio = "1:1"


        val roundImage = RoundedImageView(context)
        roundImage.setImageResource(YUtils.getPlaceHolder())
        roundImage.layoutParams = imageParam
        roundImage.id = R.id.restaurant_image
        roundImage.cornerRadius = VUtil.dpToPx(10).toFloat()
        roundImage.scaleType = ImageView.ScaleType.CENTER_CROP
        return roundImage
    }


    private fun getTextView(text: String?, textSize: Float, tvId: Int): View {
        val textView = TextView(context)
        textView.setInterFont()
        textView.text = text ?: ""
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
        textView.id = tvId
        when (tvId) {
            R.id.tv_title -> {
                textView.setInterBoldFont()
                textView.ellipsize = TextUtils.TruncateAt.END
                textView.maxLines = 1
                textView.setTextColor(ThemeConstant.textBlackColor)
            }
            R.id.tv_subtitle -> {
                textView.ellipsize = TextUtils.TruncateAt.END
                textView.maxLines = 1
                textView.setTextColor(ThemeConstant.textDarkGrayColor)
            }
            R.id.tv_locality -> {
                textView.ellipsize = TextUtils.TruncateAt.END
                textView.maxLines = 1
                textView.textSize=textSize

                textView.setTextColor(ThemeConstant.textGrayColor)
            }

            R.id.tv_time_price -> {
                textView.setTextColor(ThemeConstant.textGrayColor)
                // textView.setPadding(0, UIHelper.i3px, 0, 0)
            }
            R.id.tv_coupon_code -> {
                textView.setInterRegularFont()
                textView.setTextColor(colorTextColor)
                // textView.setPadding(0, UIHelper.i4px, 0, UIHelper.i4px)
            }
            R.id.tv_empty -> {
                textView.layoutParams = LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0, 1f)

            }
        }
        textView.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        return textView

    }

    var iconRef: Int = R.string.icon_discount_4
    /*private fun getFontTextView(text: String?, textSize: Float, minLine: Int, tvId: Int): View {
        val textView = FontIconView(context)
        textView.gravity = Gravity.START
        if (text.isNullOrEmpty()) {
            textView.visibility = View.GONE
        } else {
            textView.visibility = View.VISIBLE
            val iconCode = context.resources.getString(iconRef)
            textView.text = "$iconCode $text"
        }

        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
        textView.id = tvId
        when (tvId) {

            R.id.tv_coupon_code -> {
                textView.setPadding(0, UIHelper.i4px, 0, 0)
                textView.setTextColor(colorTextColor)
            }

        }
        return textView

    }*/

    private fun spanDrawable(textSize: Float): Pair<SpannableString, Drawable> {
        val spannableString = SpannableString("@ $colorText")

        val iconWidth = VUtil.dpToPx(textSize.toInt() - 5)
        val iconHeight = VUtil.dpToPx(textSize.toInt())
        discountDrawable.setBounds(0, 0, iconWidth, iconHeight)
        return Pair(spannableString, discountDrawable)
    }

}