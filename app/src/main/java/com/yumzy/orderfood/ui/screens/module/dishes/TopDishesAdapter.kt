package com.yumzy.orderfood.ui.screens.module.dishes

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yumzy.orderfood.data.models.DishesItem
import com.yumzy.orderfood.data.models.DishesResDTO
import com.yumzy.orderfood.databinding.AdapterDishesLayoutBinding
import com.yumzy.orderfood.ui.component.AddItemView
import com.yumzy.orderfood.ui.component.AddQuantityView

class TopDishesAdapter(
    context: Context,
    private val dishesDTO: DishesResDTO,
    private val incDecCountListener: (adapterPosition: Int, itemId: String, quantity: Int, price: String, outletId: String) -> Unit,
    private val addItemclickListener: (adapterPosition: Int, itemId: String, quantity: Int, price: String, outletId: String, AddQuantityView) -> Unit
) : RecyclerView.Adapter<TopDishesAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: AdapterDishesLayoutBinding =
            AdapterDishesLayoutBinding.inflate(layoutInflater, parent, false)

        return ItemViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return dishesDTO.items.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(dishesDTO.items[position])

    }

    inner class ItemViewHolder(itemView: AdapterDishesLayoutBinding) :
        RecyclerView.ViewHolder(itemView.root), AddItemView.AddItemListener {

        var adapterBinding: AdapterDishesLayoutBinding = itemView

        fun bind(dishesItem: DishesItem) {
            adapterBinding.addItemView.setImageURL(dishesItem.imageUrl)
            adapterBinding.addItemView.setItemPrice(dishesItem.price.toString())
            adapterBinding.addItemView.setTitles(dishesItem.name)
            adapterBinding.addItemView.itemId = dishesItem.itemId
            adapterBinding.addItemView.outletId = dishesItem.outletId

            adapterBinding.addItemView.setImageShow(true)
            adapterBinding.addItemView.isVeg = dishesItem.isVeg
            adapterBinding.addItemView.addItenListener = this
            if (dishesItem.prevQuantity > 0) adapterBinding.addItemView.getQuantityView()?.quantityCount =
                dishesItem.prevQuantity


        }

        override fun onItemAdded(itemId: String, count: Int, price: String, outlet: String) {
            if (count == 1) {
                addItemclickListener(
                    adapterPosition,
                    itemId,
                    count,
                    price,
                    outlet,
                    adapterBinding.addItemView.getQuantityView()!!
                )
            } else if (count > 1) {
                incDecCountListener(adapterPosition, itemId, count, price, outlet)

            }
        }
    }


}