package com.yumzy.orderfood.ui.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.yumzy.orderfood.R;
import com.yumzy.orderfood.ui.common.imageview.RoundedImageView;
import com.laalsa.laalsalib.ui.VUtil;

/**
 * Created by Bhupendra Kumar Sahu.
 */
public class SearchRestaurantView extends LinearLayout {
    private String header;
    private String subHeader;
    private Drawable icon;


    public SearchRestaurantView(Context context) {
        this(context, null);
    }

    public SearchRestaurantView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SearchRestaurantView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (attrs != null)
            initAttrs(context, attrs, defStyleAttr);
        if (header == null) {
            header = "";
        }
        if (subHeader == null) {
            subHeader = "";
        }
        if (icon == null) {

        }
        init();

    }

    private void initAttrs(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SearchRestaurantView, defStyleAttr, 0);
        header = a.getString(R.styleable.SearchRestaurantView_dish_title);
        icon = a.getDrawable(R.styleable.SearchRestaurantView_dish_icon);
        subHeader = a.getString(R.styleable.SearchRestaurantView_dish_subtitle);
        a.recycle();
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
        ((RoundedImageView) this.findViewById(R.id.iv_dish)).setImageDrawable(this.icon);

    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
        ((TextView) this.findViewById(R.id.tv_dish_header)).setText(this.header);

    }

    public String getSubHeader() {
        return subHeader;
    }

    public void setSubHeader(String subHeader) {
        this.subHeader = subHeader;
        ((TextView) this.findViewById(R.id.tv_dish_sub_header)).setText(this.subHeader);

    }

    private void init() {
        this.setOrientation(LinearLayout.HORIZONTAL);
        this.setGravity(Gravity.CENTER_VERTICAL | Gravity.END);
        int i55px = VUtil.dpToPx(55);
        RoundedImageView imageView = new RoundedImageView(getContext());
        imageView.setLayoutParams(new LinearLayout.LayoutParams(i55px, i55px));
        imageView.setId(R.id.iv_dish);
        imageView.setImageDrawable(icon);
        imageView.setCornerRadius(VUtil.dpToPx(15));


        LinearLayout layout = new LinearLayout(getContext());
        layout.setOrientation(LinearLayout.VERTICAL);

        layout.setLayoutParams(new LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1));
        layout.addView(getTextView(true, header));
        layout.addView(getTextView(false, subHeader));

        this.addView(imageView);
        this.addView(layout);


    }

    private View getTextView(boolean isHeading, String text) {
        int i10px = VUtil.dpToPx(15);
        int i3px = VUtil.dpToPx(2);
        TextView textView = new TextView(getContext());

        textView.setText(text);
        textView.setPadding(i10px, i3px, i10px, i3px);


        if (isHeading) {
            textView.setText(header);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f);
            textView.setTextColor(getResources().getColor(R.color.light_black));
            textView.setTypeface(Typeface.DEFAULT);
            textView.setId(R.id.tv_dish_header);
        } else {
            textView.setText(subHeader);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f);
            textView.setTextColor(getResources().getColor(R.color.cool_gray));
            textView.setId(R.id.tv_dish_sub_header);
        }


        return textView;

    }


}

