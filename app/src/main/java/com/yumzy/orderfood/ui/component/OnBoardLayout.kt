package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.content.res.ResourcesCompat
import com.laalsa.laalsalib.ui.pxf
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.FragmentOnboardLayoutBinding
import com.yumzy.orderfood.ui.helper.hideEmptyTextView

class OnBoardLayout : FrameLayout {

    private var imageSize: Float = 200.pxf
        set(value) {
            field = value
            val imageParam = binding.imgOnBoard.layoutParams
            imageParam.height = field.toInt()
            binding.imgOnBoard.layoutParams = imageParam
        }
    var title: CharSequence? = ""
        set(value) {
            field = value
            binding.tvTitle.hideEmptyTextView(field)
        }

    var subTitle: CharSequence? = ""
        set(value) {
            field = value
            binding.tvSubTitle.hideEmptyTextView(field)
        }


    var imageResource: Int? = null
        set(value) {
            field = value
            if (field != null && field != -1) {
                val drawable =
                    ResourcesCompat.getDrawable(context.resources, field!!, context.theme)
                binding.imgOnBoard.setImageDrawable(drawable)
            } else {
                binding.imgOnBoard.visibility = GONE

            }
        }

//    fun setImageUrl(url: String?) {
//        if (url != null)
//            YumUtil.setImageInView(
//                binding.imgOnBoard, url
//            )
//        if (url == null)
//            binding.imgOnBoard.visibility = View.GONE
//        else {
//            binding.imgOnBoard.visibility = View.INVISIBLE
//        }
//    }

    private val binding by lazy {
        val layoutInflater = LayoutInflater.from(context)
        FragmentOnboardLayoutBinding.inflate(layoutInflater, this, true)
    }

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {

        if (attrs != null) {
            val a =
                context.obtainStyledAttributes(
                    attrs,
                    R.styleable.OnBoardLayout,
                    defStyleAttr,
                    0
                )
            this.title = a.getString(R.styleable.OnBoardLayout_onBoardTitle)
            this.subTitle = a.getString(R.styleable.OnBoardLayout_onBoardSubtitle)
            this.imageSize =
                a.getDimension(R.styleable.OnBoardLayout_onBoardImageSize, imageSize)
            this.imageResource = a.getResourceId(
                R.styleable.OnBoardLayout_onBoardSrc,
                -1
            )
            a.recycle()

        }

    }
}