package com.yumzy.orderfood.ui.common.recyclerview.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.yumzy.orderfood.R;
import com.yumzy.orderfood.ui.common.recyclerview.listener.IItemViewHolderListener;
import com.yumzy.orderfood.ui.common.recyclerview.view.YItemViewHolder;
import com.yumzy.orderfood.ui.common.recyclerview.view.YProgressHolder;
import com.yumzy.orderfood.ui.common.toolbar.YSearchToolbar;
import com.laalsa.laalsalib.ui.VUtil;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class YMultiSelectionViewAdapter<T> extends YRecyclerViewAdapter<T> {
    protected IActionModeEventListener iItemViewEventListener;
    protected SparseBooleanArray mSelectedItemsIds;
    protected int iId;
    protected ActionMode actionMode;
    YSearchToolbar toolbar = null;

    public YMultiSelectionViewAdapter(Context context, ArrayList<T> itemHolderArrayList, IItemViewHolderListener itemViewHolderListener) {
        super(context, itemHolderArrayList, itemViewHolderListener);
        mSelectedItemsIds = new SparseBooleanArray();
    }

    public YMultiSelectionViewAdapter(Context context, ArrayList<T> itemHolderArrayList, IItemViewHolderListener itemViewHolderListener, boolean bLoadMore) {
        super(context, itemHolderArrayList, itemViewHolderListener, bLoadMore);
        mSelectedItemsIds = new SparseBooleanArray();
    }


    public void setActionModeEventListener(IActionModeEventListener iItemViewEventListener) {
        this.iItemViewEventListener = iItemViewEventListener;
    }


    @Override
    public void onBindViewHolder(@NotNull final RecyclerView.ViewHolder holder, final int position) {
        if (!(holder instanceof YProgressHolder)) {

            YItemViewHolder yItemViewHolder = ((YItemViewHolder) holder);
            itemViewHolderListener.onBindViewHolder(holder, position);
            yItemViewHolder.itemView.setActivated(mSelectedItemsIds.get(position, false));
            yItemViewHolder.itemView.setOnLongClickListener(v -> {
                    /*if (getSelectedItems().size() == 0) {
                        toggleSelection(position);
                    }*/
                if (clItemClickListener != null)
                    clItemClickListener.onItemLongClick(v, position);

                return false;
            });


//            if (clItemViewHolder.itemView != null)
            {
                if (iSwipeIndexes != null && iSwipeIndexes[position] != 0) {
//                    clItemViewHolder.itemView.setTranslationX(-clItemViewHolder.iBackgroundWidth);
                    clearSwipeIndexes();
                } else {
                    yItemViewHolder.itemView.setTranslationX(0);
                    yItemViewHolder.itemView.clearFocus();
                }
                yItemViewHolder.itemView.setOnClickListener(v -> {
                    /*if (getSelectedItems().size() > 0)
                        toggleSelection(position);*/
                    if (clItemClickListener != null) {
                        iSelectedItemPos = holder.getAdapterPosition();
                        clItemClickListener.onItemClick(v, holder.getAdapterPosition());
                    }
                });
            }

        } else {
            super.onBindViewHolder(holder, position);
        }
    }


    public void toggleSelection(int pos) {


        if (mSelectedItemsIds.get(pos, false)) {
            mSelectedItemsIds.delete(pos);
        } else {
            mSelectedItemsIds.put(pos, true);
        }

        if (iItemViewEventListener != null) {
            int iSelectedCount = mSelectedItemsIds.size();
            boolean isTabletMode = VUtil.isTablet(context) && iId > 0;

            if (iSelectedCount != 0) {
                String title = context.getResources().getString(R.string.selected_count, iSelectedCount);
                Menu clMenu = null;

                if (isTabletMode && toolbar == null) {
//                toolbar = (CLToolbar) ((Activity) context).findViewById(iId);
                    createOverlayToolbar(true);
//            toolbar.inflateMenu(menu);

                } else if (actionMode == null && !isTabletMode) {
                    ActionMode.Callback actionModeCallback = new CLActionModeListener(context);
                    actionMode = ((AppCompatActivity) context).startActionMode(actionModeCallback);
                }


                if (isTabletMode && toolbar != null) {
                    toolbar.setTitle(title);
                    clMenu = toolbar.getMenu();
                } else if (actionMode != null) {
                    actionMode.setTitle(title);
                    clMenu = actionMode.getMenu();
                }
                if (iSelectedCount <= 2) {
                    iItemViewEventListener.onCreateActionMode(actionMode, clMenu);
                }
            } else {
                if (isTabletMode) {
                    clearSelections(false);
                } else {
                    actionMode.finish();
                }
            }
        }
        //        notifyDataSetChanged();
        notifyItemChanged(pos);
    }

    private void createOverlayToolbar(boolean bActionModeView) {
        YSearchToolbar clToolbar = ((Activity) context).findViewById(iId);
        if (clToolbar != null) {
            ViewGroup clViewGroup = (ViewGroup) clToolbar.getParent();
            int iToolbarIndex = clViewGroup.indexOfChild(clToolbar);
            if (bActionModeView) {
                clToolbar.setVisibility(View.GONE);
                int iPrimaryColor = ((ColorDrawable) clToolbar.getBackground()).getColor();
                int iToolbarChildColor = clToolbar.getToolbarChildColor();

                toolbar = new YSearchToolbar(context, R.menu.toolbar_menu, clToolbar.getTitle(), iPrimaryColor, iToolbarChildColor);
                toolbar.startToolbarActionMode();
                toolbar.setX(clToolbar.getX());
                toolbar.setY(clToolbar.getY());
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clearSelections(true);
                    }
                });
                toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        iItemViewEventListener.onActionModeItemClicked(null, item);
                        return false;
                    }
                });
                clViewGroup.addView(toolbar, iToolbarIndex, clToolbar.getLayoutParams());
            } else {
                clToolbar.setVisibility(View.VISIBLE);
                clViewGroup.removeView(toolbar);
            }
//        Menu clMenu2=clToolbar.getMenu());
        }

    }


    public int getSelectedItemCount() {
        return mSelectedItemsIds.size();
    }


    public void clearSelections(boolean notify) {
        if (toolbar != null) {
            toolbar.destroyActionMode();
            createOverlayToolbar(false);
            toolbar = null;
        }
        mSelectedItemsIds.clear();
        if (notify) notifyDataSetChanged();
        if (iItemViewEventListener != null)
            iItemViewEventListener.onDestroyActionMode(actionMode);
    }

    public int[] getSelectedItems() {
//        List<Integer> items = new ArrayList<>(mSelectedItemsIds.size());
        int[] items = new int[mSelectedItemsIds.size()];
        for (int i = 0; i < mSelectedItemsIds.size(); i++) {
            items[i] = (mSelectedItemsIds.keyAt(i));
        }
        return items;
    }


    public void setToolbarId(int iId) {
        this.iId = iId;
    }

    public void removeSelectedItems(List<Integer> iSelectedIds) {
        if (iSelectedIds.size() > 0) {
            int currPos;
            int iListPosition = 0;
            for (int i = 0; i < iSelectedIds.size(); i++) {
                currPos = iSelectedIds.get(i);
                if (currPos == 0)
                    iListPosition = currPos;
                else
                    iListPosition = currPos - i;

                removeItem(iListPosition);
            }
        }
    }

    public boolean isSelectedPosition(int iPosition) {
        return mSelectedItemsIds.get(iPosition, false);
    }

//------------Action Mode Callback Class................

    public interface IActionModeEventListener {
        boolean onCreateActionMode(ActionMode mode, Menu menu);

        boolean onActionModeItemClicked(ActionMode mode, MenuItem item);

        void onDestroyActionMode(ActionMode actionMode);
    }

    private class CLActionModeListener implements ActionMode.Callback {
        Context context;

        CLActionModeListener(Context context) {
            this.context = context;
        }


        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            iItemViewEventListener.onCreateActionMode(mode, menu);
            actionMode = mode;
            /*if (CLMultiSelectionViewAdapter.this.menu > 0)
                new MenuInflater(context).inflate(CLMultiSelectionViewAdapter.this.menu, menu);
            else*/
            /*if (menu.size() <= 0)
            {
                final Toast toast = Toast.makeText(context, "Action Mode Menu is null...", Toast.LENGTH_SHORT);
                CountDownTimer toastCountDown;
                toastCountDown = new CountDownTimer(10000, 1000 *//*Tick duration*//*)
                {
                    public void onTick(long millisUntilFinished)
                    {
                        toast.show();
                    }

                    public void onFinish()
                    {
                        toast.cancel();
                    }
                };

                // Show the toast and starts the countdown
                toast.show();
                toastCountDown.start();
            }*/
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            iItemViewEventListener.onActionModeItemClicked(mode, item);
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            actionMode = null;
            clearSelections(true);
        }
    }

}
