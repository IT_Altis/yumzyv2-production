package com.yumzy.orderfood.ui.screens.scratchcard

import com.yumzy.orderfood.data.models.ScratchCardDTO
import com.yumzy.orderfood.ui.base.IView

interface IScratchCardView : IView {
    fun fetchScratchCardList()
    fun getScratchCardDetails()
    fun redeemScratchCard(scratchCard: ScratchCardDTO)
}