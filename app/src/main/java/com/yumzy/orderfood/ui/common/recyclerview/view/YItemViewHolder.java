package com.yumzy.orderfood.ui.common.recyclerview.view;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.yumzy.orderfood.ui.common.recyclerview.adapter.YExpandableListAdapter;

public class YItemViewHolder extends RecyclerView.ViewHolder {
    private boolean isExpanded = true;

    public YItemViewHolder(View v) {
        this(v, null, null);
    }

    public YItemViewHolder(View v, View foregroundView, View backgroundView) {
        super(v);
    }

    public YItemViewHolder(View v, byte byTypeofData) {
        super(v);
        isExpanded = (byTypeofData != YExpandableListAdapter.GROUP_ARRAY_LIST);
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean isExpanded) {
        this.isExpanded = isExpanded;
    }

}