package com.yumzy.orderfood.ui.common.recyclerview.view;

import java.util.ArrayList;

public class YSectionDTO {

    private final String name;
    public boolean isExpanded;
    public ArrayList<Object> childItems;
    private int iImageId;


    public YSectionDTO(String name) {
        this.name = name;
        isExpanded = false;
    }

    public YSectionDTO(String name, int iImageId) {
        this.name = name;
        this.iImageId = iImageId;
        isExpanded = false;
    }

    public ArrayList<Object> getChildItemList() {
        return childItems;
    }

    public void setChildItemList(ArrayList<Object> childItems) {
        this.childItems = childItems;
    }

    public boolean isInitiallyExpanded() {
        return isExpanded;
    }

    public void removeItem(Object clItemHolder) {
        childItems.remove(clItemHolder);
    }

    public void removeChildList() {
        childItems.clear();
    }

    public void removeChildItem(int position) {
        childItems.remove(position);
    }

    public void setExpanded(boolean isExpanded) {
        this.isExpanded = isExpanded;
    }

    public String getName() {
        return name;
    }


    public int getImageId() {
        return iImageId;
    }

    public void setImageId(int iImageId) {
        this.iImageId = iImageId;
    }
}
