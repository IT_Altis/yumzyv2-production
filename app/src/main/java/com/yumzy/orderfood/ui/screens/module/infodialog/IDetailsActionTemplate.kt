/*
 *   Created by Sourav Kumar Pandit  29 - 4 - 2020
 */

package com.yumzy.orderfood.ui.screens.module.infodialog

import android.content.Context
import android.content.DialogInterface
import android.view.View

interface IDetailsActionTemplate {
    val context: Context
    val headerHeight: Int
    val showClose: Boolean
    fun heading(): View?
    fun body(): View?
    fun actions(): View?
    fun onDialogVisible(view: View?, visible: Boolean?, dialog: DialogInterface?)
}