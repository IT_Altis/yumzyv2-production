package com.yumzy.orderfood.ui.screens.home.adapter

import android.view.View
import android.view.ViewGroup
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.zhpan.bannerview.BaseBannerAdapter

class TopBannerAdapter(
    val callerFun: (View, HomeListDTO?) -> Unit
) : BaseBannerAdapter<HomeListDTO?, BannerViewHolder?>() {
    override fun onBind(
        holder: BannerViewHolder?,
        data: HomeListDTO?,
        position: Int,
        pageSize: Int
    ) {
        holder?.bindData(data, position, pageSize)
        holder?.itemView?.setOnClickListener {
            callerFun(holder.itemView, data)
        }
        //        callerFun(data.getPromoId(), data.getLandingPage(), position);
    }


    override fun getLayoutId(viewType: Int): Int {
        return R.layout.layout_banner_item
    }

    override fun createViewHolder(
        parent: ViewGroup,
        itemView: View,
        viewType: Int
    ): BannerViewHolder? {
        return BannerViewHolder(itemView)
    }

    /*interface IMethodCaller {
        interface AdapterCallback {
            fun onMethodCallback(
                landingPage: String?,
                promoId: String?
            )
        }
    }*/
}