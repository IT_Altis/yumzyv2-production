package com.yumzy.orderfood.ui.common

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import com.yumzy.orderfood.data.models.HomeSectionDTO
import com.yumzy.orderfood.data.models.RestaurantAddOnDTO
import com.yumzy.orderfood.data.models.RestaurantItem
import com.yumzy.orderfood.ui.base.actiondialog.ActionDialog
import com.yumzy.orderfood.ui.base.actiondialog.ActionItemDTO
import com.yumzy.orderfood.ui.component.RateUsActionDialog
import com.yumzy.orderfood.ui.screens.information.BaseAnimationTemplate
import com.yumzy.orderfood.ui.screens.information.LottieFullDialog
import com.yumzy.orderfood.ui.screens.information.LottieSizedDialog
import com.yumzy.orderfood.ui.screens.module.ApplyReferralDialog
import com.yumzy.orderfood.ui.screens.module.address.*
import com.yumzy.orderfood.ui.screens.module.appRating.RatingDialog
import com.yumzy.orderfood.ui.screens.module.dishes.TopDishesDialog
import com.yumzy.orderfood.ui.screens.module.favourites.FavouritesDialog
import com.yumzy.orderfood.ui.screens.module.foodpersonalisation.FoodPersonalisationDialog
import com.yumzy.orderfood.ui.screens.module.foodpreference.FoodPreferenceDialog
import com.yumzy.orderfood.ui.screens.module.restaurant.OrderCustomizationDialog
import com.yumzy.orderfood.ui.screens.module.review.RateOrderDialog
import com.yumzy.orderfood.ui.screens.module.review.ReviewDialog
import com.yumzy.orderfood.ui.screens.module.seeall.SeeAllDialog
import com.yumzy.orderfood.ui.screens.module.toprestaurant.TopRestaurantsDialog
import com.yumzy.orderfood.util.IConstants
import java.util.concurrent.LinkedBlockingQueue

object ActionModuleHandler {


    private var mShowOrderTracking: OrderTrackingDialog? = null
    private var mShowOrderCustomization: OrderCustomizationDialog? = null
    var mTopRestaurantsDialog: TopRestaurantsDialog? = null
    var mTopDishesDialog: TopDishesDialog? = null
    var mFavouritesDialog: FavouritesDialog? = null

    fun showDialog(dialog: Dialog) {
        var dialogsToShow: LinkedBlockingQueue<Dialog> = LinkedBlockingQueue()
        if (dialogsToShow.isEmpty()) {
            dialog.show()
        }
        dialogsToShow.offer(dialog)
        dialog.setOnDismissListener { d: DialogInterface? ->
            dialogsToShow.remove(dialog)
            if (!dialogsToShow.isEmpty()) {
                dialogsToShow.peek()?.show()
            }
        }
    }

    fun showConfirmation(
        clContext: Context,
        actionType: Int,
        actionLabel: String,
        sTitle: CharSequence,
        sMessage: CharSequence,
        onActionPerformed: ConfirmActionDialog.OnActionPerformed?
    ) {
        val clFragmentManager = (clContext as AppCompatActivity).supportFragmentManager
        val actionDialog =
            ConfirmActionDialog(
                clContext,
                actionType,
                actionLabel,
                sTitle,
                sMessage,
                onActionPerformed
            )
        actionDialog.show(clFragmentManager, "$actionType")
    }

    fun showChoseActionDialog(
        clContext: Context,
        actionType: Int,
        title: CharSequence,
        sMessage: CharSequence,
        actions: ArrayList<ActionItemDTO?>?,
        onActionPerformed: ActionDialog.OnActionPerformed?,
        iconString: String="\uE0F6"
    ): ActionDialog {
        val clFragmentManager = (clContext as AppCompatActivity).supportFragmentManager
        val actionDialog =
            ActionDialog(
                actionType,
                title = title,
                iconString = iconString,
                message = sMessage,
                actions = actions,
                onActionPerformed = onActionPerformed
            )
        actionDialog.show(clFragmentManager, "$actionType")
        return actionDialog
    }

    fun showActionDialog(
        clContext: Context,
        actionType: Int,
        sMessage: CharSequence,
        positiveText: CharSequence?,
        negativeText: CharSequence?,
        onActionPerformed: CustomizeOptionDialog.OnActionPerformed
    ) {
        val clFragmentManager = (clContext as AppCompatActivity).supportFragmentManager
        val actionDialog =
            CustomizeOptionDialog(
                clContext,
                actionType,
                sMessage,
                positiveText,
                negativeText,
                onActionPerformed

            )
        actionDialog.show(clFragmentManager, "$actionType")
    }

    fun showNewRestaurantView(context: Context, sTitle: String?, moduleHandler: IModuleHandler?) {
        val clFragmentManager = (context as AppCompatActivity).supportFragmentManager
        val restaurantsDialog =
            TopRestaurantsDialog()
        restaurantsDialog.moduleId = IConstants.ActionModule.TOP_RESTAURANT
        restaurantsDialog.actionHandler = moduleHandler
        restaurantsDialog.show(clFragmentManager, sTitle)
    }

    fun showReviewDialogView(
        context: Context,
        title: String,
        rating: String,
        orderNum: String,
        orderID: String,
        outletName: String,
        items: String,
        moduleHandler: IModuleHandler?

    ) {
        val clFragmentManager = (context as AppCompatActivity).supportFragmentManager

        val reviewDialog =
            ReviewDialog(rating, orderNum, orderID, outletName, items, moduleHandler)
        reviewDialog.moduleId = IConstants.ActionModule.REVIEW

        reviewDialog.show(clFragmentManager, title)
    }

//    fun showOtpVerificationDialog(
//        context: Context,
//        number: String,
//        onOtpHandler: IModuleHandler
//    ): OTPVerifyFragment {
//        val clFragmentManager = (context as AppCompatActivity).supportFragmentManager
//        val otpVerifyDialog = OTPVerifyFragment()
//        otpVerifyDialog.actionHandler = onOtpHandler
//        otpVerifyDialog.number = number
//        otpVerifyDialog.moduleId = IConstants.ActionModule.OTP_VERIFICATION
//        otpVerifyDialog.show(clFragmentManager, number)
//        return otpVerifyDialog
//    }

    fun showAddAddressDialog(
        context: Context,
        userId: String,
        couponHandler: IModuleHandler?
    ): AddAddressDialog {
        val clFragmentManager = (context as AppCompatActivity).supportFragmentManager
        val addressDialog =
            AddAddressDialog()
        addressDialog.actionHandler = couponHandler
        addressDialog.moduleId = IConstants.ActionModule.ADD_ADDRESS
        addressDialog.show(clFragmentManager, userId)
        return addressDialog
    }

    fun showApplyReferralDialog(
        context: Context,
        userId: String,
        couponHandler: IModuleHandler?
    ): ApplyReferralDialog {
        val clFragmentManager = (context as AppCompatActivity).supportFragmentManager
        val referralDialog = ApplyReferralDialog()
        referralDialog.actionHandler = couponHandler
        referralDialog.userId = userId
        referralDialog.moduleId = IConstants.ActionModule.APPLY_REFERRAL
        referralDialog.show(clFragmentManager, userId)
        return referralDialog
    }

    fun showApplyRatingDialog(
        context: Context,
        orderID: String,
        orderNum: String,
        outletName: String,
        items: String,
        moduleHandler: IModuleHandler?
    ) {
        val clFragmentManager = (context as AppCompatActivity).supportFragmentManager
        val applyRatingDialog = RatingDialog(orderNum, orderID, outletName, items, moduleHandler)
        applyRatingDialog.moduleId = IConstants.ActionModule.APPLY_RATING
        applyRatingDialog.show(clFragmentManager, "title")
    }

    /*fun showRankRulesDialog(context: Context, title: String) {
        val clFragmentManager = (context as AppCompatActivity).supportFragmentManager
        val rankRulesDialog = ClassRankRulesDialog()
        // rankRulesDialog.actionHandler = ratingHandler
        rankRulesDialog.moduleId = IConstants.ActionModule.APPLY_RATING
        rankRulesDialog.show(clFragmentManager, title)
    }*/


    fun showFavouriteRestaurantDialog(
        context: Context,
        sTitle: String?,
        moduleHandler: IModuleHandler?
    ) {
        if (mFavouritesDialog != null && mFavouritesDialog?.isVisible == true) {
            mFavouritesDialog?.dismiss()
        }
        val clFragmentManager = (context as AppCompatActivity).supportFragmentManager
        mFavouritesDialog = FavouritesDialog()
        mFavouritesDialog?.actionHandler = moduleHandler
        mFavouritesDialog?.moduleId = IConstants.ActionModule.FAVOURITE_RESTAURANT
        mFavouritesDialog?.show(clFragmentManager, sTitle)
    }

    fun showTopRestaurantDialog(
        context: Context,
        promoId: String? = "",
        title: String?,
        heroImage: String?,
        moduleHandler: IModuleHandler?
    ) {
        if (mTopRestaurantsDialog != null && mTopRestaurantsDialog?.isVisible == true) {
            mTopRestaurantsDialog?.dismiss()
        }
        val clFragmentManager = (context as AppCompatActivity).supportFragmentManager
        mTopRestaurantsDialog = TopRestaurantsDialog()
        mTopRestaurantsDialog?.actionHandler = moduleHandler
        mTopRestaurantsDialog?.moduleId = IConstants.ActionModule.TOP_RESTAURANTS
        mTopRestaurantsDialog?.promoId = promoId ?: ""
        mTopRestaurantsDialog?.title = title ?: "Yumzy"
        mTopRestaurantsDialog?.heroImage = heroImage ?: ""
        mTopRestaurantsDialog?.show(clFragmentManager, title)
    }

    fun showOrderCustomization(
        context: Context,
        restaurantItem: RestaurantItem,
        addOnDTOS: List<RestaurantAddOnDTO>,
        moduleHandler: IOrderCustomizationHandler
    ) {
        if (mShowOrderCustomization != null && mShowOrderCustomization?.isVisible == true) {
            mShowOrderCustomization?.dismiss()
        }
        val clFragmentManager = (context as AppCompatActivity).supportFragmentManager
        mShowOrderCustomization = OrderCustomizationDialog()
        mShowOrderCustomization?.dishName = restaurantItem.name.capitalize()
        mShowOrderCustomization?.itemPrice = restaurantItem.price
        mShowOrderCustomization?.restaurantItem = restaurantItem
        mShowOrderCustomization?.mCustomizationActionHandler = moduleHandler
        mShowOrderCustomization?.moduleId = IConstants.ActionModule.CUSTOMIZE_ORDER
        mShowOrderCustomization?.mAddOnDTOList = ArrayList(addOnDTOS)
        mShowOrderCustomization?.show(clFragmentManager, "Customize Order")
    }

    fun showFoodPreference(
        context: Context,
        moduleHandler: IModuleHandler?
    ) {
        val clFragmentManager = (context as AppCompatActivity).supportFragmentManager
        val foodPreferenceDialog = FoodPreferenceDialog()
        foodPreferenceDialog.actionHandler = moduleHandler
        foodPreferenceDialog.moduleId = IConstants.ActionModule.FOOD_PREFERENCE
        foodPreferenceDialog.show(clFragmentManager, "Food Preference")
    }

    fun showSelectDeliverLocation(
        context: Context,
        moduleHandler: IModuleHandler?
    ): SelectDeliveryLocationDialog {
        val clFragmentManager = (context as AppCompatActivity).supportFragmentManager
        val selectDeliveryLocationDialog = SelectDeliveryLocationDialog()
        selectDeliveryLocationDialog.actionHandler = moduleHandler
        selectDeliveryLocationDialog.moduleId = IConstants.ActionModule.SELECT_DELIVER_LOCATION
        selectDeliveryLocationDialog.show(clFragmentManager, "Select Delivery Location Dialog")
        return selectDeliveryLocationDialog
    }

    fun rateOrder(
        context: Context,
        orderID: String,
        orderNum: String,
        outletName: String,
        items: String,
        moduleHandler: IModuleHandler?
    ): RateOrderDialog {
        val clFragmentManager = (context as AppCompatActivity).supportFragmentManager
        val rateOrderDialog = RateOrderDialog(orderNum, orderID, outletName, items)
        rateOrderDialog.actionHandler = moduleHandler
        rateOrderDialog.moduleId = IConstants.ActionModule.APPLY_RATING
        rateOrderDialog.show(clFragmentManager, "Apply Rating")
        return rateOrderDialog
    }

    fun showConfirmLocation(
        context: Context,
        action: LocationMapAction,
        actionHandler: IModuleHandler?
    ) {
        val clFragmentManager = (context as AppCompatActivity).supportFragmentManager
        val confirmLocationDialog = ConfirmLocationDialog()
        confirmLocationDialog.moduleId = IConstants.ActionModule.CONFIRM_LOCATION
        confirmLocationDialog.actionHandler = actionHandler
        confirmLocationDialog.mAction = action
        confirmLocationDialog.show(clFragmentManager, "Confirm Location Dialog")
    }

    fun showFoodPersonalisation(
        context: Context,
        selectedVegan: String,
        selectedCuisineList: String,
        moduleHandler: IModuleHandler?

    ) {
        val fragmentManager = (context as AppCompatActivity).supportFragmentManager
        val dialog = FoodPersonalisationDialog(selectedVegan, selectedCuisineList)
        dialog.actionHandler = moduleHandler
        dialog.moduleId = IConstants.ActionModule.FOOD_PERSONALISATION
        dialog.show(fragmentManager, "Food Personalization")
    }

    fun showOrderTracking(context: Context, orderId: String) {

        if (mShowOrderTracking != null && mShowOrderTracking?.isVisible == true) {
            mShowOrderTracking?.dismiss()
        }

        val fragmentManager = (context as AppCompatActivity).supportFragmentManager
        mShowOrderTracking = OrderTrackingDialog()
        mShowOrderTracking?.moduleId = IConstants.ActionModule.ORDER_TRACKING
        mShowOrderTracking?.mOrderId = orderId
        mShowOrderTracking?.show(fragmentManager, "Order Tracking")

    }

    /* fun showSavedAddressDialog(
         context: Context
         , moduleHandler: IModuleHandler?
     ) {

         val clFragmentManager = (context as AppCompatActivity).supportFragmentManager
         val savedAddressDialog = SavedAddressDialog()
         // savedAddressDialog.actionHandler =
         savedAddressDialog.actionHandler = moduleHandler
         savedAddressDialog.actionId = IConstants.ActionModule.SAVED_ADDRESS
         savedAddressDialog.show(clFragmentManager, "Saved Address")
     }*/

    fun showTopDishesDialog(
        context: Context,
        promoId: String,
        title: String?,
        moduleHandler: IModuleHandler?,
        subLayoutType: Int = -1
    ) {
        if (mTopDishesDialog != null && mTopDishesDialog?.isVisible == true) {
            mTopDishesDialog?.dismiss()
        }
        val clFragmentManager = (context as AppCompatActivity).supportFragmentManager
        mTopDishesDialog = TopDishesDialog()
        mTopDishesDialog?.actionHandler = moduleHandler
        mTopDishesDialog?.moduleId = IConstants.ActionModule.TOP_DISHES
        mTopDishesDialog?.promoId = promoId
        mTopDishesDialog?.title = title ?: "Yumzy"
        mTopDishesDialog?.subLayoutType = subLayoutType
        mTopDishesDialog?.show(clFragmentManager, "Top dishes")
    }

    /* fun showRestaurantSearchDialog(
         context: Context,
         title: String,
         menuList: List<RestaurantItem>,
         moduleHandler: IModuleHandler?
     ) {
         val fragment = (context as AppCompatActivity).supportFragmentManager
         val searchDialog = RestaurantSearchDialog()
         searchDialog.activityContext = context
         searchDialog.actionHandler = moduleHandler
         searchDialog.moduleId = IConstants.ActionModule.RESTAURANT_ITEM_SEARCH
         searchDialog.title = title
         searchDialog.menuList = menuList.toList()
         searchDialog.show(fragment, "")
     }*/

    fun showAnimDialog(context: Context, lottiImage: String) {
        val clFragmentManager = (context as AppCompatActivity).supportFragmentManager

        val dialog = LottieFullDialog()
        dialog.imageUrl = lottiImage
        dialog.show(clFragmentManager, "Info Image")

    }

    fun showAnimSizedDialog(
        context: Context,
        lottiImage: String,
        title: String?,
        subTitle: String?,
        autoClose: Boolean
    ) {
        val clFragmentManager = (context as AppCompatActivity).supportFragmentManager

        val dialog = LottieSizedDialog()
        val baseAnimationTemplate = BaseAnimationTemplate(context, lottiImage)
        baseAnimationTemplate.imgUrl = lottiImage
        baseAnimationTemplate.showClose = autoClose
        dialog.lottieTemplate = baseAnimationTemplate
        baseAnimationTemplate.titleText = title
//        baseAnimationTemplate.bodyText = subTitle
        baseAnimationTemplate.footerText = subTitle
        dialog.show(clFragmentManager, "Info Image")

    }

    fun showAnimSizedDialog(
        context: Context,
        lottiImage: String,
        title: String?,
        subTitle: String?
    ) {
        showAnimSizedDialog(
            context,
            lottiImage,
            title,
            subTitle,
            false
        )

    }

    fun showTSeeAllDialog(
        context: Context,
        title: String,
        filter: HomeSectionDTO,

        moduleHandler: IModuleHandler?
    ) {
        val clFragmentManager = (context as AppCompatActivity).supportFragmentManager
        val seeAllDialog = SeeAllDialog(filter)
        seeAllDialog.actionHandler = moduleHandler
        seeAllDialog.moduleId = IConstants.ActionModule.SEE_ALL
        seeAllDialog.title = title
        seeAllDialog.show(clFragmentManager, title)
    }

    fun showRateUsDialog(
        clContext: Context,
        actionType: Int,
        sMessage: String,
        positiveText: String,
        negativeText: String,
        onActionPerformed: RateUsActionDialog.OnActionPerformed?
    ) {
        val clFragmentManager = (clContext as AppCompatActivity).supportFragmentManager
        val actionDialog =
            RateUsActionDialog(
                clContext,
                actionType,
                sMessage,
                positiveText,
                negativeText,
                onActionPerformed

            )
        actionDialog.show(clFragmentManager, "$actionType")
    }


}
