package com.yumzy.orderfood.ui.screens.home.fragment

import com.yumzy.orderfood.ui.base.IView

/**
 * Created by Bhupendra Kumar Sahu.
 */
interface ISearchedRestaurantView : IView {
    fun fetchSearchedRestaurantList();

}