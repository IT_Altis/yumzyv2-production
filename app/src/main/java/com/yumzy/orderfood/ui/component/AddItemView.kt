package com.yumzy.orderfood.ui.component

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.os.Handler
import android.text.TextUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.laalsa.laalsalib.ui.VUtil
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.common.imageview.RoundedImageView
import com.yumzy.orderfood.ui.helper.*
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.YUtils
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView

class AddItemView : LinearLayout, AddQuantityView.OnCounterChange {


    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {

        initAttrs(attrs, context, defStyleAttr)
        initView()


    }


    private var addHandler: Handler? = null

    private var update: Runnable? = null

    var itemId: String = ""
    var outletId: String = ""

    //    private lateinit var discountDrawable: Drawable
    var itemEnable: Boolean = true
        set(value) {
            field = value
            val quantityButton = findViewById<AddQuantityView>(R.id.tv_add_quantity)
            val customizeButton = findViewById<TextView>(R.id.tv_customize)
            quantityButton?.enableButton = itemEnable
            if (itemEnable) {
//                quantityButton?.buttonColor = 0xFF58D896.toInt()
                quantityButton?.setOnCounterChange(this)
                customizeButton?.visibility = View.VISIBLE
                this.alpha = 1f
            } else {
//                quantityButton?.buttonColor = 0xFFCECECE.toInt()
                quantityButton?.setOnCounterChange(null)
                customizeButton?.visibility = View.GONE
                this.alpha = 0.8f

            }


        }
    var isVeg: Boolean = true
        set(value) {
            field = value
            if (field)
                this.findViewById<TextView>(R.id.item_is_Veg)
                    ?.setTextColor(ThemeConstant.greenConfirmColor)
            else
                this.findViewById<TextView>(R.id.item_is_Veg)
                    ?.setTextColor(ThemeConstant.redCancelColor)
        }
    var showImage: Boolean = true
        set(value) {
            field = value
            if (field)
                this.findViewById<ImageView>(R.id.item_image)?.visibility = View.VISIBLE
            else
                this.findViewById<ImageView>(R.id.item_image)?.visibility = View.GONE
        }
    var showCustomize: Boolean = false
        set(value) {
            field = value
            if (field)
                this.findViewById<TextView>(R.id.tv_customize)?.visibility = View.VISIBLE
            else
                this.findViewById<TextView>(R.id.tv_customize)?.visibility = View.GONE
        }

    var ignoreCounterChange: Boolean = false
        set(value) {
            field = value
            this.findViewById<AddQuantityView>(R.id.tv_add_quantity).ignoreClickCount = field
        }

    var showCustomizeClick: Boolean = false
        set(value) {
            field = value
            if (field)
                this.findViewById<TextView>(R.id.tv_customize_click)?.visibility = View.VISIBLE
            else
                this.findViewById<TextView>(R.id.tv_customize_click)?.visibility = View.GONE
        }

    //    private var subTitle: String = ""
//        set(value) {
//            field = value
//            this.findViewById<TextView>(R.id.tv_subtitle)?.text = field
//
//        }
    private var title: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.tv_title)?.text = field
        }
    private var footerText: String? = ""
        set(value) {
            field = value
            val readMoreText = this.findViewById<TextView>(R.id.tv_fotter)
            if (footerText?.trim().isNullOrEmpty()) {
                readMoreText?.visibility = View.GONE
                return
            }
            readMoreText?.visibility = View.VISIBLE
            readMoreText.maxLines = 2
            readMoreText.ellipsize = TextUtils.TruncateAt.END
//            readMoreText?.setTrimLines(2)
//            readMoreText?.setTrimLength(125)

            readMoreText?.setText(footerText, TextView.BufferType.NORMAL)

//            findViewById.setShowingChar()
//            TextHelper.makeTextViewResizable(findViewById, 1, "More", true)
        }


    private var priced: CharSequence = ""
        set(value) {
            field = value
//            field = "₹ $value"
            this.findViewById<TextView>(R.id.tv_item_price)?.text = field
        }

    private var offerpriced: String = ""
        set(value) {
            field = value
//            field = "₹ $value"
            this.findViewById<TextView>(R.id.tv_item_offer_price)?.text = field
        }


    private fun initAttrs(
        attrs: AttributeSet?,
        context: Context?,
        defStyleAttr: Int
    ) {
        if (attrs != null) {
            val a =
                context!!.obtainStyledAttributes(
                    attrs,
                    R.styleable.AddItemView,
                    defStyleAttr,
                    0
                )

            title = a.getString(R.styleable.AddItemView_addItemItemTitle) ?: ""
            priced = a.getString(R.styleable.AddItemView_addItemPriced) ?: ""
            footerText = a.getString(R.styleable.AddItemView_addItemSubtitle) ?: footerText
            offerpriced = a.getString(R.styleable.AddItemView_addItemOfferPriced) ?: ""
            isVeg = a.getBoolean(R.styleable.AddItemView_itemVeg, true)
            showImage = a.getBoolean(R.styleable.AddItemView_itemImage, true)
            itemEnable = a.getBoolean(R.styleable.AddItemView_itemEnabled, true)
//            discountDrawable =VUtil.getIcon(context,R.string.icon_diff_modified, Color.RED)
            a.recycle()

        }
//        else {
//        }
//        this.setClickListenerBackground()
//        this.setOnClickListener {}
    }

    private fun initView() {
        val i62px = VUtil.dpToPx(62)
        this.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        this.orientation = VERTICAL
        this.setPadding(UIHelper.i3px, UIHelper.i5px, UIHelper.i3px, UIHelper.i5px)


        val layout = LinearLayout(context)

        layout.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        layout.orientation = HORIZONTAL


        val imageView = restaurantImageView()
        imageView.id = R.id.item_image

        if (isVeg)
            imageView.visibility = View.VISIBLE
        else
            imageView.visibility = View.GONE

        imageView.setImageDrawable(
            ResourcesCompat.getDrawable(
                context.resources,
                R.drawable.ic_default_img,
                null
            )
        )
        val lp = LayoutParams(i62px, i62px)
//        lp.setMargins(i4px, i4px, i4px, i4px)
        imageView.layoutParams = lp

        layout.addView(imageView)
        layout.addView(getIsVegDrawable())
        layout.addView(middleTextLayout())
        layout.addView(getQuantityButton())
        this.addView(addRibbonView())
        this.addView(layout)

    }

    var ribbonText: String? = null
        set(value) {
            field = value
            val view = this.findViewById<RibbonView>(R.id.ribbon_view)
            view?.ribbonText = ribbonText

        }

    private fun addRibbonView(): View {

        val ribbonView = RibbonView(context)
        ribbonView.id = R.id.ribbon_view
        ribbonView.ribbonText = ribbonText
        return ribbonView

    }
//    private fun addRibbonView(): ViewOverlay {
//
//        val textView = TextView(context)
//        if (!ribbonText.isNullOrEmpty()) {
//            this.visibility = View.VISIBLE
//            textView.text = ribbonText!!.capitalize()
//        }
//        else
//        {
//            this.visibility = View.GONE
//        }
////        textView.setTrimLength(125)
//        textView.setTextColor(Color.WHITE)
//        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)
//        textView.id = R.id.tv_ribbon_view
//        textView.setPadding(UIHelper.i5px, UIHelper.i1px, UIHelper.i5px, UIHelper.i1px)
//        textView.layoutParams =
//            LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ViewConst.WRAP_CONTENT)
////                TextHelper.makeTextViewResizable(textView, 1, "More", true)
//
//        val overlay: ViewOverlay = textView.getOverlay()
//        val bannerDrawable = BannerDrawable()
//        textView.post(Runnable { //top right square
//            bannerDrawable.setBounds(0, 0, textView.getWidth() / 4, textView.getHeight() / 2)
//            overlay.add(bannerDrawable)
//        })
//
//        return overlay
//
////        //@return The ViewOverlay object for this view.
////        val overlay: ViewOverlay = card.getOverlay()
////        val bannerDrawable = BannerDrawable()
////        card.post(Runnable { //top right square
////            bannerDrawable.setBounds(0, 0, card.getWidth() / 4, card.getHeight() / 2)
////            overlay.add(bannerDrawable)
////        })
////        val ribbonView = RibbonView(context)
////        ribbonView.id = R.id.ribbon_view
////        ribbonView.ribbonText = ribbonText
////        return ribbonView
//
//    }

    private fun getQuantityButton(): View {
        val layout = LinearLayout(context)
        layout.id = R.id.item_available_frame
        layout.orientation = VERTICAL
        layout.layoutParams.apply {
            layoutParams = LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)

        }
        val param = LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT
        )
        layout.layoutParams = param
        val addQuantityView = addQuantityView()
        layout.addView(addQuantityView)
        layout.addView(addProgressIndicator())
        layout.addView(getCustomizeText())
        return layout
    }

    fun getProgressBar(): ProgressBar? {
        return findViewById(R.id.pb_item_processeing)
    }

    private fun addProgressIndicator(): ProgressBar {
        val progressBar = ProgressBar(context, null, android.R.attr.progressBarStyleHorizontal)
            .apply {

                val colorFilter = PorterDuffColorFilter(
                    ContextCompat.getColor(context, R.color.blueJeans),
                    PorterDuff.Mode.MULTIPLY
                )

                indeterminateDrawable.colorFilter = colorFilter
            }
        progressBar.id = R.id.pb_item_processeing
        progressBar.minimumHeight = 0
        progressBar.minimumWidth = 0

//    progressBar.drawableState=
//    progressBar.indeterminateDrawable=context.getDrawable(R.drawable.progress_rounded)
        progressBar.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, VUtil.dpToPx(8)).apply {
//        setMargins(0,UIHelper.i2px,0,0)
            gravity = Gravity.CENTER
        }
        progressBar.isIndeterminate = false
        progressBar.visibility = View.INVISIBLE
        return progressBar
    }

    private fun getCustomizeText(): TextView {
        val customizeText = TextView(context)
        customizeText.layoutParams = LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        customizeText.text = context.resources.getString(R.string.customization)
        customizeText.id = R.id.tv_customize
        customizeText.gravity = Gravity.CENTER
        customizeText.setInterFont()
        customizeText.setTextColor(ThemeConstant.textLightGrayColor)
        customizeText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10f)
        return customizeText
    }

    private fun itemUnavailableView(): TextView {
        val unavailableText = TextView(context)
        unavailableText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
        unavailableText.setPadding(UIHelper.i5px, UIHelper.i5px, UIHelper.i5px, UIHelper.i5px)
        unavailableText.text = context.resources.getString(R.string.text_unavailable)
        unavailableText.layoutParams =
            LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        unavailableText.gravity = Gravity.CENTER
//        addButton.setPadding(15, 8, 15, 8)
        unavailableText.setTextColor(ThemeConstant.textGrayColor)
        unavailableText.background = UIHelper.getStrokeDrawable(
            ThemeConstant.textGrayColor,
            UIHelper.i5px.toFloat(),
            2
        )
//        unavailableText.setOnClickListener {
//            addItenListener?.onItemAdded(itemId, -1, "Unavailable", outletId)
//
//        }
        return unavailableText
    }

    private fun addQuantityView(): AddQuantityView {
        val addQuantityView = AddQuantityView(context)
        val param = LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT
        )
        param.setMargins(0, UIHelper.i10px, 0, 0)
        addQuantityView.layoutParams = param
        addQuantityView.id = R.id.tv_add_quantity
        if (itemEnable)
            addQuantityView.setOnCounterChange(this)
        else
            addQuantityView.setOnCounterChange(null)

        return addQuantityView
    }

    fun getQuantityView(): AddQuantityView? {
        return this.findViewById(R.id.tv_add_quantity)
    }

    fun setQuantity(value: Int) {
        val quantityView: AddQuantityView? = this.findViewById(R.id.tv_add_quantity)
        quantityView?.quantityCount = value


    }

    private fun middleTextLayout(): LinearLayout {
        val linearLayout = LinearLayout(context)
        linearLayout.orientation = VERTICAL
        linearLayout.gravity = Gravity.START
        linearLayout.addView(getTextView(title, 14f, R.id.tv_title))

        linearLayout.addView(
            getTextView(
                context.resources.getString(R.string.customizable),
                11f,
                R.id.tv_customize_click
            )
        )
        linearLayout.addView(getReadMoreTextView(footerText, 10f, R.id.tv_fotter))
        linearLayout.addView(getHorizontalLayout())

//        val i8px = VUtil.dpToPx(8)
        linearLayout.setPadding(0, 0, UIHelper.i5px, 0)
        linearLayout.layoutParams =
            LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f)
        return linearLayout

    }

    private fun getHorizontalLayout(): View? {
        val linearLayout = LinearLayout(context)
        linearLayout.orientation = HORIZONTAL
        linearLayout.gravity = Gravity.START
        linearLayout.addView(getTextView(priced, 12f, R.id.tv_item_price))
        if(!offerpriced.isNullOrEmpty())
        linearLayout.addView(getTextView(offerpriced, 12f, R.id.tv_item_offer_price))
        return linearLayout
    }

    private fun getReadMoreTextView(text: String?, textSize: Float, tvId: Int): View? {
        val textView = TextView(context)
        textView.text = text
//        textView.setTrimLength(125)
        textView.setTextColor(Color.GRAY)
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
        textView.id = tvId
        textView.setPadding(0, 0, 0, UIHelper.i3px)
        textView.layoutParams =
            LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ViewConst.WRAP_CONTENT)
//                TextHelper.makeTextViewResizable(textView, 1, "More", true)

        return textView


    }

    fun showItemAdding(showProgress: Boolean) {
//        if (showProgress == this.clickDisabled) return
        val progressBar = getProgressBar()
        if (progressBar != null)
            if (showProgress) {
                progressBar.isIndeterminate = true
                progressBar.visibility = View.VISIBLE
//                this.isEnabled = false
                getQuantityView()?.deepForEach {
                    this.isEnabled = false
                    this.alpha = 0.5f

                }

            } else {
                progressBar.isIndeterminate = false
                progressBar.visibility = View.INVISIBLE
//                this.isEnabled = true
                getQuantityView()?.deepForEach {
                    this.isEnabled = true
                    this.alpha = 1.0f

                }
            }
//        clickDisabled = showProgress

    }

    private fun getIsVegDrawable(): View? {
        val fontIconView = FontIconView(context)
        fontIconView.id = R.id.item_is_Veg
        fontIconView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
        if (isVeg)
            fontIconView.setTextColor(ThemeConstant.greenConfirmColor)
        else
            fontIconView.setTextColor(ThemeConstant.redAccent)

        fontIconView.text = context.getString(R.string.icon_veg_non_veg)
        val i5px = VUtil.dpToPx(5)
        fontIconView.setPadding(i5px, UIHelper.i3px, i5px, UIHelper.i3px)
        fontIconView.layoutParams = LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT
        )


        return fontIconView
    }


    private fun getTextView(text: CharSequence?, textSize: Float, tvId: Int): View {
        val textView = TextView(context)
        textView.text = text
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
        textView.id = tvId
        textView.setPadding(0, 0, 0, UIHelper.i3px)

        when (tvId) {
            R.id.tv_title -> {
                textView.setTextColor(ThemeConstant.textBlackColor)
                textView.setInterFont()
            }
            R.id.tv_fotter -> {
                textView.setTextColor(ThemeConstant.textBlackColor)
                textView.setInterFont()
                textView.layoutParams =
                    LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ViewConst.WRAP_CONTENT)
                textView.ellipsize = TextUtils.TruncateAt.END
                textView.maxLines = 2

//                TextHelper.makeTextViewResizable(textView, 1, "More", true)

            }

            R.id.tv_item_price -> {
                textView.setTextColor(ThemeConstant.textDarkGrayColor)
                textView.setPadding(0, VUtil.dpToPx(2), VUtil.dpToPx(5), 0)
                //textView.paintFlags = textView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                textView.setInterBoldFont()
            }

            R.id.tv_item_offer_price -> {
                textView.setTextColor(ThemeConstant.textDarkGrayColor)
                textView.setInterBoldFont()
            }
            R.id.tv_customize_click -> {
                textView.setPadding(
                    VUtil.dpToPx(3),
                    VUtil.dpToPx(3),
                    VUtil.dpToPx(3),
                    VUtil.dpToPx(2)
                )
                textView.setTextColor(ThemeConstant.pinkies)
                textView.visibility = if (showCustomizeClick) View.VISIBLE else View.GONE
                textView.setInterBoldFont()
            }
        }
//        textView.setOnClickListener(this)
        return textView

    }


    fun getItemNameView(): TextView {
        val findViewById = findViewById<TextView>(R.id.tv_title)
        return findViewById
    }

    fun setItemSpannedPrice(priced: CharSequence?) {
        this.priced = priced!!
        this.findViewById<TextView>(R.id.tv_item_price)
            ?.setText(priced, TextView.BufferType.SPANNABLE)
    }


    fun setItemPrice(priced: String) {
        this.priced = priced
        this.findViewById<TextView>(R.id.tv_item_price)?.text = "₹${this.priced}"
    }

    fun setItemOfferPrice(priced: String) {
        this.offerpriced = priced
        this.findViewById<TextView>(R.id.tv_item_offer_price)?.text = "₹${this.offerpriced}"
    }

    fun setTitles(titles: String) {
        this.title = titles
    }

    fun setSubtitle(subtitle: String) {
        this.footerText = subtitle
    }

    fun getCustomizableText(): TextView? {
        return findViewById(R.id.tv_customize_click)
    }

    fun setImageURL(imageUrl: String) {
        if (showImage) {
            val imageView = this.findViewById<ImageView>(R.id.item_image)
//            Glide.with(imageView).load(imageUrl).into(imageView)
            YUtils.setImageInView(imageView, imageUrl)

        }

        this.showImage = showImage
    }

    fun setIsVeg(isVeg: Boolean) {
        this.isVeg = isVeg
        if (isVeg)
            this.findViewById<TextView>(R.id.item_is_Veg)
                ?.setTextColor(ThemeConstant.greenConfirmColor)
        else
            this.findViewById<TextView>(R.id.item_is_Veg)
                ?.setTextColor(ThemeConstant.redCancelColor)
    }

    fun setImageShow(isShow: Boolean) {
        this.showImage = isShow
    }

    private fun restaurantImageView(): ImageView {
        val imageParam =
            LayoutParams(UIHelper.i60px, UIHelper.i80px)
        val roundImage = RoundedImageView(context)
        roundImage.layoutParams = imageParam
        roundImage.id = R.id.restaurant_image
        roundImage.cornerRadius = VUtil.dpToPx(10).toFloat()
        roundImage.scaleType = ImageView.ScaleType.CENTER_CROP
        return roundImage
    }


    override fun onCounterChange(count: Int) {
        if (showCustomize) {
            addItenListener?.onItemAdded(itemId, count, priced.toString(), outletId)
            return
        }
        /*if (count > 0) {
            if (update == null) {
                update =
                    Runnable {
                        addItenListener?.onItemAdded(itemId, count, priced, outletId)
                        addHandler = null
                        update=null
                    }
            }
            if (addHandler == null) {
                addHandler = Handler(Looper.getMainLooper())
            } else {
                addHandler!!.removeCallbacks(update!!)
            }
            addHandler!!.postDelayed(update!!, 500L)


        } else {
            addItenListener?.onItemAdded(itemId, count, priced, outletId)
        }*/

        if (count > 0) {

            if (addHandler == null || update == null) {
                addHandler = Handler()
            }
            update?.let { addHandler?.removeCallbacks(it) }

            update =
                Runnable {
                    addItenListener?.onItemAdded(itemId, count, priced.toString(), outletId)
                }

            addHandler?.postDelayed(update!!, 500L)
            //9770000033


        } else {
            addItenListener?.onItemAdded(itemId, count, priced.toString(), outletId)
        }

//        addItenListener?.onItemAdded(itemId, count, priced, outletId)
    }

    var addItenListener: AddItemListener? = null

    interface AddItemListener {
        fun onItemAdded(itemId: String, count: Int, priced: String, outletId: String)
    }

}