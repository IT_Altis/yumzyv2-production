package com.yumzy.orderfood.ui.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.yumzy.orderfood.R;
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView;
import com.laalsa.laalsalib.ui.VUtil;

/**
 * Created by Bhupendra Kumar Sahu
 */
public class HorizontalIconLabelView extends LinearLayout {
    private String icon;
    private String label;

    public HorizontalIconLabelView(Context context) {
        this(context, null);
    }


    public HorizontalIconLabelView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HorizontalIconLabelView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            initAttrs(context, attrs, defStyleAttr);
        if (icon == null) {
            icon = "";
        }
        if (label == null) {
            label = "";
        }

        init();

    }

    private void initAttrs(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.HorizontalIconLabelView, defStyleAttr, 0);
        icon = ta.getString(R.styleable.HorizontalIconLabelView_plus_icon);
        label = ta.getString(R.styleable.HorizontalIconLabelView_add_address);
        ta.recycle();
    }

    private void init() {
        this.setOrientation(HORIZONTAL);
        this.setGravity(Gravity.CENTER_VERTICAL | Gravity.START);


        FontIconView iconView = new FontIconView(getContext());

        int i100px = VUtil.dpToPx(100);
        int i10px = VUtil.dpToPx(10);
        iconView.setLayoutParams(new LayoutParams(i100px, i100px));
        iconView.setId(R.id.iv_icon_address);
        iconView.setTextColor(getResources().getColor(R.color.colorPrimary));
        iconView.setText(icon);
        iconView.setPadding(i10px, i10px, i10px, i10px);


        TextView tvLabel = new TextView(getContext());
        tvLabel.setPadding(i10px, i10px, i10px, i10px);
        tvLabel.setId(R.id.tv_add_address);

        tvLabel.setTextColor(this.getResources().getColor(R.color.colorPrimary));
        tvLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f);
        tvLabel.setTypeface(Typeface.DEFAULT_BOLD);
        tvLabel.setText(label);

        this.addView(iconView);
        this.addView(tvLabel);
    }

    public String getIconAddress() {
        return icon;
    }

    public void setIconAddress(String icon) {
        this.icon = icon;
        ((TextView) findViewById(R.id.iv_icon_address)).setText(this.icon);
    }

    public String getLabelAddress() {
        return label;
    }

    public void setLabelAddress(String label) {
        this.label = label;
        ((TextView) findViewById(R.id.tv_add_address)).setText(this.label);
    }
}
