package com.yumzy.orderfood.ui.screens.cart

import androidx.lifecycle.distinctUntilChanged
import com.yumzy.orderfood.data.dao.CartResDao
import com.yumzy.orderfood.data.dao.SelectedItemDao
import com.yumzy.orderfood.data.dao.ValuePairSI
import com.yumzy.orderfood.data.models.CartResDTO
import com.yumzy.orderfood.data.models.ItemCountDTO
import com.yumzy.orderfood.data.models.NewAddItemDTO
import com.yumzy.orderfood.data.models.base.APIConstant
import com.yumzy.orderfood.data.models.base.ResponseDTO
import com.yumzy.orderfood.worker.*

class CartRepository(
    private val cartDao: CartResDao?,
    private val remoteSourceRemote: CartRemoteDataSource,
    private val selectedItemDao: SelectedItemDao
) {
    val userCart = cartDao?.getCartRes()

    /*
    * Update remote data base and save the response in local database and if something occurred, default value from local database is returned
    * */
    fun addToCart(
        addCart: NewAddItemDTO
    ) = resultLiveData(
        databaseQuery = {
            cartDao?.getCartRes()!!
        },
        networkCall = {
            remoteSourceRemote.addToCart(addCart)
        },
        saveCallResult = {
            cartDao?.updateOrInsert(it)
        }
    )

    fun addToCartRemote(
        addCart: NewAddItemDTO
    ) = singleNetoworkEmit(
        networkCall = {
            remoteSourceRemote.addToCart(addCart)
        },
        handleResponse = { data, status ->
            when (status) {
                APIConstant.Status.SUCCESS -> {
                    cartDao?.updateOrInsert(data)
                }
                APIConstant.Status.ERROR -> {

                }
            }
        }
    )

    /*
    * Update remote data base and save the response in local database and if something occurred, default value from local database is returned
    * */
    fun itemCount(
        position: Int,
        count: Int,
        itemCount: ItemCountDTO
    ) = resultStrictTypeLiveData(
        networkCall = {
            remoteSourceRemote.changeItemCount(position, count, itemCount)
        },
        saveCallResult = {
            cartDao?.updateOrInsert(it)
        },
        databaseQuery = {
            cartDao?.getCartRes()!!
        }
    ).distinctUntilChanged()


    /*
    * Gets cart from data base, then fetch data form network and save it to database
    * */
    fun getCart(
        cartId: String
    ) = resultLiveData(
        databaseQuery = {
            cartDao?.getCartRes()!!
        },
        networkCall = {
            remoteSourceRemote.getCart(cartId)
        },
        saveCallResult = {
            cartDao?.updateOrInsert(it)
            selectedItemDao.deleteAll()
            val selectedMap = mutableMapOf<String, Int>()
            if (it.outletDTOS?.size ?: 0 > 0) {
                it.outletDTOS?.get(0)?.itemDTOS?.forEach { item ->

                    if (selectedMap.containsKey(item.itemID)) {
                        selectedMap[item.itemID] =
                            selectedMap.get(item.itemID)?.plus(item.quantity) ?: 0
                    } else {
                        selectedMap[item.itemID] = item.quantity
                    }

                }
            }

            val selectedList = arrayListOf<ValuePairSI>()
            for ((key, value) in selectedMap) {
                selectedList.add(ValuePairSI(key, value))
            }
            if (selectedList.isNotEmpty()) selectedItemDao.insertAll(selectedList)
        }
    )


    /*
    * Gets cart from remote datasource and invoke database query to remove cart
    * */
    fun clearCart(
        cartId: String
    ) = clearCartLiveData(
        networkCall = {
            remoteSourceRemote.clearCart(cartId)
        },
        updateNetworkCall = {
            remoteSourceRemote.getCart(cartId)
        },
        saveCallResult = {
            cartDao?.updateOrInsert(it as CartResDTO)
        },
        backgroundCallback = {
            selectedItemDao.deleteAll()

        }
    ).distinctUntilChanged()

    fun addSelectedItem(valuePairSI: ValuePairSI) = backgroundLiveData<Boolean>(
        backgroundCallback = {
            run {
                selectedItemDao.insertItem(valuePairSI)
                ResponseDTO.executed(data = true)
            }

        }
    )

    fun removeSelectedItem(itemId: String) = backgroundLiveData(
        backgroundCallback = {
            if (itemId != null) {
                selectedItemDao.removeItemById(itemId)
                ResponseDTO.executed(data = true)
            } else
                ResponseDTO.executed(data = false)

        }
    )


    fun removeAllSelectedItem() = updateDatabase {
        selectedItemDao.deleteAll()
    }

}