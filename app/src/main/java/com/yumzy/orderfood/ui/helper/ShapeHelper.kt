package com.yumzy.orderfood.ui.helper

import android.graphics.*
import android.graphics.drawable.Drawable
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.Shape
import androidx.annotation.ColorInt
import com.laalsa.laalsaui.utils.ColorHelper

object ShapeHelper {

    @JvmStatic
    fun deformBubbleDrawable(
        color1: Int = 0xFF556AE9.toInt(),
        color2: Int = 0xFF2A29E8.toInt(),
        shadowColor: Int = 0xFFD7D4F3.toInt()
    ): Drawable? {
        val mPath = Path()
        val shape: Shape =
            object : Shape() {
                override fun draw(canvas: Canvas, paint: Paint) {
                    val canvasWidth = canvas.width + 4f
                    val canvasHeight = canvas.height + 4f

                    paint.setShadowLayer(10f, 4f, 2f, shadowColor)
                    paint.shader = LinearGradient(
                        0f,
                        0f,
                        canvasWidth,
                        0f,
                        color1,
                        color2,
                        Shader.TileMode.MIRROR
                    )
                    mPath.reset()
                    mPath.moveTo(0f, 0f)
                    mPath.lineTo(0f, canvasHeight * 0.8f)
                    mPath.cubicTo(
                        0f,
                        canvasHeight * 0.8f,
                        canvasWidth * 0.35f,
                        canvasHeight * 1.22f,
                        canvasWidth * 0.55f,
                        canvasHeight * 0.65f
                    )

                    mPath.cubicTo(
                        canvasWidth * 0.55f,
                        canvasHeight * 0.65f,
                        canvasWidth * 0.65f,
                        canvasHeight * 0.4f,
                        canvasWidth * 0.85f,
                        canvasHeight * 0.4f,
                    )
                    mPath.cubicTo(
                        canvasWidth * 0.85f,
                        canvasHeight * 0.4f,
                        canvasWidth * 0.95f,
                        canvasHeight * 0.4f,
                        canvasWidth * 0.95f,
                        canvasHeight * 0.3f,
                    )

                    mPath.cubicTo(
                        canvasWidth * 0.95f,
                        canvasHeight * 0.3f,
                        canvasWidth * 0.95f,
                        canvasHeight * 0.20f,
                        canvasWidth * 0.85f,
                        canvasHeight * 0.20f,
                    )

                    mPath.cubicTo(
                        canvasWidth * 0.85f,
                        canvasHeight * 0.20f,
                        canvasWidth * 0.65f,
                        canvasHeight * 0.2f,
                        canvasWidth * 0.6f,
                        0f,
                    )
                    mPath.lineTo(0f, 0f)
                    canvas.drawPath(mPath, paint)
                }
            }
        return ShapeDrawable(shape)
    }

    @JvmStatic
    fun deformBubbleRightBottom(
        color1: Int = 0xFF556AE9.toInt(),
        color2: Int = 0xFF2A29E8.toInt(),
        shadowColor: Int = 0xFFD7D4F3.toInt()
    ): Drawable? {
        val mPath = Path()
        val shape: Shape =
            object : Shape() {
                override fun draw(canvas: Canvas, paint: Paint) {
                    val canvasWidth = canvas.width + 4f
                    val canvasHeight = canvas.height + 4f

                    paint.setShadowLayer(10f, 4f, 2f, shadowColor)
                    paint.shader = LinearGradient(
                        0f,
                        0f,
                        canvasWidth,
                        0f,
                        color1,
                        color2,
                        Shader.TileMode.MIRROR
                    )
                    mPath.reset()
                    mPath.moveTo(0f, 0f)
                    mPath.lineTo(0f, canvasHeight * 0.8f)
                    mPath.cubicTo(
                        0f,
                        canvasHeight * 0.8f,
                        canvasWidth * 0.35f,
                        canvasHeight * 1.22f,
                        canvasWidth * 0.55f,
                        canvasHeight * 0.65f
                    )

                    mPath.cubicTo(
                        canvasWidth * 0.55f,
                        canvasHeight * 0.65f,
                        canvasWidth * 0.65f,
                        canvasHeight * 0.4f,
                        canvasWidth * 0.85f,
                        canvasHeight * 0.4f,
                    )
                    mPath.cubicTo(
                        canvasWidth * 0.85f,
                        canvasHeight * 0.4f,
                        canvasWidth * 0.95f,
                        canvasHeight * 0.4f,
                        canvasWidth * 0.95f,
                        canvasHeight * 0.3f,
                    )

                    mPath.cubicTo(
                        canvasWidth * 0.95f,
                        canvasHeight * 0.3f,
                        canvasWidth * 0.95f,
                        canvasHeight * 0.20f,
                        canvasWidth * 0.85f,
                        canvasHeight * 0.20f,
                    )

                    mPath.cubicTo(
                        canvasWidth * 0.85f,
                        canvasHeight * 0.20f,
                        canvasWidth * 0.65f,
                        canvasHeight * 0.2f,
                        canvasWidth * 0.6f,
                        0f,
                    )
                    mPath.lineTo(0f, 0f)
                    canvas.translate(canvasWidth, canvasHeight)
                    canvas.rotate(180f)
                    canvas.drawPath(mPath, paint)
                }
            }
        return ShapeDrawable(shape)
    }


    @JvmStatic
    fun waveTopRightShape(
        color1: Int = 0xFFFFC04C.toInt(),
        color2: Int = 0xFFFF7C18.toInt(),
        shadowColor: Int = 0xFFD7D4F3.toInt()
    ): Drawable? {
        val mPath = Path()
        val shape: Shape =
            object : Shape() {
                override fun draw(canvas: Canvas, paint: Paint) {
                    val canvasWidth = canvas.width + 2f
                    val canvasHeight = canvas.height + 2f

                    paint.setShadowLayer(5f, -2f, 2f, shadowColor)
                    paint.shader = LinearGradient(
                        0f,
                        0f,
                        0f,
                        canvasHeight,
                        color1,
                        color2,
                        Shader.TileMode.MIRROR
                    )
                    mPath.reset()
                    paint.alpha = 125
                    mPath.moveTo(canvasWidth, canvasHeight * 0.7f)
                    mPath.lineTo(canvasWidth, 0f)
                    mPath.lineTo(canvasWidth * 0.6f, 0f)
                    mPath.cubicTo(
                        canvasWidth * 0.6f, 0f,
                        canvasWidth * 0.3f, canvasHeight * 0.4f,
                        canvasWidth * 0.7f, canvasHeight * 0.4f
                    )
                    mPath.cubicTo(
                        canvasWidth * 0.7f, canvasHeight * 0.4f,
                        canvasWidth * 0.8f, canvasHeight * 0.4f,
                        canvasWidth * 0.7f, canvasHeight * 0.6f
                    )
                    mPath.cubicTo(
                        canvasWidth * 0.7f, canvasHeight * 0.6f,
                        canvasWidth * 0.5f, canvasHeight * 0.9f,
                        canvasWidth, canvasHeight * 0.7f
                    )
                    canvas.drawPath(mPath, paint)
                    mPath.reset()
                    mPath.moveTo(canvasWidth, canvasHeight * 0.8f)
                    mPath.lineTo(canvasWidth, 0f)
                    mPath.lineTo(canvasWidth * 0.7f, 0f)
                    mPath.cubicTo(
                        canvasWidth * 0.7f, 0f,
                        canvasWidth * 0.15f, canvasHeight * 0.5f,
                        canvasWidth * 0.6f, canvasHeight * 0.47f
                    )
                    mPath.cubicTo(
                        canvasWidth * 0.6f, canvasHeight * 0.47f,
                        canvasWidth * 0.95f, canvasHeight * 0.4f,
                        canvasWidth * 0.8f, canvasHeight * 0.65f
                    )
                    mPath.cubicTo(
                        canvasWidth * 0.8f, canvasHeight * 0.65f,
                        canvasWidth * 0.5f, canvasHeight,
                        canvasWidth, canvasHeight * 0.8f
                    )
                    canvas.drawPath(mPath, paint)

                }
            }
        return ShapeDrawable(shape)
    }


    @JvmStatic
    fun waveBottomRightShape(
        color1: Int = 0xFFFFC04C.toInt(),
        color2: Int = 0xFFFF7C18.toInt(),
        shadowColor: Int = 0xFFD7D4F3.toInt()
    ): Drawable? {
        val mPath = Path()
        val shape: Shape =
            object : Shape() {
                override fun draw(canvas: Canvas, paint: Paint) {
                    val canvasWidth = canvas.width + 2f
                    val canvasHeight = canvas.height + 2f

                    paint.setShadowLayer(5f, -2f, 2f, shadowColor)
                    paint.shader = LinearGradient(
                        0f,
                        0f,
                        0f,
                        canvasHeight,
                        color1,
                        color2,
                        Shader.TileMode.MIRROR
                    )
                    mPath.reset()
                    paint.alpha = 125
                    mPath.moveTo(canvasWidth, canvasHeight * 0.7f)
                    mPath.lineTo(canvasWidth, 0f)
                    mPath.lineTo(canvasWidth * 0.6f, 0f)
                    mPath.cubicTo(
                        canvasWidth * 0.6f, 0f,
                        canvasWidth * 0.3f, canvasHeight * 0.4f,
                        canvasWidth * 0.7f, canvasHeight * 0.4f
                    )
                    mPath.cubicTo(
                        canvasWidth * 0.7f, canvasHeight * 0.4f,
                        canvasWidth * 0.8f, canvasHeight * 0.4f,
                        canvasWidth * 0.7f, canvasHeight * 0.6f
                    )
                    mPath.cubicTo(
                        canvasWidth * 0.7f, canvasHeight * 0.6f,
                        canvasWidth * 0.5f, canvasHeight * 0.9f,
                        canvasWidth, canvasHeight * 0.7f
                    )
                    canvas.drawPath(mPath, paint)
                    mPath.reset()
                    mPath.moveTo(canvasWidth, canvasHeight * 0.8f)
                    mPath.lineTo(canvasWidth, 0f)
                    mPath.lineTo(canvasWidth * 0.7f, 0f)
                    mPath.cubicTo(
                        canvasWidth * 0.7f, 0f,
                        canvasWidth * 0.15f, canvasHeight * 0.5f,
                        canvasWidth * 0.6f, canvasHeight * 0.47f
                    )
                    mPath.cubicTo(
                        canvasWidth * 0.6f, canvasHeight * 0.47f,
                        canvasWidth * 0.95f, canvasHeight * 0.4f,
                        canvasWidth * 0.8f, canvasHeight * 0.65f
                    )
                    mPath.cubicTo(
                        canvasWidth * 0.8f, canvasHeight * 0.65f,
                        canvasWidth * 0.5f, canvasHeight,
                        canvasWidth, canvasHeight * 0.8f
                    )
                    canvas.translate(canvasWidth, canvasHeight)
                    canvas.rotate(180f)
                    canvas.drawPath(mPath, paint)

                }
            }
        return ShapeDrawable(shape)
    }

    fun narrowDownArcShape(@ColorInt naVColor: Int, barHeight: Float): Drawable? {
        val mPath = Path()
        val shape: Shape = object : Shape() {
            override fun draw(canvas: Canvas, paint: Paint) {
                paint.shader = LinearGradient(
                    0f, 0f, canvas.width / 2f, height,
                    ColorHelper.lightenColor(naVColor, 0.2f), naVColor, Shader.TileMode.MIRROR
                )
                //                paint.setShadowLayer(10, 4, 0, 0xffdedede);
//                paint.setColor(naVColor);
                val canvasWeight = canvas.width
                val canvasHeight = canvas.height
                mPath.reset()
                mPath.moveTo(canvasWeight / 2.toFloat(), 0f)
                mPath.lineTo(0f, 0f)
                mPath.lineTo(0f, canvasHeight * 0.8f)
                mPath.cubicTo(
                    0f, canvasHeight.toFloat(), canvasWeight / 2.toFloat(), canvasHeight * 1.2f,
                    canvasWeight * 0.6f, canvasHeight * 0.55f
                )
                mPath.cubicTo(
                    canvasWeight * 0.6f,
                    canvasHeight * 0.6f,
                    canvasWeight * 0.6f,
                    canvasHeight * 0.2f,
                    canvasWeight.toFloat(),
                    barHeight
                )
                mPath.lineTo(canvasWeight.toFloat(), 0f)
                mPath.close()
                canvas.drawPath(mPath, paint)
            }
        }
        shape.height
        return ShapeDrawable(shape)
    }

}