package com.yumzy.orderfood.ui.helper

import android.text.Html
import android.text.Spanned

object StringUtil {
    fun getQuantityString(quantity: Int): String? {
        return "Qty: $quantity"
    }

    fun convertIntToString(value: Int): String? {
        return "($value)"
    }

    fun convertDoubleToString(value: Double, decimal: Int): String? {
        return "(${value.withPrecision(decimal)})"
    }

    fun loadTextToHtml(text: String): Spanned {
        return if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
        } else {
            Html.fromHtml(text)
        }
    }
}