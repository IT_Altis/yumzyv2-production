package com.yumzy.orderfood.ui.screens.restaurant

import android.graphics.*
import android.graphics.drawable.Drawable


class RibbonDrawable(ribbonText: String?) : Drawable() {
    private val SQRT_2 = Math.sqrt(2.0)
    private var mTextBounds: Rect? = null
    private var mPaintFill: Paint? = null
    private var mPaintText: Paint? = null
    private val mMessage = ribbonText
    private val mBannerWidth = 40
    private var mTextSize = 0

    fun BannerDrawable() {
        initPaintFill()
        initPaintText()
        mTextBounds = Rect()
    }

    private fun initPaintFill() {
        mPaintFill = Paint(Paint.ANTI_ALIAS_FLAG)
        mPaintFill!!.style = Paint.Style.FILL
        mPaintFill!!.color = Color.RED
    }

    private fun initPaintText() {
        mPaintText = Paint(Paint.ANTI_ALIAS_FLAG)
        mPaintText!!.style = Paint.Style.FILL
        mPaintText!!.color = Color.WHITE
        mPaintText!!.textSize = 10f
        mPaintText!!.setShadowLayer(4.0f, 2.0f, 2.0f, Color.BLACK)
    }

    override fun draw(canvas: Canvas) {
        var bounds = bounds
        if (bounds.isEmpty) {
            bounds = canvas.clipBounds
        }
        val width = bounds.width().toFloat()
        adaptTextSize((width * 0.9).toFloat(), (mBannerWidth * 0.9).toInt())
        val bannerHyp = (mBannerWidth * SQRT_2).toFloat()
        canvas.translate(0f, bounds.centerY() - mBannerWidth.toFloat())
        canvas.rotate(315f, bounds.centerX().toFloat(), bounds.centerY() - mBannerWidth.toFloat())
        canvas.drawRect(
            bounds.left - bannerHyp,
            bounds.top.toFloat(),
            bounds.right + bannerHyp,
            bounds.top + mBannerWidth.toFloat(),
            mPaintFill!!
        )
        canvas.drawText(
            mMessage!!, 0f, mBannerWidth / 2 + mTextBounds!!.height() / 2.toFloat(),
            mPaintText!!
        )
    }

    private fun adaptTextSize(width: Float, height: Int) {
        if (mTextSize > 0) {
            mPaintText!!.textSize = mTextSize.toFloat()
            return
        }
        var textSize = 10
        var textHeight: Int
        var textWidth: Int
        var stop = false
        while (!stop) {
            mTextSize = textSize++
            mPaintText!!.textSize = mTextSize.toFloat()
            mPaintText!!.getTextBounds(mMessage, 0, mMessage!!.length, mTextBounds)
            textHeight = mTextBounds!!.height()
            textWidth = mTextBounds!!.width()
            stop = textHeight >= height || textWidth >= width
        }
    }

    override fun setAlpha(alpha: Int) {}

    override fun setColorFilter(cf: ColorFilter?) {}

    override fun getOpacity(): Int {
        return PixelFormat.OPAQUE
    }
}