package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.LayoutProfileInfoBinding

class ProfileInfoView : FrameLayout {

    var email: CharSequence? = null
        set(value) {
            field = value
            binding.email = field.toString()
        }

    var name: CharSequence? = null
        set(value) {
            field = value
            binding.name = field.toString()
        }
    var mobile: CharSequence? = null
        set(value) {
            field = value
            binding.mobile = field.toString()
        }
    var pic: CharSequence? = null
        set(value) {
            field = value
            binding.pic = field.toString()
        }


    private val binding by lazy {
        val layoutInflater = LayoutInflater.from(context)
        LayoutProfileInfoBinding.inflate(layoutInflater, this, true)
    }


    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {

        if (attrs != null) {
            val a =
                context.obtainStyledAttributes(
                    attrs,
                    R.styleable.ProfileInfoView,
                    defStyleAttr,
                    0
                )


            email = a.getString(R.styleable.ProfileInfoView_u_email) ?: ""
            name = a.getString(R.styleable.ProfileInfoView_u_name) ?: ""
            mobile = a.getString(R.styleable.ProfileInfoView_u_mobile) ?: ""
            pic = a.getString(R.styleable.ProfileInfoView_u_pic) ?: ""
            a.recycle()

        }
        editClick()

    }

    private fun editClick() {
        binding.tvActionEdit.setOnClickListener {

            onEdit?.invoke(it)

        }
    }

    var onEdit: ((view: View) -> Unit)? = null

}




