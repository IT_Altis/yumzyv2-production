package com.yumzy.orderfood.ui.screens.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.appstart.InfoBarDTO
import com.yumzy.orderfood.databinding.AdapterBottomBinding
import com.yumzy.orderfood.ui.common.ActionModuleHandler
import com.yumzy.orderfood.ui.common.IModuleHandler
import com.yumzy.orderfood.ui.component.chromtab.ChromeTabHelper
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.YUtils


class BottomPageAdapter(
    val context: AppCompatActivity,
    var infoBar: List<InfoBarDTO>,
    var moduleHandler: IModuleHandler?

)

    : RecyclerView.Adapter<BottomPageAdapter.PagerVH>() {
    override fun getItemCount(): Int = infoBar.size

    override fun onBindViewHolder(holder: PagerVH, position: Int) {

        holder.adapterBind.txtBannerTitle.text =
                if (!infoBar[position].status.isNullOrEmpty()) infoBar[position].text?.capitalize() else infoBar[position].outletName

        holder.adapterBind.txtBannerMsg.text = infoBar.get(position).description ?: ""
        YUtils.setImageInView(holder.adapterBind.imgBannerSub, infoBar.get(position).icon)
//        Glide.with(context).load(infoBar.get(position).icon).into(holder.adapterBind.imgBannerSub)


        /*holder.adapterBind.imgBannerCross.setOnClickListener {
            holder.adapterBind.root.visibility = View.GONE
//            infoBar
//            remove adapter from here
        }*/

        /*if (infoBar[position].isMandatory) {
            holder.adapterBind.imgBannerCross.visibility = View.GONE
        } else {
            holder.adapterBind.imgBannerCross.visibility = View.VISIBLE
        }*/


        when (infoBar[position].landingPage.toLowerCase()) {
            IConstants.AppStart.TRACKING -> {
                holder.adapterBind.llBanner.isClickable = false

                holder.adapterBind.txtBannerClick.visibility = View.VISIBLE
                holder.adapterBind.txtBannerClick.text = context.getString(R.string.track_now)
            }
            IConstants.AppStart.RATE -> {
                holder.adapterBind.llBanner.isClickable = false

                holder.adapterBind.txtBannerClick.visibility = View.VISIBLE
                holder.adapterBind.txtBannerClick.text = context.getString(R.string.rate_now)
            }
            IConstants.AppStart.RAIN -> {
                holder.adapterBind.llBanner.isClickable = true

                holder.adapterBind.txtBannerClick.visibility = View.VISIBLE
                holder.adapterBind.txtBannerClick.text = ""

            }
            else -> holder.adapterBind.txtBannerClick.visibility = View.GONE
        }


        if (!(infoBar[position].landingPage.contains(
                IConstants.AppStart.TRACKING,
                true
            ) || infoBar[position].landingPage.contains(IConstants.AppStart.RATE, true))
        ) {
            holder.adapterBind.txtBannerTitle.text = infoBar[position].subject
            holder.adapterBind.llBanner.setOnClickListener {
                if (!infoBar[position].webviewUrl.isNullOrEmpty()) {
                    holder.adapterBind.llBanner.isClickable = true
                    ChromeTabHelper.launchChromeTab(
                        context,
                        infoBar[position].webviewUrl.toString()
                    )

                } else {
                    holder.adapterBind.llBanner.isClickable = false
                }
            }
        }


        holder.adapterBind.txtBannerClick.setOnClickListener {
//            holder.adapterBind.txtBannerClick.isEnabled = true

            when (infoBar[position].landingPage.toLowerCase()) {
                IConstants.AppStart.TRACKING -> {
                    ActionModuleHandler.showOrderTracking(
                        context,
                        infoBar.get(position).orderId.toString()
                    )
                }
                IConstants.AppStart.RATE -> {
                    ActionModuleHandler.rateOrder(
                        context,
                        infoBar.get(position).orderId.toString(),
                        infoBar.get(position).orderNum.toString(),
                        infoBar.get(position).outletName.toString(),
                        "", moduleHandler
                    )
                }
                IConstants.AppStart.RAIN ->

                    if (!infoBar[position].webviewUrl.isNullOrEmpty()) {
                        holder.adapterBind.llBanner.isClickable = true
                    }
                else -> {
                }
            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PagerVH {
        val binding: AdapterBottomBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.adapter_bottom,
            parent,
            false
        )
        return PagerVH(binding)
    }

    //    fun updateList(infoBar: List<InfoBar>) {
//        infoBar = infoBar
//        notifyDataSetChanged()
//    }
    inner class PagerVH(val adapterBind: AdapterBottomBinding) : RecyclerView.ViewHolder(adapterBind.root)
}

