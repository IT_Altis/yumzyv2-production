package com.yumzy.orderfood.ui.base.actiondialog

import androidx.annotation.ColorInt
import com.yumzy.orderfood.ui.helper.ThemeConstant

data class ActionItemDTO(
    val key: Int,
    val text: CharSequence,
    @ColorInt val textColor: Int = ThemeConstant.white,
    @ColorInt val buttonColor: Int? = ThemeConstant.logoImage,
    val data: MutableMap<String, Any>? = null
)