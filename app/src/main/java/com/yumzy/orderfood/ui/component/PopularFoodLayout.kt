package com.yumzy.orderfood.ui.component

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsaui.drawable.DrawableUtils
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.common.imageview.RoundedImageView
import com.yumzy.orderfood.ui.helper.*
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.YUtils
import com.yumzy.orderfood.util.viewutils.YumUtil
import com.yumzy.orderfood.util.viewutils.fontutils.IconFontDrawable


/**
 *    Created by Sourav Kumar Pandit
 * */
class PopularFoodLayout : ConstraintLayout {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {

        initAttrs(attrs, context, defStyleAttr)
        initView()


    }


    private lateinit var discountDrawable: Drawable
    var availableRestaurant: Boolean = true
        set(value) {
            field = value
            if (value)
                this.alpha = 0.5f
            else
                this.alpha = 1f

        }
    var icon: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.font_icon_view)?.text = field
        }
    var subTitle: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.tv_fotter)?.text = field

        }
    var title: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.tv_title)?.text = field
        }

    var itemPrice: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.tv_coupon_code)?.text = field
        }

    var bottomText: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.tv_time_price)?.text = field
        }

    var imageUrl: String = ""
        set(value) {
            field = value
            val imageView = this.findViewById<ImageView>(R.id.restaurant_image)
//            Glide.with(imageView).load(imageUrl).into(imageView)

            YUtils.setImageInView(imageView, imageUrl)

        }


    private fun initAttrs(
        attrs: AttributeSet?,
        context: Context?,
        defStyleAttr: Int
    ) {
        if (attrs != null) {
            val a =
                context!!.obtainStyledAttributes(
                    attrs,
                    R.styleable.PopularFoodLayout,
                    defStyleAttr,
                    0
                )
            this.layoutParams =
                ViewGroup.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)

            title = a.getString(R.styleable.PopularFoodLayout_foodItemTitle) ?: ""
            subTitle = a.getString(R.styleable.PopularFoodLayout_foodSubtitle) ?: ""
            itemPrice = a.getString(R.styleable.PopularFoodLayout_foodColorText) ?: ""
            bottomText = a.getString(R.styleable.PopularFoodLayout_foodBottomText) ?: ""
            discountDrawable =
                YumUtil.getIcon(context, R.drawable.ic_discount, ThemeConstant.pinkies)!!
            a.recycle()

        }
//        else {
//        }
        this.setClickScaleEffect()
        this.setOnClickListener {}
    }

    private fun initView() {

        this.layoutParams =

            ViewGroup.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)

        val roundImage = restaurantImageView()

        val layout = LinearLayout(context)
        val linearParam = LayoutParams(0, 0)
        linearParam.endToEnd = LayoutParams.PARENT_ID
        linearParam.topToTop = LayoutParams.PARENT_ID
        linearParam.bottomToBottom = R.id.restaurant_image
        linearParam.startToEnd = R.id.restaurant_image
        layout.layoutParams = linearParam
        layout.orientation = LinearLayout.VERTICAL
        layout.setPadding(VUtil.dpToPx(15), 0, 0, 0)


        layout.addView(getTextView(title, 16f, 1, R.id.tv_title))
        layout.addView(getTextView(subTitle, 12f, 1, R.id.tv_fotter))
        layout.addView(getTextView(itemPrice, 12f, 1, R.id.tv_price_drawable))
        layout.addView(getButtonView(bottomText, 1, R.id.btn_add_item))

        val rattingView = getRattingView(context)

        this.addView(roundImage)
        this.addView(layout)
        this.addView(rattingView)

    }

    private fun getButtonView(bottomText: String, i: Int, btnAddItem: Int): View {
        return Button(context).apply {

            this.text = "Add to Plate"
            this.setTextColor(Color.WHITE)
            this.setInterBoldFont()
            isAllCaps = false
            this.background = DrawableUtils.getRoundDrawableListState(
                ThemeConstant.greenAddItem,
                UIHelper.i8px.toFloat(),
                ThemeConstant.greenDarkAddItem,
                UIHelper.i8px.toFloat()
            )
            layoutParams = LinearLayout.LayoutParams(ViewConst.WRAP_CONTENT, ViewConst.WRAP_CONTENT)
            this.setPadding(UIHelper.i20px, UIHelper.i10px, UIHelper.i20px, UIHelper.i10px)
            this.minimumWidth = 0
            this.minimumHeight = 0
            this.minHeight = 0
            this.minWidth = 0

        }
    }

    private fun getRattingView(context: Context): View {

        val rattingView = RattingCardView(context)
        val rattingParam = LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT
        )

        rattingParam.marginStart = VUtil.dpToPx(5)
        rattingParam.bottomMargin = VUtil.dpToPx(5)
        rattingParam.bottomToBottom = R.id.restaurant_image
        rattingParam.startToStart = LayoutParams.PARENT_ID
        rattingView.layoutParams = rattingParam

        return rattingView
    }

    private fun restaurantImageView(): ImageView {
        val imageParam =
            LayoutParams(VUtil.dpToPx(100), 0)
        imageParam.topToTop = LayoutParams.PARENT_ID
        imageParam.startToStart = LayoutParams.PARENT_ID
        imageParam.dimensionRatio = "3:4"


        val roundImage = RoundedImageView(context)
        roundImage.layoutParams = imageParam
//        roundImage.minimumWidth=VUtil.dpToPx(250)
        roundImage.id = R.id.restaurant_image
        roundImage.cornerRadius = VUtil.dpToPx(10).toFloat()
        roundImage.setImageResource(YUtils.getPlaceHolder())
        roundImage.scaleType = ImageView.ScaleType.CENTER_CROP
        return roundImage
    }


    private fun getTextView(text: String, textSize: Float, minLine: Int, tvId: Int): View {
        val textView = TextView(context)
        textView.setInterFont()
        textView.text = text
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
        textView.id = tvId
        when (tvId) {
            R.id.tv_title -> {
                textView.setInterBoldFont()
                textView.setPadding(0, 0, 0, UIHelper.i5px)
                textView.setTextColor(ThemeConstant.textBlackColor)
            }
            R.id.tv_fotter -> {
                textView.setTextColor(ThemeConstant.mediumGray)
//                textView.gravity = Gravity.TOP or Gravity.START
                textView.layoutParams = LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0, 1f)
            }
            R.id.tv_price_drawable -> {
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)
                textView.setPadding(0, 0, 0, UIHelper.i5px)
                val iconDrawable = IconFontDrawable(context, R.string.icon_veg_non_veg)
                textView.setCompoundDrawablesWithIntrinsicBounds(iconDrawable, null, null, null)
                textView.compoundDrawablePadding = UIHelper.i5px
                textView.setTextColor(ThemeConstant.textBlackColor)

            }
            R.id.btn_add_item -> {
                textView.setTextColor(ThemeConstant.textGrayColor)
            }
        }
        return textView

    }

}