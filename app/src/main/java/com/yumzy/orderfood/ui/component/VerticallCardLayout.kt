/*
 * Created By Shriom Tripathi 9 - 5 - 2020
 */

package com.yumzy.orderfood.ui.component

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ImageSpan
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.laalsa.laalsalib.ui.VUtil
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.common.imageview.RoundedImageView
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.setClickScaleEffect
import com.yumzy.orderfood.ui.helper.setInterBoldFont
import com.yumzy.orderfood.ui.helper.setInterFont
import com.yumzy.orderfood.util.YUtils
import com.yumzy.orderfood.util.viewutils.YumUtil

class VerticallCardLayout : ConstraintLayout {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {

        initAttrs(attrs, context, defStyleAttr)
        initView()


    }

    private lateinit var discountDrawable: Drawable
    private var availableRestaurant: Boolean = true
        set(value) {
            field = value
            if (value)
                this.alpha = 0.5f
            else
                this.alpha = 1f

        }
    private var icon: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.font_icon_view)?.text = field
        }
    private var subTitle: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.tv_fotter)?.text = field

        }
    private var title: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.tv_title)?.text = field
        }

    private var colorText: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.tv_coupon_code)?.text = field
        }

    private var bottomText: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.tv_time_price)?.text = field
        }
    private var restaurantCardListener: RestaurantCardListener? = null


    private fun initAttrs(
        attrs: AttributeSet?,
        context: Context?,
        defStyleAttr: Int
    ) {
        if (attrs != null) {
            val a =
                context!!.obtainStyledAttributes(
                    attrs,
                    R.styleable.HorizontalCardLayout,
                    defStyleAttr,
                    0
                )

            title = a.getString(R.styleable.HorizontalCardLayout_horizontalItemTitle) ?: ""
            subTitle = a.getString(R.styleable.HorizontalCardLayout_horizontalSubtitle) ?: ""
            colorText = a.getString(R.styleable.HorizontalCardLayout_horizontalColorText) ?: ""
            bottomText = a.getString(R.styleable.HorizontalCardLayout_horizontalBottomText) ?: ""
            discountDrawable =
                YumUtil.getIcon(context, R.drawable.ic_discount, ThemeConstant.pinkies)!!
            a.recycle()

        }
//        else {
//        }
        this.setClickScaleEffect()
        this.setOnClickListener {}
    }

    private fun initView() {

        this.layoutParams =

            ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)

        val roundImage = restaurantImageView()

        val layout = LinearLayout(context)
        val linearParam = LayoutParams(LayoutParams.MATCH_PARENT, 0)
        linearParam.endToEnd = LayoutParams.PARENT_ID
        linearParam.startToStart = LayoutParams.PARENT_ID
        linearParam.topToBottom = R.id.restaurant_image
        layout.layoutParams = linearParam
        layout.orientation = LinearLayout.VERTICAL
        layout.setPadding(VUtil.dpToPx(15), 0, 0, 0)


        layout.addView(getTextView(title, 14f, 1, R.id.tv_title))
        layout.addView(getTextView(subTitle, 12f, 1, R.id.tv_fotter))
        layout.addView(getTextView(colorText, 12f, 2, R.id.tv_coupon_code))
        layout.addView(getTextView(bottomText, 12f, 1, R.id.tv_time_price))

        val rattingView = getRattingView(context)

        this.addView(roundImage)
        this.addView(layout)
        this.addView(rattingView)

    }

    private fun getRattingView(context: Context): View {

        val rattingView = RattingCardView(context).apply {
            layoutParams = LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT
            ).apply {
                setMargins(VUtil.dpToPx(5), 0, 0, VUtil.dpToPx(5))
                startToStart = LayoutParams.PARENT_ID
            }
            setRating(
                resources.getString(R.string.icon_star),
                null,
                backgroundColorInt = ThemeConstant.white
            )
        }
        return rattingView
    }

    private fun restaurantImageView(): ImageView {
        val imageParam =
            LayoutParams(VUtil.dpToPx(70), 0)
        imageParam.topToTop = LayoutParams.PARENT_ID
        imageParam.startToStart = LayoutParams.PARENT_ID
        imageParam.dimensionRatio = "1:1"

        val roundImage = RoundedImageView(context)
        roundImage.layoutParams = imageParam
        roundImage.id = R.id.restaurant_image
        roundImage.cornerRadius = VUtil.dpToPx(10).toFloat()
        roundImage.setImageResource(YUtils.getPlaceHolder())
        roundImage.scaleType = ImageView.ScaleType.CENTER_CROP
        return roundImage
    }


    private fun getTextView(text: String, textSize: Float, minLine: Int, tvId: Int): View {
        val textView = TextView(context)
        textView.setInterFont()
        textView.text = text
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
        textView.id = tvId
        when (tvId) {
            R.id.tv_title -> {
                textView.setInterBoldFont()
                textView.setTextColor(ThemeConstant.textBlackColor)
            }
            R.id.tv_fotter -> {
                textView.setTextColor(ThemeConstant.textDarkGrayColor)
//                textView.gravity = Gravity.TOP or Gravity.START
                textView.layoutParams = LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0, 1f)
            }
            R.id.tv_coupon_code -> {
                textView.setTextColor(ThemeConstant.pinkies)
                val (spannableString, d: Drawable) = spanDrawable(textSize)
                val span = ImageSpan(d, 2)
                spannableString.setSpan(
                    span,
                    0,
                    1,
                    Spannable.SPAN_INCLUSIVE_EXCLUSIVE
                )
                textView.text = spannableString
            }
            R.id.tv_time_price -> {
                textView.setTextColor(ThemeConstant.textGrayColor)
            }
        }
//        textView.setOnClickListener(this)
        return textView

    }

    private fun spanDrawable(textSize: Float): Pair<SpannableString, Drawable> {
        val spannableString = SpannableString("@ $colorText")

        val iconWidth = VUtil.dpToPx(textSize.toInt() - 5)
        val iconHeight = VUtil.dpToPx(textSize.toInt())
        discountDrawable.setBounds(0, 0, iconWidth, iconHeight)
        return Pair(spannableString, discountDrawable)
    }

    /*override fun onClick(view: View) {
        when (view.id) {
            R.id.iv_rounded_image -> this.restaurantCardListener?.onRestaurantImageClick()
            R.id.tv_title -> this.restaurantCardListener?.onRestaurantImageClick()
            R.id.tv_subtitle -> this.restaurantCardListener?.onRestaurantImageClick()
            else -> this.restaurantCardListener?.onRestaurantClick()
        }
    }
*/

    fun setAddressClickListener(restaurantCardListener: RestaurantCardListener) {
        this.restaurantCardListener = restaurantCardListener
    }

    interface RestaurantCardListener {
        fun onRestaurantImageClick()
        fun onRestaurantTitleClick(value: String?)
        fun onRestaurantClick()
    }
}