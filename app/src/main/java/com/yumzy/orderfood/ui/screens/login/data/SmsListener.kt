package com.yumzy.orderfood.ui.screens.login.data

/**
 * Created by Bhupendra Kumar Sahu on 26-Jun-20.
 */
interface SmsListener {
    fun messageReceived(messageText: String?)
    fun onTimeOut(messageText: String?)

}