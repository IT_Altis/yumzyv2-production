package com.yumzy.orderfood.ui.component

import android.content.Context
import android.text.TextUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.widget.ImageView
import android.widget.TextView
import com.google.android.material.card.MaterialCardView
import com.laalsa.laalsalib.ui.px
import com.laalsa.laalsalib.ui.pxf
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.common.imageview.RoundedImageView
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.hideEmptyTextView
import com.yumzy.orderfood.ui.helper.setInterBoldFont
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.YUtils
import com.yumzy.orderfood.util.viewutils.VDrawableUtils

class PopularCuisineItemView : MaterialCardView {

    var url: String = ""
        set(value) {
            YUtils.setImageInView(this.findViewById<RoundedImageView>(R.id.restaurant_image), value)
            field = value
        }


    var title: String? = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.tv_title).hideEmptyTextView(value)
        }

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, atr: AttributeSet?) : this(context, atr, 0)
    constructor(context: Context, atr: AttributeSet?, style: Int) : super(context, atr, style) {
        this.addView(roundedImageView(url))
        this.addView(getBottomTitle(title))
    }

    fun setPopularCuisine(url: String, title: String?) {
        this.url = url
        this.title = title
    }

    private fun roundedImageView(url: String): ImageView {
        val imageParam = LayoutParams(ViewConst.MATCH_PARENT, ViewConst.MATCH_PARENT)
        val roundImage = ImageView(context)
        roundImage.layoutParams = imageParam
        roundImage.id = R.id.restaurant_image
        roundImage.scaleType = ImageView.ScaleType.CENTER_CROP
        YUtils.setImageInView(roundImage, url)

        return roundImage
    }

    private fun getBottomTitle(title: String?): TextView {
        return TextView(context).apply {
            val lp = LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
            lp.gravity = Gravity.BOTTOM
            layoutParams = lp
            minHeight = UIHelper.i45px
            setInterBoldFont()
            hideEmptyTextView(title)
            ellipsize = TextUtils.TruncateAt.END
            maxLines = 2
            minLines = 2
            this.background = VDrawableUtils.topAngledDrawable(0x80000000.toInt(), 4.pxf)

            setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)
            setTextColor(ThemeConstant.white)
            gravity = Gravity.CENTER
            id = R.id.tv_title

            setPadding(10.px, 20.px, 10.px, 8.px)
        }
    }

}