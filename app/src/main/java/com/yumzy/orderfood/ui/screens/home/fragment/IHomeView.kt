package com.yumzy.orderfood.ui.screens.home.fragment

import com.yumzy.orderfood.ui.base.IView
import com.yumzy.orderfood.ui.common.ConfirmActionDialog
import com.yumzy.orderfood.ui.common.IModuleHandler

interface IHomeView : IView, IModuleHandler, ConfirmActionDialog.OnActionPerformed {
    fun fetchHomeData(latitude: String, longitude: String)
}
