package com.yumzy.orderfood.ui.common

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.annotation.IdRes
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.appbar.AppBarLayout
import com.yumzy.orderfood.R

class ElevationAppBarLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : AppBarLayout(context, attrs) {

    @IdRes
    private var attachedScrollerId: Int = 0
    private var elevationAmount: Float = 6f

    init {
        attrs?.let {
            val typedArray = context.obtainStyledAttributes(
                it,
                R.styleable.ElevationAppBarLayout, 0, 0
            )
            attachedScrollerId =
                typedArray.getResourceId(R.styleable.ElevationAppBarLayout_scrollingView, 0)
            elevationAmount =
                typedArray.getFloat(R.styleable.ElevationAppBarLayout_elevationAmount, 6f)
            typedArray.recycle()
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        if (attachedScrollerId != 0) {
            val connectedView: View = rootView.findViewById(attachedScrollerId)
            if (connectedView is RecyclerView || connectedView is NestedScrollView) {
                monitorScroll(connectedView)
            } else {
                throw IllegalStateException("You can only attach a RecyclerView or NestedScrollView to your ScrollingAwareAppBarLayout!")
            }
        } else {
            throw IllegalStateException("You should attach a scrollable view to your ScrollingAwareAppBarLayout!")
        }
    }

    private fun monitorScroll(scroller: View) {
        when (scroller) {
            is RecyclerView -> {
                scroller.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                        elevation = if (!scroller.canScrollVertically(-1)) {
                            0f
                        } else {
                            elevationAmount
                        }
                    }

                    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                        // No op
                    }
                })
            }
            is NestedScrollView -> {
                scroller.setOnScrollChangeListener { _: NestedScrollView, _: Int, scrollY: Int, _: Int, _: Int ->
                    elevation = if (scrollY == 0) {
                        0f
                    } else {
                        elevationAmount
                    }
                }
            }
        }
    }
}