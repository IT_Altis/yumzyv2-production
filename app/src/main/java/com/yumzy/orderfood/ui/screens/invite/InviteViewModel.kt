package com.yumzy.orderfood.ui.screens.invite

import com.yumzy.orderfood.data.models.ContactModelDTO
import com.yumzy.orderfood.ui.base.BaseViewModel
import com.yumzy.orderfood.ui.screens.invite.data.InviteRepository


import javax.inject.Inject


class InviteViewModel @Inject constructor( private val repository: InviteRepository
) : BaseViewModel<IInvite>() {

    fun userDetails() = repository.profileUser()

    fun saveUserContacts(userPhoneNumber: String, contacts: List<ContactModelDTO>) = repository.saveUserContacts(userPhoneNumber, contacts)

}
