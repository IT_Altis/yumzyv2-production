package com.yumzy.orderfood.ui.screens.restaurant.adapter

/*

class SearchItemAdapter(
    private val context: Context? = null,
    private val interaction: OutletInteraction? = null
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val menuItems: ArrayList<RestaurantItem> = ArrayList()
    private val selectedItem: ArrayList<ValuePairSI> = ArrayList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(context)
        val itemBinding: AdapterRestaurentMenuBinding =
            AdapterRestaurentMenuBinding.inflate(layoutInflater, parent, false)

        return RestaurantItemHolder(
            itemBinding,
            interaction
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is RestaurantItemHolder -> {
                menuItems.get(position).let { holder.bind(it) }
            }
        }
    }

    override fun getItemCount(): Int {
        return menuItems.size
    }

    fun setListData(itemList: List<RestaurantItem>) {
        menuItems.clear()
        menuItems.addAll(itemList)
        interaction?.adapterSizeChange(menuItems.size)
        notifyDataSetChanged()
    }

    fun setSelectedItemData(itemList: List<ValuePairSI>) {
        selectedItem.clear()
        selectedItem.addAll(itemList)
        notifyDataSetChanged()
    }

    inner class RestaurantItemHolder
    constructor(
        val adapterBinding: AdapterRestaurentMenuBinding,
        private val interaction: OutletInteraction?
    ) : RecyclerView.ViewHolder(adapterBinding.root) {


        private lateinit var restaurantItem: RestaurantItem

        fun bind(item: RestaurantItem) = with(itemView) {
            setOnClickListener {
                interaction?.onItemSelected(adapterPosition, restaurantItem)
            }

//            if ()


            restaurantItem = item
            adapterBinding.addItemView.setTitles(restaurantItem.name.capitalize())
            adapterBinding.addItemView.setSubtitle(restaurantItem.description)
            adapterBinding.addItemView.setItemPrice("${restaurantItem.price}")
            adapterBinding.addItemView.ribbonText = item.ribbon

//            adapterBinding.addItemView.setImageURL(restaurantItem.imageUrl)
            adapterBinding.addItemView.showImage = false
            adapterBinding.addItemView.isVeg = restaurantItem.isVeg
            adapterBinding.addItemView.showCustomize = restaurantItem.addons.isNotEmpty()
            adapterBinding.addItemView.setItemPrice(restaurantItem.price.toString())
            adapterBinding.addItemView.showItemAdding(item.isItemAdding)

//            if (selectedItem.)
            var itemQuantity: Int = 0
//            kotlin.run {  }
            selectedItem.run {
                this.forEach {
                    if (it.id == item.itemId) {
                        itemQuantity = it.value
                        return@forEach
                    }
                }
            }
            adapterBinding.addItemView.getQuantityView()?.quantityCount = itemQuantity

            adapterBinding.addItemView.addItenListener = object : AddItemView.AddItemListener,
                ICartHandler {
                override fun onItemAdded(
                    itemId: String,
                    count: Int,
                    priced: String,
                    outletId: String
                ) {
                    if (adapterPosition >= 0) {
                        menuItems[adapterPosition].isItemAdding = true
                        adapterBinding.addItemView.showItemAdding(true)
                        interaction?.onItemCountChange(
                            this,
                            count,
                            restaurantItem,
                            adapterBinding.addItemView.getQuantityView()!!
                        )
                    }
                }

                override fun itemAdded(count: Int) {

                    if (adapterPosition >= 0) {
                        menuItems[adapterPosition].isItemAdding = false
                        adapterBinding.addItemView.showItemAdding(false)

                    }

                }

                override fun itemRepeated() {
                }

                override fun failedToAdd(
                    count: Int,
                    view: AddQuantityView
                ) {
                    if (adapterPosition >= 0) {
                        menuItems[adapterPosition].isItemAdding = false
                        adapterBinding.addItemView.showItemAdding(false)
                        adapterBinding.addItemView.setQuantity(menuItems[adapterPosition].prevQuantity)
                    }

                }

            }
        }
    }

}*/
