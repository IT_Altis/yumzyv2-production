package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.ui.px
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.setHorizontalListDecoration
import com.yumzy.orderfood.ui.screens.home.fragment.globalHomeItemListener
import com.yumzy.orderfood.ui.screens.restaurant.adapter.BindingWinterSpecialItem
import com.yumzy.orderfood.ui.screens.restaurant.adapter.PopularCuisinesItem
import com.yumzy.orderfood.ui.screens.restaurant.adapter.TheBrandItem
import io.cabriole.decorator.LinearBoundsMarginDecoration
import io.cabriole.decorator.LinearDividerDecoration
import io.cabriole.decorator.LinearMarginDecoration

class WinterSpecialListView : RecyclerView {

    //save our FastAdapter
    private var fastItemAdapter: FastItemAdapter<BindingWinterSpecialItem> = FastItemAdapter()

    init {
        fastItemAdapter.onClickListener =
            { v: View?, _: IAdapter<BindingWinterSpecialItem>, item: BindingWinterSpecialItem, _: Int ->
                if (v != null) {
                    globalHomeItemListener?.invoke(v, item.model)
                }
                false
            }


    }


    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        //get our recyclerView and do basic setup
        this.layoutManager = LinearLayoutManager(context, HORIZONTAL, false)
        this.overScrollMode = OVER_SCROLL_NEVER
        this.adapter = fastItemAdapter
        this.clipToPadding = false
        this.clipChildren = false
        this.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        this.setHorizontalListDecoration()
    }

    fun clearData() {
        fastItemAdapter.clear()
        this.visibility = View.GONE
    }

    fun setData(
        homeList: List<HomeListDTO>
    ) {
        if (homeList.isNullOrEmpty()) {
            this.visibility = View.GONE
            return
        }
        this.visibility = View.VISIBLE
        val items = ArrayList<BindingWinterSpecialItem>()
        homeList.forEach { homeItem ->
            homeItem.let { item ->
                items.add(
                    BindingWinterSpecialItem(item)
                )
            }
        }

        fastItemAdapter.add(items)

    }
}


class BrandListListView : RecyclerView {

    private var fastItemAdapter: FastItemAdapter<TheBrandItem> = FastItemAdapter()


    init {
        fastItemAdapter.onClickListener =
            { v: View?, _: IAdapter<TheBrandItem>, item: TheBrandItem, _: Int ->

                if (v != null) {
                    globalHomeItemListener?.invoke(v, item.model)
                }
                false
            }
    }


    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        //get our recyclerView and do basic setup
        this.layoutManager = LinearLayoutManager(context, HORIZONTAL, false)
        this.overScrollMode = OVER_SCROLL_NEVER
        this.adapter = fastItemAdapter
        this.clipToPadding = false
        this.clipChildren = false
        this.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)

        this.setPadding(12.px, 0, 0, 0)
        this.setHorizontalListDecoration()

    }

    fun clearData() {
        fastItemAdapter.clear()
        this.visibility = View.GONE
    }


    fun setData(
        section: List<HomeListDTO>?
    ) {
        if (section.isNullOrEmpty()) {
            this.visibility = View.GONE
            return
        }
        this.visibility = View.VISIBLE


        val items = ArrayList<TheBrandItem>()
        section.forEach { homeItem ->
            homeItem.let { item ->
                items.add(
                    TheBrandItem(item)
                )
            }
        }

        fastItemAdapter.add(items)

    }
}

class PopularCuisinesListView : RecyclerView {
    //save our FastAdapter
    private var fastItemAdapter: FastItemAdapter<PopularCuisinesItem> = FastItemAdapter()
    init {
        fastItemAdapter.onClickListener =
            { v: View?, _: IAdapter<PopularCuisinesItem>, item: PopularCuisinesItem, _: Int ->
                if (v != null) {
                    globalHomeItemListener?.invoke(v, item.model)
                }
                false
            }
    }

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        //get our recyclerView and do basic setup
        this.layoutManager = LinearLayoutManager(context, HORIZONTAL, false)
        this.overScrollMode = OVER_SCROLL_NEVER
        this.adapter = fastItemAdapter
        this.clipToPadding = false
        this.clipChildren = false
        this.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        this.setHorizontalListDecoration()
    }

    fun clearData() {
        fastItemAdapter.clear()
        this.visibility = View.GONE
    }

    fun setData(
        homeList: List<HomeListDTO>
    ) {

        if (homeList.isNullOrEmpty()) {
            this.visibility = View.GONE
            return
        }
        this.visibility = View.VISIBLE

        val items = ArrayList<PopularCuisinesItem>()
        homeList.forEach { homeItem ->
            homeItem.let { item ->
                items.add(
                    PopularCuisinesItem(item)
                )
            }
        }

        fastItemAdapter.add(items)

    }
}


class OutletCategoryListView : RecyclerView {

    //save our FastAdapter
    private var fastItemAdapter: FastItemAdapter<BindingWinterSpecialItem> = FastItemAdapter()

    init {
        fastItemAdapter.onClickListener =
            { v: View?, _: IAdapter<BindingWinterSpecialItem>, item: BindingWinterSpecialItem, _: Int ->
                if (v != null) {
                    globalHomeItemListener?.invoke(v, item.model)
                }
                false
            }


    }


    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        //get our recyclerView and do basic setup
        this.layoutManager = LinearLayoutManager(context, HORIZONTAL, false)
        this.overScrollMode = OVER_SCROLL_NEVER
        this.adapter = fastItemAdapter
        this.clipToPadding = false
        this.clipChildren = false
        this.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)

    }

    fun clearData() {
        fastItemAdapter.clear()
        this.visibility = View.GONE
    }

    fun setData(
        homeList: List<HomeListDTO>
    ) {
        if (homeList.isNullOrEmpty()) {
            this.visibility = View.GONE
            return
        }
        this.visibility = View.VISIBLE
        val items = ArrayList<BindingWinterSpecialItem>()
        homeList.forEach { homeItem ->
            homeItem.let { item ->
                items.add(
                    BindingWinterSpecialItem(item)
                )
            }
        }

        fastItemAdapter.add(items)

    }
}

