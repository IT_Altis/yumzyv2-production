package com.yumzy.orderfood.ui.helper

import android.content.Context
import android.graphics.Color
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt

object ThemeConstant {

    const val activityBackground: Int = Color.WHITE
    const val redRibbonColor = 0xFFFC6576.toInt()
    const val popShadow = 0x7f0e011c.toInt()
    const val lightBlue = 0xFFC4D6EF.toInt()
    const val vegColor = 0xFF008300.toInt()
    const val thinBlue2 = 0x8381ACF8.toInt()
    const val thinBlue1 = 0x83C6D7F6.toInt()
    const val blueJeans = 0xFF5D9CEC.toInt()
    const val mediumGray = 0xFFCCD1D9.toInt()
    const val transparent = 0x00000000.toInt()
    const val greenConfirmColor = 0xFF58D896.toInt()
    const val blueLinkColor = 0xFF1492E6.toInt()
    const val greenAccent = 0xFF58D896.toInt()
    const val greenAddItem = 0xFFFE0076.toInt()
    const val greenAlphaMenu = 0xBF58D896.toInt()
    const val greenDarkAddItem = 0xFF48AA77.toInt()
    const val redCancelColor = 0xFFDD2D4A.toInt()
    const val nonVegColor = Color.RED
    const val redAccent = 0xFFEC87Cf0.toInt()
    const val textBlackColor = 0xff252427.toInt()
    const val alphaBlackColor = 0xAD000000.toInt()
    const val white = Color.WHITE
    const val textDarkGrayColor = Color.DKGRAY
    const val textGrayColor = Color.GRAY
    const val pinkies = 0xFFFE0076.toInt()
    const val pinkiesLight = 0xFFFF097B.toInt()
    const val textLightGrayColor = Color.LTGRAY
    const val buttonColor = 0xFF58D896.toInt()
    const val buttonColorDark = 0xFF40B87A.toInt()
    const val lightAlphaGray = 0X3ABFC2C2.toInt()
    const val lightGray = 0XFFF5F7FA.toInt()
    const val whiteSmoke = 0xFFEFF2EF.toInt()
    const val yellow = 0xFFFFC107.toInt()
    const val darkBlue = 0xFF2D3597.toInt()
    const val lightGreen = 0xFF593DDC97.toInt()
    const val pinkiesOne = 0xFFFEA6B1.toInt()
    const val pinkiesTwo = 0xFFEF718D.toInt()
    const val alphaLightGreen = 0xFFF9FDFB.toInt()
    const val alphaMediumGray = 0x1A000000.toInt()
    const val sunglowYellow = 0xFFFFCE47.toInt()
    const val eggColor = 0xFFFFCE47.toInt()
    const val crayolaOrange = 0xFFFC7536.toInt()
    const val frenchPink = 0xFFFF4797.toInt()
    const val normalVoilet = 0xFF7F2FEE.toInt()
    const val crayolaGreen = 0xFFC8EF81.toInt()
    const val lightSkyBlue = 0xC1F8F8F8.toInt()
    const val rippleColor = 0x1EA2A2A2.toInt()
    const val rainbowBlue = 0xFF127a97.toInt()
    const val logoImage = pinkies
    const val logoText = 0xFF213455.toInt()
    const val alphaLogoText = 0x40213455.toInt()
    const val logoTextLite = 0xFF3C4759.toInt()
    const val divider = 0xBFDDDDDD.toInt()

    @ColorInt
    fun getThemedColor(context: Context, @AttrRes themeResId: Int): Int {
        val a = context.obtainStyledAttributes(null, intArrayOf(themeResId))
        try {
            return a.getColor(0, 9)
        } finally {
            a.recycle()
        }
    }
}