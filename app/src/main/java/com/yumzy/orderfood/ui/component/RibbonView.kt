package com.yumzy.orderfood.ui.component

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.laalsa.laalsaui.utils.dpf
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.setInterFont
import com.yumzy.orderfood.util.ViewConst

class RibbonView : CardView {
    private var ribbonTextView: TextView? = null

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        this.layoutParams = ViewGroup.LayoutParams(ViewConst.WRAP_CONTENT, ViewConst.WRAP_CONTENT)
        val param = LayoutParams(ViewConst.WRAP_CONTENT, ViewConst.WRAP_CONTENT)
        param.setMargins(UIHelper.i3px, UIHelper.i3px, 0, UIHelper.i3px)
        this.layoutParams = param

        ribbonTextView = TextView(context)
        ribbonTextView!!.setPadding(UIHelper.i5px, UIHelper.i1px, UIHelper.i5px, UIHelper.i1px)
        ribbonTextView!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)
        ribbonTextView!!.setTextColor(Color.WHITE)
        ribbonTextView!!.setInterFont()
        radius = 5.dpf
        clipToPadding = false
        clipChildren = false
        setCardBackgroundColor(ColorStateList.valueOf(ThemeConstant.redRibbonColor))
        elevation = 2f
        this.addView(ribbonTextView)
        ribbonText = null

    }

    var ribbonText: String? = null
        set(value) {
            field = value?.trim()
            if (!ribbonText.isNullOrEmpty()) {
                this.visibility = View.VISIBLE
                ribbonTextView?.text = ribbonText!!.capitalize()
                ribbonTextView?.invalidate()
            } else
                this.visibility = View.GONE

        }

    fun ribbonTextSize(textSize: Float) {
        ribbonTextView?.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
    }
}
