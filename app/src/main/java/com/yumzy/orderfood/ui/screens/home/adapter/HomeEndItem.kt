package com.yumzy.orderfood.ui.screens.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.IExpandable
import com.mikepenz.fastadapter.IParentItem
import com.mikepenz.fastadapter.ISubItem
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.mikepenz.fastadapter.binding.BindingViewHolder
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.ItemHomeEndBinding

class HomeEndItem() : AbstractBindingItem<ItemHomeEndBinding>(),
    IExpandable<BindingViewHolder<ItemHomeEndBinding>> {

    var fassiLicNo: String = ""

    var sectionName: String? = ""
    var sectionItemCount: Int? = 0
    var sectionId: Int = 0

    override var parent: IParentItem<*>? = null
    override var isExpanded: Boolean = false

    var header: String? = null

    override var subItems: MutableList<ISubItem<*>>
        get() = mutableListOf()
        set(_) {}

    override val isAutoExpanding: Boolean
        get() = true

    /**
     * defines the type defining this item. must be unique. preferably an id
     *
     * @return the type
     */
    override val type: Int
        get() = R.id.home_end_item


    /**
     * setter method for the Icon
     *
     * @param item the icon
     * @return this
     */
    fun witLicNo(fassiLicNo: String): HomeEndItem {
        this.fassiLicNo = fassiLicNo
        return this
    }

    fun withSection(sectionId: Int, section: String, sectionSize: Int): HomeEndItem {
        this.sectionId = sectionId
        this.sectionName = section
        this.sectionItemCount = sectionSize
        return this
    }

    /**
     * binds the data of this item onto the viewHolder
     */
    override fun bindView(binding: ItemHomeEndBinding, payloads: List<Any>) {
        //define our data for the view
        if (fassiLicNo != null && fassiLicNo.isNotBlank()) {
            binding.tvFassi.text = "Licence No. ${fassiLicNo}"
        } else {
            binding.tvFassi.text = "Applied for Licence"
        }
    }

    override fun unbindView(binding: ItemHomeEndBinding) {
        //TODO unbind the values here from binding

    }

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ItemHomeEndBinding {
        return ItemHomeEndBinding.inflate(inflater, parent, false)
    }
}