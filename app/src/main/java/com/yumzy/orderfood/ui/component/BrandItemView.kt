package com.yumzy.orderfood.ui.component

import android.content.Context
import android.text.TextUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsaui.drawable.DrawableUtils
import com.laalsa.laalsaui.utils.pxf
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.common.imageview.RoundedImageView
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.helper.setInterBoldFont
import com.yumzy.orderfood.ui.helper.setInterThineFont
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.YUtils

class BrandItemView : LinearLayout {


    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, atr: AttributeSet?) : this(context, atr, 0)
    constructor(context: Context, atr: AttributeSet?, style: Int) : super(context, atr, style) {

        this.apply {
            orientation = VERTICAL
            gravity = Gravity.CENTER_VERTICAL
            setPadding(0, UIHelper.i15px, 0, UIHelper.i15px)
            layoutParams = LayoutParams(UIHelper.i150px, ViewConst.WRAP_CONTENT)
//                .apply {
//                setMargins(VUtil.dpToPx(8), VUtil.dpToPx(4), VUtil.dpToPx(8), VUtil.dpToPx(4))
//            }
        }

        this.background = DrawableUtils.getRoundDrawableListState(
            0xFFF7FAFF.toInt(),
            10.pxf,
            0xFFF7FAFF.toInt(),
            10.pxf
        )
    }

    fun setWinterSpecial(url: String, title: String, subtitle: Double?) {
        this.apply {
            removeAllViews()
            addView(roundedImageView(url))
            addView(getBottomTitle(title))
            if (subtitle != 0.0) {
                addView(subtitle?.let { getBottomLabel(it) })

            }

        }

    }

    private fun roundedImageView(url: String): RoundedImageView {
        val imageParam = LayoutParams(VUtil.dpToPx(90), VUtil.dpToPx(90)).apply {
            setMargins(0, UIHelper.i10px, 0, UIHelper.i10px)

        }

        val roundImage = RoundedImageView(context)
        imageParam.gravity = Gravity.CENTER
        roundImage.layoutParams = imageParam
        roundImage.id = R.id.restaurant_image
        roundImage.isOval = true

//        roundImage.cornerRadius = VUtil.dpToPx(100).toFloat()
        roundImage.scaleType = ImageView.ScaleType.CENTER_CROP
//        Glide.with(roundImage).load(url).into(roundImage)
        YUtils.setImageInView(roundImage, url)

        return roundImage
    }

    private fun getBottomTitle(title: String): TextView? {
        return TextView(context).apply {
            setInterBoldFont()
            text = title
            maxLines = 2
            minLines = 2
            ellipsize = TextUtils.TruncateAt.END
            // gravity = Gravity.TOP


            setTextSize(TypedValue.COMPLEX_UNIT_SP, 13f)
            setTextColor(ThemeConstant.textBlackColor)
            gravity = Gravity.CENTER
            layoutParams =
                LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
//                    .apply {
//                    setMargins(UIHelper.i12px, UIHelper.i3px, UIHelper.i12px,UIHelper.i3px)

//                }
            setPadding(UIHelper.i12px, 0, UIHelper.i12px, UIHelper.i3px)
        }
    }

    private fun getBottomLabel(title: Double): TextView? {
        return TextView(context).apply {
            setInterThineFont()
            if (title != 0.0) {
                if (title == 1.0) {
                    text = title.toInt().toString() + IConstants.DeliveryTime.MIN
                } else {
                    text = title.toInt().toString() + IConstants.DeliveryTime.MINS

                }
            }

            setTextSize(TypedValue.COMPLEX_UNIT_SP, 11f)
            setTextColor(ThemeConstant.textGrayColor)
            maxLines = 1
            minLines = 1
            ellipsize = TextUtils.TruncateAt.END
            gravity = Gravity.CENTER
            layoutParams =
                LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)

//                    .apply {
//                    setMargins(VUtil.dpToPx(2), VUtil.dpToPx(2), VUtil.dpToPx(2), VUtil.dpToPx(2))
//                    }
            setPadding(UIHelper.i12px, 0, UIHelper.i12px, UIHelper.i3px)
        }
    }

}