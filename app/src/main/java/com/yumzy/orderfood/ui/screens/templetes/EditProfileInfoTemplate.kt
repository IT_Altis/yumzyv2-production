/*
 *   Created by Bhupendra Kumar Sahu
 */

package com.yumzy.orderfood.ui.screens.templetes

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.res.ColorStateList
import android.graphics.Color
import android.text.InputFilter
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.button.MaterialButton
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.ui.px
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.helper.*
import com.yumzy.orderfood.ui.screens.module.infodialog.IDetailsActionTemplate
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.viewutils.YumUtil

open class
EditProfileInfoTemplate(
    override val context: Context,
    val title: String = "",
    val etValue: String = "",
    val moduleId: Int = 0,
    cEvent: UpdateSingleInfoListener,
    val userKey: String
) : IDetailsActionTemplate {
    var mUpdateSingleInfoListener: UpdateSingleInfoListener

    init {
        mUpdateSingleInfoListener = cEvent
    }

    interface UpdateSingleInfoListener {
        fun sendData(text: String, userKey: String)

    }


    override var headerHeight: Int = VUtil.dpToPx(40)
        set(value) {
            field = VUtil.dpToPx(value)
        }

    override val showClose = true
    override fun heading(): View? {
        val layout = LinearLayout(context)
        layout.gravity = Gravity.START
        layout.setPadding(0, 0, 0, VUtil.dpToPx(20))
        layout.addView(getTextView(R.id.tv_info_header, title))

        return layout
    }

    @SuppressLint("SetTextI18n")
    override fun body(): View? {
        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.VERTICAL
        layout.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)

        layout.minimumHeight = headerHeight
        val edittext = EditText(context)
        edittext.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        edittext.setPadding(VUtil.dpToPx(10), VUtil.dpToPx(10), VUtil.dpToPx(10), VUtil.dpToPx(10))
        edittext.id = R.id.edt_update_profile
        edittext.background = ResourcesCompat.getDrawable(
            context.resources,
            R.drawable.rounded_light_gray,
            context.theme
        )

        edittext.setInterRegularFont()
        edittext.setText(etValue)
        edittext.setTextColor(
            ResourcesCompat.getColor(
                context.resources,
                R.color.black,
                context.theme
            )
        )

        val contextThemeWrapper = ContextThemeWrapper(context, R.style.Yumzy_Button)
        val btn = MaterialButton(contextThemeWrapper)

        btn.layoutParams =LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, 50.px)
        btn.setTextColor(Color.WHITE)
        btn.backgroundTintList = ColorStateList.valueOf(ThemeConstant.pinkies)
        btn.rippleColor = ColorStateList.valueOf(ThemeConstant.rippleColor)
        btn.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15f)
        val typeface = ResourcesCompat.getFont(context, R.font.poppins_semibold)
        btn.typeface = typeface
        btn.id = R.id.btn_update_profile
        btn.gapTop(VUtil.dpToPx(5))
        btn.text = "Update"
        layout.addView(edittext)
        layout.addView(btn)


        return layout
    }

    override fun actions(): View? = null
    override fun onDialogVisible(view: View?, visible: Boolean?, dialog: DialogInterface?) {

        val edittext = view?.findViewById<EditText>(R.id.edt_update_profile)
        edittext?.maxLines = 1
        edittext?.minLines = 1
        edittext?.isSingleLine = true
        edittext?.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(40))

        view?.findViewById<Button>(R.id.btn_update_profile)?.setOnClickListener {
            if (YumUtil.isValidNameIgnoreSpace(edittext?.text.toString())) {

                mUpdateSingleInfoListener.sendData(
                    edittext?.text.toString().trim().replace("\\s+".toRegex(), " ").capitalize(),
                    userKey
                )
                dialog?.dismiss()

            } else {

                edittext?.let { it1 -> YumUtil.animateShake(it1, 20, 5) }

            }
        }


    }

    open fun getTextView(tvId: Int, msg: String): View? {
        val i5px = VUtil.dpToPx(5)

        val textView = TextView(context)
        textView.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        textView.text = msg
        textView.id = tvId
        textView.gravity = Gravity.CENTER
        textView.setPadding(0, i5px, 0, 0)
        textView.setInterFont()

        if (tvId == R.id.tv_info_header) {
            textView.setInterBoldFont()
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f)
            textView.setTextColor(Color.DKGRAY)
        } else if (tvId == R.id.tv_info_sub_header) {
            textView.setTextColor(Color.GRAY)
        }

        return textView

    }


}

