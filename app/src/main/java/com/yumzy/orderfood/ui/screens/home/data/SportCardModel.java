package com.yumzy.orderfood.ui.screens.home.data;

public class SportCardModel {
    private String sportTitle;
    private String sportSubtitle;
    private int backgroundColorResId;

    private SportCardModel(Builder builder) {
        sportTitle = builder.sportTitle;
        sportSubtitle = builder.sportSubtitle;
        backgroundColorResId = builder.backgroundColorResId;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private String sportTitle;
        private String sportSubtitle;
        private int backgroundColorResId;

        private Builder() {
        }


        public Builder withSportTitle(String sportTitle) {
            this.sportTitle = sportTitle;
            return this;
        }

        public Builder withSportSubtitle(String sportSubtitle) {
            this.sportSubtitle = sportSubtitle;
            return this;
        }


        public Builder withBackgroundColorResId(int backgroundColorResId) {
            this.backgroundColorResId = backgroundColorResId;
            return this;
        }

        public SportCardModel build() {
            return new SportCardModel(this);
        }
    }

}
