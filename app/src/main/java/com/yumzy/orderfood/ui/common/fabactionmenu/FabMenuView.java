package com.yumzy.orderfood.ui.common.fabactionmenu;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.yumzy.orderfood.R;
import com.yumzy.orderfood.ui.helper.ThemeConstant;
import com.yumzy.orderfood.util.viewutils.YumUtil;
import com.yumzy.orderfood.util.viewutils.fontutils.IconFontDrawable;
import com.laalsa.laalsalib.ui.VUtil;

public class FabMenuView extends FloatingActionButton implements View.OnClickListener {

    private IFabMenuListener clFabMenuListener;
    private boolean isFABOpen = false;
    private FabItemDTO[] clActionArray = null;
    private byte byFirst = -1;

    public FabMenuView(Context clContext, FabItemDTO[] arrItems, IFabMenuListener clFabMenuListener) {
        super(clContext);
        this.clFabMenuListener = clFabMenuListener;
        this.clActionArray = arrItems;
        init();
    }

    public FabMenuView(Context context) {
        super(context);
        init();
    }

    private void init() {
        Drawable clPlusDrawable = new IconFontDrawable(getContext(), R.string.icon_menu_1);
        setImageDrawable(clPlusDrawable);
        setOnClickListener(this);
    }

    private void addMenuToFAB(Context clContext) {

        RelativeLayout clFABMenuHolder = new RelativeLayout(clContext);
        clFABMenuHolder.setId(R.id.fab_layout_holder);
        ViewGroup clMainLayout;

        clMainLayout = ((ViewGroup) getParent());
        // shadow layout
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOnClickListener(this);
        linearLayout.setBackgroundColor(ThemeConstant.popShadow);
        linearLayout.setVisibility(GONE);
        linearLayout.setId(R.id.fab_overlay_view);
        linearLayout.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        clMainLayout.addView(linearLayout);


        clMainLayout.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (isFABOpen) {
                        closeFABMenu((Activity) getContext());
                    }
                }
                return false;
            }
        });

        for (int i = 0; i < clActionArray.length; i++) {
            LinearLayout clFabMenuLayout = getFabMenuView(clContext, clActionArray[i], i);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            int iSize = 5;
            if (YumUtil.INSTANCE.getAndroidVersion() < Build.VERSION_CODES.LOLLIPOP)
                iSize = 15;
            params.setMargins(0, 0, getMarginRight(5), getMarginBottom(iSize));
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            clFabMenuLayout.setLayoutParams(params);
            clFABMenuHolder.addView(clFabMenuLayout);
        }
        if (getParent() != null) {
            clMainLayout = ((ViewGroup) getParent());
            clMainLayout.removeView(this);
            clMainLayout.addView(clFABMenuHolder);
            clMainLayout.addView(this);
        }
    }

    private LinearLayout getFabMenuView(Context clContext, FabItemDTO clActionArray, int iPosition) {
        GradientDrawable iconShape = new GradientDrawable();
        iconShape.setGradientType(GradientDrawable.OVAL);
        iconShape.setStroke(2, ThemeConstant.mediumGray);
        iconShape.setCornerRadius(VUtil.dpToPx(6));
        iconShape.setColor(ThemeConstant.lightAlphaGray);

        LinearLayout clFabHolder = new LinearLayout(clContext);
        clFabHolder.setClipToPadding(false);
//        clFabHolder.setBackground(CLThemeUtil.getRippleEffect(clContext));
        clFabHolder.setId(iPosition);
        if (YumUtil.INSTANCE.getAndroidVersion() >= Build.VERSION_CODES.LOLLIPOP)
            clFabHolder.setElevation(5);

        clFabHolder.setBackground(null);
        clFabHolder.setVisibility(View.INVISIBLE);
        clFabHolder.setGravity(Gravity.CENTER_VERTICAL);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        clFabHolder.setLayoutParams(layoutParams);
        clFabHolder.setOnClickListener(this);


        if (clActionArray.getActionName() != null && clActionArray.getActionName().length() > 0) {
            LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            if (clActionArray.getIcon() != null) {
                textParams.setMargins(0, 0, VUtil.dpToPx(12), 0);
            }
            TextView clFabName = new TextView(clContext);
            clFabName.setGravity(Gravity.CENTER_VERTICAL);
            clFabName.setLayoutParams(textParams);
            clFabName.setMinimumHeight(VUtil.dpToPx(35));
            clFabName.setPadding(10, 0, 10, 0);
            clFabName.setBackground(iconShape);
            clFabName.setText(clActionArray.getActionName());
            clFabHolder.addView(clFabName);
        }
        if (clActionArray.getIcon() != null) {
            LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            FloatingActionButton clSmallFab = new FloatingActionButton(clContext);
            clSmallFab.setLayoutParams(textParams);
            /*if (CLViewUtil.getAndroidVersion() >= Build.VERSION_CODES.LOLLIPOP)
                clSmallFab.setElevation(0);*/
            clSmallFab.setSize(FloatingActionButton.SIZE_MINI);
            clSmallFab.setScaleType(ScaleType.CENTER);
//            clSmallFab.setPadding(10, 0, 10, 0);
            clSmallFab.setImageDrawable(clActionArray.getIcon());
            clFabHolder.addView(clSmallFab);
        }
        return clFabHolder;
    }

    private int getMarginRight(int iMargin) {

        int iLayoutMargin = 8;
        Context clContext = getContext();
        if (getLayoutParams() instanceof CoordinatorLayout.LayoutParams) {
            iLayoutMargin = ((CoordinatorLayout.LayoutParams) getLayoutParams()).rightMargin + toDip(clContext, iMargin);
        } else if (getLayoutParams() instanceof FrameLayout.LayoutParams) {
            iLayoutMargin = ((FrameLayout.LayoutParams) getLayoutParams()).rightMargin + toDip(clContext, iMargin);
        } else if (getLayoutParams() instanceof LinearLayout.LayoutParams) {
            iLayoutMargin = ((LinearLayout.LayoutParams) getLayoutParams()).rightMargin + toDip(clContext, iMargin);
        } else if (getLayoutParams() instanceof RelativeLayout.LayoutParams)
            iLayoutMargin = ((RelativeLayout.LayoutParams) getLayoutParams()).rightMargin + toDip(clContext, iMargin);
        return iLayoutMargin;
    }

    private int getMarginBottom(int iMargin) {

        int iLayoutMargin = 5;
        Context clContext = getContext();
        if (getLayoutParams() instanceof CoordinatorLayout.LayoutParams) {
            iLayoutMargin = ((CoordinatorLayout.LayoutParams) getLayoutParams()).bottomMargin + toDip(clContext, iMargin);
        } else if (getLayoutParams() instanceof FrameLayout.LayoutParams) {
            iLayoutMargin = ((FrameLayout.LayoutParams) getLayoutParams()).bottomMargin + toDip(clContext, iMargin);
        } else if (getLayoutParams() instanceof LinearLayout.LayoutParams) {
            iLayoutMargin = ((LinearLayout.LayoutParams) getLayoutParams()).bottomMargin + toDip(clContext, iMargin);
        } else if (getLayoutParams() instanceof RelativeLayout.LayoutParams)
            iLayoutMargin = ((RelativeLayout.LayoutParams) getLayoutParams()).bottomMargin + toDip(clContext, iMargin);
        return iLayoutMargin;
    }

    public void closeFABMenu(Activity clContext) {
        if (!isFABOpen) {
            return;
        }
        isFABOpen = false;
        animate().rotation(0f);

        final RelativeLayout clFABHolder = ((ViewGroup) getParent()).findViewById(R.id.fab_layout_holder);
        final int iChildCount = clFABHolder.getChildCount();
        for (int i = 0; i < iChildCount; i++) {
            final LinearLayout clFabMenuLayout = (LinearLayout) clFABHolder.findViewById(i);
            clFabMenuLayout.setClickable(false);
            if (i == iChildCount - 1) {
                clFabMenuLayout.animate().translationY(0).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                        setEnabled(false);
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        setEnabled(true);
                        if (!isFABOpen) {
                            for (int i = 0; i < iChildCount; i++) {
                                ((LinearLayout) clFABHolder.findViewById(i)).setVisibility(View.GONE);
                            }
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {
                    }
                });
            } else {
                clFabMenuLayout.animate().translationY(0);
            }
        }
        // shadow layout
        FrameLayout frameLayout = ((FrameLayout) getParent());
        LinearLayout layout = frameLayout.findViewById(R.id.fab_overlay_view);
        layout.setVisibility(GONE);
    }

    private void showFABMenu(Activity clContext) {
        LinearLayout clFabMenuLayout;
        isFABOpen = true;

        animate().rotation(135f);
        // shadow layout
        FrameLayout frameLayout = ((FrameLayout) getParent());
        LinearLayout layout = frameLayout.findViewById(R.id.fab_overlay_view);
        layout.setVisibility(VISIBLE);

        RelativeLayout clFABHolder = ((ViewGroup) getParent()).findViewById(R.id.fab_layout_holder);

        int iChildCount = clFABHolder.getChildCount();
        int iCount = 1;

        for (int i = 0; i < iChildCount; i++) {
            clFabMenuLayout = (LinearLayout) clFABHolder.findViewById(i);
            clFabMenuLayout.setClickable(true);
            clFabMenuLayout.setVisibility(View.VISIBLE);
            if (i == 0) {

                /*
                 56 is Default FAB SIZE and 45 is Animating FAB for pushing up from current position
                 */

                clFabMenuLayout.animate().translationY(-toDip(clContext, 56));
            } else {
                clFabMenuLayout.animate().translationY(-toDip(clContext, 56) + (-toDip(clContext, 45) * iCount));
                iCount = iCount + 1;
            }
        }
    }

    private short toDip(Context context, int pixels) {
        return (short) ((int) ((float) pixels * context.getResources().getDisplayMetrics().density + 0.5F));
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == getId()) {
            if (byFirst == -1) {
                addMenuToFAB(getContext());
                byFirst = 1;
            }

            if (!isFABOpen) {
                showFABMenu((Activity) getContext());
            } else {
                closeFABMenu((Activity) getContext());
            }
        } else if (v.getId() == R.id.fab_overlay_view) {
            closeFABMenu((Activity) getContext());
        } else {
            closeFABMenu((Activity) getContext());
            if (clFabMenuListener != null) {
                clFabMenuListener.onFabItemClick(v, clActionArray[v.getId()]);
            }
        }
    }

    public void setFabMenuListener(IFabMenuListener clFabMenuListener) {
        this.clFabMenuListener = clFabMenuListener;
    }

    public void setActionArray(FabItemDTO[] clActionArray) {
        this.clActionArray = clActionArray;
    }
}