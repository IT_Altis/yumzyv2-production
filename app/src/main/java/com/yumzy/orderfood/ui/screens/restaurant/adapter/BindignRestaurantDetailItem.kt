package com.yumzy.orderfood.ui.screens.restaurant.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.yumzy.orderfood.data.models.RestaurantDetailDTO
import com.yumzy.orderfood.data.models.RestaurantMenuCategoryDTO
import com.yumzy.orderfood.databinding.AdapterOutletDetailBinding
import com.yumzy.orderfood.ui.helper.unifiedCoupon
import com.yumzy.orderfood.ui.helper.withPrecision
import com.yumzy.orderfood.util.IConstants

/**
 * Created by mikepenz on 28.12.15.
 */
class BindignRestaurantDetailItem(val context: Context) :
    AbstractBindingItem<AdapterOutletDetailBinding>() {

    var sectionName: String? = ""
    var sectionItemCount: Int? = 0
    var imageUrl: String = ""
    var name: String = ""
    var locality: String = ""
    var rating: String = ""
    var deliveryTime: Double = 0.0
    var costForTwo: String = ""
    var offer: String = ""
    var cusisines: String = ""
    var details: RestaurantDetailDTO? = null
    var categoryLiveData: MutableLiveData<List<RestaurantMenuCategoryDTO>>? = null


    var header: String? = null


    override val type: Int
        get() = IConstants.FastAdapter.REST_DETAIL_VIEW

    fun withRestaurantDetail(details: RestaurantDetailDTO?): BindignRestaurantDetailItem {

        this.imageUrl = details?.imageUrl.toString()
        this.name = details?.outletName.toString().capitalize()
        this.locality = details?.locality.toString().capitalize()
        this.rating = details?.rating ?: ""
        this.deliveryTime = details?.deliveryTime!!
        this.offer = details.offers.toString()
        this.costForTwo = details.costForTwo
        this.cusisines = details.cuisines
        this.details = details
        return this
    }

    fun withCategory(categoryLiveData: MutableLiveData<List<RestaurantMenuCategoryDTO>>): BindignRestaurantDetailItem {
        this.categoryLiveData = categoryLiveData
        return this
    }

    /**
     * binds the data of this item onto the viewHolder
     */
    override fun bindView(binding: AdapterOutletDetailBinding, payloads: List<Any>) {
        //define our data for the view

        binding.clVegonly.visibility = View.VISIBLE

        binding.title = this.name

        binding.cuisines = this.cusisines

//        binding.tvRestaurantAddress.text = this.locality

        if (rating.isNotEmpty()) {
            binding.textRating.visibility = View.VISIBLE
            binding.rating = rating
        } else {
            binding.textRating.visibility = View.GONE

        }

        if (deliveryTime != 0.0) {
            binding.textDuration.visibility = View.VISIBLE

            if (deliveryTime == 1.0) {
                binding.duration = deliveryTime.withPrecision(0) + IConstants.DeliveryTime.MIN
            } else {
                binding.duration = deliveryTime.withPrecision(0) + IConstants.DeliveryTime.MINS
            }
        } else {
            binding.textDuration.visibility = View.GONE
        }

        if (offer.isEmpty()) {
            binding.llOfferContainer.visibility = View.GONE
        } else {
            binding.llOfferContainer.visibility = View.VISIBLE
            val index = offer.lastIndexOf(" ")
            val unifieCoupon =
                unifiedCoupon(offer.substring(0, index), " " + offer.substring(index + 1))
            binding.tvOffer.text = unifieCoupon
//            binding.llOfferContainer.background = UIHelper.roundDotedRedStrokedDrawable()
        }
        if (costForTwo.isNotEmpty()) {
            val costForTwo = details?.costForTwo + " for one"
            binding.costfortwo = costForTwo
        } else {
            binding.costfortwo = ""
        }
        categoryLiveData?.observe(context as LifecycleOwner, {
            //
        })

    }

    override fun unbindView(binding: AdapterOutletDetailBinding) {
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterOutletDetailBinding {
        return AdapterOutletDetailBinding.inflate(inflater, parent, false)
    }
}