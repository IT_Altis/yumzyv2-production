package com.yumzy.orderfood.ui.screens

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.yumzy.orderfood.R

class TestActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
    }
}