package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.yumzy.orderfood.data.models.CustomizeOrderSectionDTO
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.ui.common.VerticalItemDecoration
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.ui.screens.restaurant.adapter.BindingHomeYumzyBestItem
import com.yumzy.orderfood.util.SnapHelperOneByOne

class YumzyBestListView : RecyclerView {

    //save our FastAdapter
    private var fastItemAdapter: FastItemAdapter<BindingHomeYumzyBestItem> = FastItemAdapter()

    internal var section: CustomizeOrderSectionDTO? = null

    private var callBack: ((HomeListDTO, View) -> Unit?)? = null

    init {

        //create our FastAdapter which will manage everything

        //configure our fastAdapter
        fastItemAdapter.onClickListener =
            { v: View?, _: IAdapter<BindingHomeYumzyBestItem>, item: BindingHomeYumzyBestItem, _: Int ->
                v?.let { }
                false
            }
        fastItemAdapter.onPreClickListener =
            { _: View?, _: IAdapter<BindingHomeYumzyBestItem>, _: BindingHomeYumzyBestItem, _: Int ->
                true // consume otherwise radio/checkbox will be deselected
            }


        fastItemAdapter.addEventHook(BindingHomeYumzyBestItem.HomeYumzyBestClickEvent { item, view ->
            callBack?.let { it(item, view) }
        })


    }


    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        //get our recyclerView and do basic setup
        this.addItemDecoration(VerticalItemDecoration(UIHelper.i10px, false))
        this.layoutManager = LinearLayoutManager(context, HORIZONTAL, false)
        /*this.itemAnimator = AlphaCrossFadeAnimator().apply {
            addDuration = 600
            removeDuration = 600
        }*/
        this.overScrollMode = OVER_SCROLL_NEVER
        this.adapter = fastItemAdapter
        this.setPadding(
            UIHelper.i15px,
            UIHelper.i25px,
            UIHelper.i15px,
            UIHelper.i15px
        )
        this.clipToPadding = false
        this.clipChildren = false
        this.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        val linearSpanHelperOneByOne = SnapHelperOneByOne()
        linearSpanHelperOneByOne.attachToRecyclerView(this)
        /*OverScrollDecoratorHelper.setUpOverScroll(
            this,
            OverScrollDecoratorHelper.ORIENTATION_HORIZONTAL
        )*/

    }

    fun withItem(
        section: List<HomeListDTO>,
        listener: (HomeListDTO, View) -> Unit?
    ): YumzyBestListView {
        this.callBack = listener

        val items = ArrayList<BindingHomeYumzyBestItem>()
        section.forEach { homeItem ->
            homeItem.let { item ->
                items.add(
                    BindingHomeYumzyBestItem(item)
                )
            }
        }

        fastItemAdapter.add(items)

        return this
    }

}