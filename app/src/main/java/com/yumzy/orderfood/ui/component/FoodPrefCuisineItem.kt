package com.yumzy.orderfood.ui.component

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.checkbox.MaterialCheckBox
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.mikepenz.fastadapter.items.AbstractItem
import com.mikepenz.fastadapter.listeners.ClickEventHook
import com.mikepenz.fastadapter.select.SelectExtension
import com.mikepenz.fastadapter.ui.utils.StringHolder
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.CustomizeOrderItemDTO
import com.yumzy.orderfood.data.models.FoodPreferenceDTO
import com.yumzy.orderfood.databinding.AdapterFoodPrefItemBinding
import com.yumzy.orderfood.databinding.AdapterOutletRegularItemBinding
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.util.viewutils.YumUtil
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView

/**
 * Created by mikepenz on 28.12.15.
 */
class FoodPrefCuisineItem() :
    AbstractBindingItem<AdapterFoodPrefItemBinding>() {

    var data: FoodPreferenceDTO? = null
    var isMultiSelectEnabled: Boolean = false

    /**
     * defines the type defining this item. must be unique. preferably an id
     *
     * @return the type
     */
    override val type: Int = R.id.item_food_pref


    fun withData(data: FoodPreferenceDTO, isMultiSelectEnabled: Boolean = false): FoodPrefCuisineItem {
        this.data = data
        this.isMultiSelectEnabled = isMultiSelectEnabled
        return this
    }

    /**
     * binds the data of this item onto the viewHolder
     *
     * @param holder the viewHolder of this item
     */
    override fun bindView(binding: AdapterFoodPrefItemBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)

        binding.title = data?.displayName.toString()
        binding.imageUrl = data?.imageurl
        binding.root.background = if (isSelected) {
            ResourcesCompat.getDrawable(
                binding.root.context.resources,
                R.drawable.cuisine_selected_drawable,
                binding.root.context.theme
            )
        } else {
            null
        }

    }

    override fun unbindView(binding: AdapterFoodPrefItemBinding) {
        binding.title = null
        binding.imageUrl = null
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterFoodPrefItemBinding {
        return AdapterFoodPrefItemBinding.inflate(inflater, parent, false)
    }

    class FoodPrefClickEvent(): ClickEventHook<FoodPrefCuisineItem>() {

        override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
            return  viewHolder.itemView
        }

        override fun onClick(
            v: View,
            position: Int,
            fastAdapter: FastAdapter<FoodPrefCuisineItem>,
            item: FoodPrefCuisineItem
        ) {
            if (item.isMultiSelectEnabled) {
                //logic for multi selection
                val selectExtension: SelectExtension<FoodPrefCuisineItem> =
                    fastAdapter.requireExtension()
                selectExtension.toggleSelection(position)
            } else {
                //logic for single selection
                if (!item.isSelected) {
                    val selectExtension: SelectExtension<FoodPrefCuisineItem> =
                        fastAdapter.requireExtension()
                    val selections = selectExtension.selections
                    if (selections.isNotEmpty()) {
                        val selectedPosition = selections.iterator().next()
                        selectExtension.deselect()
                        fastAdapter.notifyItemChanged(selectedPosition)
                    }
                    selectExtension.select(position)
                }
            }

        }
    }

}
