package com.yumzy.orderfood.ui.component

import android.content.Context
import android.os.Handler
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.DishesItem
import com.yumzy.orderfood.data.models.RestaurantItem
import com.yumzy.orderfood.databinding.ViewRestaurantRecommendItemBinding
import com.yumzy.orderfood.ui.common.ItemType
import com.yumzy.orderfood.ui.helper.bindImageFromUrl
import com.yumzy.orderfood.ui.helper.deepForEach
import com.yumzy.orderfood.ui.helper.outletNameDescription
import com.yumzy.orderfood.ui.helper.withPrecision
import com.yumzy.orderfood.util.ViewConst
import com.yumzy.orderfood.util.viewutils.YumUtil

/*
* Created By shriom 14/10/2020
* */

class RestaurantRecommendItemView @JvmOverloads constructor(
    context: Context?,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : androidx.constraintlayout.widget.ConstraintLayout(context!!, attrs, defStyleAttr),
    AddQuantityView.OnCounterChange {

    internal var addItemListener: AddItemView.AddItemListener? = null

    private var addHandler: Handler? = null

    private var update: Runnable? = null

    private var bd: ViewRestaurantRecommendItemBinding

    private var restaurantItem: RestaurantItem? = null
        set(value) {
            if (value != null && value.description.isNotEmpty()) {
                value.description =
                    value.description.split(" ").map { item -> item.capitalize() }.joinToString(" ")
            }

            if (value != null && value.ribbon.isNotEmpty()) {
                val items = value.ribbon.split(" ").toMutableList();
                items[0] = items[0].capitalize();
                value.ribbon = items.joinToString(" ")
            }

            field = value
        }

    private var dishItem: DishesItem? = null
        set(value) {
            if (value != null && value.outletName.isNotEmpty()) {
                value.outletName =
                    value.outletName.split(" ").map { item -> item.capitalize() }.joinToString(" ")
            }
            if (value != null && !value.ribbon.isNullOrEmpty()) {
                val items = value.ribbon!!.split(" ").toMutableList();
                items[0] = items[0].capitalize();
                value.ribbon = items.joinToString(" ")
            }
            field = value
        }

    private var outletId: String = ""

    private var itemId: String = ""

    private var price: Double = 0.0

    init {
        this.layoutParams = LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)

        bd = ViewRestaurantRecommendItemBinding.inflate(LayoutInflater.from(context), this, true)
        val a = context?.theme?.obtainStyledAttributes(
            attrs,
            R.styleable.RestaurantRegularItemView,
            defStyleAttr,
            0
        )
        a?.recycle()

    }

    fun setMenuItem(item: RestaurantItem?) {

        restaurantItem = item

        if (restaurantItem != null) {

            restaurantItem?.let {

                outletId = it.outletId
                itemId = it.itemId
                price = it.price

                bd.title = it.name.capitalize()
                bd.imageUrl = it.imageUrl
                bd.description = it.description
                bd.recommendedText = it.ribbon
                bd.itemId = it.itemId

//                bd.cardItem.isGone = it.imageUrl == ""
                bd.imvItem.bindImageFromUrl(it.imageUrl, it.indexPosition)

                bd.dishType =
                    if (it.containsEgg && it.isVeg) ItemType.EGG else if (it.isVeg) ItemType.VEG else ItemType.NON_VEG
                bd.btnAdd.quantityCount = it.prevQuantity

                if (it.currentOfferPrice > 0) {

                    val priced = YumUtil.getOfferItemPrice(it.currentOfferPrice, it.price)
                    bd.price = priced

                } else {
                    bd.price = it.price.withPrecision(2)
                }

                val isItemAvailable =
                    it.itemOperational ?: false && it.outletOperational ?: false && it.itemAvailable ?: false && it.outletAvailable ?: false
//                if (isItemAvailable){
//                }
                bd.btnAdd.enableButton = isItemAvailable
                if (isItemAvailable)
                    bd.btnAdd.setOnCounterChange(this)
                else
                    bd.btnAdd.setOnCounterChange(null)


                bd.hasCustomization = it.addons.isNotEmpty()

//                bd.divider.isInvisible = it.isLastItem

            }
        } else {
            bd.title = null
//            bd.imageUrl = null
            bd.description = null
            bd.recommendedText = null
            bd.itemId = null
            bd.dishType = ItemType.VEG
            bd.price = null
            bd.hasCustomization = false
        }
    }

    fun setTopDishRecommendedItem(item: DishesItem?) {

        this.dishItem = item

        if (dishItem != null) {
            dishItem?.let {

                outletId = it.outletId
                itemId = it.itemId
                price = it.price

                bd.title = it.name.capitalize()

                val outletDesc = getSpannedOutletDescription(it.outletName, it.description)
                bd.description = outletDesc

                bd.dishType =
                    if (it.containsEgg && it.isVeg) ItemType.EGG else if (it.isVeg) ItemType.VEG else ItemType.NON_VEG
                bd.recommendedText = it.ribbon
                bd.hasCustomization = false

//                bd.cardItem.isGone = it.imageUrl == ""
                bd.imvItem.bindImageFromUrl(it.imageUrl)

                bd.btnAdd.quantityCount = it.prevQuantity
                bd.btnAdd.ignoreClickCount = true
                bd.btnAdd.setOnCounterChange(this)
                bd.btnAdd.setButtonText(" Add Dish ")
                bd.price = it.price.withPrecision(2)

            }
        } else {
            bd.title = null
//            bd.imageUrl = null
            bd.description = null
            bd.recommendedText = null
            bd.itemId = null
            bd.dishType = ItemType.VEG
            bd.price = null
            bd.hasCustomization = false
        }
    }

    private fun getSpannedOutletDescription(
        outletName: String?,
        description: String?
    ): CharSequence {


        val text2 = if (outletName.isNullOrEmpty())
            "$description"
        else
            "\n$description"

        return outletNameDescription("$outletName", text2)
    }

    var imageUrl: String? = null
        set(value) {
            field = value
            if (field.isNullOrEmpty()) {

                val layoutParams =
                    ConstraintLayout.LayoutParams(ViewConst.WRAP_CONTENT, ViewConst.WRAP_CONTENT)
                layoutParams.leftToRight = bd.imvItem.id
                layoutParams.leftToRight = bd.imvItem.id
                layoutParams.leftToRight = bd.imvItem.id
                bd.btnAdd.layoutParams = layoutParams

            } else {

                val layoutParams =
                    ConstraintLayout.LayoutParams(ViewConst.WRAP_CONTENT, ViewConst.WRAP_CONTENT)


                layoutParams.leftToRight = bd.imvItem.id
                layoutParams.leftToRight = bd.imvItem.id
                layoutParams.leftToRight = bd.imvItem.id

            }

        }

    fun getQuantityView() = bd.btnAdd

    fun showItemAdding(showProgress: Boolean) {
        val progressBar = bd.progressBar
        if (showProgress) {
            progressBar.isIndeterminate = true
            progressBar.visibility = View.VISIBLE
            getQuantityView().deepForEach {
                this.isEnabled = false
                this.alpha = 0.5f

            }

        } else {
            progressBar.isIndeterminate = false
            progressBar.visibility = View.INVISIBLE
            getQuantityView().deepForEach {
                this.isEnabled = true
                this.alpha = 1.0f

            }
        }
    }

    override fun onCounterChange(count: Int) {
        if (bd.hasCustomization == true) {
            addItemListener?.onItemAdded(
                itemId,
                count,
                price.withPrecision(2),
                outletId
            )
            return
        }

        if (count > 0) {

            if (addHandler == null || update == null) {
                addHandler = Handler()
            }
            update?.let { addHandler?.removeCallbacks(it) }

            update =
                Runnable {
                    addItemListener?.onItemAdded(
                        itemId,
                        count,
                        price.withPrecision(2),
                        outletId
                    )
                }

            addHandler?.postDelayed(update!!, 500L)
            //9770000033


        } else {
            addItemListener?.onItemAdded(
                itemId,
                count,
                price.withPrecision(2),
                outletId
            )
        }


    }
}