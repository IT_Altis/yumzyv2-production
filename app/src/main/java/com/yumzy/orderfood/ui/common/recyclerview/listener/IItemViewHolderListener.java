package com.yumzy.orderfood.ui.common.recyclerview.listener;

import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.yumzy.orderfood.ui.common.recyclerview.view.YItemViewHolder;

public interface IItemViewHolderListener {
    YItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType);

    void onBindViewHolder(RecyclerView.ViewHolder itemViewHolder, int position);

    int getItemViewType(int iPosition);
}
