package com.yumzy.orderfood.ui.screens.home.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.laalsa.laalsalib.utilcode.util.ClickUtils
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.ui.component.VerticalCardLayout
import com.yumzy.orderfood.ui.helper.UIHelper
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil

class NewlyLaunchedAdapter(
    val context: Context,
    var homeLayoutList: ArrayList<HomeListDTO>,
    val callerFun: (View, HomeListDTO) -> Unit
) : RecyclerView.Adapter<NewlyLaunchedAdapter.GridViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GridViewHolder {
        val verticalCardLayout = VerticalCardLayout(context = context).apply {
            removeAddQuantity = true
            imageRatio = "5:3"
            removeAddQuantity
            updateImageParams(0, UIHelper.i120px, "5:3")
            removeVegIcon = true
        }
        verticalCardLayout.removeAddQuantity()
        verticalCardLayout.removeVegIcon
        return GridViewHolder(verticalCardLayout)
    }

    override fun getItemCount() = homeLayoutList.size

    override fun onBindViewHolder(holder: GridViewHolder, position: Int) {
        holder.bindView(homeLayoutList[position])
    }

    fun setHomeDataList(homeList: ArrayList<HomeListDTO>) {
        this.homeLayoutList = homeList
        notifyDataSetChanged()
    }


    inner class GridViewHolder(val layout: VerticalCardLayout) : RecyclerView.ViewHolder(layout) {

        fun bindView(homeList: HomeListDTO?) {

            layout.title = homeList?.title ?: "-"
            layout.subtitle = homeList?.subTitle.toString()
            layout.cuisines =
                homeList?.cuisines?.joinToString(separator = ", ", postfix = "") ?: "New Arrivals"
            //  layout.description = homeList?.bannerText ?: ""
            // layout.price = homeList?.meta?.price?: ""
            val zoneOffer = YumUtil.setZoneOfferString(homeList?.meta?.offers)
            layout.colorText = zoneOffer

            layout.isVeg = homeList?.meta?.isVeg ?: true
            val ratting = homeList?.rating.toString()
            if (!ratting.isNullOrEmpty() && !ratting.equals("null")) {
                layout.setRating(ratting)
            }


            layout.costForTwo = YumUtil.getDurationAndCostForTwo(homeList!!)

            layout.imageUrl = homeList.image?.url ?: ""
            layout.ribbonText = homeList.bannerText
            ClickUtils.applyGlobalDebouncing(
                layout,
                IConstants.AppConstant.CLICK_DELAY
            ) {
                if (homeList != null) {
                    callerFun(layout, homeList)
                }
            }

        }

    }
}

