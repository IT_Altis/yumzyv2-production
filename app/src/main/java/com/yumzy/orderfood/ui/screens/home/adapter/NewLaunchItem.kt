package com.yumzy.orderfood.ui.screens.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.ModelAbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.HomeListDTO
import com.yumzy.orderfood.databinding.ViewNewLaunchBinding

class NewLaunchItem(private val homeListDTO: HomeListDTO) :
    ModelAbstractBindingItem<HomeListDTO, ViewNewLaunchBinding>(homeListDTO) {
    override val type = R.id.new_launch_item_id

    override fun bindView(binding: ViewNewLaunchBinding, payloads: List<Any>) {
        binding.homeNewLaunch.clearData()
        binding.homeNewLaunch.setNewLaunchData(homeListDTO.nestedList!!)
    }

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ViewNewLaunchBinding {
        return ViewNewLaunchBinding.inflate(inflater, parent, false)
    }
}
