package com.yumzy.orderfood.ui.screens.module.address

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Handler
import android.os.Looper
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.lifecycle.LifecycleService
import com.yumzy.orderfood.BuildConfig
import com.yumzy.orderfood.R
import com.yumzy.orderfood.api.OrderServiceAPI
import com.yumzy.orderfood.api.session.SessionManager
import com.yumzy.orderfood.appGson
import com.yumzy.orderfood.data.AppDatabase
import com.yumzy.orderfood.data.models.base.RHandler
import com.yumzy.orderfood.data.models.orderdetails.NewOrderDetailsDTO
import com.yumzy.orderfood.ui.base.IView
import com.yumzy.orderfood.ui.screens.home.HomePageActivity
import com.yumzy.orderfood.ui.screens.orderhistory.data.OrderRemoteDataSource
import com.yumzy.orderfood.ui.screens.orderhistory.data.OrderRepository
import com.yumzy.orderfood.util.IConstants
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers.io
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class OrderTrackingService : LifecycleService(), IView {

    private var mRetrofit: Retrofit? = null
    private var mAppDatabase: AppDatabase? = null
    private var mApi: OrderServiceAPI? = null
    private var mOrderList: List<NewOrderDetailsDTO>? = null
    private var mNotificationManager: NotificationManager? = null
    private var mRepository: OrderRepository? = null

    private val GROUP_KEY_ORDER_STATUS = " com.yumzy.orderfood.ORDER_STATUS"
    private var mChannelId = ""

    private lateinit var mainHandler: Handler

    private val task = Runnable { fetchData() }

    override fun onCreate() {
        super.onCreate()
        init()
        mainHandler = Handler(Looper.getMainLooper())
        this.startForeground()
    }

    private fun init() {

        val sharedPreferences =
            application.getSharedPreferences(IConstants.SHARE_PREF_NAME, Context.MODE_PRIVATE)
        val token = sharedPreferences.getString(SessionManager.USER_TOKEN, "") ?: ""

        if (token.isNotEmpty()) {
            mRetrofit = Retrofit.Builder()
                .baseUrl(IConstants.BASE_URL)
                .client(
                    OkHttpClient.Builder()
                        .addInterceptor(object : Interceptor {
                            override fun intercept(chain: Interceptor.Chain): Response {
                                val requestBuilder = chain.request().newBuilder()
                                requestBuilder.addHeader("x-access-token", token)
                                return chain.proceed(requestBuilder.build())
                            }
                        })
                        .addInterceptor(HttpLoggingInterceptor().apply {
                            level =
                                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
                        })
                        .build()
                )
                .addConverterFactory(GsonConverterFactory.create(appGson))
                .build()


        }


        mAppDatabase = AppDatabase.getInstance(application)

        mApi = mRetrofit?.create(OrderServiceAPI::class.java)!!

        mRepository =
            OrderRepository(
                OrderRemoteDataSource(mApi!!),
                mAppDatabase?.ordersDao()!!
            )

        mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        mRepository?.orderList?.observeForever { mOrderList = it }
        fetchData()
        return Service.START_NOT_STICKY
    }


    private fun fetchData() {

        if (!mOrderList.isNullOrEmpty()) {

            Observable.fromCallable {
                return@fromCallable mOrderList  //return the list of order
            }.flatMap {
                Observable.fromIterable(it) //make iterable list as observable
            }.concatMap {
                //fetch the data from server by blocking this thread
                var pair: Pair<Boolean, NewOrderDetailsDTO?>? = null

                val response = mApi?.enqueOrderDetails(it.orderId)?.execute()?.body()
                if (response?.data != null) {
                    RHandler<NewOrderDetailsDTO>(
                        this,
                        response
                    ) {
                        when (it.orderStatus) {
                            IConstants.OrderTracking.ORDER_STATUS_DELIVERED,
                            IConstants.OrderTracking.ORDER_STATUS_COMPLETE,
                            IConstants.OrderTracking.ORDER_STATUS_CANCELLED,
                            IConstants.OrderTracking.ORDER_STATUS_REJECTED,
                            IConstants.OrderTracking.ORDER_STATUS_IN_DISPUTE,
                            IConstants.OrderTracking.ORDER_STATUS_ENFORCED_CANCEL -> {
                                updateNotification(
                                    "Order ${it.orderNum}",
                                    "${it.statusDesc}",
                                    it.orderNum!!,
                                    it.orderId
                                )
                                pair = Pair(false, it)
                            }
                            else -> {
                                updateNotification(
                                    "Order #${it.orderNum}",
                                    "${it.outlets.get(0).outletName}, ${it.statusDesc}",
                                    it.orderNum!!,
                                    it.orderId
                                )
                                pair = Pair(true, it)
                            }
                        }
                    }
                } else {
                    pair = Pair(false, null)
                    updateNotification("", "", "-1", "")
                }

                return@concatMap Observable.just(pair)
            }.observeOn(io())
                .doOnNext {
                    // in pair if false that it means it is not ongoing order i.e it is complete, in dispute or something happened else it's ongoing order
                    if (it?.first == true) {
                        //update item in db
                        mRepository?.insertToDatabase(it.second!!)

                    } else if (it?.first == false && it.second != null) {
                        mRepository?.deleteFromDatabase(it.second!!)

                    }
                }.subscribeOn(io())
                .doOnComplete {
                    mainHandler.postDelayed(
                        task,
                        IConstants.OrderTracking.ORDER_TRACKING_DURATION
                    )
                }
                .doOnError { /*TODO Log report in firebase*/ }
                .subscribe()

        } else {
            updateNotification("", "", "-1", "")
            mainHandler.postDelayed(task, IConstants.OrderTracking.ORDER_TRACKING_DURATION)
        }
    }

    private fun startForeground() {
        mChannelId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel()
            } else {
                ""
            }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            /*TODO SHRIOM*/
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                startForeground(
                    IConstants.OrderTracking.ORDER_TRACK_DEFAULT_NOTIFY_ID,
                    createNotification("Yumzy", "We are are finding restaurant near you", ""), 0
                )
            }
        }
//        else {
//            startForeground(
//                IConstants.OrderTracking.ORDER_TRACK_DEFAULT_NOTIFY_ID,
//                null
//            )
//        }


        /*startForeground(
            IConstants.OrderTracking.ORDER_TRACK_DEFAULT_NOTIFY_ID,
            null
        )*/
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(): String {
        val chan = NotificationChannel(
            IConstants.OrderTracking.CHANNEL_ID,
            IConstants.OrderTracking.CHANNEL_NAME,
            NotificationManager.IMPORTANCE_LOW
        )
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        chan.setSound(null, null)
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return IConstants.OrderTracking.CHANNEL_ID
    }

    private fun createNotification(title: String, text: String, orderId: String): Notification {

        val intent = Intent(this, HomePageActivity::class.java)
        intent.putExtra(IConstants.OrderTracking.ORDER_TRACK_ID, orderId)

        val contentIntent = PendingIntent.getActivity(
            this,
            IConstants.OrderTracking.ORDER_TRACK_REQUEST_CODE,
            intent,
            0
        )

        return NotificationCompat.Builder(this, mChannelId)
            .setContentTitle(title)
            .setContentText(text)
            .setSmallIcon(R.drawable.ic_stat_app_logo)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setSound(null)
            .setContentIntent(contentIntent)
            .setGroup(GROUP_KEY_ORDER_STATUS)
            .build()
    }

    private fun updateNotification(
        title: String,
        description: String,
        orderNum: String,
        orderId: String
    ) {
        if (orderNum == "-1") {
            mNotificationManager?.cancel(IConstants.OrderTracking.ORDER_TRACK_DEFAULT_NOTIFY_ID)
        } else {
            val notification = createNotification(title, description, orderId)
            try {
                mNotificationManager?.notify(Integer.parseInt(orderNum), notification)
            } catch (e: Exception) {
                mNotificationManager?.notify(
                    IConstants.OrderTracking.ORDER_TRACK_DEFAULT_NOTIFY_ID,
                    notification
                )
            }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        mainHandler.removeCallbacks(task)
    }

    override fun showProgressBar() {

    }

    override fun hideProgressBar() {

    }


    override fun showCodeError(code: Int?, title: String?, message: String) {

    }

    override fun onResponse(code: Int?, response: Any?) {

    }

}