package com.yumzy.orderfood.ui.screens.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.ModelAbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.ListElement
import com.yumzy.orderfood.databinding.AdapterAccountCardBinding
import com.yumzy.orderfood.ui.screens.home.data.SportCardModel

class AccountCardItem(model: SportCardModel) :
    ModelAbstractBindingItem<SportCardModel, AdapterAccountCardBinding>(model) {

    internal var restaurantItem: ListElement? = null

    override fun bindView(binding: AdapterAccountCardBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)
//        binding.imageUrl = restaurantItem?.imageUrl ?: ""
//        binding.title = restaurantItem?.outletName ?: ""
//        binding.subtitle = if(!restaurantItem?.subTitle.isNullOrEmpty()) restaurantItem?.subTitle else ""
//        binding.cuisines =  restaurantItem?.cuisines?.joinToString(separator = ", ", postfix = "") ?: "New Arrivals"
//        binding.duration = restaurantItem?.costForTwo.toString()
//        binding.offer = YumUtil.setZoneOfferString(restaurantItem?.offers)

        /*TODO add rating view*/
//        binding.rating = restaurantItem?.rating.toString()
//        binding.horizontalView.colorTextSize(13f)
    }


    override val type = R.id.account_card_item

    override fun unbindView(binding: AdapterAccountCardBinding) {
//        binding.imageUrl = null

//        binding.title = null
//        binding.subtitle = null
//        binding.cuisines = null
//        binding.offer = null
//        binding.rating = null
//        binding.duration = null
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterAccountCardBinding {
        return AdapterAccountCardBinding.inflate(inflater, parent, false)
    }
}