package com.yumzy.orderfood.ui.component.outlets

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.LayoutOutletHeaderBinding
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.invisibleEmptyTextView

class OutletHeaderLayout : FrameLayout {
    var outletName: CharSequence? = null
        set(value) {
            field = value
            if (field == null || field!!.isEmpty()) {
                binding.tvResName.invisibleEmptyTextView(field)
            } else {
                binding.tvResName.text = field.toString()
                binding.tvResName.visibility = View.VISIBLE

            }

        }

    var isFav: Boolean = false
        set(value) {
            field = value
            if (field) {
                binding.fontIcon = resources.getString(R.string.icon_heart_1)
                binding.fontIconColor = ThemeConstant.redRibbonColor

            } else {
                binding.fontIcon = resources.getString(R.string.icon_heart_empty_1)
                binding.fontIconColor = ThemeConstant.textBlackColor


            }

        }
    val binding by lazy {
        val layoutInflater = LayoutInflater.from(context)
        LayoutOutletHeaderBinding.inflate(layoutInflater, this, true)
    }

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        if (attrs != null) {
            val a =
                context.obtainStyledAttributes(
                    attrs,
                    R.styleable.OutletHeaderLayout,
                    defStyleAttr,
                    0
                )

            outletName = a.getString(R.styleable.OutletHeaderLayout_outletName) ?: ""
            isFav = a.getBoolean(R.styleable.OutletHeaderLayout_isFav, false)
            a.recycle()
        }
        binding.searchIcon.setOnClickListener {

            search?.invoke(it)
        }
        binding.shareIcon.setOnClickListener {

            share?.invoke(it)
        }
        binding.favIcon.setOnClickListener {

            favrouite?.invoke(it)
        }

        binding.iconBackArrow.setOnClickListener {

            backArrow?.invoke(it)
        }
    }

    var search: ((view: View) -> Unit)? = null
    var share: ((view: View) -> Unit)? = null
    var favrouite: ((view: View) -> Unit)? = null
    var backArrow: ((view: View) -> Unit)? = null

}