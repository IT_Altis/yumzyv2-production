package com.yumzy.orderfood.ui.screens.orderhistory

import com.yumzy.orderfood.ui.base.IView

/**
 * Created by Bhupendra Kumar Sahu.
 */
interface IOrderHistory : IView {
    fun fetchOrderHistoryList()
    fun getOrderID(orderID: String, position: Int)

}