package com.yumzy.orderfood.ui.component;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import com.laalsa.laalsaui.drawable.DrawableUtils;
import com.yumzy.orderfood.R;
import com.yumzy.orderfood.ui.helper.ThemeConstant;
import com.laalsa.laalsalib.ui.VUtil;


public class RatingReviewView extends LinearLayout {

    private String title;
    private String subTitle;
    private String smallTitle;


    public RatingReviewView(Context context) {
        this(context, null);
    }

    public RatingReviewView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RatingReviewView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (attrs != null)
            initAttrs(context, attrs, defStyleAttr);
        if (title == null) {
            title = "";
        }
        if (subTitle == null) {
            subTitle = "";
        }
        if (smallTitle == null) {
            smallTitle = "";
        }
        init();
    }

    private void initAttrs(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.RatingReviewView, defStyleAttr, 0);
        title = a.getString(R.styleable.RatingReviewView_review_tag);
        subTitle = a.getString(R.styleable.RatingReviewView_reviewed_by);
        smallTitle = a.getString(R.styleable.RatingReviewView_reviewed_date);
        a.recycle();
    }


    public void init() {
        this.setOrientation(VERTICAL);
        this.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        this.setBackground(DrawableUtils.getRoundDrawable(ThemeConstant.lightAlphaGray, VUtil.dpToPx(8)));

        int i12px = VUtil.dpToPx(12);

        /*Review*/
        TextView tvReviewTag = new TextView(getContext());
        LayoutParams titleParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tvReviewTag.setText(title);
        titleParams.setMargins(i12px, i12px, i12px, i12px);
        tvReviewTag.setId(R.id.tv_review);
        tvReviewTag.setMaxLines(3);
        tvReviewTag.setEllipsize(TextUtils.TruncateAt.END);
        tvReviewTag.setTextColor(ContextCompat.getColor(getContext(), android.R.color.black));
        tvReviewTag.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f);
        tvReviewTag.setLayoutParams(titleParams);

        /*Reviewd By*/
        TextView tvReviewdBy = new TextView(getContext());
        LayoutParams subtitleParam = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        subtitleParam.setMargins(i12px, 0, i12px, 0);
        tvReviewdBy.setTextColor(ContextCompat.getColor(getContext(), R.color.cool_gray));
        tvReviewdBy.setText(subTitle);
        tvReviewdBy.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11f);
        tvReviewdBy.setId(R.id.tv_reviewd_by);
        tvReviewdBy.setLayoutParams(subtitleParam);


        /*Revie Date*/
        TextView tvReviewDate = new TextView(getContext());
        LayoutParams smalltitleParam = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        smalltitleParam.setMargins(i12px, 0, i12px, i12px);
        tvReviewDate.setTextColor(ContextCompat.getColor(getContext(), R.color.cool_gray));
        tvReviewDate.setText(smallTitle);
        tvReviewDate.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10f);
        tvReviewDate.setId(R.id.tv_review_date);
        tvReviewDate.setLayoutParams(smalltitleParam);


        this.addView(tvReviewTag);
        this.addView(tvReviewdBy);
        this.addView(tvReviewDate);
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        ((TextView) findViewById(R.id.tv_review)).setText(this.title);
    }


    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
        ((TextView) findViewById(R.id.tv_reviewd_by)).setText(this.subTitle);

    }

    public String getSmallTitle() {
        return smallTitle;
    }

    public void setSmallTitle(String smallTitle) {
        this.smallTitle = smallTitle;
        ((TextView) findViewById(R.id.tv_review_date)).setText(this.smallTitle);

    }
}
