package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.yumzy.orderfood.R
import com.yumzy.orderfood.databinding.ViewExploreListBinding


class ExploreView : ConstraintLayout {


    var title: String = ""
        set(value) {
            field = value
            binding.title = field
        }
    var subTitle: String = ""
        set(value) {
            field = value
            binding.subtitle = field
        }

    var typeOf: String = ""
        set(value) {
            field = value
            binding.type = field
        }

    var imageUrl: String = ""
        set(value) {
            field = value
            binding.imageUrl = field

        }


    private val binding by lazy {
        val layoutInflater = LayoutInflater.from(context)
        ViewExploreListBinding.inflate(layoutInflater, this, true)
    }


    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {

        if (attrs != null) {
            val a =
                context.obtainStyledAttributes(
                    attrs,
                    R.styleable.ExploreView,
                    defStyleAttr,
                    0
                )

            title = a.getString(R.styleable.ExploreView_exp_item_title) ?: ""
            subTitle = a.getString(R.styleable.ExploreView_exp_sub_title) ?: ""
            imageUrl = a.getString(R.styleable.ExploreView_exp_img) ?: ""
            typeOf = a.getString(R.styleable.ExploreView_exp_type_of) ?: ""
            a.recycle()

        }
        this.setOnClickListener {
            itemClick?.invoke(it)
        }
    }

    var itemClick: ((view: View) -> Unit)? = null
}