package com.yumzy.orderfood.ui.component

import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.checkbox.MaterialCheckBox
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.mikepenz.fastadapter.listeners.ClickEventHook
import com.mikepenz.fastadapter.select.SelectExtension
import com.mikepenz.fastadapter.ui.utils.StringHolder
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.CustomizeOrderItemDTO
import com.yumzy.orderfood.ui.helper.ThemeConstant
import com.yumzy.orderfood.ui.helper.setInterRegularFont
import com.yumzy.orderfood.util.viewutils.YumUtil
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView

/**
 * Created by mikepenz on 28.12.15.
 */
class OrderCustomizationOptionItem(val optionItemType: OptionItemType) :
    AbstractItem<OrderCustomizationOptionItem.ViewHolder>() {

    var header: String? = null
    var name: StringHolder? = null
    var description: StringHolder? = null
    var data: CustomizeOrderItemDTO? = null

    /**
     * defines the type defining this item. must be unique. preferably an id
     *
     * @return the type
     */
    override val type: Int
        get() = if (optionItemType.equals(OptionItemType.MULTI_SELECT))
            R.id.item_checkbox_item_id
        else
            R.id.item_radio_item_id

    /**
     * defines the layout which will be used for this item in the list
     *
     * @return the layout for this item
     */
    override val layoutRes: Int
        get() = if (optionItemType.equals(OptionItemType.MULTI_SELECT))
            R.layout.order_customization_checkbox_item
        else
            R.layout.order_customization_radio_item


    fun withData(data: CustomizeOrderItemDTO): OrderCustomizationOptionItem {
        this.data = data
        this.name = StringHolder(data.itemName)
        return this
    }

    /**
     * binds the data of this item onto the viewHolder
     *
     * @param holder the viewHolder of this item
     */
    override fun bindView(holder: ViewHolder, payloads: List<Any>) {
        super.bindView(holder, payloads)

        holder.selectionBox.isChecked = isSelected

        holder.icon.text = data?.iconText
        holder.icon.setTextColor(
            if(data?.isVeg == true)
                ThemeConstant.greenConfirmColor
            else
                ThemeConstant.redCancelColor
        )

        holder.name.text = data?.itemName.toString().capitalize()
        holder.price.setText(
            YumUtil.getOfferItemPrice(data?.itemOfferPrice, data?.itemPrice),
            TextView.BufferType.SPANNABLE
        )
        holder.price.setInterRegularFont()

    }

    override fun unbindView(holder: ViewHolder) {
        super.unbindView(holder)
        holder.name.text = null
        holder.selectionBox.text = null
        holder.price.text = null
    }

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v, optionItemType)
    }

    /**
     * our ViewHolder
     */
    open class ViewHolder(protected var view: View, optionItemType: OptionItemType) :
        RecyclerView.ViewHolder(view) {
        var icon: FontIconView = view.findViewById(R.id.font_icon_view)
        var selectionBox: CompoundButton = if (optionItemType.equals(OptionItemType.MULTI_SELECT))
            view.findViewById(R.id.checkbox)
        else
            view.findViewById(R.id.radiobutton)

        internal var name: TextView = view.findViewById(R.id.tv_item_name)
        internal var price: TextView = view.findViewById(R.id.tv_item_price)
    }

    class CheckBoxClickEvent() : ClickEventHook<OrderCustomizationOptionItem>() {

        override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
            return if (viewHolder is ViewHolder) {
                viewHolder.itemView
            } else null
        }

        override fun onBindMany(viewHolder: RecyclerView.ViewHolder): List<View>? {
            return if (viewHolder is ViewHolder) {
                listOf(viewHolder.itemView, viewHolder.selectionBox)
            } else
                null
        }

        override fun onClick(
            v: View,
            position: Int,
            fastAdapter: FastAdapter<OrderCustomizationOptionItem>,
            item: OrderCustomizationOptionItem
        ) {
            val childAt = if(v is ViewGroup) v.getChildAt(1) else v
            if (childAt is MaterialCheckBox) {
                //logic for multi selection
                val selectExtension: SelectExtension<OrderCustomizationOptionItem> =
                    fastAdapter.requireExtension()
                selectExtension.toggleSelection(position)
            } else {
                //login for single selection
                if (!item.isSelected) {
                    val selectExtension: SelectExtension<OrderCustomizationOptionItem> =
                        fastAdapter.requireExtension()
                    val selections = selectExtension.selections
                    if (selections.isNotEmpty()) {
                        val selectedPosition = selections.iterator().next()
                        selectExtension.deselect()
                        fastAdapter.notifyItemChanged(selectedPosition)
                    }
                    selectExtension.select(position)
                }
            }
        }
    }

    companion object {
        enum class OptionItemType {
            SINGLE_SELECT,
            MULTI_SELECT
        }
    }

}
