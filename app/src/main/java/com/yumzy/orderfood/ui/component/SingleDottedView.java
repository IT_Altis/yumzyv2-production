package com.yumzy.orderfood.ui.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.google.android.material.textview.MaterialTextView;
import com.yumzy.orderfood.R;
import com.yumzy.orderfood.util.viewutils.YumUtil;
import com.laalsa.laalsalib.ui.VUtil;

/**
 * Created by Bhupendra Kumar Sahu
 */
public class SingleDottedView extends LinearLayout {

    private String title;
    private int mColor;

    @RequiresApi(api = Build.VERSION_CODES.M)
    public SingleDottedView(Context context) {
        this(context, null);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public SingleDottedView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public SingleDottedView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            initAttrs(context, attrs, defStyleAttr);
        if (title == null) {
            title = "";
        }

        init();

    }

    private void initAttrs(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.SingleDottedView, defStyleAttr, 0);
        title = a.getString(R.styleable.SingleDottedView_view_title);
        mColor = a.getColor(R.styleable.SingleDottedView_view_color, 0);
        a.recycle();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void init() {
        this.setOrientation(HORIZONTAL);
        this.setGravity(Gravity.CENTER);

        int i6px = VUtil.dpToPx(6);
        int i30px = VUtil.dpToPx(30);

        MaterialTextView tvTag = new MaterialTextView(getContext());
        tvTag.setPadding(i6px, i6px, i6px, i6px);
        tvTag.setId(R.id.tv_view_title);
        tvTag.setTextColor(mColor);
        tvTag.setGravity(Gravity.CENTER);
        tvTag.setTextAppearance(R.style.TextAppearance_MyTheme_Bold);

        tvTag.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f);
        tvTag.setText(title);
        this.addView(tvTag);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        ((MaterialTextView) findViewById(R.id.tv_view_title)).setText(this.title);

    }


    public int getmColor() {
        return mColor;
    }

    public void setmColor(int mColor) {
        this.mColor = mColor;
        ((MaterialTextView) findViewById(R.id.tv_view_title)).setTextColor(this.mColor);

    }
}
