package com.yumzy.orderfood.ui.common.recyclerview.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.laalsa.laalsalib.ui.VUtil;
import com.laalsa.laalsalib.utilcode.util.ClickUtils;
import com.yumzy.orderfood.R;
import com.yumzy.orderfood.ui.common.recyclerview.listener.IItemViewHolderListener;
import com.yumzy.orderfood.ui.common.recyclerview.view.YItemViewHolder;
import com.yumzy.orderfood.ui.common.recyclerview.view.YProgressHolder;
import com.yumzy.orderfood.ui.common.recyclerview.view.YRecyclerView;
import com.yumzy.orderfood.util.IConstants;
import com.yumzy.orderfood.util.ViewConst;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;

public class YRecyclerViewAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements IItemViewHolderListener {
    //protected IActionModeEventListener iItemViewEventListener;
    protected static final int TYPE_FULL = 3;
    protected static final int TYPE_HALF = 4;
    protected static final int TYPE_QUARTER = 5;
    protected static final int TYPE_ITEM = 2;
    protected static final int TYPE_PROGRESS = 8;
    public int[] iSwipeIndexes;
    protected ArrayList<T> itemHolderArrayList;
    protected Context context;
    protected IItemViewHolderListener itemViewHolderListener;
    protected YRecyclerView.IItemClickListener clItemClickListener = null;
    protected boolean mWithHeader = false;
    protected boolean mWithFooter = false;
    protected boolean isStaggeredLM = false;
    protected boolean hasMoreRecords = false;
    protected int iSelectedItemPos = -1;
    private int PAGE_SIZE = 20;

    public YRecyclerViewAdapter(Context context, ArrayList<T> itemHolderArrayList, IItemViewHolderListener itemViewHolderListener) {
        this.itemHolderArrayList = itemHolderArrayList;
        this.context = context;
        this.itemViewHolderListener = itemViewHolderListener;
    }

    public YRecyclerViewAdapter(Context context, ArrayList<T> itemHolderArrayList, IItemViewHolderListener itemViewHolderListener, boolean hasMoreRecords) {
        this.itemHolderArrayList = itemHolderArrayList;
        this.context = context;
        this.itemViewHolderListener = itemViewHolderListener;
        this.hasMoreRecords = hasMoreRecords;
    }

    public void enableStaggeredLayoutManager(boolean isStaggeredLM) {
        this.isStaggeredLM = isStaggeredLM;
    }

    public void setItemClickListener(YRecyclerView.IItemClickListener clItemClickListener) {
        this.clItemClickListener = clItemClickListener;
    }

//    public void  applyDebouchingClickListener(View[] views) {/*TODO*/
//        ClickUtils.applyGlobalDebouncing(views, clItemClickListener)
//        ClickUtils.applyPressedViewScale(*views)
//    }

    @NotNull
    @Override
    public YItemViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
//        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        YItemViewHolder yItemViewHolder;
        if (viewType != TYPE_PROGRESS)
            yItemViewHolder = itemViewHolderListener.onCreateViewHolder(parent, viewType);
        else {
            LinearLayout layout = new LinearLayout(context);
            layout.setLayoutParams(new LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.MATCH_PARENT));
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setGravity(Gravity.CENTER);

            ProgressBar progressBar = new ProgressBar(context);
            progressBar.setLayoutParams(new LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, VUtil.dpToPx(45)));
            progressBar.setId(R.id.progress_indicator);
            layout.addView(progressBar);
            yItemViewHolder = new YItemViewHolder(layout);
        }

        /*else if (viewType == TYPE_HEADER) {
            return getHeaderView(inflater, parent);
        } else if (viewType == TYPE_FOOTER) {
            return getFooterView(inflater, parent);
        }*/
        if (isStaggeredLM) {
            final View itemView = yItemViewHolder.itemView;
            itemView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    if (itemView.getLayoutParams() instanceof StaggeredGridLayoutManager.LayoutParams) {
                        StaggeredGridLayoutManager.LayoutParams sglp =
                                (StaggeredGridLayoutManager.LayoutParams) itemView.getLayoutParams();
                        switch (viewType) {
                            case TYPE_FULL:
                                sglp.setFullSpan(true);
                                break;
                            case TYPE_HALF:
                                sglp.setFullSpan(false);
                                sglp.width = itemView.getWidth() / 2;
                                break;
                            case TYPE_QUARTER:
                                sglp.setFullSpan(false);
                                sglp.width = itemView.getWidth() / 2;
                                sglp.height = itemView.getHeight() / 2;
                                break;
                        }
                        final StaggeredGridLayoutManager lm =
                                (StaggeredGridLayoutManager) ((RecyclerView) parent).getLayoutManager();
                        if (lm != null) {
                            lm.invalidateSpanAssignments();
                        }

                    }
                    itemView.getViewTreeObserver().removeOnPreDrawListener(this);
                    return true;
                }
            });
        }
        /*else
            throw new RuntimeException("there is no type that matches the type " + viewType + " make sure your using types correctly");*/
        View view = itemViewHolderListener.onCreateViewHolder(parent, viewType).itemView;
        FrameLayout mainLayout = new FrameLayout(context);
        mainLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mainLayout.addView(view);

        return yItemViewHolder;
//        else if(viewType==EXPANDABLE_VIEW_TYPE_SECTION){
//                return itemViewHolderListener.onCreateViewHolder(parent,viewType);
        //}


    }

    private YItemViewHolder getHeaderView(LayoutInflater inflater, ViewGroup parent) {
        LinearLayout.LayoutParams txtParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, VUtil.dpToPx(200));
        TextView textView = new TextView(context);
        textView.setText("Header View");
        textView.setTextSize(20);
        textView.setTextColor(context.getResources().getColor(R.color.white));
        textView.setLayoutParams(txtParams);
        textView.setGravity(Gravity.CENTER);

        textView.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
        return null;
    }

    private YItemViewHolder getFooterView(LayoutInflater inflater, ViewGroup parent) {
        LinearLayout.LayoutParams txtParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, VUtil.dpToPx(200));
        TextView textView = new TextView(context);
        textView.setText("Footer View");
        textView.setTextSize(20);
        textView.setGravity(Gravity.CENTER);
        textView.setTextColor(context.getResources().getColor(R.color.white));
        textView.setLayoutParams(txtParams);

        textView.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
        return null;
    }


    @Override
    public void onBindViewHolder(@NotNull final RecyclerView.ViewHolder holder, final int position) {
        if (!(holder instanceof YProgressHolder)) {

            itemViewHolderListener.onBindViewHolder(holder, holder.getAdapterPosition());

//            ((YItemViewHolder) holder).itemView.setOnClickListener(view{});

            ClickUtils.applySingleDebouncing(
                    ((YItemViewHolder) holder).itemView,
                    IConstants.AppConstant.CLICK_DELAY,
                    v -> {

                        if (clItemClickListener != null) {
                            iSelectedItemPos = holder.getAdapterPosition();
                            clItemClickListener.onItemClick(v, holder.getAdapterPosition());
                            //     ClickUtils.applyGlobalDebouncing(v, null);
                            //   ClickUtils.applyPressedViewScale(v);

                        }
                    }
            );
           /* ((YItemViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (clItemClickListener != null) {
                        iSelectedItemPos = holder.getAdapterPosition();
                        clItemClickListener.onItemClick(v, holder.getAdapterPosition());
                        //     ClickUtils.applyGlobalDebouncing(v, null);
                        //   ClickUtils.applyPressedViewScale(v);

                    }

                        *//*if (iItemViewEventListener != null)
                            iItemViewEventListener.onSingleTap(v, actionMode,position);*//*
                }
            });*/
            if (isStaggeredLM) {

            }
        } else {
            ((YProgressHolder) holder).clLoadMoreProgress.setIndeterminate(true);
            ((YProgressHolder) holder).clLoadMoreProgress.setIndeterminate(false);


        }
    }

    @Override
    public int getItemCount() {
        if (itemHolderArrayList != null)
            return itemHolderArrayList.size();
        return -1;
    }

    @Override
    public int getItemViewType(int position) {
        int itemType = itemViewHolderListener.getItemViewType(position);
        if (itemType < 0) {
            if (hasMoreRecords && itemHolderArrayList.get(position) == null)
                return TYPE_PROGRESS;
            if (isStaggeredLM)
                return itemViewHolderListener.getItemViewType(position);
            else {
                int iItemType = itemViewHolderListener.getItemViewType(position);
                if (iItemType > 0)
                    return iItemType;
                else
                    return TYPE_ITEM;
            }
        }
        return itemType;

    }

    public void setSwipeIndex(int iSwipeIndex, int i) {
        if (iSwipeIndexes == null)
            iSwipeIndexes = new int[itemHolderArrayList.size()];
        iSwipeIndexes[iSwipeIndex] = i;
        notifyItemChanged(iSwipeIndex);
    }

    public void clearSwipeIndexes() {
        this.iSwipeIndexes = null;
    }


    public void setHasMoreRecords(boolean isLoadMore) {
        hasMoreRecords = isLoadMore;
    }

    public void onMove(RecyclerView.ViewHolder source, RecyclerView.ViewHolder target) {
        int fromPosition = source.getAdapterPosition(), toPosition = target.getAdapterPosition();
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(itemHolderArrayList, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(itemHolderArrayList, i, i - 1);
            }
        }

        moveSelectedItem(fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
    }


    private void moveSelectedItem(int fromPosition, int toPosition) {
        /*List<Integer> items = getSelectedItems();
        if (items.contains(fromPosition) && items.contains(toPosition)) return;
        if (items.contains(fromPosition) || items.contains(toPosition)) {
            mSelectedItemsIds.put(toPosition, true);
            if (items.contains(toPosition)) {
                mSelectedItemsIds.put(fromPosition, true);
                mSelectedItemsIds.delete(toPosition);
            } else mSelectedItemsIds.delete(fromPosition);
        }*/
    }


    public void removeItem(int position) {
        if (itemHolderArrayList != null) {
            itemHolderArrayList.remove(position);
            notifyItemRemoved(position);
        }
    }


    /*public void setActionModeEventListener(IActionModeEventListener iItemViewEventListener){
        this.iItemViewEventListener = iItemViewEventListener;
    }*/

    // ----------interfaces-----------

  /*  public interface IActionModeEventListener {
        boolean onLongPress(View view, ActionMode actionMode);
        void onSingleTap(View view, ActionMode actionMode,int iPosition);
        boolean onActionModeItemClicked(ActionMode mode, MenuItem item);
    }
*/


    /*   class HeaderViewHolder extends CLItemActionViewHolder {
           public HeaderViewHolder(View itemView) {
               super(itemView);
           }
       }

       class FooterViewHolder extends CLItemActionViewHolder{
           public FooterViewHolder(View itemView) {
               super(itemView);
           }
       }
   */
    public void addNewItem(T item) {
        if (itemHolderArrayList != null) {
            itemHolderArrayList.add(item);
            notifyItemInserted(itemHolderArrayList.size() - 1);
            notifyDataSetChanged();
        }

    }

    public void addNewItem(T item, int iPosition) {
        if (itemHolderArrayList != null) {
            itemHolderArrayList.add(iPosition, item);
            notifyItemInserted(iPosition);
//            notifyDataSetChanged();
        }

    }

    public ArrayList<T> getData() {
        return itemHolderArrayList;
    }

    public void setData(ArrayList<T> arrayList) {
        if (arrayList == null) {
            itemHolderArrayList = new ArrayList<>();
        } else
            itemHolderArrayList = arrayList;
        notifyDataSetChanged();
    }

    public int getiSelectedItemPosition() {
        return iSelectedItemPos;
    }
}




