package com.yumzy.orderfood.ui.screens.home

import com.google.android.material.bottomnavigation.BottomNavigationView
import com.yumzy.orderfood.tools.update.RemoteConfigUpdate
import com.yumzy.orderfood.ui.base.IView
import com.yumzy.orderfood.ui.common.ConfirmActionDialog
import com.yumzy.orderfood.ui.common.IModuleHandler

interface IHomePageView : IView,
    BottomNavigationView.OnNavigationItemSelectedListener, IModuleHandler,
    ConfirmActionDialog.OnActionPerformed,
    RemoteConfigUpdate.OnRemoteConfigListener {
    fun displayNewEvent()
}