package com.yumzy.orderfood.ui.contracthandler

import android.app.Activity
import android.content.Intent
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.ActivityResultRegistry
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner


class ContractResultActivity(
    private val registry: ActivityResultRegistry,
) :
    DefaultLifecycleObserver {
    lateinit var startForResult: ActivityResultLauncher<Intent>
    private var resultOk: ((Boolean, ActivityResult) -> Unit)? = null
    override fun onCreate(owner: LifecycleOwner) {
        startForResult = registry.register(
            ContractAction.ACTIVITY_FOR_RESULT,
            ActivityResultContracts.StartActivityForResult()
        )
        { result: ActivityResult ->
            resultOk?.invoke(result.resultCode == Activity.RESULT_OK, result)
        }
    }

    fun startIntentForResult(
        intent: Intent,
        callback: ((resultOk: Boolean, result: ActivityResult) -> Unit)?
    ) {
        this.resultOk = callback
        startForResult.launch(intent)
    }

    fun startForResult(
        activity: AppCompatActivity,
        callerClass: Class<out AppCompatActivity>,
        options: ActivityOptionsCompat? = null,
        callback: ((resultOk: Boolean, result: ActivityResult) -> Unit)?
    ) {
        this.resultOk = callback
        val basic = ActivityOptionsCompat.makeBasic()
        val intent = Intent(activity, callerClass)
        if (options != null) {
            startForResult.launch(intent, basic)
        } else
            startForResult.launch(intent)


    }

    fun startIntentResult(
        intent: Intent,
        options: ActivityOptionsCompat? = null,
        callback: ((resultOk: Boolean, result: ActivityResult) -> Unit)?
    ) {
        this.resultOk = callback
        if (options != null) {
            startForResult.launch(intent, options)
        } else
            startForResult.launch(intent)


    }

}