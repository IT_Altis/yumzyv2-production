/*
 * Created By Shriom Tripathi 2 - 5 - 2020
 */

package com.yumzy.orderfood.ui.component

import android.animation.LayoutTransition
import android.content.Context
import android.text.TextUtils
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.button.MaterialButton
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsalib.ui.px
import com.laalsa.laalsaui.drawable.DrawableUtils
import com.laalsa.laalsaui.utils.pxf
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.ui.component.groupview.FlowGroupView
import com.yumzy.orderfood.ui.component.groupview.GroupItemActions
import com.yumzy.orderfood.ui.component.groupview.OnGroupItemClickListener
import com.yumzy.orderfood.ui.helper.*
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.viewutils.YumUtil
import java.util.*

class ConfirmLocationCardView : CardView, View.OnClickListener,
    FlowGroupView.OnSelectedItemChangeListener {

    private var mAddress: AddressDTO = AddressDTO()

    var mAddrTypeFlowGroup: FlowGroupView? = null

    private var mAddressType = ""
        set(value) {
            if (value.toLowerCase(Locale.getDefault()) != "home" && value.toLowerCase(Locale.getDefault()) != "office") {
                val input =
                    this.findViewById<EditText>(IConstants.ConfirmLocationModule.VIEW_TEXT_OTHERS)
                input.visibility = View.VISIBLE

                /*if (value.toLowerCase(Locale.getDefault()) == "other") {
                    input.setText(value)
                    mLocationTitle = value
                }*/

            } else {
                val input =
                    this.findViewById<EditText>(IConstants.ConfirmLocationModule.VIEW_TEXT_OTHERS)
                if (input != null) {
                    input.visibility = View.GONE
                }
            }

            mLocationTitle = value
            field = value
        }
        get() {
            if (field.toLowerCase(Locale.getDefault()) != "home" && field.toLowerCase(Locale.getDefault()) != "office") {
                val input =
                    this.findViewById<EditText>(IConstants.ConfirmLocationModule.VIEW_TEXT_OTHERS)
                input.visibility = View.VISIBLE
                val text = input.text.toString()
                if (text.isEmpty()) {
                    YumUtil.animateShake(input, 50, 15)
                    return ""
                }
                mLocationTitle = text
                field = text
                return field
            } else {
                return field
            }
        }


    private var mLocationTitle = ""
        set(value) {
            this.findViewById<TextView>(IConstants.ConfirmLocationModule.VIEW_TEXT_TITLE)?.let {
                it.text = value
            }
            field = value
        }
    private var mFullAddress = ""
        set(value) {
            this.findViewById<TextView>(IConstants.ConfirmLocationModule.VIEW_TEXT_FULL_ADDRESS)
                .text =
                value
            field = value
        }

    var mCollapsed = false
        set(value) {
            val detailsView =
                this.findViewById<LinearLayout>(IConstants.ConfirmLocationModule.VIEW_CONTAINER_DETAIL_ADDRESS)
            if (value && detailsView.visibility == View.VISIBLE) {
                detailsView?.visibility = View.GONE
            } else if (!value && detailsView.visibility == View.GONE) {
                detailsView.visibility = View.VISIBLE
            }
            field = value
        }

    private var mOnConfirmLocationClickListener: OnConfirmLocationClickListener? = null

    private var mHouseNo = ""
        get() {
            val viewFlatNo =
                this.findViewById<TextView>(IConstants.ConfirmLocationModule.VIEW_TEXT_FLAT_DETAIL)
            if (viewFlatNo.text.isBlank()) {
                YumUtil.animateShake(viewFlatNo, 50, 15)
                mCollapsed = false
            } else {
                field = viewFlatNo.text.toString()
            }
            return field
        }
        set(value) {
            this.findViewById<TextView>(IConstants.ConfirmLocationModule.VIEW_TEXT_FLAT_DETAIL)
                .text =
                value
            field = value
        }


    private var mLandMark = ""
        get() {
            val viewLandmark =
                this.findViewById<TextView>(IConstants.ConfirmLocationModule.VIEW_TEXT_LANDMARK)
            if (viewLandmark.text.isBlank()) {
                YumUtil.animateShake(viewLandmark, 50, 15)
                mCollapsed = false
            } else {
                field = viewLandmark.text.toString()
            }
            return field
        }
        set(value) {

            this.findViewById<TextView>(IConstants.ConfirmLocationModule.VIEW_TEXT_LANDMARK).text =
                value
            field = value

        }


    private val i30px = VUtil.dpToPx(30)
    private val i24px = VUtil.dpToPx(24)
    private val i16px = VUtil.dpToPx(16)
    private val i14px = VUtil.dpToPx(14)
    private val i12px = VUtil.dpToPx(12)
    private val i10px = VUtil.dpToPx(10)
    private val i8px = VUtil.dpToPx(8)
    private val i6px = VUtil.dpToPx(6)
    private val i4px = VUtil.dpToPx(4)
    private val i2px = VUtil.dpToPx(2)

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, atr: AttributeSet?) : this(context, atr, 0)
    constructor(context: Context, atr: AttributeSet?, style: Int) : super(context, atr, style) {

        val lp = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        this.radius =10.pxf
        this.layoutParams = lp

        setOnClickListener { }
        initalize()

    }

    private fun initalize() {
        this.addView(getLinearyParent())
    }

    private fun getLinearyParent(): LinearLayout? {

        val linearLayout = LinearLayout(context)
        linearLayout.setPadding(i8px, i16px, i8px, i16px)
        linearLayout.layoutParams =
            LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        linearLayout.orientation = LinearLayout.VERTICAL
        linearLayout.setBackgroundColor(ThemeConstant.white)

        val l = LayoutTransition()
        l.enableTransitionType(LayoutTransition.APPEARING)
        linearLayout.layoutTransition = l

        addHeaderView(linearLayout)
        addFullAddressView(linearLayout)
        addDetialAddressContainer(linearLayout)
        addAddressTypeContainer(linearLayout)
        addOtherAddressInput(linearLayout)
        addConfirmLocationBtn(linearLayout)

        return linearLayout
    }

    private fun addDetialAddressContainer(parent: LinearLayout) {
        val linearLayout = LinearLayout(context)
        linearLayout.id = IConstants.ConfirmLocationModule.VIEW_CONTAINER_DETAIL_ADDRESS
        linearLayout.visibility = View.GONE
        linearLayout.orientation = LinearLayout.VERTICAL
        linearLayout.layoutParams =
            LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)

        addFlatNoView(
            linearLayout,
            context.resources.getString(R.string.house_flat_block_no),
            context.resources.getString(R.string.write_home_flat_block_no),
            IConstants.ConfirmLocationModule.VIEW_TEXT_FLAT_DETAIL
        )
        addFlatNoView(
            linearLayout,
            context.resources.getString(R.string.landmark),
            context.resources.getString(R.string.mention_landmark_here),
            IConstants.ConfirmLocationModule.VIEW_TEXT_LANDMARK
        )

        parent.addView(linearLayout)
    }

    private fun addHeaderView(linearLayout: LinearLayout) {

        val horizLiearLayout = LinearLayout(context)
        horizLiearLayout.orientation = LinearLayout.HORIZONTAL
        horizLiearLayout.gravity = Gravity.CENTER_VERTICAL

        //image icon
        val imageView = ImageView(context)
        imageView.id = IConstants.ConfirmLocationModule.VIEW_ICON_IMAGE
        imageView.setImageDrawable(
            ResourcesCompat.getDrawable(
                context.resources,
                R.drawable.ic_pin_blue_down,
                null
            )
        )

        val imageLp = LinearLayout.LayoutParams(i30px, i30px)
        imageView.layoutParams = imageLp
        horizLiearLayout.addView(imageView)

        //title
        val titleTextView = TextView(context)
        titleTextView.id = IConstants.ConfirmLocationModule.VIEW_TEXT_TITLE
        titleTextView.setInterBoldFont()
        titleTextView.textSize = 22f
        titleTextView.maxLines = 1
        titleTextView.ellipsize = TextUtils.TruncateAt.END

        val titleLp =
            LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f)
        titleLp.setMargins(i16px, 0, 0, 0)
        titleTextView.layoutParams = titleLp

        horizLiearLayout.addView(titleTextView)

        //btn change
        val changeBtn = TextView(context)
        changeBtn.id = IConstants.ConfirmLocationModule.VIEW_BTN_CHANGE
        changeBtn.setInterThineFont()
        changeBtn.text = context.resources.getString(R.string.change)
        changeBtn.setPadding(UIHelper.i5px, UIHelper.i5px, UIHelper.i5px, UIHelper.i5px)
        changeBtn.setTextColor(ResourcesCompat.getColor(context.resources, R.color.pinkies, null))
        changeBtn.setInterBoldFont()
        changeBtn.background = DrawableUtils.getRoundDrawable(
            ResourcesCompat.getColor(context.resources, R.color.lightGray, null),
            8f
        )

        val changeBtnLp =
            LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        changeBtn.layoutParams = changeBtnLp

        changeBtn.setClickScaleEffect()
        changeBtn.setOnClickListener(this)
        horizLiearLayout.addView(changeBtn)


        linearLayout.addView(horizLiearLayout)

    }

    private fun addFullAddressView(linearLayout: LinearLayout) {
        //full address
        val fullAddressTextView = TextView(context)
        fullAddressTextView.id = IConstants.ConfirmLocationModule.VIEW_TEXT_FULL_ADDRESS
        fullAddressTextView.setInterRegularFont()
        fullAddressTextView.setLineSpacing(2f, 1f)
        fullAddressTextView.textSize = 14f
        fullAddressTextView.setTextColor(
            ResourcesCompat.getColor(
                context.resources,
                R.color.cool_gray,
                null
            )
        )

        val fullAddressLp =
            LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        fullAddressLp.setMargins(i2px, i8px, i2px, i8px)
        fullAddressTextView.layoutParams = fullAddressLp
        linearLayout.addView(fullAddressTextView)
    }

    private fun addFlatNoView(
        linearLayout: LinearLayout,
        label: String,
        hint: String,
        inputViewId: Int
    ) {

        //flat no label
        val flatLabel = TextView(context)
        flatLabel.setInterRegularFont()
        flatLabel.textSize = 12f
        flatLabel.text = label
        flatLabel.setTextColor(
            ResourcesCompat.getColor(
                context.resources,
                R.color.darkGray,
                null
            )
        )

        val flatLabelLp =
            LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        flatLabelLp.setMargins(i2px, i14px, i2px, 0)
        flatLabel.layoutParams = flatLabelLp
        linearLayout.addView(flatLabel)

        //flat input view
        val flatInputView = EditText(context)
        flatInputView.id = inputViewId
        flatInputView.setInterRegularFont()
        flatInputView.textSize = 14f
        flatInputView.maxLines = 1
        flatInputView.setSingleLine()
        flatInputView.hint = hint
        if (inputViewId == IConstants.ConfirmLocationModule.VIEW_TEXT_FLAT_DETAIL) {
            flatInputView.nextFocusForwardId = IConstants.ConfirmLocationModule.VIEW_TEXT_LANDMARK
        }


//        flatInputView.minimumHeight=VUtil.dpToPx(UIHelper.i35px)
        flatInputView.setPadding(UIHelper.i8px, UIHelper.i8px, UIHelper.i8px, UIHelper.i8px)
        flatInputView.setTextColor(
            ResourcesCompat.getColor(
                context.resources,
                R.color.black,
                null
            )
        )

        flatInputView.setHintTextColor(
            ResourcesCompat.getColor(
                context.resources,
                R.color.darkGray,
                null
            )
        )

        flatInputView.background = DrawableUtils.getRoundDrawable(
            ResourcesCompat.getColor(context.resources, R.color.lightGray, null),
            UIHelper.i5px.toFloat()
        )

        linearLayout.addView(flatInputView)

    }

    private fun addAddressTypeContainer(linearLayout: LinearLayout) {

        mAddrTypeFlowGroup = FlowGroupView(context).apply {
            id = IConstants.ConfirmLocationModule.VIEW_CONTAINER_ADDRESS_TYPE

            val addressTypes = listOf("Home", "Office", "Other")

            setItems(
                addressTypes,
                fun(listener: OnGroupItemClickListener<TagTextView>, item: String): TagTextView =
                    TagTextView(context).apply {
                        setText(item)
                        setOnItemClickListener(listener)
                        setPadding(i6px, i6px, i6px, i6px)
                    }
            )
/*
            setProperties(
                wrapMode = Flow.WRAP_CHAIN,
                horizontalSpreadStyle = Flow.CHAIN_SPREAD_INSIDE
            )*/

            setOnSelectedItemChangeListener(this@ConfirmLocationCardView)
            setItemSelectedAt(0)
            setPadding(i16px, i8px, i16px, i8px)
        }
        return linearLayout.addView(mAddrTypeFlowGroup)

    }

    private fun addOtherAddressInput(linearLayout: LinearLayout) {
        linearLayout.apply {
            //flat input view
            val addressTypeView = EditText(context)
            addressTypeView.id = IConstants.ConfirmLocationModule.VIEW_TEXT_OTHERS
            addressTypeView.setInterRegularFont()
            addressTypeView.maxLines = 1
            addressTypeView.setSingleLine()
            addressTypeView.textSize = 14f
            addressTypeView.hint = context.resources.getString(R.string.address_name)


//        addressTypeView.minimumHeight=VUtil.dpToPx(UIHelper.i35px)
            addressTypeView.setPadding(UIHelper.i8px, UIHelper.i8px, UIHelper.i8px, UIHelper.i8px)
            addressTypeView.setTextColor(
                ResourcesCompat.getColor(
                    context.resources,
                    R.color.black,
                    null
                )
            )

            addressTypeView.setHintTextColor(
                ResourcesCompat.getColor(
                    context.resources,
                    R.color.darkGray,
                    null
                )
            )

            addressTypeView.background = DrawableUtils.getRoundDrawable(
                ResourcesCompat.getColor(context.resources, R.color.lightGray, null),
                UIHelper.i5px.toFloat()
            )

            addressTypeView.visibility = View.GONE
            addView(addressTypeView)
        }
    }

    private fun addConfirmLocationBtn(linearLayout: LinearLayout) {


        //ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(getContext(), R.style.Yumzy_OutLineButton);
        val confirmLocationBtn = MaterialButton(context, null, R.style.Yumzy_Button)
        confirmLocationBtn.id = IConstants.ConfirmLocationModule.VIEW_BTN_CONFIRM_LOCATION
        confirmLocationBtn.text = context.resources.getString(R.string.confirm_location)
        confirmLocationBtn.isFocusable = true
        confirmLocationBtn.isClickable = true
        confirmLocationBtn.setPadding(i14px, i8px, i14px, i8px)
        confirmLocationBtn.isAllCaps = false
        confirmLocationBtn.setTextColor(
            ResourcesCompat.getColor(
                context.resources,
                R.color.white,
                null
            )
        )
        confirmLocationBtn.gravity = Gravity.CENTER
        confirmLocationBtn.setOnClickListener(this)


        val lp = LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 40.px)
        lp.setMargins(i2px, i8px, i2px, i8px)
        confirmLocationBtn.layoutParams = lp

        linearLayout.addView(confirmLocationBtn)

    }

    override fun onSelectedItemChanged(list: MutableList<GroupItemActions<*>>, listType: Int) {
        mAddressType =
            (list[0] as TagTextView).findViewById<TextView>(IConstants.TagTextView.VIEW_TEXT_TAG)
                .text.toString()
    }

    override fun onShowMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun setAddress(address: AddressDTO) {
        mAddress = address
        mAddressType = address.addressTag
        mFullAddress = address.fullAddress
        mHouseNo = address.houseNum
        mLandMark = address.landmark
        mLocationTitle = mAddressType
        mCollapsed = !address.isExpanded
        if (address.addressTag != "") {
            when (address.addressTag.toLowerCase(Locale.getDefault())) {
                "home" -> mAddrTypeFlowGroup?.setItemSelectedAt(0)
                "office" -> mAddrTypeFlowGroup?.setItemSelectedAt(1)
                else -> {
                    mAddrTypeFlowGroup?.setItemSelectedAt(2)
                    mAddressType = address.addressTag
                }
            }
        }
    }

    fun setOnConfirmLocationClickListener(listener: OnConfirmLocationClickListener) {
        mOnConfirmLocationClickListener = listener
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            IConstants.ConfirmLocationModule.VIEW_BTN_CHANGE -> {
                mCollapsed = !mCollapsed
            }
            IConstants.ConfirmLocationModule.VIEW_BTN_CONFIRM_LOCATION -> {
                if (mHouseNo.isNotEmpty() && mAddressType.isNotEmpty() && mLandMark.isNotEmpty()) {

                    mAddress.apply {
                        houseNum = mHouseNo.trim().replace("\\s+".toRegex(), " ").capitalize()
                        landmark = mLandMark.trim().replace("\\s+".toRegex(), " ").capitalize()
                        addressTag = mAddressType.trim().replace("\\s+".toRegex(), " ").capitalize()
                        name = mAddressType.trim().replace("\\s+".toRegex(), " ").capitalize()
                    }
                    mOnConfirmLocationClickListener?.onConfirmLocationClicked(mAddress)
                }
            }
        }
    }

    interface OnConfirmLocationClickListener {
        fun onConfirmLocationClicked(mAddress: AddressDTO)
    }

}