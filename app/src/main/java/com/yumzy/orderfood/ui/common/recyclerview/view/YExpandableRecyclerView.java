package com.yumzy.orderfood.ui.common.recyclerview.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.yumzy.orderfood.ui.common.recyclerview.adapter.YExpandableListAdapter;
import com.yumzy.orderfood.ui.common.recyclerview.interfaces.IListGroupDTO;
import com.yumzy.orderfood.ui.common.recyclerview.interfaces.OnLoadMoreListener;
import com.yumzy.orderfood.ui.common.recyclerview.listener.IItemViewHolderListener;

import java.util.ArrayList;
import java.util.List;

public class YExpandableRecyclerView<T extends IListGroupDTO<T>> extends YRecyclerView<T> {


    private YExpandableListAdapter<T> clRecyclerViewAdapter;//RecycleView Adapter Class
    private YRecyclerDividerItemDecoration yRecyclerDividerItemDecoration;
    private boolean isEnableActionMode = false;
    private boolean bExpandable = false;


    public YExpandableRecyclerView(Context context) {
        super(context);
        initRecyclerViewType(context, null, null);
    }

    public YExpandableRecyclerView(Context context, AttributeSet attr) {
        super(context, attr);
        initRecyclerViewType(context, null, null);
    }

    public YExpandableRecyclerView(Context context, ArrayList<T> viewHolderList, IItemViewHolderListener itemViewListener) {
        super(context);
        initRecyclerViewType(context, viewHolderList, itemViewListener);
    }

    public YExpandableRecyclerView(Context context, ArrayList<T> viewHolderList, IItemViewHolderListener itemViewListener, boolean bExpandable, boolean bAdapter, boolean bLoadMore) {
        super(context);
        initWithAdapter(context, viewHolderList, itemViewListener, bAdapter, bExpandable, bLoadMore);
    }

    public YExpandableRecyclerView(Context context, ArrayList<T> viewHolderList, IItemViewHolderListener itemViewListener, boolean bAdapter) {
        super(context);
        initWithAdapter(context, viewHolderList, itemViewListener, bAdapter, false, false);
    }

    public YExpandableRecyclerView(Context context, ArrayList<T> viewHolderList, boolean isEnableActionMode, IItemViewHolderListener itemViewListener, boolean bAdapter, boolean bLoadMore) {
        super(context);
        this.isEnableActionMode = isEnableActionMode;
        initWithAdapter(context, viewHolderList, itemViewListener, bAdapter, false, bLoadMore);
    }

    private void initRecyclerViewType(Context context, ArrayList<T> viewHolderList, IItemViewHolderListener itemViewListener) {
        // use a linear layout manager
        initWithAdapter(context, viewHolderList, itemViewListener, false, false, false);

    }

    private void initWithAdapter(Context context, ArrayList<T> viewHolderList, IItemViewHolderListener itemViewListener, boolean bAdapter, boolean bExpandable, boolean bLaodMore) {
        this.bExpandable = bExpandable;
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        setLayoutParams(relativeParams);

        if (bAdapter) {
            clRecyclerViewAdapter = new YExpandableListAdapter<T>(context, viewHolderList/*mDataArrayList*/, itemViewListener, bExpandable, isEnableActionMode, bLaodMore);
            setAdapter(clRecyclerViewAdapter);
        }
        super.setLayoutManager(mLayoutManager);
        setItemAnimator(new DefaultItemAnimator());
    }


    public void removeItemDecoration() {
        removeItemDecoration(yRecyclerDividerItemDecoration);
    }

    @Override
    public void setItemClickListener(IItemClickListener clItemClickListener) {
        clRecyclerViewAdapter.setItemClickListener(clItemClickListener);
    }


    public void setActionModeEventListener(YExpandableListAdapter.IActionModeEventListener itemViewEventListener) {
        clRecyclerViewAdapter.setActionModeEventListener(itemViewEventListener);
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        super.setOnLoadMoreListener(onLoadMoreListener);
    }

    public void removeItem(int position) {
        clRecyclerViewAdapter.removeItemAt(position);
    }

    public void removeSelectedItems(List<Integer> iSelectedIds) {
        if (iSelectedIds.size() > 0) {
            int currPos;
            int iListPosition = 0;
            for (int i = 0; i < iSelectedIds.size(); i++) {
                currPos = iSelectedIds.get(i);
                if (currPos == 0)
                    iListPosition = currPos;
                else
                    iListPosition = currPos - i;

                removeItem(iListPosition);
            }
        }

    }

    public void setData(ArrayList<T> arrayList) {
        clRecyclerViewAdapter.setData(arrayList);
    }

    public void addNewItem(T item) {
        if (clRecyclerViewAdapter != null)
            clRecyclerViewAdapter.addNewItem(item);
    }
}
