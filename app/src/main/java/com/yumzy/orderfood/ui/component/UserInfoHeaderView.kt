package com.yumzy.orderfood.ui.component

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.LoginDTO
import com.yumzy.orderfood.data.models.ProfileResponseDTO
import com.yumzy.orderfood.ui.common.imageview.RoundedImageView
import com.yumzy.orderfood.ui.helper.setClickScaleEffect
import com.yumzy.orderfood.ui.helper.setInterBoldFont
import com.yumzy.orderfood.util.IConstants
import com.yumzy.orderfood.util.YUtils
import com.laalsa.laalsalib.ui.VUtil

class UserInfoHeaderView : LinearLayout, View.OnClickListener {

    private lateinit var mOnUserHeaderItemClickListener: OnUserHeaderItemClickListener
    private lateinit var mUserInof: LoginDTO

    private val i16px = VUtil.dpToPx(16)
    private val i18px = VUtil.dpToPx(18)
    private val i10px = VUtil.dpToPx(10)
    private val i2px = VUtil.dpToPx(2)

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, atr: AttributeSet?) : this(context, atr, 0)
    constructor(context: Context, atr: AttributeSet?, style: Int) : super(context, atr, style) {

        this.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        this.orientation = VERTICAL

        this.setPadding(i16px, i16px, i16px, i16px)

    }

    private fun userRoundImage(user: ProfileResponseDTO): RoundedImageView {
        val imageParam = LayoutParams(VUtil.dpToPx(100), VUtil.dpToPx(100))
        imageParam.gravity = Gravity.CENTER

        val roundImage = RoundedImageView(context)
        roundImage.layoutParams = imageParam

        roundImage.id = IConstants.UserModule.USER_IMAGE
        roundImage.cornerRadius = VUtil.dpToPx(50).toFloat()
//        Glide.with(this).load(user.profilePicLink).placeholder(R.drawable.app_logo).into(roundImage)
        YUtils.setImageInView(roundImage, user.profilePicLink ?: "")

        roundImage.scaleType = ImageView.ScaleType.CENTER_CROP
        roundImage.setOnClickListener(this)
        return roundImage
    }

    private fun userName(userName: String, classIcon: Drawable, userClass: String): LinearLayout {
        val ll = LinearLayout(context)
        ll.layoutParams = LayoutParams(0, LayoutParams.MATCH_PARENT, 1f)
        ll.gravity = Gravity.CENTER_VERTICAL
        ll.orientation = VERTICAL
        ll.setPadding(i16px, i10px, i16px, i10px)


        //  ll.addView(userName.getSectionTitleView())
        // ll.addView(getUserClassView(classIcon, userClass))

        return ll
    }

    private fun getUserClassView(classIcon: Drawable, userClass: String): LinearLayout {

        val ll = LinearLayout(context)
        ll.id = IConstants.UserModule.USER_CLASS
        ll.layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        ll.orientation = HORIZONTAL
        ll.gravity = Gravity.CENTER_VERTICAL

        val fontIconView = ImageView(context)
        fontIconView.layoutParams =
            LayoutParams(40, 50)
        fontIconView.setImageResource(R.drawable.ic_bronze_award_big)

        ll.addView(fontIconView)

        val classText = TextView(context)
        classText.layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        classText.setPadding(i2px, i2px, i2px, i2px)
        classText.text = "${userClass} > "
        classText.setTextColor(resources.getColor(R.color.pinkies))

        ll.addView(classText)

        ll.setClickScaleEffect()
        ll.setOnClickListener(this@UserInfoHeaderView)

        return ll
    }

    private fun String.getSectionTitleView(): TextView {
        val titleTextView = TextView(context)
        titleTextView.setPadding(
            i10px,
            i2px,
            i10px,
            i2px
        )
        titleTextView.setInterBoldFont()
        titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
        titleTextView.text = this
        titleTextView.setOnClickListener(this@UserInfoHeaderView)
        return titleTextView
    }


    fun setOnUserHeaderItemClickListener(onUserHeaderItemClickListener: OnUserHeaderItemClickListener) {
        mOnUserHeaderItemClickListener = onUserHeaderItemClickListener
    }

    fun setUserHeaderInfo(user: ProfileResponseDTO) {

        this.addView(userRoundImage(user))
        this.addView(
            ResourcesCompat.getDrawable(
                context.resources,
                R.drawable.ic_bronze_award_big,
                context.theme
            )
                ?.let {
                    userName(
                        user.name ?: "",
                        it,
                        "Gold Class"
                    )
                }
        )

    }


    override fun onClick(v: View?) {
        if (v != null) {
            mOnUserHeaderItemClickListener.onUserInfoClicked(v.id)
        }
    }

    interface OnUserHeaderItemClickListener {
        fun onUserInfoClicked(moduleId: Int)

    }

}