package com.yumzy.orderfood.ui

import android.app.Activity
import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.View
import androidx.activity.result.ActivityResult
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import com.google.android.material.transition.platform.MaterialContainerTransformSharedElementCallback
import com.laalsa.laalsalib.ui.VUtil
import com.yumzy.orderfood.ui.SharedTransition.EXTRA_TRANSITION_NAME
import com.yumzy.orderfood.ui.contracthandler.ContractResultActivity
import com.yumzy.orderfood.util.IConstants


object AppIntentUtil {

    @JvmStatic
    fun launchWithBaseAnimation(
        view: View?,
        activity: Activity,
        launchClass: Class<out Activity>
    ) {

        val intent = Intent(activity, launchClass)
        intent.putExtra(EXTRA_TRANSITION_NAME, SharedTransition.TRANSITION_NAME)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Set up shared element transition and disable overlay so views don't show above system bars
            activity.setExitSharedElementCallback(MaterialContainerTransformSharedElementCallback())
            activity.window.sharedElementsUseOverlay = true
            val options: ActivityOptions =
                ActivityOptions.makeSceneTransitionAnimation(
                    activity,
                    view,
                    SharedTransition.TRANSITION_NAME
                )
            activity.startActivity(intent, options.toBundle())
        } else {
            activity.startActivity(intent)
        }

    }

    @JvmStatic
    fun launchIntentBaseAnimation(
        view: View,
        activity: Activity,
        intent: Intent
    ) {

        intent.putExtra(EXTRA_TRANSITION_NAME, SharedTransition.TRANSITION_NAME)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Set up shared element transition and disable overlay so views don't show above system bars
            activity.setExitSharedElementCallback(MaterialContainerTransformSharedElementCallback())
            activity.window.sharedElementsUseOverlay = true
            val options: ActivityOptions =
                ActivityOptions.makeSceneTransitionAnimation(
                    activity,
                    view,
                    SharedTransition.TRANSITION_NAME
                )
            activity.startActivity(intent, options.toBundle())
        } else {
            activity.startActivity(intent)
        }

    }

    @JvmStatic
    fun launchForResultWithBaseAnimation(
        view: View?,
        activity: AppCompatActivity,
        launchClass: Class<out AppCompatActivity>,
        callback: ((resultOk: Boolean, result: ActivityResult) -> Unit)?
    ) {
        val resultActivity = ContractResultActivity(activity.activityResultRegistry)
        activity.lifecycle.addObserver(resultActivity)

        val intent = Intent(activity, launchClass)
        intent.putExtra(EXTRA_TRANSITION_NAME, SharedTransition.TRANSITION_NAME)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Set up shared element transition and disable overlay so views don't show above system bars
            activity.setExitSharedElementCallback(MaterialContainerTransformSharedElementCallback())
            activity.window.sharedElementsUseOverlay = true
            val options = if (view != null)
                ActivityOptionsCompat.makeSceneTransitionAnimation(
                    activity,
                    view,
                    SharedTransition.TRANSITION_NAME
                )
            else
                null
            resultActivity.startIntentResult(
                intent = intent,
                options = options,
                callback = callback
            )
        } else {
            resultActivity.startForResult(
                activity = activity,
                callerClass = launchClass,
                callback = callback
            )
        }

    }

    @JvmStatic
    fun launchForResultWithBaseAnimation(
        view: View?,
        activity: AppCompatActivity,
        intent: Intent,
        callback: ((resultOk: Boolean, result: ActivityResult) -> Unit)?
    ) {
        val resultActivity = ContractResultActivity(activity.activityResultRegistry)
        activity.lifecycle.addObserver(resultActivity)
        intent.putExtra(EXTRA_TRANSITION_NAME, SharedTransition.TRANSITION_NAME)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Set up shared element transition and disable overlay so views don't show above system bars
            activity.setExitSharedElementCallback(MaterialContainerTransformSharedElementCallback())
            activity.window.sharedElementsUseOverlay = true
            val options = if (view != null)
                ActivityOptionsCompat.makeSceneTransitionAnimation(
                    activity,
                    view,
                    SharedTransition.TRANSITION_NAME
                )
            else
                null
            resultActivity.startIntentResult(
                intent = intent,
                options = options,
                callback = callback
            )
        } else {
            resultActivity.startIntentForResult(
                intent = intent,
                callback = callback
            )
        }

    }

    @JvmStatic
    fun launchWithNoAnimation(
        activity: AppCompatActivity,
        launchClass: Class<out Activity>
    ) {
        activity.startActivity(Intent(activity, launchClass))
        activity.finish()
        activity.overridePendingTransition(0, 0);
    }

    @JvmStatic
    fun launchForResultWithNoAnimation(

        activity: AppCompatActivity,
        launchClass: Class<out AppCompatActivity>,
        callback: ((resultOk: Boolean, result: ActivityResult) -> Unit)?

    ) {
        val resultActivity = ContractResultActivity(activity.activityResultRegistry)
        activity.lifecycle.addObserver(resultActivity)
        val intent = Intent(activity, launchClass)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            resultActivity.startIntentResult(
                intent = intent,
                callback = callback
            )
            activity.overridePendingTransition(0, 0)
        } else {
            resultActivity.startIntentResult(
                intent = intent,
                callback = callback
            )
        }
    }

    @JvmStatic
    fun launchWithSheetAnimation(
        activity: Activity,
        launchClass: Class<out Activity>
    ) {
        val intent = Intent(activity, launchClass)
        launchWithSheetAnimation(activity, intent)
    }

    @JvmStatic
    fun launchWithSheetAnimation(
        activity: Activity,
        intent: Intent
    ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val options: ActivityOptionsCompat =
                ActivityOptionsCompat.makeBasic()
            activity.startActivity(intent, options.toBundle())
        } else {
            activity.startActivity(intent)
        }

    }

    @JvmStatic
    fun launchForResultWithSheetAnimation(
        activity: AppCompatActivity,
        launchClass: Class<out Activity>,
        callback: ((resultOk: Boolean, result: ActivityResult) -> Unit)?

    ) {
        val resultActivity = ContractResultActivity(activity.activityResultRegistry)
        activity.lifecycle.addObserver(resultActivity)

        val intent = Intent(activity, launchClass)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val options: ActivityOptionsCompat =
                ActivityOptionsCompat.makeBasic()
            resultActivity.startIntentResult(
                intent = intent,
                options = options,
                callback = callback
            )
        } else {
            resultActivity.startIntentResult(
                intent = intent,
                callback = callback
            )
        }

    }

    @JvmStatic
    fun launchForResultWithSheetAnimation(
        activity: AppCompatActivity,
        intent: Intent,
        callback: ((resultOk: Boolean, result: ActivityResult) -> Unit)?

    ) {
        val resultActivity = ContractResultActivity(activity.activityResultRegistry)
        activity.lifecycle.addObserver(resultActivity)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val options: ActivityOptionsCompat =
                ActivityOptionsCompat.makeBasic()
            resultActivity.startIntentResult(
                intent = intent,
                options = options,
                callback = callback
            )
        } else {
            resultActivity.startIntentResult(
                intent = intent,
                callback = callback
            )
        }

    }

    /* @JvmStatic
     fun launchForResultWithImage(
         imageView: ImageView?,
         activity: AppCompatActivity,
         launchClass: Class<out AppCompatActivity>,
         callback: ((resultOk: Boolean, result: ActivityResult) -> Unit)?
     ) {
         val resultActivity = ContractResultActivity(activity.activityResultRegistry)
         activity.lifecycle.addObserver(resultActivity)

         val intent = Intent(activity, launchClass)
         intent.putExtra(EXTRA_TRANSITION_NAME, SharedTransition.TRANSITION_NAME)

         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
             // Set up shared element transition and disable overlay so views don't show above system bars

             activity.setExitSharedElementCallback(MaterialContainerTransformSharedElementCallback())
             activity.window.sharedElementsUseOverlay = true


             val bitmap = (imageView?.drawable as BitmapDrawable).bitmap
             val options = if (bitmap != null) {
                 TODO("Dont use this Method ")
 //                val  inflateTransition = TransitionInflater.from(
 //                    activity
 //                ).inflateTransition(R.transition.change_image_trans)
 //
 //                activity.setEnterSharedElementCallback(
 //                )
                 val imageTransitionName = imageView.transitionName
                 intent.putExtra(SharedTransition.ACTIVITY_IMAGE_TRANSITION, imageTransitionName)
                 intent.putExtra(
                     SharedTransition.PARCELABLE_BITMAP,
                     (imageView.drawable as BitmapDrawable).bitmap
                 )
                 ActivityOptionsCompat.makeThumbnailScaleUpAnimation(
                     imageView,
                     bitmap,
                     imageView.x.toInt(),
                     imageView.y.toInt(),
                 )
             } else
                 null
             resultActivity.startIntentResult(
                 intent = intent,
                 options = options,
                 callback = callback
             )
         } else {
             resultActivity.startForResult(
                 activity = activity,
                 callerClass = launchClass,
                 callback = callback
             )
         }

     }*/

    fun launchMultipleShareTransition(
        view: View,
        activity: AppCompatActivity, launchClass: Class<out AppCompatActivity>,
        vararg sharedElements: androidx.core.util.Pair<View?, String?>
    ) {
        val intent = Intent(activity, launchClass)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Set up shared element transition and disable overlay so views don't show above system bars
            activity.setExitSharedElementCallback(MaterialContainerTransformSharedElementCallback())
            activity.window.sharedElementsUseOverlay = true
            intent.putExtra(
                SharedTransition.EXTRA_MULTI_TRANSITION_NAME,
                SharedTransition.TRANSITION_MULTI_OBJECT
            )
            val options: ActivityOptionsCompat =
                ActivityOptionsCompat.makeSceneTransitionAnimation(activity, *sharedElements)
            activity.startActivity(intent, options.toBundle())
        } else {
            activity.startActivity(intent)
        }
    }

    fun launchCircularRevealActivity(view: View, activity: Activity, landingActivity: Class<*>) {
        val options: ActivityOptionsCompat =
            ActivityOptionsCompat.makeSceneTransitionAnimation(
                activity,
                view,
                SharedTransition.EXTRA_TRANSITION_NAME
            )
        val revealX = (view.x + view.width / 2).toInt()
        val revealY = (view.y + view.height / 2).toInt()
        val intent = Intent(activity, landingActivity)
        intent.putExtra(SharedTransition.EXTRA_TRANSITION_NAME, SharedTransition.TRANSITION_NAME)
        intent.putExtra(IConstants.ActivitiesFlags.EXTRA_CIRCULAR_REVEAL_X, revealX)
        intent.putExtra(IConstants.ActivitiesFlags.EXTRA_CIRCULAR_REVEAL_Y, revealY)
        ActivityCompat.startActivity(activity, intent, options.toBundle())
    }

    fun launchClipReveal(view: View, context: Context, landingActivity: Class<*>) {
        val intent = Intent(context, landingActivity);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val options = ActivityOptions.makeClipRevealAnimation(
                view, view.width / 2, view.height / 2,
                VUtil.screenWidth / 2, VUtil.screenHeight / 2
            )
            context.startActivity(intent, options.toBundle());
        } else {
            context.startActivity(intent);
        }
    }

    fun launchClipRevealFromIntent(view: View, context: Context, intent: Intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val options = ActivityOptions.makeClipRevealAnimation(
                view, view.width / 2, view.height / 2,
                VUtil.screenWidth / 2, VUtil.screenHeight / 2
            )
            context.startActivity(intent, options.toBundle());
        } else {
            context.startActivity(intent);
        }
    }
}