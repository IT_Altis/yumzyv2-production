package com.yumzy.orderfood.ui.component

import android.animation.LayoutTransition
import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.Outlet
import com.yumzy.orderfood.ui.helper.setInterFont
import com.yumzy.orderfood.ui.helper.setInterThineFont
import com.yumzy.orderfood.util.viewutils.fontutils.IconFontDrawable
import com.laalsa.laalsalib.ui.VUtil
import com.laalsa.laalsaui.utils.sp2Px


class OutletOptionItem : LinearLayout {


    private lateinit var onOutletClickListener: OnOutletClick
    private var outlet: Outlet? = null

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, atr: AttributeSet?) : this(context, atr, 0)
    constructor(context: Context, atr: AttributeSet?, style: Int) : super(context, atr, style) {
        inti()
    }

    private fun inti() {

        val l = LayoutTransition()
        l.enableTransitionType(LayoutTransition.APPEARING)
        this.layoutTransition = l
        initiate()
    }

    private fun initiate() {

        this.layoutParams =
            LayoutParams(LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val i5Px = VUtil.dpToPx(5)
        val i8Px = VUtil.dpToPx(8)
        this.orientation = HORIZONTAL
        this.setPadding(i8Px, i5Px, i8Px, i5Px)
//        this.minimumHeight = VUtil.dpToPx(45)
        this.gravity = Gravity.CENTER_VERTICAL

        this.addView(getOutletTitleTextView())
        this.addView(getOutletRatingTextView())
        this.addView(getOutletSeparatorTextView())
        this.addView(getOutletDurationTextView())
        this.addView(getOutletSeparatorTextView())
        this.addView(getOutletOffTextView())

        this.setOnClickListener { if (outlet != null) onOutletClickListener.onOutletClick(outlet) }
    }

    private fun getOutletTitleTextView(): View? {
        val titleTextView = TextView(context)
        titleTextView.id = R.id.outlet_item_title
        titleTextView.setInterFont()
        titleTextView.layoutParams = LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f)
        titleTextView.gravity = Gravity.CENTER_VERTICAL
        titleTextView.setPadding(15, 8, 15, 8)
        titleTextView.setTextColor(Color.BLUE)
        return titleTextView
    }

    private fun getOutletRatingTextView(): View? {

        val ratingTextView = TextView(context)
        ratingTextView.id = R.id.outlet_item_rating
        ratingTextView.setInterFont()
        ratingTextView.layoutParams =
            LayoutParams(LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        ratingTextView.gravity = Gravity.CENTER
        ratingTextView.setPadding(15, 8, 15, 8)
        ratingTextView.setTextColor(Color.BLACK)


        val iconFontDrawable = IconFontDrawable(context, R.string.icon_star)
        iconFontDrawable.textSize = 18f.sp2Px
        iconFontDrawable.setTextColor(Color.BLACK)

        ratingTextView.setCompoundDrawablesWithIntrinsicBounds(iconFontDrawable, null, null, null)
        ratingTextView.compoundDrawablePadding = 4
        return ratingTextView
    }

    private fun getOutletOffTextView(): View? {

        val offerTextView = TextView(context)
        offerTextView.id = R.id.outlet_item_offer
        offerTextView.setInterFont()
        offerTextView.layoutParams =
            LayoutParams(LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        offerTextView.gravity = Gravity.CENTER
        offerTextView.setPadding(15, 8, 15, 8)
        offerTextView.setTextColor(Color.RED)

        val iconFontDrawable = IconFontDrawable(context, R.string.icon_tag)
        iconFontDrawable.textSize =18f.sp2Px
        iconFontDrawable.setTextColor(Color.RED)
        offerTextView.setCompoundDrawablesWithIntrinsicBounds(
            iconFontDrawable, null, null, null
        )
        offerTextView.compoundDrawablePadding = 8
        offerTextView.compoundDrawablePadding = 4

        return offerTextView
    }

    private fun getOutletDurationTextView(): View? {

        val durationTextView = TextView(context)
        durationTextView.id = R.id.outlet_item_duration
        durationTextView.setInterThineFont()
        durationTextView.layoutParams =
            LayoutParams(LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        durationTextView.gravity = Gravity.CENTER
        durationTextView.setPadding(15, 8, 15, 8)
        durationTextView.setTextColor(Color.GRAY)

        return durationTextView
    }

    private fun getOutletSeparatorTextView(): View? {

        val separatorView = View(context)
        separatorView.layoutParams = LayoutParams(3, LayoutParams.MATCH_PARENT)
        val color = ResourcesCompat.getColor(resources, R.color.cool_gray, context.theme)
        separatorView.setBackgroundColor(color)
        separatorView.setPadding(8, 12, 8, 12)
        return separatorView
    }

    public fun setOutlet(outlet: Outlet) {
        this.outlet = outlet
        this.findViewById<TextView>(R.id.outlet_item_title).text = outlet.title
        this.findViewById<TextView>(R.id.outlet_item_rating).text =
            String.format("%.1f", outlet.rating)
        this.findViewById<TextView>(R.id.outlet_item_duration).text = "${outlet.duration} mins"
        this.findViewById<TextView>(R.id.outlet_item_offer).text = "${outlet.offer} Off"

    }

     fun setOutletClickListener(onOutletClick: OnOutletClick) {
        onOutletClickListener = onOutletClick
    }

     interface OnOutletClick {
         fun onOutletClick(outlet: Outlet?)
    }

}