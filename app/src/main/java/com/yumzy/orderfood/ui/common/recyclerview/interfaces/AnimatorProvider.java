package com.yumzy.orderfood.ui.common.recyclerview.interfaces;

import android.view.View;

interface AnimatorProvider {
    ValueAnimatorCompat emptyValueAnimator();

    void clearInterpolator(View var1);
}