/*
 *   Created by Sourav Kumar Pandit  29 - 4 - 2020
 */

package com.yumzy.orderfood.ui.screens.templetes

import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.laalsa.laalsalib.ui.VUtil
import com.yumzy.orderfood.R
import com.yumzy.orderfood.ui.common.imageview.RoundedImageView
import com.yumzy.orderfood.ui.helper.setInterFont
import com.yumzy.orderfood.ui.screens.module.infodialog.IDetailsActionTemplate
import com.yumzy.orderfood.util.ViewConst

open class InfoTemplate(
    override val context: Context,
    val imgUrl: Int = 0,
    val title: CharSequence = "",
    val subTitle: CharSequence = "",
    val extraInfo: CharSequence = ""
) : IDetailsActionTemplate {

    override var headerHeight: Int = VUtil.dpToPx(100)
        set(value) {
            field = VUtil.dpToPx(value)
        }
    override val showClose = true
    override fun heading(): View? {
        val layout = LinearLayout(context)
        layout.minimumHeight = headerHeight
        val imageView = RoundedImageView(context)
        imageView.cornerRadius = headerHeight.toFloat()
        imageView.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, headerHeight)
        if (imgUrl == -1)
            imageView.setImageResource(R.mipmap.ic_launcher_round)
        else
            headerHeight = 0
        layout.addView(imageView)
        return layout
    }

    override fun body(): View? {
        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.VERTICAL
        layout.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        if (title.isNotEmpty())
            layout.addView(getTextView(R.id.tv_info_header, title))
        if (subTitle.isNotEmpty())
            layout.addView(getTextView(R.id.tv_info_sub_header, subTitle))
        if (extraInfo.isNotEmpty())
            layout.addView(getTextView(R.id.tv_info_sub_sub_header, extraInfo))

        return layout
    }

    override fun actions(): View? = null
    override fun onDialogVisible(view: View?, visible: Boolean?, dialog: DialogInterface?) {

    }


    open fun getTextView(tvId: Int, msg: CharSequence): View? {
        val i5px = VUtil.dpToPx(5)

        val textView = TextView(context)
        textView.layoutParams =
            LinearLayout.LayoutParams(ViewConst.MATCH_PARENT, ViewConst.WRAP_CONTENT)
        textView.text = msg
        textView.id = tvId
        textView.gravity = Gravity.CENTER
        textView.setPadding(0, i5px, 0, 0)
        textView.setInterFont()

        if (tvId == R.id.tv_info_header) {
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f)
            textView.setTextColor(Color.DKGRAY)
        } else if (tvId == R.id.tv_info_sub_header) {
            textView.setTextColor(Color.GRAY)
        } else if (tvId == R.id.tv_info_sub_sub_header) {
            textView.setTextColor(Color.GRAY)
        }
        return textView
    }


}
