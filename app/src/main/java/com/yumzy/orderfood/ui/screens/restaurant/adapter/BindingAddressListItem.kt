package com.yumzy.orderfood.ui.screens.restaurant.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.AddressDTO
import com.yumzy.orderfood.databinding.AdapterAddressItemBinding

class BindingAddressListItem : AbstractBindingItem<AdapterAddressItemBinding>() {

    internal var predictionAddress: AutocompletePrediction? = null

    internal var addessDto: AddressDTO? = null

    override val type: Int
        get() = R.id.item_address

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AdapterAddressItemBinding {
        return AdapterAddressItemBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: AdapterAddressItemBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        attachToWindow(binding)

        if (addessDto == null) {
            binding.imvLoc.text = binding.root.context.getText(R.string.icon_015_map)
            binding.imvArrow.visibility = View.VISIBLE
            binding.tvName.text = predictionAddress?.getPrimaryText(null)
            binding.tvAddress.text = predictionAddress?.getFullText(null)
        } else {
            binding.imvArrow.visibility = View.GONE
            when {
                addessDto?.name?.toLowerCase().equals("home") -> {
//                    binding.imvLoc.setImageResource(R.drawable.ic_outline_location_pin)
                    binding.imvLoc.text = binding.root.context.getText(R.string.icon_location_1)
                }
                addessDto?.name?.toLowerCase().equals("office") -> {
//                    binding.imvLoc.setImageResource(R.drawable.ic_outline_location_pin)
                    binding.imvLoc.text = binding.root.context.getText(R.string.icon_briefcase_alt)

                }
                else -> {
//                    binding.imvLoc.setImageResource(R.drawable.ic_outline_location_pin)
                    binding.imvLoc.text = binding.root.context.getText(R.string.icon_compass)

                }
            }
            binding.tvName.text = addessDto?.name.toString().capitalize()
            binding.tvAddress.text = "${addessDto?.houseNum}, ${if(addessDto?.landmark.isNullOrEmpty()) "" else addessDto?.landmark.toString().capitalize()}, ${addessDto?.fullAddress}"
        }
    }

    internal fun withAddress(address: AutocompletePrediction): BindingAddressListItem {
        predictionAddress = address
        return this
    }

    internal fun withAddressDTO(address: AddressDTO): BindingAddressListItem {
        addessDto = address
        return this
    }
}