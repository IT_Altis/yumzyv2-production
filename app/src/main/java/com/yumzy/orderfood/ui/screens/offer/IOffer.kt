package com.yumzy.orderfood.ui.screens.offer

import com.yumzy.orderfood.ui.base.IView

/**
 * Created by Bhupendra Kumar Sahu.
 */
interface IOffer :IView{
    fun fetchCouponList()
}