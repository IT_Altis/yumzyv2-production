package com.yumzy.orderfood.ui.component;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.google.android.material.textview.MaterialTextView;
import com.laalsa.laalsalib.ui.VUtil;
import com.laalsa.laalsaui.drawable.DrawableUtils;
import com.yumzy.orderfood.R;
import com.yumzy.orderfood.ui.helper.ThemeConstant;
import com.yumzy.orderfood.ui.helper.UIHelper;
import com.yumzy.orderfood.util.viewutils.fontutils.FontIconView;

import top.defaults.drawabletoolbox.DrawableBuilder;

/**
 * Created by Bhupendra Kumar Sahu
 */
public class AddressInfoCard extends LinearLayout implements View.OnClickListener {
    public boolean isShow;
    private String title;
    private String subTitle;
    private String icon;
    private AddressEventListener addressEventListener;


    public AddressInfoCard(Context context) {
        this(context, null);
    }

    public AddressInfoCard(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AddressInfoCard(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (attrs != null)
            initAttrs(context, attrs, defStyleAttr);
        if (title == null) {
            title = "";
        }
        if (subTitle == null) {
            subTitle = "";
        }
        if (icon == null) {
            icon = "";
        }
        init();

    }

    private void initAttrs(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.AddressInfoCard, defStyleAttr, 0);
        title = a.getString(R.styleable.AddressInfoCard_address_tag);
        subTitle = a.getString(R.styleable.AddressInfoCard_address);
        icon = a.getString(R.styleable.AddressInfoCard_address_tag_icon);
        isShow = a.getBoolean(R.styleable.AddressInfoCard_action_show, false);

        a.recycle();
    }


    @SuppressLint("SetTextI18n")
    public void init() {
        this.setOrientation(VERTICAL);

        this.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        this.setBackground(DrawableUtils.getRoundDrawable(ThemeConstant.lightGray, VUtil.dpToPx(8)));
        LinearLayout container = new LinearLayout(getContext());
        LayoutParams containerLayoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        container.setGravity(Gravity.END);
        container.setLayoutParams(containerLayoutParams);
        container.setOrientation(VERTICAL);

        int i45px = VUtil.dpToPx(45);
        int i35px = VUtil.dpToPx(35);
        int i8px = VUtil.dpToPx(8);
        int i5px = VUtil.dpToPx(5);

        /*Addess Tag*/
        LinearLayout llAddressTag = new LinearLayout(getContext());
        llAddressTag.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        llAddressTag.setGravity(Gravity.CENTER);
       /* Drawable baseBuilder = new DrawableBuilder()
                .rectangle()
                .rounded()
                .hairlineBordered()
                .strokeColor(ThemeConstant.logoImage)
                .ripple();*/
//        llAddressTag.setBackground(baseBuilder);

        Drawable drawable = new DrawableBuilder().rectangle()
                .solidColor(ThemeConstant.alphaLogoText)
                .topLeftRadius(UIHelper.INSTANCE.getI8px())
                .topRightRadius(UIHelper.INSTANCE.getI8px())
                .bottomLeftRadius(0)
                .bottomRightRadius(0).build();
        llAddressTag.setBackground(drawable);

        FontIconView iconView = new FontIconView(getContext());
        iconView.setLayoutParams(new LayoutParams(i45px, i45px));
        iconView.setId(R.id.iv_address_icon);
        iconView.setTextColor(getResources().getColor(R.color.white));
        iconView.setText(icon);
        iconView.setPadding(i8px, i8px, i8px, i8px);

        FontIconView deleteIcon = new FontIconView(getContext());
        deleteIcon.setLayoutParams(new LayoutParams(i35px, i35px));
//        deleteIcon.setId(R.id.iv_address_icon);
        deleteIcon.setId(R.id.tv_delete);
        deleteIcon.setTextColor(getResources().getColor(R.color.white));
        deleteIcon.setText(R.string.icon_delete);
        deleteIcon.setPadding(i8px, i8px, i8px, i8px);
        deleteIcon.setOnClickListener(this);

        MaterialTextView tvAddressTag = new MaterialTextView(getContext());

        LayoutParams addressTagParam = new LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
        tvAddressTag.setText(title);
        tvAddressTag.setId(R.id.tv_address_tag);
        tvAddressTag.setTextAppearance(getContext(), R.style.TextAppearance_MyTheme_Subtitle2);
        tvAddressTag.setAllCaps(true);
        tvAddressTag.setTextSize(18f);

        // YumUtil.INSTANCE.setInterFontBold(getContext(), tvAddressTag);
        tvAddressTag.setTextColor(ContextCompat.getColor(getContext(), android.R.color.white));
        tvAddressTag.setTypeface(Typeface.DEFAULT_BOLD);
        tvAddressTag.setLayoutParams(addressTagParam);

        /*Address*/
        MaterialTextView tvAddress = new MaterialTextView(getContext());
        LayoutParams addressParam = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tvAddress.setTextColor(ContextCompat.getColor(getContext(), R.color.darkGrayDark));
        tvAddress.setText(subTitle);
        tvAddress.setPadding(i8px, i8px, i8px, i8px);
        tvAddress.setId(R.id.tv_address);
        tvAddress.setTextAppearance(getContext(), R.style.TextAppearance_MyTheme_Body1);
        //  YumUtil.INSTANCE.setInterFontRegular(getContext(), tvAddressTag);

        tvAddress.setLayoutParams(addressParam);

        /*Delete And Edit*/
//        LinearLayout llEventControl = new LinearLayout(getContext());
//        LayoutParams llEventControlLayoutParam = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        llEventControlLayoutParam.setMargins(i45px, i5px, 0, i8px);
//        llEventControl.setLayoutParams(llEventControlLayoutParam);
        final Drawable editDrawable = new DrawableBuilder()
                .rectangle()
                .rounded()
                .solidColor(ThemeConstant.greenAccent)
                .solidColorPressed(ThemeConstant.logoText)
                .build();

        MaterialTextView tvEdit = new MaterialTextView(getContext());
        LayoutParams editParam = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tvEdit.setTextAppearance(getContext(), R.style.TextAppearance_MyTheme_Subtitle2);
        editParam.setMargins(i8px, i8px, i8px, i8px);
        tvEdit.setPadding(i8px + i5px, i5px, i8px + i5px, i5px);
        tvEdit.setText("edit");
        tvEdit.setId(R.id.tv_edit);
        tvEdit.setBackground(editDrawable);
        tvEdit.setTypeface(Typeface.DEFAULT_BOLD);
        tvEdit.setTextSize(14f);
        tvEdit.setVisibility(isShow ? GONE : VISIBLE);
        tvEdit.setTextColor(ContextCompat.getColor(getContext(), android.R.color.white));
        tvEdit.setOnClickListener(this);
        tvEdit.setLayoutParams(editParam);

       /* MaterialButton tvDelete = new MaterialButton(getContext());
        LayoutParams deleteParam = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tvDelete.setPadding(i8px, i5px, 0, 0);

        tvDelete.setText("delete");
        tvDelete.setOnClickListener(this);
        tvDelete.setId(R.id.tv_delete);
        tvDelete.setVisibility(isShow ? GONE : VISIBLE);
        tvDelete.setTextAppearance(getContext(), R.style.TextAppearance_MyTheme_Subtitle2);

        //   YumUtil.INSTANCE.setInterFontBold(getContext(), tvDelete);
        tvDelete.setTextColor(ContextCompat.getColor(getContext(), android.R.color.holo_red_dark));
        tvDelete.setLayoutParams(deleteParam);
*/
        llAddressTag.addView(iconView);
        llAddressTag.addView(tvAddressTag);
        llAddressTag.addView(deleteIcon);
        container.addView(llAddressTag);
        container.addView(tvAddress);

//        llEventControl.addView(tvEdit);
//        llEventControl.addView(tvDelete);
        container.addView(tvEdit);
        this.addView(container);
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        ((TextView) findViewById(R.id.tv_address_tag)).setText(this.title);
    }


    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
        ((TextView) findViewById(R.id.tv_address)).setText(this.subTitle);

    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
        ((TextView) findViewById(R.id.iv_address_icon)).setText(this.icon);
    }

    public boolean isShow() {
        return isShow;
    }

    public void setShow(boolean show) {
        isShow = show;
        if (isShow) {
            findViewById(R.id.tv_edit).setVisibility(VISIBLE);
//            findViewById(R.id.tv_delete).setVisibility(VISIBLE);

        } else {
            findViewById(R.id.tv_edit).setVisibility(GONE);
//            findViewById(R.id.tv_delete).setVisibility(GONE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tv_edit:
                if (addressEventListener != null)
                    addressEventListener.edit();
                break;

            case R.id.tv_delete:
                if (addressEventListener != null)
                    addressEventListener.delete();
                break;
        }
    }

    public void setAddressEventListener(AddressEventListener addressEventListener) {
        this.addressEventListener = addressEventListener;
    }


    public interface AddressEventListener {
        void delete();

        void edit();
    }
}
