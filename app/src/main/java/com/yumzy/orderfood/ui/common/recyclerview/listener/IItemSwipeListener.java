package com.yumzy.orderfood.ui.common.recyclerview.listener;


import androidx.recyclerview.widget.RecyclerView;

import com.yumzy.orderfood.ui.common.recyclerview.adapter.YRecyclerViewAdapter;
import com.yumzy.orderfood.ui.common.recyclerview.view.YSwipeItemDTO;

import java.util.List;

public interface IItemSwipeListener {
    //purpose of this method is return View when swipe action is performed
    List<YSwipeItemDTO> getSwipeView(int direction, RecyclerView.ViewHolder clViewHolder);

    //this method will be called after an item swipe is done
    void onSwiped(YRecyclerViewAdapter<?> adapter, RecyclerView.ViewHolder viewHolder, int direction);

    //this method is called when an item is moving
    boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder fromPosition, RecyclerView.ViewHolder toPosition);


    void onSwipeItemClick(YSwipeItemDTO ySwipeItemDTO, int iPosition, RecyclerView.ViewHolder clViewHolder);
    //void onMoved(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, int fromPos, RecyclerView.ViewHolder target, int toPos, int x, int y);

}
