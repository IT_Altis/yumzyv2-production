package com.yumzy.orderfood.ui.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.yumzy.orderfood.R;
import com.laalsa.laalsalib.ui.VUtil;

public class NoInternetLayout extends LinearLayout {
    private Drawable icon;
    private String title;
    private String subTitle;

    public NoInternetLayout(Context context) {
        this(context, null);
    }

    public NoInternetLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NoInternetLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            initAttrs(context, attrs, defStyleAttr);
        if (icon == null) {
        }
        if (title == null) {
            title = "";
        }
        if (subTitle == null) {
            subTitle = "";
        }
        init();


    }

    private void initAttrs(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.NoInternetLayout, defStyleAttr, 0);
        icon = ta.getDrawable(R.styleable.NoInternetLayout_iv_no_internet);
        title = ta.getString(R.styleable.NoInternetLayout_tv_no_internet_title);
        subTitle = ta.getString(R.styleable.NoInternetLayout_tv_no_internet_sub_title);
        ta.recycle();
    }

    private void init() {
        this.setOrientation(VERTICAL);
        this.setGravity(Gravity.CENTER);

        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(VERTICAL);
        linearLayout.setGravity(Gravity.CENTER);
        int i15px = VUtil.dpToPx(15);
        int l200px = VUtil.dpToPx(200);


        ImageView img = new ImageView(getContext());
        img.setId(R.id.iv_no_internet);
        img.setImageDrawable(icon);

        img.setPadding(0, i15px, 0, i15px);
        linearLayout.addView(img, new LayoutParams(l200px, l200px));

        linearLayout.addView(getTextView(true, title));
        linearLayout.addView(getTextView(false, subTitle));
        this.addView(linearLayout);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        ((TextView) findViewById(R.id.tv_no_internet_title)).setText(this.title);

    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
        ((TextView) findViewById(R.id.tv_no_internet_subtitle)).setText(this.subTitle);

    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
        ((ImageView) findViewById(R.id.iv_no_internet)).setImageDrawable(icon);
    }

    private View getTextView(boolean isHeader, String msg) {
        TextView textView = new TextView(getContext());
        textView.setGravity(Gravity.CENTER);
        textView.setText(msg);
        int tv15px = VUtil.dpToPx(15);
        int tv2px = VUtil.dpToPx(2);
        int tv40px = VUtil.dpToPx(40);
        if (isHeader) {
            textView.setId(R.id.tv_no_internet_title);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f);
            textView.setTypeface(Typeface.DEFAULT_BOLD);
            textView.setTextColor(Color.BLACK);
        } else {
            textView.setPadding(tv40px, tv2px, tv40px, tv15px);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f);

            textView.setId(R.id.tv_no_internet_subtitle);
            textView.setTextColor(Color.GRAY);
        }
        return textView;
    }
}