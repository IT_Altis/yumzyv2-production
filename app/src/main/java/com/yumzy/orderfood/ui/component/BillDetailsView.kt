/*
 * Created By Shriom Tripathi 2 - 5 - 2020
 */

package com.yumzy.orderfood.ui.component

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.laalsa.laalsalib.ui.VUtil
import com.yumzy.orderfood.R
import com.yumzy.orderfood.data.models.Billing
import com.yumzy.orderfood.ui.helper.*
import com.yumzy.orderfood.util.viewutils.YumUtil

class BillDetailsView : LinearLayout {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initAttrs(attrs, context, defStyleAttr)

        intiViews()

    }


    var title: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.tv_billing_title)?.text = field

        }

    var isBold: Boolean = true
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.tv_billing_title)?.setInterBoldFont()
            this.findViewById<TextView>(R.id.tv_billing_subtitle)?.setInterBoldFont()
        }
    var subTitle: String = ""
        set(value) {
            field = value
            this.findViewById<TextView>(R.id.tv_billing_subtitle)?.text = field
        }

    var billing: Billing? = null
        set(value) {
            if (value != null) {
                setBillingInfoList(value)
            }
            field = value
        }

    private fun initAttrs(
        attrs: AttributeSet?,
        context: Context?,
        defStyleAttr: Int
    ) {
        if (attrs != null) {
            val a =
                context!!.obtainStyledAttributes(
                    attrs,
                    R.styleable.BillingDetailsView,
                    defStyleAttr,
                    0
                )

            title = a.getString(R.styleable.BillingDetailsView_billing_title) ?: ""
            subTitle = a.getString(R.styleable.BillingDetailsView_billing_subtitle) ?: ""

            a.recycle()

        }
    }


    private fun intiViews() {
        this.orientation = VERTICAL
        this.setPadding(VUtil.dpToPx(8), VUtil.dpToPx(8), VUtil.dpToPx(8), VUtil.dpToPx(8))
        this.background = ResourcesCompat.getDrawable(
            context.resources,
            R.drawable.rounded_light_gray,
            context.theme
        )

    }

    fun setOutletInfoHeader() {

        this.addView(getRow(context.resources.getString(R.string.billing_details), "", true, 0))
        val dividerView =
            YumUtil.dividerView(context, ThemeConstant.lightAlphaGray, UIHelper.i1px)

        this.addView(dividerView)

    }

    private fun setBillingInfoList(billing: Billing) {

        this.addView(
            getRow(
                context.resources.getString(R.string.item_total),
                "₹" + billing.subTotal.withPrecision(2).toString(),
                false,
                0
            )
        )
        for (charges in billing.charges!!) {
            if (!billing.charges.isNullOrEmpty()) {
                if (charges.value.toString() != "0.0" || charges.name == "deliveryCharge") {
                    val charge = if (charges.name == "deliveryCharge" && charges.value != billing.originalDelivery) YumUtil.getOfferItemPrice(
                        charges.value,
                        billing.originalDelivery,
                        true,
                        strikeColor = ThemeConstant.pinkies,
                        offerColor = ThemeConstant.greenConfirmColor
                    ) else "₹" + charges.value.withPrecision(2)
                    this.addView(
                        getRow(
                            charges.displayName,
                            charge,
                            false,
                            0
                        )
                    )
                }
            }
        }

        for (tex in billing.taxes!!) {
            if (!billing.taxes.isNullOrEmpty()) {
                if (!tex.value.toString().equals("0.0")) {
                    this.addView(
                        getRow(
                            tex.displayName,
                            "₹" + tex.value.withPrecision(2),
                            false,
                            0
                        )
                    )
                }
            }
        }
        if (!billing.discount.toString().equals("0.0")) {
            this.addView(
                getRow(
                    context.resources.getString(R.string.discount),
                    "- ₹" + billing.discount.withPrecision(2).toString(),
                    false,
                    1
                )
            )

        }
        val dividerView = YumUtil.dividerView(context, ThemeConstant.lightAlphaGray, UIHelper.i1px)

        this.addView(dividerView)
        this.addView(
            getRow(
                context.resources.getString(R.string.to_pay),
                "₹" + billing.totalAmount.withPrecision(2).toString(),
                true,
                0
            )
        )


    }

    private fun getRow(
        title: String,
        amount: CharSequence?,
        toBold: Boolean,
        toChangeColor: Int
    ): View {

        val linearLayout = LinearLayout(context)
        linearLayout.orientation = HORIZONTAL
        linearLayout.setPadding(VUtil.dpToPx(2), VUtil.dpToPx(2), VUtil.dpToPx(2), VUtil.dpToPx(2))

        val lp = LayoutParams(0, LayoutParams.MATCH_PARENT, 1f)

        val titleView = TextView(context)
        if (toBold) {
            titleView.setInterBoldFont()
            titleView.setPadding(0, VUtil.dpToPx(5), 0, VUtil.dpToPx(5))
        } else {
            titleView.setInterRegularFont()

        }
        if (toChangeColor == 1) {
            titleView.setInterBoldFont()
            titleView.setTextColor(ThemeConstant.greenConfirmColor)
        } else {
            titleView.setTextColor(ThemeConstant.textBlackColor)

        }


        titleView.text = title
        titleView.id = R.id.tv_billing_title
        titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
        titleView.gravity = Gravity.START
        titleView.layoutParams = lp

        val priceView = TextView(context)
        if (toBold) {
            priceView.setInterBoldFont()
            priceView.setPadding(0, VUtil.dpToPx(5), 0, VUtil.dpToPx(5))

        } else {
            priceView.setInterRegularFont()

        }
        if (toChangeColor == 1) {
            priceView.setInterBoldFont()
            priceView.setTextColor(ThemeConstant.greenConfirmColor)
        } else {
            priceView.setTextColor(ThemeConstant.textBlackColor)

        }

        priceView.text = amount
        priceView.id = R.id.tv_billing_subtitle
        priceView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
        priceView.gravity = Gravity.END

        linearLayout.addView(titleView)
        linearLayout.addView(priceView)

        return linearLayout

    }


}