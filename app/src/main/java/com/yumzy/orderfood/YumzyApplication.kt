package com.yumzy.orderfood

import android.annotation.SuppressLint
import com.clevertap.android.sdk.ActivityLifecycleCallback
import com.clevertap.android.sdk.CleverTapAPI
import com.yumzy.orderfood.di.DaggerApplication
import com.yumzy.orderfood.di.component.DaggerAppComponent
import com.yumzy.orderfood.util.IConstants
import com.zoho.salesiqembed.ZohoSalesIQ
import dagger.android.AndroidInjector

class YumzyApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        sdkInitialization()
        return DaggerAppComponent.builder().application(this)
            .build()
    }

    @SuppressLint("MissingPermission")
    private fun sdkInitialization() {
        appCleverTap = CleverTapAPI.getDefaultInstance(this@YumzyApplication)!!
    }
    override fun onCreate() {
        ActivityLifecycleCallback.register(this)
        super.onCreate()
        appCleverTap.enableDeviceNetworkInfoReporting(true)
        ZohoSalesIQ.init(
            this, IConstants.AppConstant.ZOHO_APP_KEY,
            IConstants.AppConstant.ZOHO_ACCESS_KEY
        )
        ZohoSalesIQ.showLauncher(false)

    }

}